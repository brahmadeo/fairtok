package org.wysaid.algorithm;

public class Vector2 {
    public float f25x;
    public float f26y;

    public Vector2(float _x, float _y) {
        this.f25x = _x;
        this.f26y = _y;
    }
}
