package org.wysaid.algorithm;

public class Vector4 {
    public float f30w;
    public float f31x;
    public float f32y;
    public float f33z;

    public Vector4(float _x, float _y, float _z, float _w) {
        this.f31x = _x;
        this.f32y = _y;
        this.f33z = _z;
        this.f30w = _w;
    }
}
