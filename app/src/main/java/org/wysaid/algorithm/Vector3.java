package org.wysaid.algorithm;

public class Vector3 {
    public float f27x;
    public float f28y;
    public float f29z;

    public Vector3(float _x, float _y, float _z) {
        this.f27x = _x;
        this.f28y = _y;
        this.f29z = _z;
    }
}
