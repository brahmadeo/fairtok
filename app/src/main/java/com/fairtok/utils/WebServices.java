package com.fairtok.utils;

import android.app.Activity;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.view.base.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class WebServices {

    public static void postApi(final Activity mActivity,
                               String url,
                               HashMap<String, String> hashMap,
                               final boolean loader,
                               boolean softKeyboard, final WebServicesCallback apiCallback) {

        if (AppUtils.isNetworkAvailable(mActivity)) {

            if (loader)
                AppUtils.showRequestDialog(mActivity);

            if (softKeyboard)
                AppUtils.hideSoftKeyboard(mActivity);

            Log.v("postApi-URL", url);
            Log.v("postApi-jsonObject", hashMap.toString());

            AndroidNetworking.post(url)
                    .addUrlEncodeFormBodyParameter(hashMap)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            if (loader)
                                AppUtils.hideDialog();

                            Log.v("postApi-response", response.toString());

                            apiCallback.OnJsonSuccess(response);

                        }

                        @Override
                        public void onError(ANError anError) {

                            AppUtils.hideDialog();

                            apiCallback.OnFail(anError.getErrorDetail());

                            Log.v("postApi-error", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorDetail()));
                        }
                    });

        } else
            AppUtils.showNoInternetToastSort(mActivity);

    }

    public static void postApiHeader(final Activity mActivity,
                                     String url,
                                     HashMap<String, String> hashMap,
                                     final boolean loader,
                                     boolean softKeyboard, final WebServicesCallback apiCallback) {



        if (AppUtils.isNetworkAvailable(mActivity)) {

            if (loader)
                AppUtils.showRequestDialog(mActivity);

            if (softKeyboard)
                AppUtils.hideSoftKeyboard(mActivity);

            Log.v("postApi-URL", url);
            Log.v("postApi-jsonObject", hashMap.toString());
            Log.v("postApi-Token",  Global.ACCESS_TOKEN);

            AndroidNetworking.post(url)
                    .addUrlEncodeFormBodyParameter(hashMap)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            if (loader)
                                AppUtils.hideDialog();

                            Log.v("postApi-response", response.toString());

                            apiCallback.OnJsonSuccess(response);

                        }

                        @Override
                        public void onError(ANError anError) {

                            AppUtils.hideDialog();

                            apiCallback.OnFail(anError.getErrorDetail());

                            Log.v("postApi-error", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorDetail()));
                        }
                    });

        } else
            AppUtils.showNoInternetToastSort(mActivity);

    }



    public static void getApi(final Activity mActivity, String url,
                              final boolean loader,
                              boolean softKeyboard,
                              final WebServicesCallback apiCallback) {


        if (AppUtils.isNetworkAvailable(mActivity)) {

            if (loader)
                AppUtils.showRequestDialog(mActivity);

            if (softKeyboard)
                AppUtils.hideSoftKeyboard(mActivity);

            Log.v("postApi-URL", url);
            Log.v("postApi-Token", Global.ACCESS_TOKEN);

            AndroidNetworking.get(url)
                    .setPriority(Priority.IMMEDIATE)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            if (loader)
                                AppUtils.hideDialog();

                            Log.v("postApi-response", response.toString());

                            apiCallback.OnJsonSuccess(response);

                        }

                        @Override
                        public void onError(ANError anError) {

                            AppUtils.hideDialog();

                            Log.v("postApi-error", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-error", String.valueOf(anError.getErrorCode()));
                        }
                    });


        } else
            AppUtils.showNoInternetToastSort(mActivity);

    }

}
