package com.fairtok.utils;

public interface Const {

    String BASE_URL = "https://fairtok.com/banal/api/";
    String BASE_URL_IMAGE = "https://fairtok.com/banal/uploads/";
   // String BASE_URL = "http://3.108.183.116/banal/api/";
//    String ITEM_BASE_URL = "https://fairtok.com/banal/uploads/";
//    String ITEM_BASE_URL = "https://bucket-fairtok.s3.ap-south-1.amazonaws.com/";
//    String VIDEO_ITEM_BASE_URL = "https://bucket-fairtok.s3.ap-south-1.amazonaws.com/";

    String ITEM_BASE_URL = "https://bucket-store-tok-videos2.s3.ap-south-1.amazonaws.com/";
    //String ITEM_BASE_URL = "https://fairtok.com/banal/uploads/";
    String VIDEO_ITEM_BASE_URL = "https://bucket-store-tok-videos2.s3.ap-south-1.amazonaws.com/";

    String POST_VIDEO="https://fairtok.com/banal/api/Post/add_post";

    String BASE_URL_SHARE = "https://fairtokk.app.link";

    String IS_LOGIN = "is_login";
    String PREF_NAME = "Fairtok_prefer";
    String GOOGLE_LOGIN = "google";
    String FACEBOOK_LOGIN = "facebook";
    String GO_LIVE = "User/go_live";
    String STOP_LIVE = "User/stop_live";
    String STOP_LIVE_CALL = "User/stopLiveCall";
    String LIVE_LIST = "User/live_users";
    String join_live = "User/join_live";
    String exit_live = "User/exit_live";
    String live_audience = "User/live_audience";
    String update_instagram = "User/update_instagram";
    String user_details = "User/user_details";
    String search_live_users = "User/search_live_users";
    String coin_plans = "User/coin_plans";
    String liveSendCoin = "User/live_send_coin";
    String followUnfollow = "Post/follow_unfollow";
    String live_details = "User/live_details";
    String convert_gold_coins = "User/convert_gold_coins";
    String convert_silver_coins = "User/convert_silver_coins";
    String goLiveCall = "User/goLiveCall";
    String liveCallUsers = "User/liveCallUsers";
    String initiateLiveCall = "User/initiateLiveCall";
    String updateLiveCall = "User/updateLiveCall";
    String liveCallSendCoin = "User/liveCallSendCoin";
    String stopLiveCall = "User/stopLiveCall";
    String purchaseMinute = "User/purchaseMinute";
    String USER = "user";
    String COMMENT = "comment";
    String FAV = "fav";
    String ADS_URL = "https://amzn.to/2O1zS7p";

    String updateLiveUsers = "User/update_live_users";

    String fan_plans = "User/fan_plans";
    String sendCohostingRequest = "User/send_cohosting_request";
    String updateCohostingRequest = "User/update_cohosting_request";
    String cohostingRequest = "User/cohosting_request";
    String cohostingList = "User/cohosting_list";
    String buyCohosting = "User/buy_cohosting";
    String joinLiveDirectly = "User/join_live_directly";

    // Ravi
    String joinLiveUser = "User/join_live_users"; // for viewer
    String premiumLiveUsers = "User/send_premium_gift_request";
    String LEADERBOARD = "User/live_leaderboard_request";
    String normaltopremium = "User/go_live_premium";
    String stopPremiumLive = "User/stop_live_premium";
}