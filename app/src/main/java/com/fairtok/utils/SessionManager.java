package com.fairtok.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.fairtok.model.comment.Comment;
import com.fairtok.model.user.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class SessionManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        this.pref = context.getSharedPreferences(Const.PREF_NAME, MODE_PRIVATE);
        this.editor = this.pref.edit();
    }

    public void saveStringValue(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringValue(String key) {
        return pref.getString(key, "");
    }

    public void saveBooleanValue(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBooleanValue(String key) {
        return pref.getBoolean(key, false);
    }

    public void saveUser(User user) {
        editor.putString(Const.USER, new Gson().toJson(user));
        editor.apply();
    }

    public User getUser() {
        try {
            String userString = pref.getString(Const.USER, "");
            if (!userString.isEmpty()) {
                return new Gson().fromJson(userString, User.class);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public void saveComment(Comment comment) {
        editor.putString(Const.COMMENT, new Gson().toJson(comment));
        editor.apply();
    }
    public Comment getComment() {
        try {
            String commentString = pref.getString(Const.COMMENT, "");
            if (!commentString.isEmpty()) {
                return new Gson().fromJson(commentString, Comment.class);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveFavouriteMusic(String id) {
        ArrayList<String> fav = getFavouriteMusic();
        if (fav != null) {
            if (fav.contains(id)) {
                fav.remove(id);

            } else {
                fav.add(id);

            }
        } else {
            fav = new ArrayList<>();
            fav.add(id);

        }
        editor.putString(Const.FAV, new Gson().toJson(fav));
        editor.apply();
    }

    public ArrayList<String> getFavouriteMusic() {
        String userString = pref.getString(Const.FAV, "");
        if (!userString.isEmpty()) {
            return new Gson().fromJson(userString, new TypeToken<ArrayList<String>>() {
            }.getType());
        }
        return null;
    }

    public void clear() {
        try {
            editor.clear().apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
        saveBooleanValue(Const.IS_LOGIN, false);
        Global.ACCESS_TOKEN = "";
        Global.USER_ID = "";
    }

}
