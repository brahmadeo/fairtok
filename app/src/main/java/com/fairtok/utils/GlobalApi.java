package com.fairtok.utils;

import android.util.Log;
import android.widget.Toast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class GlobalApi {

    private CompositeDisposable disposable = new CompositeDisposable();

    public void rewardUser(String rewardActionId) {
        disposable.add(Global.initRetrofit().rewardUser(Global.ACCESS_TOKEN, rewardActionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((reward, throwable) -> {

                }));
    }


    public void increaseView(String postId) {
        disposable.add(Global.initRetrofit().increaseView(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((reward, throwable) -> {

                }));
    }

    public void timeSpendinApp(String type, Long timeSpend) {
        disposable.add(Global.initRetrofit().timeSpend(Global.ACCESS_TOKEN, type, timeSpend)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((reward, throwable) -> {
                   /* Log.i("responseeee ", "abcd");
                    if (reward!=null){
                        Log.i("responseeee ", reward.getMessage());
                    }
                    Log.i("responseeee2 ", "abcd");
                    if (throwable!=null){
                        Log.i("responseeee2 ", throwable.getMessage());
                    }*/

                }));
    }

    public void reportAudience(boolean isSexual, boolean isViolent, boolean isAbusive, boolean isSpam, boolean isOther) {
        disposable.add(Global.initRetrofit().reportAudience(Global.ACCESS_TOKEN, isSexual, isViolent)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((reward, throwable) -> {
                    //Log.i("responseeee ", reward.getMessage());
                }));

    }
    public boolean blockAudience(String user) {
        /*disposable.add(Global.initRetrofit().blockAudience(Global.ACCESS_TOKEN, user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((reward, throwable) -> {
                    Log.i("responseeee ", reward.getMessage());
                }));*/
        return true;
    }
}
