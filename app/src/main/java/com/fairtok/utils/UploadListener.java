package com.fairtok.utils;

public interface UploadListener {

    void onProgressUpdate(int percentage);

}
