package com.fairtok.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.model.ServiceData;
import com.fairtok.utils.Global;
import com.fairtok.utils.GlobalApi;
import com.fairtok.view.home.MainActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Upload_Service extends Service {

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public Upload_Service getService() {
            return Upload_Service.this;
        }
    }

    boolean mAllowRebind;
    ServiceCallback Callback;
    ServiceData data;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }



//    Uri uri;
//
//    String video_base64="",thumb_base_64="",Gif_base_64="";
//
//    String description;
//
//    SharedPreferences sharedPreferences;

    public Upload_Service() {
        super();
    }

    public Upload_Service(ServiceCallback serviceCallback) {
        Callback=serviceCallback;
    }

    public void setCallbacks(ServiceCallback serviceCallback){
        Callback=serviceCallback;
    }


    @Override
    public void onCreate() {
//        sharedPreferences=getSharedPreferences(Variables.pref_name,MODE_PRIVATE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent!=null) {
            if (MyApplication.data != null) {
                data = MyApplication.data;

            if (intent.getAction().equals("startservice")) {

               new Thread(new Runnable() {
                   @Override
                   public void run() {

                      try {
                          disposable.add(Global.initRetrofit()
                                  .uploadPost(data.getAccessToken(), data.getHashMap(), data.getBody(), data.getBody1(), data.getBody2(), data.getBody3())
                                  .subscribeOn(Schedulers.io())
                                  .unsubscribeOn(Schedulers.io())
                                  .observeOn(AndroidSchedulers.from(Looper.getMainLooper(), true))
                                  .doOnSubscribe(disposable1 -> {
                                      showNotification();
                                  })
                                  .doOnTerminate(() -> {
                                      stopForeground(true);
                                      //showNotification("The video has been uploaded successfully.",false);
                                      new GlobalApi().rewardUser("3");
                                  })
                                  .subscribe((updateUser, throwable) -> {
                                      //if(TextUtils.isEmpty(updateUser.getMessage()))
                                          Log.v("TAG","This issue is now fixed and crash should not happen now");
                                      //else
                                        //Log.d("TEST199",updateUser.toString());
                                  }));
                      }
                      catch (Exception e)
                      {
                          e.printStackTrace();
                      }

                   }
               }).start();

            }
            else if(intent.getAction().equals("stopservice")){
                stopForeground(true);
                stopSelf();
            }
            }
        }

        return Service.START_STICKY;
    }

    private void showNotification(String message,boolean flag)
    {
        int icon = R.mipmap.ic_launcher;
        String title = getString(R.string.app_name);
        final String CHANNEL_ID = "DEFAULT";
//        final String CHANNEL_NAME = title;

        int requestID = (int) System.currentTimeMillis();

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT
                | PendingIntent.FLAG_ONE_SHOT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, title, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("");
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});
            channel.setSound(uri, audioAttributes);
            manager.createNotificationChannel(channel);


            Notification.Builder builder = new Notification.Builder(this, CHANNEL_ID);
            builder.setSmallIcon(icon)
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setStyle(new Notification.BigTextStyle())
                    .setContentText(message);
                manager.notify(0, builder.build());

        } else {

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                    .setContentIntent(pendingIntent)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(uri)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSmallIcon(icon)
                    .setAutoCancel(true);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                if (notificationManager != null) {
                    notificationManager.notify(0, notificationBuilder.build());
                }

        }
    }


    // this will show the sticky notification during uploading video
    private void showNotification() {

        final String CHANNEL_ID = "default";
        final String CHANNEL_NAME = getString(R.string.app_name);

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel defaultChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(defaultChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(android.R.drawable.stat_sys_upload)
                .setContentTitle("Uploading Video")
                .setContentText("Please wait! Video is uploading. Do not close the app to ensure successful upload")
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),
                        android.R.drawable.stat_sys_upload));

        Notification notification = builder.build();
        startForeground(101, notification);
    }
}
