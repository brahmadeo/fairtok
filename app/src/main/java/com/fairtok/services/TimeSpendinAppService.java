package com.fairtok.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.fairtok.MyApplication;
import com.fairtok.model.ServiceData;
import com.fairtok.utils.GlobalApi;

import java.util.Date;

import io.reactivex.disposables.CompositeDisposable;

public class TimeSpendinAppService extends Service {

    private ServiceCallback Callback;
    private boolean isSuccess = false;
    private boolean isReady = false;
    private ServiceData data;
    private CompositeDisposable disposable = new CompositeDisposable();

    private final IBinder mBinder = new MyBinder();

    public class MyBinder extends Binder{
        public TimeSpendinAppService getService(){
            return TimeSpendinAppService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public TimeSpendinAppService() {
        super();
    }

    public TimeSpendinAppService(ServiceCallback serviceCallback) {
        Callback=serviceCallback;
    }

    public void setCallbacks(ServiceCallback serviceCallback){
        Callback=serviceCallback;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent!=null){
            if (MyApplication.dataTime!=null){
                data = MyApplication.dataTime;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("timespenddd ", data.getTimeSpend()+"");
                        if (intent.getAction().equals("startLive")){
                            new GlobalApi().timeSpendinApp("live", data.getTimeSpend());
                        }else if (intent.getAction().equals("startVideo")){
                            new GlobalApi().timeSpendinApp("video", data.getTimeSpend());
                        }
                    }
                }).start();
            }
        }

        stopSelf();
        return Service.START_STICKY;
    }
}
