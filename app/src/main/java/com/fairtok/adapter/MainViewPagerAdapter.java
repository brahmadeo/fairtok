package com.fairtok.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.fairtok.chat.PrefHandler;
import com.fairtok.view.home.HomeFragment;
import com.fairtok.view.profile.ProfileFragment;
import com.fairtok.view.search.SearchFragment;

public class MainViewPagerAdapter extends FragmentPagerAdapter {

    Context context;
    static Fragment currentFragment;

    public MainViewPagerAdapter(Context context, @NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.context=context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Bundle bundle1 = new Bundle();
                bundle1.putString("playVideo", "true");
                currentFragment= new HomeFragment();
                currentFragment.setArguments(bundle1);
                return currentFragment;

            case 1:
                Bundle bundle2 = new Bundle();
                bundle2.putString("playVideo", "false");
                currentFragment=new SearchFragment();
                currentFragment.setArguments(bundle2);
                return currentFragment;

            case 2:

                /*currentFragment=new NotificationFragment();
                return new NotificationFragment();*/

            case 3:
                return new Fragment();

            default:
                currentFragment=new ProfileFragment();
                PrefHandler prefHandler = new PrefHandler(context);
                Bundle bundle = new Bundle();
                bundle.putString("userid", prefHandler.getuId());
                ProfileFragment profileFragment = new ProfileFragment();
                profileFragment.setArguments(bundle);
                return profileFragment;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}
