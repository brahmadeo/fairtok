package com.fairtok.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.model.ViewerModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterViewers extends RecyclerView.Adapter<AdapterViewers.MyViewHolder> {

    ArrayList<ViewerModel> data;

    public AdapterViewers(ArrayList<ViewerModel> arrayList) {

        data = arrayList;

    }

    @NonNull
    @Override
    public AdapterViewers.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_premium_invite, viewGroup, false);
        return new AdapterViewers.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewers.MyViewHolder holder, final int position) {
        ViewerModel model = data.get(position);
        holder.tvName.setText(model.getName());
        holder.rbSelect.setChecked(model.getSelected());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout clItem;
        CircleImageView civProfilePicture;
        TextView tvName;
        RadioButton rbSelect;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            clItem = itemView.findViewById(R.id.clItem);
            civProfilePicture = itemView.findViewById(R.id.civProfilePicture);
            tvName = itemView.findViewById(R.id.tvName);
            rbSelect = itemView.findViewById(R.id.rbSelect);
        }
    }
}