package com.fairtok.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.databinding.ItemCommentListBinding;
import com.fairtok.model.comment.Comment;
import com.fairtok.model.comment.CommentReply;
import com.fairtok.utils.Global;
import com.like.LikeButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentsViewHolder> {
    public ArrayList<Comment.Data> mList = new ArrayList<>();
    public OnRecyclerViewItemClick onRecyclerViewItemClick;
    Context context;
    public  OnReplyRecyclerViewItemClick onReplyRecyclerViewItemClick;


    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_list, parent, false);
        context=parent.getContext();
        return new CommentsViewHolder(view);
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {
        holder.setModel(mList.get(position), position);
        holder.comment_like.setText(""+mList.get(position).getCommentLike()+" Likes");

        ArrayList<CommentReply> replyList = mList.get(position).getComment_reply();

        if(replyList.size()>0) {

            holder.binding.commentReply.setText(replyList.size() + " Reply");
            holder.binding.replyLayout.removeAllViews();
            setMargins(holder.binding.replyLayout, 130, 0, 0, 0);


            for(CommentReply comment : replyList)
            {

                LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View v = vi.inflate(R.layout.item_comment_list_reply, null);

                ImageView img_profile = v.findViewById(R.id.img_profile);
                TextView tv_username = v.findViewById(R.id.tv_username);
                TextView tv_comment = v.findViewById(R.id.tv_comment);
                TextView comment_like = v.findViewById(R.id.comment_like);
                LikeButton likebtn = v.findViewById(R.id.likebtn);
                TextView comment_reply = v.findViewById(R.id.comment_reply);
                likebtn.setLiked(comment.getCommentIsLiked());

                try {
                    Picasso.get().load(comment.getUserProfile()).into(img_profile);
                }
                catch (Exception e){e.printStackTrace();}

                tv_username.setText(comment.getUserName());
                tv_comment.setText(comment.getComment());
                comment_like.setText(comment.getCommentLike() + " Likes");

                //Global.PARENT_ID=
                likebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        comment_like.setText((comment.getCommentLike() +1)  + " Likes");
                        onReplyRecyclerViewItemClick.onCommentReplyItemClick(comment,3);
                    }
                });

                comment_reply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Global.PARENT_ID= mList.get(position).getCommentsId();
                        onReplyRecyclerViewItemClick.onCommentReplyItemClick(comment,4);
                    }
                });
                holder.binding.replyLayout.addView(v, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateData(List<Comment.Data> list) {
        mList = (ArrayList<Comment.Data>) list;
        notifyDataSetChanged();

    }

    public void loadMore(List<Comment.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

    public interface OnRecyclerViewItemClick {
        void onCommentItemClick(Comment.Data data, int position, int type);
    }

    public interface OnReplyRecyclerViewItemClick {
        void onCommentReplyItemClick(CommentReply commentReply, int type);
    }


    class CommentsViewHolder extends RecyclerView.ViewHolder {
        ItemCommentListBinding binding;
        TextView comment_like;

        CommentsViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            comment_like = itemView.findViewById(R.id.comment_like);

        }

        public void setModel(Comment.Data comment, int position) {
            binding.setComment(comment);
            if (!TextUtils.isEmpty(Global.USER_ID)) {
                if (Global.USER_ID.equals(comment.getUserId())) {
                    binding.imgDelete.setVisibility(View.VISIBLE);
                }
            }

            binding.imgDelete.setOnClickListener(v -> onRecyclerViewItemClick.onCommentItemClick(comment, position, 1));
            binding.tvUsername.setOnClickListener(view -> onRecyclerViewItemClick.onCommentItemClick(comment, position, 2));
            binding.imgProfile.setOnClickListener(view -> onRecyclerViewItemClick.onCommentItemClick(comment, position, 2));
            binding.likebtn.setOnClickListener(view -> onRecyclerViewItemClick.onCommentItemClick(comment, position, 3));
            binding.commentReply.setOnClickListener(view -> onRecyclerViewItemClick.onCommentItemClick(comment, position, 4));

        }
    }
}
