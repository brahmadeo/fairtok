package com.fairtok.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.databinding.ItemRowFamilyBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.view.familyroom.MomentSingActivity;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class RecommendFamilyAdapter extends RecyclerView.Adapter<RecommendFamilyAdapter.RecomendFamilyViewHolder> {

    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;
    @NonNull
    @Override
    public RecomendFamilyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_family, parent, false);
        return new RecommendFamilyAdapter.RecomendFamilyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecomendFamilyViewHolder holder, int position) {

        if (position == 0){
            holder.binding.btnJoin.setBackground(holder.binding.getRoot().getContext().getDrawable(R.drawable.bg_white_round));
            holder.binding.btnJoin.setTextColor(holder.binding.getRoot().getContext().getResources().getColor(R.color.black));
        }
        if (position%2 == 0){
            holder.binding.mainLayout.setBackground(holder.binding.getRoot().getContext().getDrawable(R.drawable.gradient_border_yellow));
        }else {
            holder.binding.mainLayout.setBackground(holder.binding.getRoot().getContext().getDrawable(R.drawable.gradient_border));
        }
        holder.setModel(position);
        holder.binding.imgLayout.setOnClickListener(view -> {
            holder.binding.getRoot().getContext().startActivity(new Intent(holder.binding.getRoot().getContext(), MomentSingActivity.class));
        });
        holder.binding.textLayout.setOnClickListener(view -> {
            holder.binding.getRoot().getContext().startActivity(new Intent(holder.binding.getRoot().getContext(), MomentSingActivity.class));
        });
        
        holder.binding.btnJoin.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Coming Soon...", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class RecomendFamilyViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout textLayout, imgLayout;
        private AppCompatButton btnJoin;
        ItemRowFamilyBinding binding;
        public RecomendFamilyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            //imgLayout = itemView.findViewById(R.id.imgLayout);
            //textLayout = itemView.findViewById(R.id.textLayout);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

}
