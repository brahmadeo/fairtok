package com.fairtok.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterParticipants1 extends RecyclerView.Adapter<AdapterParticipants1.MyViewHolder> {
    public interface ParticipantListener{
        void participant(String userName, int pos);
    }
    private Context context;
    ArrayList<HashMap<String, String>> data;
    private ParticipantListener listener;
    private boolean isBroadcaster;
    public AdapterParticipants1(Context context, ArrayList<HashMap<String, String>> arrayList, boolean isBroadcaster) {
        this.context = context;
        data = arrayList;
        listener = (ParticipantListener) context;
        this.isBroadcaster = isBroadcaster;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_participant, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tvName.setText(data.get(position).get("full_name"));
        Glide.with(context).load(Const.ITEM_BASE_URL + data.get(position).get("user_profile")).into(holder.ivProfile);

        if (!isBroadcaster){
            holder.imgCohost.setVisibility(View.GONE);
        }else {
            holder.imgCohost.setVisibility(View.VISIBLE);
        }

       /* holder.imgCohost.setOnClickListener(view -> {
            listener.participant(data.get(position).get("user_name"), position);
        });
        holder.tvName.setOnClickListener(view -> {
            if (isBroadcaster)
                listener.participant("", position);
        });
        holder.ivProfile.setOnClickListener(view -> {
            if (isBroadcaster)
                listener.participant("", position);
        });*/
        holder.imgKickOff.setOnClickListener(view -> {
            listener.participant(data.get(position).get("user_name"), position);
        });
    }

    @Override
    public int getItemCount() {

        return data!=null ? data.size() : 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView ivProfile, imgCohost, imgKickOff;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            imgCohost = itemView.findViewById(R.id.imgCohost);
            imgKickOff = itemView.findViewById(R.id.imgKickOff);

        }
    }
}
