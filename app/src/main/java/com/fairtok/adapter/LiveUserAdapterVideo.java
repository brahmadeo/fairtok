package com.fairtok.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class LiveUserAdapterVideo extends RecyclerView.Adapter<LiveUserAdapterVideo.HashTagVideoViewHolder> {
    public interface Listener {
        void liveUserClick(String live_id, String channelname);
    }
    public ArrayList<HashMap<String, String>> mList;
    public boolean isHashTag = false;
    public String word = "";
    private String type;
    private Context context;
    private Listener liveUserClick;
    private int[] colorArr = {R.color.redcolor, R.color.color_true, R.color.pink, R.color.blue};

    public LiveUserAdapterVideo(Context context, ArrayList<HashMap<String, String>> arrayList, Listener listener, String type) {
        this.context = context;
        mList = arrayList;
        this.type = type;
        this.liveUserClick = listener;
    }

    @NonNull
    @Override
    public HashTagVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_live_popup, parent, false);;

        return new HashTagVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HashTagVideoViewHolder holder, int position) {

        if (colorArr.length<4){
            holder.tvName.setTextColor(context.getResources().getColor(colorArr[position]));
        }else {
            holder.tvName.setTextColor(context.getResources().getColor(colorArr[position%4]));
        }
        holder.clMain.setOnClickListener(v -> {

            boolean isGaming=false;
            EngineConfig engineConfig = new EngineConfig();
            engineConfig.setHostUserId(mList.get(position).get("user_id"));
            Utils.hostUserId = mList.get(position).get("user_id");
            Utils.hostProfilePic = mList.get(position).get("user_profile");
            Utils.hostFullName = mList.get(position).get("user_full_name");
            Utils.party_id = mList.get(position).get("live_id");


            Intent intent = new Intent("custom-message");
            intent.putExtra("isBroadCaster",false);
            intent.putExtra("isGaming",isGaming);
            intent.putExtra("channelName",mList.get(position).get("user_name"));
            intent.putExtra("live_id",mList.get(position).get("live_id"));

            liveUserClick.liveUserClick(mList.get(position).get("live_id"), "@"+mList.get(position).get("user_name"));
            //LocalBroadcastManager.getInstance(holder.rlMain.getContext()).sendBroadcast(intent);

        });

        holder.tvName.setText(mList.get(position).get("user_full_name"));
        holder.tvUsername.setText("@"+mList.get(position).get("user_name"));
        holder.tvEarnings.setText(mList.get(position).get("coinsReceivedSilver"));
        holder.tvViewCount.setText(mList.get(position).get("viewers_count"));

        if (mList.get(position).get("live_thumbnail")!=null){
            AppUtils.loadPicassoImage(mList.get(position).get("live_thumbnail"), holder.ivProfileSmall);
        }

        //AppUtils.loadPicassoImage(Const.ITEM_BASE_URL+mList.get(position).get("user_profile"), holder.ivProfileSmall);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    static class HashTagVideoViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout clMain;

        ImageView ivProfile, ivProfileSmall;

        TextView tvName, tvUsername, tvViewCount, tvEarnings;

        HashTagVideoViewHolder(@NonNull View itemView) {
            super(itemView);

            clMain = itemView.findViewById(R.id.clMain);

            //ivProfile = itemView.findViewById(R.id.ivProfile);
            ivProfileSmall = itemView.findViewById(R.id.ivProfileSmall);

            //tvViewers = itemView.findViewById(R.id.tvViewers);
            //tvGoldCoin = itemView.findViewById(R.id.tvGoldCoin);
            tvName = itemView.findViewById(R.id.tvName);
            tvUsername = itemView.findViewById(R.id.tvUsername);
            tvEarnings = itemView.findViewById(R.id.tvEarnings);
            tvViewCount = itemView.findViewById(R.id.tvViewCount);

        }
    }
}
