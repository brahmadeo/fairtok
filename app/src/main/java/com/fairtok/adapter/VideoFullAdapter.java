package com.fairtok.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.AdIconView;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.ItemAdsLayBinding;
import com.fairtok.databinding.ItemVideoListBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Const;
import com.fairtok.utils.GlideLoader;
import com.fairtok.utils.Global;
import com.fairtok.view.search.FetchUserActivity;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.like.LikeButton;
import com.like.OnLikeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VideoFullAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int AD_TYPE = 1;
    private static final int AD_DISPLAY_FREQUENCY = 10;
    private static final int POST_TYPE = 2;
    public ArrayList<Video.Data> mList = new ArrayList<>();
    public OnRecyclerViewItemClick onRecyclerViewItemClick;
    public int itemToPlay = 0;
    public String postId = "";
    public UnifiedNativeAd unifiedNativeAd;
    public ArrayList<UnifiedNativeAd> nativeAds = new ArrayList<>();
    public NativeAd facebookNativeAd;
    public ArrayList<NativeAd> nativeAd = new ArrayList<>();
    private NativeAdLayout nativeAdLayout;
    Context context;


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();

        if (viewType == AD_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads_lay, parent, false);
            return new AdsViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_list, parent, false);
            return new VideoFullViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof VideoFullViewHolder) {
            VideoFullViewHolder holder = (VideoFullViewHolder) viewHolder;
            holder.mVideoViews.setText(mList.get(position).getPostViewCount() +" Viewers");

            if(position == 0) {
                Intent intent = new Intent("live-popup-messagenew");
                intent.putExtra("isPopup", true);
                //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }

            if (mList.get(position).getIs_trending() == 1) {
                holder.mTrending.setVisibility(View.VISIBLE);
            } else {
                holder.mTrending.setVisibility(View.GONE);
            }

            if (mList.get(position).getIsFollow() == 0) {
                holder.mFollow.setVisibility(View.VISIBLE);
            } else {
                holder.mFollow.setVisibility(View.GONE);
            }

            holder.binding.tvUser.setText("@"+mList.get(position).getUserName());

            holder.setModel(position);
        }
        else if (viewHolder instanceof AdsViewHolder) {
                AdsViewHolder holder = (AdsViewHolder) viewHolder;
                if (nativeAds.size()>0 && unifiedNativeAd != null) {
                    holder.binding.frame.setVisibility(View.VISIBLE);
                    LinearLayout frameLayout = holder.binding.frame;
                    UnifiedNativeAdView adView = (UnifiedNativeAdView) LayoutInflater.from(holder.binding.getRoot().getContext()).inflate(R.layout.admob_native, null, false);
                    populateUnifiedNativeAdView(anyNativeItem(), adView);
                    frameLayout.removeAllViews();
                    frameLayout.addView(adView);
                    holder.binding.unbind();
                } else if(nativeAd.size()>0 && nativeAd != null) {
                    inflateAd(anyItem(), holder.binding);
                } else {
                      /*holder.binding.selfAd.getRoot().setVisibility(View.VISIBLE);
                      holder.binding.selfAd.btnBrand.setOnClickListener(v -> holder.binding.getRoot().getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Const.ADS_URL))));*/
                }
        }
    }

    private Random randomGenerator;

    public NativeAd anyItem() {
        randomGenerator = new Random();
        int index = randomGenerator.nextInt(nativeAd.size());
        NativeAd item = nativeAd.get(index);
        System.out.println("Managers choice this week" + item + "our recommendation to you");
        return item;
    }

    public UnifiedNativeAd anyNativeItem() {
        randomGenerator = new Random();
        int index = randomGenerator.nextInt(nativeAds.size());
        UnifiedNativeAd item = nativeAds.get(index);
        System.out.println("Managers choice this week" + item + "our recommendation to you");
        return item;
    }

    private void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView adView) {
         try {

        adView.setMediaView(adView.findViewById(R.id.ad_media));
        // Set other ad assets.
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));

        // The headline and mediaContent are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        //adView.getMediaView().setMediaContent(nativeAd.getMediaContent());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (unifiedNativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.GONE);

        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(unifiedNativeAd.getBody());
        }

        if (unifiedNativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) adView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }

        if (unifiedNativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            new GlideLoader(adView.getContext()).loadRoundDrawable(unifiedNativeAd.getIcon().getDrawable(), (ImageView) adView.getIconView());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (unifiedNativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(unifiedNativeAd.getPrice());
        }

        if (unifiedNativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(unifiedNativeAd.getStore());
        }

        if (unifiedNativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(unifiedNativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (unifiedNativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(unifiedNativeAd);

        // Get the video controller for the ad. One will always be provided, even if the ad doesn't
        // have a video asset.
        VideoController vc = unifiedNativeAd.getVideoController();

        // Updates the UI to say whether or not this ad has a video asset.
         } catch (Exception e) {
             e.printStackTrace();
         }
    }

    private void inflateAd(NativeAd nativeAd, ItemAdsLayBinding binding) {

        try {
            nativeAd.unregisterView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Add the Ad view into the ad container.
        nativeAdLayout = binding.fbNative;
        binding.fbNative.setVisibility(View.VISIBLE);
        LayoutInflater inflater = LayoutInflater.from(binding.getRoot().getContext());
        // Inflate the Ad view.  The layout referenced should be the one you created in the last step.
        RelativeLayout adView = (RelativeLayout) inflater.inflate(R.layout.fb_native_full, nativeAdLayout, false);
        nativeAdLayout.addView(adView);

        // Add the AdOptionsView
        LinearLayout adChoicesContainer = adView.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView(adView.getContext(), nativeAd, nativeAdLayout);
        adChoicesContainer.removeAllViews();
        adChoicesContainer.addView(adOptionsView, 0);

        // Create native UI using the ad metadata.
        AdIconView nativeAdIcon = adView.findViewById(R.id.native_ad_icon);
        TextView nativeAdTitle = adView.findViewById(R.id.native_ad_title);
        MediaView nativeAdMedia = adView.findViewById(R.id.native_ad_media);
        TextView nativeAdSocialContext = adView.findViewById(R.id.native_ad_social_context);
        TextView nativeAdBody = adView.findViewById(R.id.native_ad_body);
        TextView sponsoredLabel = adView.findViewById(R.id.native_ad_sponsored_label);
        Button nativeAdCallToAction = adView.findViewById(R.id.native_ad_call_to_action);

        // Set the Text.
        nativeAdTitle.setText(nativeAd.getAdvertiserName());
        nativeAdBody.setText(nativeAd.getAdBodyText());
        nativeAdSocialContext.setText(nativeAd.getAdSocialContext());
        nativeAdCallToAction.setVisibility(nativeAd.hasCallToAction() ? View.VISIBLE : View.INVISIBLE);
        nativeAdCallToAction.setText(nativeAd.getAdCallToAction());
        sponsoredLabel.setText(nativeAd.getSponsoredTranslation());

        // Create a list of clickable views
        List<View> clickableViews = new ArrayList<>();
        clickableViews.add(nativeAdTitle);
        clickableViews.add(nativeAdCallToAction);


        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(
                adView,
                nativeAdMedia,
                nativeAdIcon,
                clickableViews);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        //mList.get(position) == null
        if (position == 4) {
            return AD_TYPE;
        } else {
            return POST_TYPE;
        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        mList.addAll(data);
        notifyDataSetChanged();
    }

    public interface OnRecyclerViewItemClick {
        void onItemClick(Video.Data model, int position, int type, ItemVideoListBinding binding);
        void onHashTagClick(String hashTag);
    }
    class VideoFullViewHolder extends RecyclerView.ViewHolder {
        ItemVideoListBinding binding;
        TextView mVideoViews;
        Button mFollow,mTrending;

        VideoFullViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            if (binding != null) {
                binding.executePendingBindings();
            }
            mVideoViews = itemView.findViewById(R.id.video_views);
            mFollow = itemView.findViewById(R.id.new_follow_btn);
            mTrending = itemView.findViewById(R.id.new_trending_btn);

            if(Utils.hideViewCounts==true) {
                mVideoViews.setVisibility(View.GONE);
            } else {
                mVideoViews.setVisibility(View.VISIBLE);
            }
        }

        @SuppressLint("ClickableViewAccessibility")
        public void setModel(int position) {
            if (position < mList.size()) {
                binding.setModel(mList.get(position));
                if (position == itemToPlay || postId.equals(mList.get(position).getPostId())) {
                    Animation animation = AnimationUtils.loadAnimation(binding.getRoot().getContext(), R.anim.slow_rotate);
                    binding.imgSound.startAnimation(animation);
                    onRecyclerViewItemClick.onItemClick(mList.get(position), position, 9, binding);
                }
                binding.tvDuetWith.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), FetchUserActivity.class);
                        intent.putExtra("userid", mList.get(position).getCreater_id());
                        binding.getRoot().getContext().startActivity(intent);
                    }
                });

                binding.tvSoundName.setSelected(true);
                binding.tvLikeCount.setText(Global.prettyCount(mList.get(position).getDummyLikeCount()));

//                binding.playerView.setOnTouchListener(new OnSwipeTouchListener(binding.getRoot().getContext()) {
//                    public void onSwipeTop() {
//                        Toast.makeText(binding.getRoot().getContext(),"SWIPETOP",Toast.LENGTH_SHORT).show();
//                    }
//                    public void onSwipeRight() {
//                        onRecyclerViewItemClick.onItemClick(mList.get(position), position, 0, binding);
//                    }
//                    public void onSwipeLeft() {
//                        onRecyclerViewItemClick.onItemClick(mList.get(position), position, 0, binding);
//                    }
//                    public void onSwipeBottom() {
//                        Toast.makeText(binding.getRoot().getContext(),"SWIPEbottom",Toast.LENGTH_SHORT).show();
//                    }
//                });

//                binding.flingLayout.setOnTouchListener(new View.OnTouchListener() {
//                    GestureDetector gestureDetector = new GestureDetector(binding.getRoot().getContext(), new GestureDetector.SimpleOnGestureListener() {
//                        @Override
//                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//                            super.onFling(e1, e2, velocityX, velocityY);
//                            float deltaX = e1.getX() - e2.getX();
//                            float deltaXAbs = Math.abs(deltaX);
//                            // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
//                            if((deltaXAbs > 100) && (deltaXAbs < 1000))
//                            {
//                                if(deltaX > 0)
//                                {
//                                    onRecyclerViewItemClick.onItemClick(mList.get(position), position, 0, binding);
//                                }
//                            }
//                            return true;
//                        }
//                    });
//
//                    @Override
//                    public boolean onTouch(View v, MotionEvent event) {
//                        gestureDetector.onTouchEvent(event);
//                        return true;
//                    }
//                });

                binding.playerView.setOnTouchListener(new View.OnTouchListener() {
                    GestureDetector gestureDetector = new GestureDetector(binding.getRoot().getContext(), new GestureDetector.SimpleOnGestureListener() {

                        @Override
                        public boolean onDoubleTap(MotionEvent e) {

                            if(mList.get(position).getVideoIsLiked()){
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                                mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) - 1));
                                binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                                mList.get(position).setVideoIsLiked(0);
                                binding.likebtn.setLiked(false);
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 21, binding);
                            } else {
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                                mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) + 1));
                                binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                                mList.get(position).setVideoIsLiked(1);
                                binding.likebtn.setLiked(true);
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 20, binding);
                            }
                            return super.onDoubleTap(e);
                        }

                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            onRecyclerViewItemClick.onItemClick(mList.get(position), position, 2, binding);
                            return super.onSingleTapConfirmed(e);
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {
                            //onRecyclerViewItemClick.onItemClick(mList.get(position), position, 8, binding); //no need for reporting instead like the video
                            if(mList.get(position).getVideoIsLiked()){
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                                mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) - 1));
                                binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                                mList.get(position).setVideoIsLiked(0);
                                binding.likebtn.setLiked(false);
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 21, binding);
                            } else {
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                                mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) + 1));
                                binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                                mList.get(position).setVideoIsLiked(1);
                                binding.likebtn.setLiked(true);
                                onRecyclerViewItemClick.onItemClick(mList.get(position), position, 20, binding);
                            }
                            super.onLongPress(e);
                        }
                    });

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        gestureDetector.onTouchEvent(event);
                        return true;
                    }
                });
                binding.tvDescreption.setOnHashtagClickListener((view, text) -> onRecyclerViewItemClick.onHashTagClick(text.toString()));

                binding.loutUser.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 1, binding));

                binding.imgSendBubble.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 3, binding));
                binding.imgGift.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 23, binding));
                binding.imgLang.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 24, binding));
                binding.likebtn.setOnLikeListener(new OnLikeListener() {
                    @Override
                    public void liked(LikeButton likeButton) {
                        onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                        if (!Global.ACCESS_TOKEN.isEmpty()){
                            mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) + 1));
                            binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                            mList.get(position).setVideoIsLiked(1);
                        }
                    }

                    @Override
                    public void unLiked(LikeButton likeButton) {
                        onRecyclerViewItemClick.onItemClick(mList.get(position), position, 4, binding);
                        if (!Global.ACCESS_TOKEN.isEmpty()){
                            mList.get(position).setPostLikesCount(String.valueOf(Integer.parseInt(mList.get(position).getPostLikesCount()) - 1));
                            binding.tvLikeCount.setText(Global.prettyCount(Long.parseLong(mList.get(position).getPostLikesCount())));
                            mList.get(position).setVideoIsLiked(0);
                        }
                    }
                });
                binding.imgComment.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 5, binding));
                binding.imgShare.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 6, binding));
                binding.imgSound.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 7, binding));
                binding.newFollowBtn.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 10, binding));
                binding.rlDuet.setOnClickListener(v -> onRecyclerViewItemClick.onItemClick(mList.get(position), position, 22, binding));
            }
        }

    }

    class AdsViewHolder extends RecyclerView.ViewHolder {
        ItemAdsLayBinding binding;

        public AdsViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.executePendingBindings();
        }
    }
}
