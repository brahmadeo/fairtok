package com.fairtok.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.openduo.activities.CallActivity;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

public class LiveUserAdapterNew extends RecyclerView.Adapter<LiveUserAdapterNew.HashTagVideoViewHolder> {
    public ArrayList<HashMap<String, String>> mList;
    public boolean isHashTag = false;
    public String word = "";
    public Activity activity;
    private String type;

    public LiveUserAdapterNew(ArrayList<HashMap<String, String>> arrayList, Activity activity, String type) {
        mList = arrayList;
        this.activity = activity;
        this.type = type;
    }

    @NonNull
    @Override
    public HashTagVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (type.equals("video")){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_live, parent, false);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflate_live, parent, false);
        }

        return new HashTagVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HashTagVideoViewHolder holder, int position) {

        holder.rlMain.setOnClickListener(v -> {

            boolean isGaming=false;
            EngineConfig engineConfig = new EngineConfig();
            engineConfig.setHostUserId(mList.get(position).get("user_id"));
            Utils.hostUserId = mList.get(position).get("user_id");
            Utils.hostProfilePic = mList.get(position).get("user_profile");
            Utils.hostFullName = mList.get(position).get("user_full_name");
            Utils.party_id = mList.get(position).get("live_id");


            Intent intent = new Intent("custom-message");
            intent.putExtra("isBroadCaster",false);
            intent.putExtra("isGaming",isGaming);
            intent.putExtra("isPremium",mList.get(position).get("isPremium"));
            intent.putExtra("premiumCoins",mList.get(position).get("premiumCoins"));
            intent.putExtra("user_profile",mList.get(position).get("user_profile"));
            intent.putExtra("isGaming",isGaming);
            intent.putExtra("channelName",mList.get(position).get("user_name"));
            intent.putExtra("live_id",mList.get(position).get("live_id"));

            LocalBroadcastManager.getInstance(holder.rlMain.getContext()).sendBroadcast(intent);

        });

        holder.tvName.setText(mList.get(position).get("user_full_name"));
        holder.tvHashTag.setText(mList.get(position).get("post_hash_tag"));
        holder.tvGoldCoin.setText(mList.get(position).get("coinsReceived"));
        holder.tvViewers.setText(mList.get(position).get("viewers_count"));

        if (mList.get(position).get("live_thumbnail")!=null){
            AppUtils.loadPicassoImage(mList.get(position).get("live_thumbnail"), holder.ivProfile);
        }
        if (mList.get(position).get("isPremium").equals("1")){
            holder.imgLock.setVisibility(View.VISIBLE);
        }else {
            holder.imgLock.setVisibility(View.GONE);
        }
        AppUtils.loadPicassoImage(Const.BASE_URL_IMAGE+mList.get(position).get("user_profile"), holder.ivProfileSmall);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    static class HashTagVideoViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlMain;

        ImageView ivProfile, ivProfileSmall, imgLock;

        TextView tvViewers, tvGoldCoin, tvName, tvHashTag;

        HashTagVideoViewHolder(@NonNull View itemView) {
            super(itemView);

            rlMain = itemView.findViewById(R.id.rlMain);

            ivProfile = itemView.findViewById(R.id.ivProfile);
            ivProfileSmall = itemView.findViewById(R.id.ivProfileSmall);
            imgLock = itemView.findViewById(R.id.imgLock);

            tvViewers = itemView.findViewById(R.id.tvViewers);
            tvGoldCoin = itemView.findViewById(R.id.tvGoldCoin);
            tvName = itemView.findViewById(R.id.tvName);
            tvHashTag = itemView.findViewById(R.id.tvHashTag);

        }
    }
}
