package com.fairtok.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.ItemLiveUsersBinding;
import com.fairtok.model.LiveUserBean;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.utils.Global;

import java.util.ArrayList;
import java.util.List;

public class LiveUsersAdapter extends RecyclerView.Adapter<LiveUsersAdapter.HashTagVideoViewHolder> {
    public ArrayList<LiveUserBean.PostDataBean.DataArrBean> mList = new ArrayList<LiveUserBean.PostDataBean.DataArrBean>();
    public boolean isHashTag = false;
    public String word = "";


    @NonNull
    @Override
    public HashTagVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_users, parent, false);
        return new HashTagVideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HashTagVideoViewHolder holder, int position) {
        holder.setModel(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateData(List<LiveUserBean.PostDataBean.DataArrBean> list) {
        mList = (ArrayList<LiveUserBean.PostDataBean.DataArrBean>) list;
        notifyDataSetChanged();

    }

    public void loadMore(List<LiveUserBean.PostDataBean.DataArrBean> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

    class HashTagVideoViewHolder extends RecyclerView.ViewHolder {
        ItemLiveUsersBinding binding;

        HashTagVideoViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            if (binding != null) {
                binding.executePendingBindings();
            }
        }

        public void setModel(int position) {

            binding.setModel(mList.get(position));
            if(mList.get(position).getIsGaming().equalsIgnoreCase("1"))
                binding.tvDescreption.setText("Game: You are invited to join this Game.");
            else
                binding.tvDescreption.setText("Party: You are invited to join this party.");

            binding.tvLikeCount.setText(mList.get(position).getTotal_member());
            binding.tvCoins.setText(Global.prettyCount(Long.parseLong(mList.get(position).getCoinsReceived())));

            binding.getRoot().setOnClickListener(v -> {

                boolean isGaming=false;
                EngineConfig engineConfig = new EngineConfig();
                engineConfig.setHostUserId(mList.get(position).getUser_id());
                Utils.hostUserId = mList.get(position).getUser_id();
                Utils.hostProfilePic = mList.get(position).getPhoto();
                Utils.party_id = mList.get(position).getParty_id();

                Log.v("jhljkhl", Utils.hostUserId+"---"+Utils.hostProfilePic+"----"+Utils.party_id);

                if (mList.get(position).getIsGaming().equalsIgnoreCase("1"))
                {
                    isGaming=true;
                }

//                Intent intent = new Intent(binding.getRoot().getContext(), BroadcastActivity.class);

                Intent intent = new Intent("custom-message");
                intent.putExtra("isBroadCaster",false);
                intent.putExtra("isGaming",isGaming);
                intent.putExtra("channelName",mList.get(position).getUser_name());
                Log.v("ljhljhl", mList.get(position).getUser_name());
                //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
                LocalBroadcastManager.getInstance(binding.getRoot().getContext()).sendBroadcast(intent);
            });
        }

    }
}
