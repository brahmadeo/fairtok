package com.fairtok.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fairtok.R;
import com.fairtok.chat.bean.ChatListPOJO;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyPerformanceArrayAdapter extends ArrayAdapter<ChatListPOJO> {
    private final Activity context;
    private final List<ChatListPOJO> names;
    boolean showInvite=false;
    SessionManager sessionManager;

    static class ViewHolder {
        public TextView text,tvInvite;
        public CircleImageView image;
    }

    public MyPerformanceArrayAdapter(Activity context, List<ChatListPOJO> names,boolean showInvite) {
        super(context, R.layout.user_list_row_live, names);
        this.context = context;
        this.names = names;
        this.showInvite=showInvite;
        sessionManager = new SessionManager(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.user_list_row_live, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.tvName);
            viewHolder.image =  rowView.findViewById(R.id.img);
            viewHolder.tvInvite = rowView.findViewById(R.id.tvInvite);

//            if(showInvite)
//                viewHolder.tvInvite.setVisibility(View.VISIBLE);
//            else
//                viewHolder.tvInvite.setVisibility(View.GONE);

            //viewHolder.tvInvite.setVisibility(View.GONE);




            rowView.setTag(viewHolder);

        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        String s = names.get(position).getReceiver_fullname();
        holder.text.setText(s);

        Picasso.
                get().
                load(names.get(position).getReceiver_photo()).
                into(holder.image);

        holder.tvInvite.setText("Coins Sent : "+Global.prettyCount(Long.parseLong(names.get(position).getLast_msg())));

//        if(sessionManager.getUser().getData().getUserName().matches(names.get(position).getReceiver_fullname()))
//            holder.tvInvite.setVisibility(View.GONE);
//        else {
//            if (showInvite)
//                holder.tvInvite.setVisibility(View.VISIBLE);
//            else
//                holder.tvInvite.setVisibility(View.GONE);
//        }

        //holder.tvInvite.setVisibility(View.GONE);

        return rowView;
    }
}