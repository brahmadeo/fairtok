package com.fairtok.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.openlive.utils.GiftUtil;

public class AdapterBeauty extends RecyclerView.Adapter<AdapterBeauty.MyViewHolder> {

    public interface BeautyListener{
        void beautyListener(String type, int position);
    }
    private Context context;
    int[] data;
    private int selected = -1;
    private BeautyListener beautyListener;
    public AdapterBeauty(Context context, int[] stringArray) {
        this.beautyListener = beautyListener;
        data = stringArray;
        this.beautyListener = (BeautyListener) context;

    }

    @NonNull
    @Override
    public AdapterBeauty.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_masks, viewGroup, false);
        return new AdapterBeauty.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBeauty.MyViewHolder holder, final int position) {

        holder.ivImage.setImageResource(GiftUtil.getBeautyAnimRes(position));
        holder.llMain.setOnClickListener(view -> {
            beautyListener.beautyListener("preview", position);
        });

    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llMain, llAmount;
        ConstraintLayout clIcon;
        TextView tvAmount;
        ImageView ivImage, ivAmount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            llMain = itemView.findViewById(R.id.llMain);
        }
    }
}
