package com.fairtok.adapter;


import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.databinding.ItemNotificationBinding;
import com.fairtok.model.notification.Notification;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Global;
import com.fairtok.view.search.FetchUserActivity;
import com.fairtok.view.video.PlayerActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {
    public static ArrayList<Notification.Data> mList = new ArrayList<>();
    public static List<Video.Data> list = new ArrayList<>();
    public static int count = 20;
    public static int start = 0;

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.setModel(mList.get(position));

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void updateData(List<Notification.Data> list) {
        mList = (ArrayList<Notification.Data>) list;
        notifyDataSetChanged();

    }

    public void loadMore(List<Notification.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }


    static class NotificationViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding binding;
        int position = 0;

        NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void setModel(Notification.Data notification)
        {
            binding.setNotification(notification);
            binding.imgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                    intent.putExtra("userid", notification.getSenderUserId());
                    binding.getRoot().getContext().startActivity(intent);
                }
            });
            binding.getRoot().setOnClickListener(v -> {
                if (notification.getNotificationType().equals("following"))
                {
                    Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                    intent.putExtra("userid", notification.getSenderUserId());
                    binding.getRoot().getContext().startActivity(intent);
                }
                else if (notification.getNotificationType().equals("send_coin"))
                {
                    Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                    intent.putExtra("userid", notification.getSenderUserId());
                    binding.getRoot().getContext().startActivity(intent);
                }
                else
                {
                    if (notification.getVideo().toLowerCase().equals("n/a"))
                    {
                        Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                        intent.putExtra("userid", notification.getSenderUserId());
                        binding.getRoot().getContext().startActivity(intent);
                    }
                    else
                    {
                        fetchUserVideos(notification,binding);
                    }
                }

            });

        }

        public void fetchUserVideos(Notification.Data notification,ItemNotificationBinding binding)
        {

            CompositeDisposable disposable = new CompositeDisposable();
            disposable.add(Global.initRetrofit().getUserVideos(notification.getReceivedUserId(),
                    NotificationAdapter.count, start, Global.USER_ID)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .doOnTerminate(() -> {

                    })
                    .subscribe((video, throwable) -> {
                        if (video != null && video.getData() != null && !video.getData().isEmpty())
                        {
                            list = video.getData();
                            if (list.size() > 0)
                            {
                                for (int i=0; i<list.size();i++)
                                {
                                    if (list.get(i).getPostId().equals(notification.getPost_id()))
                                    {
                                        position = i;
                                        break;
                                    }
                                }
                                Intent intent = new Intent(binding.getRoot().getContext(), PlayerActivity.class);
                                intent.putExtra("from", "notification");
                                intent.putExtra("video_list", new Gson().toJson(list));
                                intent.putExtra("position", position);
                                intent.putExtra("type", 0);
                                intent.putExtra("user_id", notification.getReceivedUserId());
                                binding.getRoot().getContext().startActivity(intent);
                            }
                            else
                            {
                                Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                                intent.putExtra("userid", notification.getReceivedUserId());
                                binding.getRoot().getContext().startActivity(intent);
                            }
                        }
                        else
                        {
                            Toast.makeText(binding.getRoot().getContext(),"Video not available1",Toast.LENGTH_SHORT).show();
                        }
                    }));
        }

    }

}
