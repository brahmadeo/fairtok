package com.fairtok.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.fairtok.model.FanModel;
import com.fairtok.openlive.fragment.FanFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FanPagerAdapter extends FragmentStateAdapter {

    ArrayList<FanModel> fanlist;
    public FanPagerAdapter(@NonNull FragmentActivity fragmentActivity, ArrayList<FanModel> fanlist) {
        super(fragmentActivity);
        this.fanlist = fanlist;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return FanFragment.newInstance(String.valueOf(position), fanlist);
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
