package com.fairtok.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.model.videos.Video;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class SingingAdapter extends RecyclerView.Adapter<SingingAdapter.SingingViewHolder> {

    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;

    @NonNull
    @Override
    public SingingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_singing, parent, false);
        return new SingingAdapter.SingingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingingViewHolder holder, int position) {
        //holder.setModel(position);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class SingingViewHolder extends RecyclerView.ViewHolder{
        public SingingViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

}
