package com.fairtok.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.model.videos.Video;
import com.fairtok.view.familyroom.Model.TagModel;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class TaglistAdapter extends RecyclerView.Adapter<TaglistAdapter.TaglistViewHolder> {

    public interface clickListener{
        void itemClick(String name);
    }
    private Context context;
    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;
    private List<TagModel> modelList;
    private clickListener listener;
    public TaglistAdapter(Context context, List<TagModel> modelList){
        this.context = context;
        this.modelList = modelList;
        listener = (clickListener) context;
    }
    @NonNull
    @Override
    public TaglistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_taglist, parent, false);
        return new TaglistAdapter.TaglistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaglistViewHolder holder, int position) {
        holder.tvName.setText(modelList.get(position).getName());
        holder.itemView.setOnClickListener(view -> {
            listener.itemClick(modelList.get(position).getName());
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class TaglistViewHolder extends RecyclerView.ViewHolder{
        private TextView tvName;
        public TaglistViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
        }

        public void setModel(int position){

        }
    }

    /*public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }*/

}
