package com.fairtok.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.openlive.utils.GiftUtil;

public class AdapterMask extends RecyclerView.Adapter<AdapterMask.MyViewHolder> {

    private Context context;
    int[] data;
    private int selected = -1;
    public AdapterMask(Context context, int[] stringArray) {
        this.context = context;
        data = stringArray;
    }

    @NonNull
    @Override
    public AdapterMask.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_masks, viewGroup, false);
        return new AdapterMask.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMask.MyViewHolder holder, final int position) {

        holder.ivImage.setImageResource(GiftUtil.getMaskAnimRes(position));
        holder.llMain.setOnClickListener(view -> {
            Toast.makeText(context, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItem, llAmount, llMain;
        ConstraintLayout clIcon;
        TextView tvAmount;
        ImageView ivImage, ivAmount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            llMain = itemView.findViewById(R.id.llMain);
        }
    }
}
