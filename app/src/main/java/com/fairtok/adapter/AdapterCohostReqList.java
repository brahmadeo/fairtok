package com.fairtok.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.model.comment.Comment;
import com.fairtok.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AdapterCohostReqList extends RecyclerView.Adapter<AdapterCohostReqList.MyViewHolder> {
    public interface CohostRequestListener{
        void cohostRequestListener(String type, String userName, String request_id, int pos);
    }
    private Context context;
    ArrayList<HashMap<String, String>> data;
    private CohostRequestListener listener;
    public AdapterCohostReqList(Context context, ArrayList<HashMap<String, String>> arrayList) {
        this.context = context;
        data = arrayList;
        listener = (CohostRequestListener) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_cohost_request, viewGroup, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.tvName.setText(data.get(position).get("user_name"));
        Glide.with(context).load(Const.ITEM_BASE_URL + data.get(position).get("user_profile")).into(holder.imgUser);
        holder.imgAccept.setOnClickListener(view -> {
            listener.cohostRequestListener("accept", data.get(position).get("user_name"), data.get(position).get("request_id"), position);
        });
        holder.imgReject.setOnClickListener(view -> {
            listener.cohostRequestListener("reject", data.get(position).get("user_name"), data.get(position).get("request_id"), position);
        });

    }

    @Override
    public int getItemCount() {

        //return data!=null ? data.size() : 0;
        return data.size();
    }
    public void updateData(ArrayList<HashMap<String, String>> list) {
        data = list;
        notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView imgUser, imgAccept, imgReject;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvName);
            imgUser = itemView.findViewById(R.id.imgUser);
            imgReject = itemView.findViewById(R.id.imgReject);
            imgAccept = itemView.findViewById(R.id.imgAccept);

        }
    }
}
