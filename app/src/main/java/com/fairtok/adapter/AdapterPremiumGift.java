package com.fairtok.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.openlive.utils.GiftUtil;

import java.util.ArrayList;

public class AdapterPremiumGift extends RecyclerView.Adapter<AdapterPremiumGift.MyViewHolder> {

    public interface PremiumListener{
        void listenerPre(int pos);
    }
    private Context context;
    int[] data;
    private int selected = -1;
    private PremiumListener listener;
    public AdapterPremiumGift(Context context, int[] stringArray) {
        data = stringArray;
        listener = (PremiumListener) context;
    }

    @NonNull
    @Override
    public AdapterPremiumGift.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_premium_gifts, viewGroup, false);
        return new AdapterPremiumGift.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPremiumGift.MyViewHolder holder, final int position) {
        //GiftModel model = data.get(position);
        /*if(model.getSelected()) {
            holder.llItem.setBackgroundResource(R.drawable.bg_btn_gradient_rectangle);
            holder.clIcon.setBackgroundResource(R.drawable.bg_white_corner_5);
            holder.llAmount.setBackgroundResource(R.drawable.bg_btn_round_white);
            holder.ivAmount.setVisibility(View.GONE);
            holder.tvAmount.setTypeface(null, Typeface.BOLD);
        } else {
            holder.llItem.setBackgroundResource(R.drawable.bg_btn_gray_rectangle);
            holder.clIcon.setBackgroundResource(0);
            holder.llAmount.setBackgroundResource(R.drawable.bg_btn_round_border_gradient);
            holder.ivAmount.setVisibility(View.VISIBLE);
            holder.tvAmount.setTypeface(null, Typeface.NORMAL);
        }*/
        holder.tvAmount.setText(String.valueOf(data[position]));

        holder.ivIcon.setImageResource(GiftUtil.getGiftAnimRes(position));

        holder.llItem.setOnClickListener(v -> {
            selected = position;
            listener.listenerPre(position);
            notifyDataSetChanged();
        });

        if (selected == position){
            holder.llItem.setBackgroundResource(R.drawable.bg_btn_gradient_rectangle);
            holder.clIcon.setBackgroundResource(R.drawable.bg_white_corner_5);
            holder.llAmount.setBackgroundResource(R.drawable.bg_btn_round_white);
            holder.ivAmount.setVisibility(View.GONE);
            holder.tvAmount.setTypeface(null, Typeface.BOLD);
        }else {
            holder.llItem.setBackgroundResource(R.drawable.bg_btn_gray_rectangle);
            holder.clIcon.setBackgroundResource(0);
            holder.llAmount.setBackgroundResource(R.drawable.bg_btn_round_border_gradient);
            holder.ivAmount.setVisibility(View.VISIBLE);
            holder.tvAmount.setTypeface(null, Typeface.NORMAL);
        }

    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItem, llAmount;
        ConstraintLayout clIcon;
        TextView tvAmount;
        ImageView ivIcon, ivAmount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llItem = itemView.findViewById(R.id.llItem);
            clIcon = itemView.findViewById(R.id.clIcon);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            llAmount = itemView.findViewById(R.id.llAmount);
            ivAmount = itemView.findViewById(R.id.ivAmount);
            tvAmount = itemView.findViewById(R.id.tvAmount);
        }
    }
}
