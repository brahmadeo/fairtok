package com.fairtok.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.model.videos.Video;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class MomentAdapter extends RecyclerView.Adapter<MomentAdapter.MomentViewHolder> {

    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;

    @NonNull
    @Override
    public MomentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_moment, parent, false);
        return new MomentAdapter.MomentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MomentViewHolder holder, int position) {
        holder.setModel(position);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class MomentViewHolder extends RecyclerView.ViewHolder{
        public MomentViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

}
