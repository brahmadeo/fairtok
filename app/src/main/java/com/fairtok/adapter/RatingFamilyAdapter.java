package com.fairtok.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.databinding.ItemRowRatingFamilyBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class RatingFamilyAdapter extends RecyclerView.Adapter<RatingFamilyAdapter.RecomendFamilyViewHolder> {

    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;
    @NonNull
    @Override
    public RecomendFamilyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_rating_family, parent, false);
        return new RatingFamilyAdapter.RecomendFamilyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecomendFamilyViewHolder holder, int position) {
        holder.setModel(position);

        if (position%2 == 0){
            holder.binding.mainLayout.setBackground(holder.binding.getRoot().getContext().getDrawable(R.drawable.gradient_border_yellow));
        }else {
            holder.binding.mainLayout.setBackground(holder.binding.getRoot().getContext().getDrawable(R.drawable.gradient_border));
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class RecomendFamilyViewHolder extends RecyclerView.ViewHolder{
        private ItemRowRatingFamilyBinding binding;
        public RecomendFamilyViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

}
