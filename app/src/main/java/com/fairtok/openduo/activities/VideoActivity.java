package com.fairtok.openduo.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.model.user.User;
import com.fairtok.openduo.Constants;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.agora.rtm.RemoteInvitation;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class VideoActivity extends BaseCallActivity implements CompleteTaskListner {
    private static final String TAG = VideoActivity.class.getSimpleName();

    private FrameLayout mLocalPreviewLayout;
    private FrameLayout mRemotePreviewLayout;
    private AppCompatImageView mMuteBtn;
    private String mChannel;
    private int mPeerUid;
    PrefHandler prefHandler;

    long startTime;
    Chronometer stopWatch;

    String start_Time,end_Time;
    String charges,callCharge;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        initUI();
        initVideo();

        stopWatch = (Chronometer) findViewById(R.id.tvTime);
        startTime = SystemClock.elapsedRealtime();
        prefHandler = new PrefHandler(this);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        start_Time = sdf.format(new Date());

        //TextView textGoesHere = (TextView) findViewById(R.id.tvTime);
        stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer arg0) {
                if(Utils.isCaller)
                    checkBalance();
            }
        });

        stopWatch.setBase(SystemClock.elapsedRealtime());
        stopWatch.start();
    }

    private void initUI() {
        mLocalPreviewLayout = findViewById(R.id.local_preview_layout);
        mRemotePreviewLayout = findViewById(R.id.remote_preview_layout);

        mMuteBtn = findViewById(R.id.btn_mute);
        mMuteBtn.setActivated(true);
    }

    @Override
    protected void onGlobalLayoutCompleted() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLocalPreviewLayout.getLayoutParams();
        params.topMargin += statusBarHeight;
        mLocalPreviewLayout.setLayoutParams(params);

        RelativeLayout buttonLayout = findViewById(R.id.button_layout);
        params = (RelativeLayout.LayoutParams) buttonLayout.getLayoutParams();
        params.bottomMargin = displayMetrics.heightPixels / 8;
        params.leftMargin = displayMetrics.widthPixels / 6;
        params.rightMargin = displayMetrics.widthPixels / 6;
        buttonLayout.setLayoutParams(params);
    }

    private void initVideo() {
        Intent intent  = getIntent();
        mChannel = intent.getStringExtra(Constants.KEY_CALLING_CHANNEL);

        try {
            mPeerUid = Integer.valueOf(intent.getStringExtra(Constants.KEY_CALLING_PEER));

            Log.v("ljhjlh1", mChannel);
            Log.v("ljhjlh2", String.valueOf(mPeerUid));
            Log.v("ljhjlh3", Global.USER_ID);
        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.message_wrong_number,
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        rtcEngine().setClientRole(io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
        setVideoConfiguration();
        setupLocalPreview();
        Log.v("kjhsq1", config().getUserId());
        Log.v("kjhsq2", mChannel);
        joinRtcChannel(mChannel, "", Integer.parseInt(config().getUserId()));
    }

    private void setupLocalPreview() {
        SurfaceView surfaceView = setupVideo(Integer.parseInt(config().getUserId()), true);
        surfaceView.setZOrderOnTop(true);
        mLocalPreviewLayout.addView(surfaceView);
    }

    @Override
    public void onUserJoined(final int uid, int elapsed) {
        if (uid != mPeerUid) return;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mRemotePreviewLayout.getChildCount() == 0) {
                    Log.v(" fddbsdfvsdvsv", "1");
                    SurfaceView surfaceView = setupVideo(uid, false);
                    mRemotePreviewLayout.addView(surfaceView);
                }
            }
        });
    }

    //if (uid != mPeerUid) return;
    @Override
    public void onUserOffline(int uid, int reason) {
        Log.v("paras","paras user went offline uid = "+uid);
        Log.v("paras","paras mPeerUid = "+mPeerUid);
        finish();
    }

    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_endcall:
                finish();
                break;
            case R.id.btn_mute:
                rtcEngine().muteLocalAudioStream(mMuteBtn.isActivated());
                mMuteBtn.setActivated(!mMuteBtn.isActivated());
                break;
            case R.id.btn_switch_camera:
                rtcEngine().switchCamera();
                break;
        }
    }

    @Override
    public void finish() {

        stopWatch.stop();
        if(Utils.isCaller)
            showElapsedTime();
        else {
            super.finish();
            leaveChannel();
        }
    }

    @Override
    public void onRemoteInvitationReceived(RemoteInvitation remoteInvitation) {
        // Do not respond to any other calls
        Log.i(TAG, "Ignore remote invitation from " +
                remoteInvitation.getCallerId() + " while in calling");
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    private void showElapsedTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        end_Time = sdf.format(new Date());

        long elapsedMillis = SystemClock.elapsedRealtime() - stopWatch.getBase();
//        Toast.makeText(getApplicationContext(), "Call duration: " + millisecondsToTime(elapsedMillis), Toast.LENGTH_SHORT).show();
        leaveChannel();

        charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(VideoActivity.this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - chargeInt;
        Log.v("paras","paras currentWalletBalance = "+currentWalletBalance);
        Log.v("paras","paras finalBalane = "+finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins(""+finalBalane);
        sessionManager.saveUser(user);

        String duration = millisecondsToTime(elapsedMillis);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Your dialog code.
                showAlert("Call duration: " + duration +"\n"+charges,""+finalBalane,duration);
            }
        });

    }

    public void showAlert(String msg,String balance,String duration) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(VideoActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                updateWallet(VideoActivity.this,balance,duration);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private String millisecondsToTime(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;
        String secondsStr = Long.toString(seconds);
        String secs;
        if (secondsStr.length() >= 2) {
            secs = secondsStr.substring(0, 2);
        } else {
            secs = "0" + secondsStr;
        }

        return minutes + ":" + secs;
    }

    private String getCharges(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes=minutes+1;
        }

        int charges = (int) (prefHandler.getVideoCharges() * minutes);

        callCharge = ""+charges;
        return "Call charges: "+charges;
    }

    private int getChargesInt(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes=minutes+1;
        }

        int charges = (int) (prefHandler.getVideoCharges() * minutes);
        return charges;
    }

    private void checkBalance() {
        long elapsedMillis = SystemClock.elapsedRealtime() - stopWatch.getBase();
        String charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(VideoActivity.this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        if(chargeInt>=currentWalletBalance)
            showElapsedTime();
    }

    public void updateWallet(Context context,String balance,String duration) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "update"));
        params.add(new Pair<>("balance", balance));
        params.add(new Pair<>("userId", prefHandler.getuId()));
        params.add(new Pair<>("callerName", prefHandler.getName()));
        params.add(new Pair<>("calleeId", Utils.CURRENT_RECEIVER_ID));
        params.add(new Pair<>("calleeName", Utils.CURRENT_RECEIVER_NAME));
        params.add(new Pair<>("duration", duration));
        params.add(new Pair<>("startTime", start_Time));
        params.add(new Pair<>("endTime", end_Time));
        params.add(new Pair<>("charge", callCharge));
        params.add(new Pair<>("callType", "Video"));

        new AsyncHttpsRequest("Wait...", context, params, this, 0, false).execute(Utils.UPDATE_BALANCE);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if(response_code == 0)
        {
            try
            {
                //JSONObject obj = new JSONObject(result);
                super.finish();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }

    }
}
