package com.fairtok.openduo.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telecom.PhoneAccount;
import android.telecom.TelecomManager;
import android.telecom.VideoProfile;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.fairtok.chat.Utils;
import com.fairtok.openduo.Constants;
import com.fairtok.openduo.ui.calling.connectionservice.OpenDuoConnectionService;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.WebServices;
import com.fairtok.utils.WebServicesCallback;
import com.fairtok.view.liveCall.LiveCallListActivity;
import com.fairtok.view.liveCall.VideoLiveActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.agora.rtm.ErrorInfo;
import io.agora.rtm.LocalInvitation;
import io.agora.rtm.RemoteInvitation;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmCallManager;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmMessage;

public abstract class BaseCallActivity extends BaseRtcActivity implements RtmChannelListener, ResultCallback<Void> {
    private static final String TAG = BaseCallActivity.class.getSimpleName();

    protected RtmCallManager mRtmCallManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        mRtmCallManager = rtmCallManager();
    }

    public void gotoCallingInterface(String peerUid, String channel, int role, String callerName, String callerImg, String calleeName, String calleeImg, String type) {
        if (config().useSystemCallInterface()) {
            // Not supported yet.
            // placeSystemCall(String.valueOf(config().getUserId()), peerUid, channel);
        } else {

            gotoCallingActivity(channel, peerUid, role, callerName, callerImg, calleeName, calleeImg, type);
        }
    }

    private void placeSystemCall(String myUid, String peerUid, String channel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Bundle extras = new Bundle();
            extras.putInt(TelecomManager.EXTRA_START_CALL_WITH_VIDEO_STATE, VideoProfile.STATE_BIDIRECTIONAL);

            Bundle extraBundle = new Bundle();
            extraBundle.putString(Constants.CS_KEY_UID, myUid);
            extraBundle.putString(Constants.CS_KEY_SUBSCRIBER, peerUid);
            extraBundle.putString(Constants.CS_KEY_CHANNEL, channel);
            extraBundle.putInt(Constants.CS_KEY_ROLE, Constants.CALL_ID_OUT);
            extras.putBundle(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS, extraBundle);

            try {
                TelecomManager telecomManager = (TelecomManager)
                        getApplicationContext().getSystemService(Context.TELECOM_SERVICE);
                PhoneAccount pa = telecomManager.getPhoneAccount(
                        config().getPhoneAccountOut().getAccountHandle());
                extras.putParcelable(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, pa.getAccountHandle());
                telecomManager.placeCall(Uri.fromParts(
                        OpenDuoConnectionService.SCHEME_AG, peerUid, null), extras);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    protected void gotoCallingActivity(String channel, String peer, int role, String callerName, String callerImg, String calleeName, String calleeImg, String type) {

        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra(Constants.KEY_CALLING_CHANNEL, channel);
        intent.putExtra(Constants.KEY_CALLING_PEER, peer);
        intent.putExtra(Constants.KEY_CALLING_ROLE, role);
        intent.putExtra(Constants.KEY_CALLEE_NAME, calleeName);
        intent.putExtra(Constants.KEY_CALLEE_IMG, calleeImg);
        intent.putExtra(Constants.KEY_CALLER_NAME, callerName);
        intent.putExtra(Constants.KEY_CALLER_IMG, callerImg);
        intent.putExtra(Constants.KEY_CALL_TYPE, type);

        startActivity(intent);
    }

    protected void inviteCall(final String peerUid, final String channel, final String content) {
        LocalInvitation invitation = mRtmCallManager.createLocalInvitation(peerUid);
        invitation.setContent(channel);
        invitation.setChannelId(channel);
        invitation.setContent(content);
        mRtmCallManager.sendLocalInvitation(invitation, this);
        global().setLocalInvitation(invitation);
    }

    protected void answerCall(final RemoteInvitation invitation) {
        if (mRtmCallManager != null && invitation != null) {
            mRtmCallManager.acceptRemoteInvitation(invitation, this);

            hitAcceptRejectCallApi("2", invitation);
        }
    }

    protected void cancelLocalInvitation() {
        if (mRtmCallManager != null && global().getLocalInvitation() != null) {
            Utils.isCaller = false;
            Log.v("jjhjh", "1");
            mRtmCallManager.cancelLocalInvitation(global().getLocalInvitation(), this);

        }
    }

    protected void refuseRemoteInvitation(@NonNull RemoteInvitation invitation) {
        if (mRtmCallManager != null) {
            Utils.isCaller = false;
            Log.v("jjhjh", "2");
            mRtmCallManager.refuseRemoteInvitation(invitation, this);
            hitAcceptRejectCallApi("3", invitation);
        }
    }

    @Override
    public void onMemberCountUpdated(int count) {

    }

    @Override
    public void onAttributesUpdated(List<RtmChannelAttribute> list) {

    }

    @Override
    public void onMessageReceived(RtmMessage rtmMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onMemberJoined(RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onMemberLeft(RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onSuccess(Void aVoid) {

    }

    @Override
    public void onFailure(ErrorInfo errorInfo) {

    }

    @Override
    public void onLocalInvitationReceived(LocalInvitation localInvitation) {
        super.onLocalInvitationReceived(localInvitation);
    }

    @Override
    public void onLocalInvitationAccepted(LocalInvitation localInvitation, String response) {
        Log.i("BaseActivity", "onLocalInvitationAccepted by peer:" + localInvitation.getCalleeId());

        Log.i("BaseActivity", "onLocalInvitationAccepted channel Id:" + localInvitation.getChannelId());

        if (localInvitation.getChannelId().contains("Audio")) {
            //gotoAudioCallActivity
            gotoAudioActivity(localInvitation.getChannelId(), localInvitation.getCalleeId());
        } else {
            Log.v("kjgkjgkkj", localInvitation.getChannelId());
            Log.v("kjgkjgkkj", localInvitation.getCalleeId());
            Log.v("kjgkjgkkj", localInvitation.getContent());

            String[] split = localInvitation.getContent().split("-");

            if (split[split.length - 1].equals("1")) {
                gotoLiveVideoActivity(localInvitation.getChannelId(), localInvitation.getCalleeId(), localInvitation.getContent());
            }
            else {
                gotoVideoActivity(localInvitation.getChannelId(), localInvitation.getCalleeId());
            }
        }
    }

    @Override
    public void onLocalInvitationRefused(LocalInvitation localInvitation, String response) {
        super.onLocalInvitationRefused(localInvitation, response);
    }

    @Override
    public void onLocalInvitationCanceled(LocalInvitation localInvitation) {
        super.onLocalInvitationCanceled(localInvitation);
    }

    @Override
    public void onLocalInvitationFailure(LocalInvitation localInvitation, int errorCode) {
        super.onLocalInvitationFailure(localInvitation, errorCode);
        Log.w("BaseActivity", "onLocalInvitationFailure:" + errorCode);
    }

    @Override
    public void onRemoteInvitationReceived(RemoteInvitation remoteInvitation) {
        Log.i("BaseActivity", "onRemoteInvitationReceived from caller:" + remoteInvitation.getCallerId());
        global().setRemoteInvitation(remoteInvitation);

        String[] parts = remoteInvitation.getContent().split("-");
        String callerName = parts[0];
        String callerImg = parts[1];
        String calleeName = parts[2];
        String calleeImg = parts[3];
        String type = "";

        gotoCallingActivity(remoteInvitation.getChannelId(), remoteInvitation.getCallerId(),
                Constants.ROLE_CALLEE, callerName, callerImg, calleeName, calleeImg, type);
    }

    @Override
    public void onRemoteInvitationAccepted(RemoteInvitation remoteInvitation) {
        Log.i("BaseActivity", "paras onRemoteInvitation channelId:" + remoteInvitation.getChannelId());

        if (remoteInvitation.getChannelId().contains("Audio")) {
            //gotoAudioCallActivity
            gotoAudioActivity(remoteInvitation.getChannelId(), remoteInvitation.getCallerId());
        } else {
            Log.i("BaseActivity", "paras onRemoteInvitationAccepted from caller:" + remoteInvitation.getCallerId());
            Log.v("lhlkhlk1", remoteInvitation.getCallerId());
            Log.v("lhlkhlk2", remoteInvitation.getChannelId());
            Log.v("lhlkhlk3", remoteInvitation.getContent());
            Log.v("lhlkhlk4", remoteInvitation.getResponse());
            Log.v("lhlkhlk5", String.valueOf(remoteInvitation.getState()));


            String[] split = remoteInvitation.getContent().split("-");

            if (split[split.length - 1].equals("1")) {

                gotoLiveVideoActivity(remoteInvitation.getChannelId(), remoteInvitation.getCallerId(), remoteInvitation.getContent());
            } else {
                gotoVideoActivity(remoteInvitation.getChannelId(), remoteInvitation.getCallerId());
            }
        }
    }

    @Override
    public void onRemoteInvitationRefused(RemoteInvitation remoteInvitation) {
        super.onRemoteInvitationRefused(remoteInvitation);
    }

    @Override
    public void onRemoteInvitationCanceled(RemoteInvitation remoteInvitation) {
        super.onRemoteInvitationCanceled(remoteInvitation);
    }

    @Override
    public void onRemoteInvitationFailure(RemoteInvitation remoteInvitation, int errorCode) {
        super.onRemoteInvitationFailure(remoteInvitation, errorCode);
        Log.w("BaseActivity", "onRemoteInvitationFailure:" + errorCode);
    }

    public void gotoVideoActivity(String channel, String peer) {
        Intent intent = new Intent(this, VideoActivity.class);
        intent.putExtra(Constants.KEY_CALLING_CHANNEL, channel);
        intent.putExtra(Constants.KEY_CALLING_PEER, peer);
        startActivity(intent);
    }

    public void gotoLiveVideoActivity(String channel, String peer, String content) {

        Intent intent = new Intent(this, VideoLiveActivity.class);
        intent.putExtra(Constants.KEY_CALLING_CHANNEL, channel);
        intent.putExtra(Constants.KEY_CALLING_PEER, peer);
        intent.putExtra(Constants.KEY_CALLING_CONTENT, content);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, channel);
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, peer);
        startActivity(intent);

    }

    public void gotoAudioActivity(String channel, String peer) {
        Intent intent = new Intent(this, VoiceChatViewActivity.class);
        intent.putExtra(Constants.KEY_CALLING_CHANNEL, channel);
        intent.putExtra(Constants.KEY_CALLING_PEER, peer);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void hitAcceptRejectCallApi(String type, RemoteInvitation remoteInvitation) {

        String[] split = remoteInvitation.getContent().split("-");

        Utils.LIVE_CALL_LIVE_ID = split[split.length-3];
        Utils.LIVE_CALL_CALL_ID = split[split.length-2];

        HashMap<String, String> hashMap=new HashMap<>();
        hashMap.put("liveId", Utils.LIVE_CALL_LIVE_ID);
        hashMap.put("callId", Utils.LIVE_CALL_CALL_ID);
        hashMap.put("status", type);

        WebServices.postApiHeader(this,
                Const.BASE_URL + Const.updateLiveCall, hashMap,false,false,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                        parseUpdateCall(response);

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    private void parseUpdateCall(JSONObject response) {

        try{

            if (response.getBoolean("status")) {



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
