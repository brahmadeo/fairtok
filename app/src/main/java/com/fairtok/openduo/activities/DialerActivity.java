package com.fairtok.openduo.activities;

import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.openduo.ui.calling.conventional.DialerLayout;

import java.util.ArrayList;
import java.util.List;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class DialerActivity extends BaseCallActivity implements CompleteTaskListner {

    public static String userId,userImg,userName, type;
    public static boolean isAudio=false;
    PrefHandler prefHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer);
        userName = getIntent().getStringExtra("userName");
        userId = getIntent().getStringExtra("userId");
        userImg = getIntent().getStringExtra("userImg");
        type = getIntent().getStringExtra("type");
        isAudio = getIntent().getBooleanExtra("isAudio",false);
        prefHandler = new PrefHandler(this);

        initUI();
    }

    public void sendAlert()
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "alert"));
        params.add(new Pair<>("receiverid", userId));
        params.add(new Pair<>("msg", "You have missed the call from "+prefHandler.getName()));

        Log.v("Paras","Paras calling alert api");

        new AsyncHttpsRequest("Wait...!", this, params, this, 1, false).execute(Utils.SendNotification);
        onBackPressed();
    }


    private void initUI() {
        TextView identifier = findViewById(R.id.identifier_text);
        identifier.setText(String.format(getResources().
                getString(R.string.identifier_format), config().getUserId()));
    }

    @Override
    protected void onGlobalLayoutCompleted() {
        DialerLayout dialerLayout = findViewById(R.id.dialer_layout);
        dialerLayout.setActivity(this);
        dialerLayout.adjustScreenWidth(displayMetrics.widthPixels);

        int dialerHeight = dialerLayout.getHeight();
        int dialerRemainHeight = displayMetrics.heightPixels - statusBarHeight - dialerHeight;
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) dialerLayout.getLayoutParams();
        params.topMargin = dialerRemainHeight / 4 + statusBarHeight;
        dialerLayout.setLayoutParams(params);

        //PrefHandler prefHandler = new PrefHandler(DialerActivity.this);

        char[] num = userId.toCharArray();
        DialerLayout.CallInputManager callManger = dialerLayout.mCallInputManager;
        for(char c: num)
        {
            callManger.append(String.valueOf(c));
        }

        dialerLayout.startCall(userId);
    }

    @Override
    public RtmClient rtmClient() {
        return application().rtmClient();
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void completeTask(String result, int response_code) {

    }
}
