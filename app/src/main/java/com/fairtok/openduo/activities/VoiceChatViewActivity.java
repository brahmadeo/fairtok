package com.fairtok.openduo.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.model.user.User;
import com.fairtok.utils.Const;
import com.fairtok.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;

public class VoiceChatViewActivity extends AppCompatActivity implements CompleteTaskListner {

    private static final String LOG_TAG = VoiceChatViewActivity.class.getSimpleName();

    private static final int PERMISSION_REQ_ID_RECORD_AUDIO = 22;
    PrefHandler prefHandler;
    String start_Time,end_Time;
    String charges,callCharge;
    private RtcEngine mRtcEngine; // Tutorial Step 1
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() { // Tutorial Step 1

        /**
         * Occurs when a remote user (Communication)/host (Live Broadcast) leaves the channel.
         *
         * There are two reasons for users to become offline:
         *
         *     Leave the channel: When the user/host leaves the channel, the user/host sends a goodbye message. When this message is received, the SDK determines that the user/host leaves the channel.
         *     Drop offline: When no data packet of the user or host is received for a certain period of time (20 seconds for the communication profile, and more for the live broadcast profile), the SDK assumes that the user/host drops offline. A poor network connection may lead to false detections, so we recommend using the Agora RTM SDK for reliable offline detection.
         *
         * @param uid ID of the user or host who
         * leaves
         * the channel or goes offline.
         * @param reason Reason why the user goes offline:
         *
         *     USER_OFFLINE_QUIT(0): The user left the current channel.
         *     USER_OFFLINE_DROPPED(1): The SDK timed out and the user dropped offline because no data packet was received within a certain period of time. If a user quits the call and the message is not passed to the SDK (due to an unreliable channel), the SDK assumes the user dropped offline.
         *     USER_OFFLINE_BECOME_AUDIENCE(2): (Live broadcast only.) The client role switched from the host to the audience.
         */
        @Override
        public void onUserOffline(final int uid, final int reason) { // Tutorial Step 4
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onRemoteUserLeft(uid, reason);
                }
            });
        }

        /**
         * Occurs when a remote user stops/resumes sending the audio stream.
         * The SDK triggers this callback when the remote user stops or resumes sending the audio stream by calling the muteLocalAudioStream method.
         *
         * @param uid ID of the remote user.
         * @param muted Whether the remote user's audio stream is muted/unmuted:
         *
         *     true: Muted.
         *     false: Unmuted.
         */
        @Override
        public void onUserMuteAudio(final int uid, final boolean muted) { // Tutorial Step 6
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onRemoteUserVoiceMuted(uid, muted);
                }
            });
        }
    };

    String mChannel;
    long startTime;
    Chronometer stopWatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_chat_view);

        mChannel = getIntent().getStringExtra(com.fairtok.openduo.Constants.KEY_CALLING_CHANNEL);

        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO, PERMISSION_REQ_ID_RECORD_AUDIO)) {
            initAgoraEngineAndJoinChannel();
        }

        prefHandler = new PrefHandler(this);

        ImageView portrait = findViewById(R.id.peer_image);
        portrait.setImageResource(R.drawable.portrait);
        Glide.with(this).load(Const.ITEM_BASE_URL+ CallActivity.img).into(portrait);

        TextView tvName = findViewById(R.id.tvName);
        tvName.setText(CallActivity.name);

        stopWatch = (Chronometer) findViewById(R.id.tvTime);
        startTime = SystemClock.elapsedRealtime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        start_Time = sdf.format(new Date());

        //TextView textGoesHere = (TextView) findViewById(R.id.tvTime);
        stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer arg0) {
                if(Utils.isCaller)
                    checkBalance();
            }
        });

        stopWatch.setBase(SystemClock.elapsedRealtime());
        stopWatch.start();

    }

    private void initAgoraEngineAndJoinChannel() {
        initializeAgoraEngine();     // Tutorial Step 1
        joinChannel();               // Tutorial Step 2
    }

    public boolean checkSelfPermission(String permission, int requestCode) {
        Log.i(LOG_TAG, "checkSelfPermission " + permission + " " + requestCode);
        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{permission},
                    requestCode);
            return false;
        }
        return true;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.i(LOG_TAG, "onRequestPermissionsResult " + grantResults[0] + " " + requestCode);

        switch (requestCode) {
            case PERMISSION_REQ_ID_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initAgoraEngineAndJoinChannel();
                } else {
                    showLongToast("No permission for " + Manifest.permission.RECORD_AUDIO);
                    finish();
                }
                break;
            }
        }
    }

    public final void showLongToast(final String msg) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    boolean isLeaved=false;

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(isLeaved==true){
            isLeaved=false;
        }
        else {
            leaveChannel();
//            RtcEngine.destroy();
//            mRtcEngine = null;
        }
    }

    private void showElapsedTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        end_Time = sdf.format(new Date());

        long elapsedMillis = SystemClock.elapsedRealtime() - stopWatch.getBase();
//        Toast.makeText(getApplicationContext(), "Call duration: " + millisecondsToTime(elapsedMillis), Toast.LENGTH_SHORT).show();
        leaveChannel();
//        RtcEngine.destroy();
//        mRtcEngine = null;
        charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(VoiceChatViewActivity.this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - chargeInt;
        Log.v("paras","paras currentWalletBalance = "+currentWalletBalance);
        Log.v("paras","paras finalBalane = "+finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins(""+finalBalane);
        sessionManager.saveUser(user);
        String duration = millisecondsToTime(elapsedMillis);



        showAlert("Call duration: " + duration +"\n"+charges,""+finalBalane,duration);
    }

    public void showAlert(String msg,String balance,String duration) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(VoiceChatViewActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                updateWallet(VoiceChatViewActivity.this,balance,duration);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private String millisecondsToTime(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;
        String secondsStr = Long.toString(seconds);
        String secs;
        if (secondsStr.length() >= 2) {
            secs = secondsStr.substring(0, 2);
        } else {
            secs = "0" + secondsStr;
        }

        return minutes + ":" + secs;
    }

    private String getCharges(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes=minutes+1;
        }

        int charges = (int) (prefHandler.getAudioCharges() * minutes);
        callCharge=""+charges;
        return "Call charges: "+charges;
    }

    private int getChargesInt(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes=minutes+1;
        }

        int charges = (int) (prefHandler.getAudioCharges() * minutes);
        return charges;
    }

    private void checkBalance() {
        long elapsedMillis = SystemClock.elapsedRealtime() - stopWatch.getBase();
        String charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(VoiceChatViewActivity.this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        if(chargeInt>=currentWalletBalance)
            showElapsedTime();
    }

    public void updateWallet(Context context,String balance,String duration) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "update"));
        params.add(new Pair<>("balance", balance));
        params.add(new Pair<>("userId", prefHandler.getuId()));
        params.add(new Pair<>("callerName", prefHandler.getName()));
        params.add(new Pair<>("calleeId", Utils.CURRENT_RECEIVER_ID));
        params.add(new Pair<>("calleeName", Utils.CURRENT_RECEIVER_NAME));
        params.add(new Pair<>("duration", duration));
        params.add(new Pair<>("startTime", start_Time));
        params.add(new Pair<>("endTime", end_Time));

        params.add(new Pair<>("charge", callCharge));
        params.add(new Pair<>("callType", "Audio"));

        new AsyncHttpsRequest("Wait...", context, params, this, 0, false).execute(Utils.UPDATE_BALANCE);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if(response_code == 0)
        {
            try
            {
                //JSONObject obj = new JSONObject(result);
                finish();

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }

    }

    // Tutorial Step 7
    public void onLocalAudioMuteClicked(View view) {
        ImageView iv = (ImageView) view;
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.clearColorFilter();
        } else {
            iv.setSelected(true);
            iv.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        // Stops/Resumes sending the local audio stream.
        mRtcEngine.muteLocalAudioStream(iv.isSelected());
    }

    // Tutorial Step 5
    public void onSwitchSpeakerphoneClicked(View view) {
        ImageView iv = (ImageView) view;
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.clearColorFilter();
        } else {
            iv.setSelected(true);
            iv.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        }

        // Enables/Disables the audio playback route to the speakerphone.
        //
        // This method sets whether the audio is routed to the speakerphone or earpiece. After calling this method, the SDK returns the onAudioRouteChanged callback to indicate the changes.
        mRtcEngine.setEnableSpeakerphone(view.isSelected());
    }

    // Tutorial Step 3
    public void onEncCallClicked(View view) {
        stopWatch.stop();
        if( Utils.isCaller==true)
            showElapsedTime();
        else
            finish();
    }

    // Tutorial Step 1
    private void initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.private_app_id), mRtcEventHandler);
        } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    // Tutorial Step 2
    private void joinChannel() {
        String accessToken = getString(R.string.agora_access_token);
        if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "#YOUR ACCESS TOKEN#")) {
            accessToken = null; // default, no token
        }
        
        // Sets the channel profile of the Agora RtcEngine.
        // CHANNEL_PROFILE_COMMUNICATION(0): (Default) The Communication profile. Use this profile in one-on-one calls or group calls, where all users can talk freely.
        // CHANNEL_PROFILE_LIVE_BROADCASTING(1): The Live-Broadcast profile. Users in a live-broadcast channel have a role as either broadcaster or audience. A broadcaster can both send and receive streams; an audience can only receive streams.
        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);

        // Allows a user to join a channel.
        mRtcEngine.joinChannel(accessToken, mChannel, "Extra Optional Data", 0); // if you do not specify the uid, we will generate the uid for you
    }

    // Tutorial Step 3
    private void leaveChannel() {
        isLeaved=true;
        stopWatch.stop();
        mRtcEngine.leaveChannel();
    }

    // Tutorial Step 4
    private void onRemoteUserLeft(int uid, int reason) {
        stopWatch.stop();
        showLongToast(String.format(Locale.US, "user %d left %d", (uid & 0xFFFFFFFFL), reason));

        if( Utils.isCaller==true)
            showElapsedTime();
        else
            finish();


    }

    // Tutorial Step 6
    private void onRemoteUserVoiceMuted(int uid, boolean muted) {
        if(muted)
            Toast.makeText(getApplicationContext(),"user has muted the audio",Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(),"user has unmuted the audio",Toast.LENGTH_SHORT).show();
    }
}
