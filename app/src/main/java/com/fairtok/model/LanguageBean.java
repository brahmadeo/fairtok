package com.fairtok.model;

public class LanguageBean
{
    String id, name;

    public LanguageBean(String id, String name)
    {
        this.id=id;
        this.name=name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
