package com.fairtok.model;

import java.io.Serializable;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ServiceData implements Serializable {

    String accessToken;
    HashMap<String, RequestBody> hashMap;
    MultipartBody.Part body,body1,body2,body3;

    public ServiceData(String accessToken, HashMap<String, RequestBody> hashMap, MultipartBody.Part body, MultipartBody.Part body1, MultipartBody.Part body2, MultipartBody.Part body3) {
        this.accessToken = accessToken;
        this.hashMap = hashMap;
        this.body = body;
        this.body1 = body1;
        this.body2 = body2;
        this.body3 = body3;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public HashMap<String, RequestBody> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, RequestBody> hashMap) {
        this.hashMap = hashMap;
    }

    public MultipartBody.Part getBody() {
        return body;
    }

    public void setBody(MultipartBody.Part body) {
        this.body = body;
    }

    public MultipartBody.Part getBody1() {
        return body1;
    }

    public void setBody1(MultipartBody.Part body1) {
        this.body1 = body1;
    }

    public MultipartBody.Part getBody2() {
        return body2;
    }

    public void setBody2(MultipartBody.Part body2) {
        this.body2 = body2;
    }

    public MultipartBody.Part getBody3() {
        return body3;
    }

    public void setBody3(MultipartBody.Part body3) {
        this.body3 = body3;
    }

    // time spend
    private Long timeSpend;
    public ServiceData(Long timeSpend, String accessToken){
        this.timeSpend = timeSpend;
        this.accessToken = accessToken;
    }

    public void setTimeSpend(Long timeSpend) {
        this.timeSpend = timeSpend;
    }

    public Long getTimeSpend() {
        return timeSpend;
    }
}
