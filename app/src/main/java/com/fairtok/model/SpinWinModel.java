package com.fairtok.model;

import com.fairtok.model.wallet.CoinRate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpinWinModel {
    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public static class Data {

        @SerializedName("setting_id")
        private String setting_id;
        @Expose
        private String type;
        @SerializedName("value")
        private String value;

        @SerializedName("created_at")
        private String allcreated_atRate;

        public String getSetting_id() {
            return setting_id;
        }

        public void setSetting_id(String setting_id) {
            this.setting_id = setting_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getAllcreated_atRate() {
            return allcreated_atRate;
        }

        public void setAllcreated_atRate(String allcreated_atRate) {
            this.allcreated_atRate = allcreated_atRate;
        }
    }
}
