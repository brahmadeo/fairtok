
package com.fairtok.model.wallet;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoinRate {

    @Expose
    private Data data;
    @Expose
    private String message;
    @Expose
    private Boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public static class Data {

        @SerializedName("coin_rate_id")
        private String coinRateId;
        @Expose
        private String status;
        @SerializedName("usd_rate")
        private String usdRate;

        @SerializedName("all_rate")
        private String allRate;

        @SerializedName("gold_rate")
        private String goldRate;

        @SerializedName("min_gold")
        private String minGold;

        @SerializedName("min_silver")
        private String minSilver;


        public String getCoinRateId() {
            return coinRateId;
        }

        public void setCoinRateId(String coinRateId) {
            this.coinRateId = coinRateId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUsdRate() {
            return usdRate;
        }

        public void setUsdRate(String usdRate) {
            this.usdRate = usdRate;
        }

        public String getAllRate() {
            return allRate;
        }

        public void setAllRate(String allRate) {
            this.allRate = allRate;
        }

        public String getGoldRate() {
            return goldRate;
        }

        public void setGoldRate(String goldRate) {
            this.goldRate = goldRate;
        }

        public String getMinGold() {
            return minGold;
        }

        public void setMinGold(String minGold) {
            this.minGold = minGold;
        }

        public String getMinSilver() {
            return minSilver;
        }

        public void setMinSilver(String minSilver) {
            this.minSilver = minSilver;
        }
    }
}
