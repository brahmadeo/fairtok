package com.fairtok.model;

public class FanModel {
    String name, price;

    public FanModel(String name, String price){
        this.name = name;
        this.price = price;
    }
    public FanModel(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
