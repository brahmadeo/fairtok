
package com.fairtok.model.comment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommentReply {


    @Expose
    private String message;
    @Expose
    private Boolean status;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }




        @Expose
        private String comment;
        @SerializedName("comments_id")
        private String commentsId;
        @SerializedName("created_date")
        private String createdDate;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("is_verify")
        private String isVerify;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("user_profile")
        private String userProfile;

        @SerializedName("comments_like")
        private int commentLike;

        @SerializedName("comment_likes_or_not")
        private int commentIsLiked;


        @SerializedName("comment_reply")
        private ArrayList<CommentReply> comment_reply;

        public int getCommentLike() {
            return commentLike;
        }

        public void setCommentLike(int commentLike) {
            this.commentLike = commentLike;
        }

        public ArrayList<CommentReply> getComment_reply() {
            return comment_reply;
        }

        public void setComment_reply(ArrayList<CommentReply> comment_reply) {
            this.comment_reply = comment_reply;
        }

        public boolean getCommentIsLiked() {
            return commentIsLiked == 1;
        }

        public void setCommentIsLiked(int videoIsLiked) {
            this.commentIsLiked = videoIsLiked;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCommentsId() {
            return commentsId;
        }

        public void setCommentsId(String commentsId) {
            this.commentsId = commentsId;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public boolean getIsVerify() {
            return isVerify.equals("1");
        }

        public void setIsVerify(String isVerify) {
            this.isVerify = isVerify;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserProfile() {
            return userProfile;
        }

        public void setUserProfile(String userProfile) {
            this.userProfile = userProfile;
        }


}
