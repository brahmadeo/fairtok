package com.fairtok.model;

import com.fairtok.model.wallet.MyWallet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveUserData {
    @Expose
    private Data data;

    @Expose
    private String message;
    @Expose
    private Boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public static class Data {
        @SerializedName("live_thumbnail")
        private String live_thumbnail;

        @SerializedName("user_id")
        private String user_id;

        public String getLive_thumbnail() {
            return live_thumbnail;
        }

        public void setLive_thumbnail(String live_thumbnail) {
            this.live_thumbnail = live_thumbnail;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }

    /*public static class AllData{
        @SerializedName("user_id")
        private String user_id;

        @SerializedName("name")
        private String name;

        @SerializedName("isLiveStreaming")
        private String isLiveStreaming;

        @SerializedName("post_hash_tag")
        private String post_hash_tag;

        @SerializedName("start_time")
        private String start_time;

        @SerializedName("thumbnail")
        private String thumbnail;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsLiveStreaming() {
            return isLiveStreaming;
        }

        public void setIsLiveStreaming(String isLiveStreaming) {
            this.isLiveStreaming = isLiveStreaming;
        }

        public String getPost_hash_tag() {
            return post_hash_tag;
        }

        public void setPost_hash_tag(String post_hash_tag) {
            this.post_hash_tag = post_hash_tag;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }
    }*/
}
