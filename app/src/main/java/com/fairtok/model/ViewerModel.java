package com.fairtok.model;

public class ViewerModel {
    String name, profilePicture;
    Boolean isSelected;

    public ViewerModel(String name, String profilePicture, Boolean isSelected) {
        this.name = name;
        this.profilePicture = profilePicture;
        this.isSelected = isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public Boolean getSelected() {
        return isSelected;
    }
}
