package com.fairtok.beauty.CameraGallery;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

public class AspectFrameLayout extends FrameLayout {
    private static final String TAG = "Sunny";
    private double mTargetAspect = -1.0d;
    public AspectFrameLayout(Context context) {
        super(context);
    }
    public AspectFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAspectRatio(double aspectRatio) {
        if (aspectRatio < 0.0d) {
            throw new IllegalArgumentException();
        }
        Log.d(TAG, "Setting aspect ratio to " + aspectRatio + " (was " + this.mTargetAspect + ")");
        if (this.mTargetAspect != aspectRatio) {
            this.mTargetAspect = aspectRatio;
            requestLayout();
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG, "onMeasure target=" + this.mTargetAspect + " width=[" + MeasureSpec.toString(widthMeasureSpec) + "] height=[" + MeasureSpec.toString(heightMeasureSpec) + "]");
        if (this.mTargetAspect > 0.0d) {
            int initialWidth = MeasureSpec.getSize(widthMeasureSpec);
            int horizPadding = getPaddingLeft() + getPaddingRight();
            int vertPadding = getPaddingTop() + getPaddingBottom();
            initialWidth -= horizPadding;
            int initialHeight = MeasureSpec.getSize(heightMeasureSpec) - vertPadding;
            double aspectDiff = (this.mTargetAspect / (((double) initialWidth) / ((double) initialHeight))) - 1.0d;
            if (Math.abs(aspectDiff) < 0.01d) {
                Log.d(TAG, "aspect ratio is good (target=" + this.mTargetAspect + ", view=" + initialWidth + "x" + initialHeight + ")");
            } else {
                if (aspectDiff > 0.0d) {
                    initialHeight = (int) (((double) initialWidth) / this.mTargetAspect);
                } else {
                    initialWidth = (int) (((double) initialHeight) * this.mTargetAspect);
                }
                Log.d(TAG, "new size=" + initialWidth + "x" + initialHeight + " + padding " + horizPadding + "x" + vertPadding);
                initialHeight += vertPadding;
                widthMeasureSpec = MeasureSpec.makeMeasureSpec(initialWidth + horizPadding, 1073741824);
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(initialHeight, 1073741824);
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}