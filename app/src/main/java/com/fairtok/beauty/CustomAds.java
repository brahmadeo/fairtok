package com.fairtok.beauty;

import android.content.Context;
import android.widget.LinearLayout;

import com.fairtok.R;
import com.google.android.gms.ads.AdRequest;

public class CustomAds
{
    public static com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    public static void googleAdBanner(Context context, LinearLayout adContainer){
        com.google.android.gms.ads.AdView mAdView = new com.google.android.gms.ads.AdView(context);
        mAdView.setAdSize(com.google.android.gms.ads.AdSize.SMART_BANNER);
        mAdView.setAdUnitId(context.getString(R.string.id_b_g));
        adContainer.addView(mAdView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        mAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
            @Override
            public void onAdLoaded() { }
            @Override
            public void onAdClosed() { }
            @Override
            public void onAdFailedToLoad(int errorCode) { }
            @Override
            public void onAdLeftApplication() { }
            @Override
            public void onAdOpened() { super.onAdOpened(); }
        });
        mAdView.loadAd(adRequest);
    }
    public static void googleAdInterstitial(Context context) {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getString(R.string.id_i_g));
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
        mInterstitialAd.setAdListener(new com.google.android.gms.ads.AdListener() {
            public void onAdLoaded() {
                if (mInterstitialAd.isLoaded())
                {
                    mInterstitialAd.show();
                }
            }
        });
    }
    public static void dismissInterstitialGoogle(Context context){
        if(mInterstitialAd!=null)
        {
            mInterstitialAd.setAdListener(null);
        }
    }
}
