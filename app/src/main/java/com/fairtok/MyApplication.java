package com.fairtok;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.facebook.FacebookSdk;
import com.facebook.ads.AudienceNetworkAds;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.model.ServiceData;
import com.fairtok.openduo.agora.Config;
import com.fairtok.openduo.agora.EngineEventListener;
import com.fairtok.openduo.agora.IEventListener;
import com.fairtok.openlive.Constants;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.rtc.AgoraEventHandler;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.openlive.rtc.EventHandler;
import com.fairtok.openlive.stats.StatsManager;
import com.fairtok.openlive.utils.FileUtil;
import com.fairtok.openlive.utils.PrefManager;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
//import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import io.agora.rtc.RtcEngine;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmCallManager;
import io.agora.rtm.RtmClient;
import io.branch.referral.Branch;

import static android.content.ContentValues.TAG;


public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks{

    public static SimpleCache simpleCache = null;
    public static LeastRecentlyUsedCacheEvictor leastRecentlyUsedCacheEvictor = null;
    public static ExoDatabaseProvider exoDatabaseProvider = null;
    public static Long exoPlayerCacheSize = (long) (90 * 1024 * 1024);
    public static Boolean msg = false;
    public static ServiceData data = null;
    public static ServiceData dataTime = null;
    public static SessionManager sessionManager = null;

    private RtcEngine mRtcEngine;
    private EngineConfig mGlobalConfig = new EngineConfig();
    private AgoraEventHandler mHandler = new AgoraEventHandler();
    private StatsManager mStatsManager = new StatsManager();
    private ChatManager mChatManager;
    static MyApplication sInstance;

    int NOTIF_ID=1220;


    private RtmClient mRtmClient;
    private RtmCallManager rtmCallManager;
    private EngineEventListener mEventListener;
    private Config mConfig;
    private com.fairtok.openduo.agora.Global mGlobal;

    String start_Time,end_Time;
    Date startDate;
    Date endDate;


    public static MyApplication the() {
        return sInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Branch.getAutoInstance(this);
        MobileAds.initialize(this);
        registerActivityLifecycleCallbacks(this);
        AudienceNetworkAds.initialize(this);

        Intent tsIntent = new Intent(this, TransferService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String id = UUID.randomUUID().toString();
            String name = "fairtok-bucket";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, tsIntent, 0);

            // Notification manager to listen to a channel
            NotificationChannel channel = new NotificationChannel(id, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            // Valid notification object required
            Notification notification = new Notification.Builder(this, id)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("India's own short video app")
                    .setContentIntent(pendingIntent)
                    .build();

            tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION, notification);
            tsIntent.putExtra(TransferService.INTENT_KEY_NOTIFICATION_ID, 15);
            tsIntent.putExtra(TransferService.INTENT_KEY_REMOVE_NOTIFICATION, true);

            // Foreground service required starting from Android Oreo
            startForegroundService(tsIntent);

        } else {
            startService(tsIntent);
        }

//        if (BuildConfig.DEBUG) {
//            AdSettings.setTestMode(true);
//        }

        FacebookSdk.sdkInitialize(this);
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                    Log.w("notification", "getInstanceId failed", task.getException());
                    return;
                }

                // Get new Instance ID token
                if (task.getResult() != null) {
                    String token = task.getResult();
                    Global.FIREBASE_DEVICE_TOKEN = token;
                    Log.d("notification", token);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Error ", "getInstanceId failed"+ e.getMessage());
            }
        });
       /* FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("notification", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        if (task.getResult() != null) {
                            String token = task.getResult().getToken();
                            Global.FIREBASE_DEVICE_TOKEN = token;
                            Log.d("notification", token);
                        }
                        // Log and toast

                    }
                });*/

        SessionManager sessionManager = new SessionManager(this);
        Global.ACCESS_TOKEN = sessionManager.getUser() != null ? sessionManager.getUser().getData().getToken() : "";
        Global.USER_ID = sessionManager.getUser() != null ? sessionManager.getUser().getData().getUserId() : "";
        if (leastRecentlyUsedCacheEvictor == null) {
            leastRecentlyUsedCacheEvictor = new LeastRecentlyUsedCacheEvictor(exoPlayerCacheSize);
        }

        if (exoDatabaseProvider != null) {
            exoDatabaseProvider = new ExoDatabaseProvider(this);
        }

        if (simpleCache == null) {
            simpleCache = new SimpleCache(getCacheDir(), leastRecentlyUsedCacheEvictor, exoDatabaseProvider);
            if (simpleCache.getCacheSpace() >= 400207768) {
                freeMemory();
            }
            Log.i(TAG, "onCreate: " + simpleCache.getCacheSpace());
        }

        sInstance = this;
        mChatManager = new ChatManager(this);
        mChatManager.init();


        try {
            mRtcEngine = RtcEngine.create(getApplicationContext(), getString(R.string.private_app_id), mHandler);
            // Sets the channel profile of the Agora RtcEngine.
            // The Agora RtcEngine differentiates channel profiles and applies different optimization algorithms accordingly. For example, it prioritizes smoothness and low latency for a video call, and prioritizes video quality for a video broadcast.
            mRtcEngine.setChannelProfile(io.agora.rtc.Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.enableVideo();
            mRtcEngine.setLogFile(FileUtil.initializeLogFile(this));

        } catch (Exception e) {
            e.printStackTrace();
        }

        initConfig();

    }

    public void freeMemory() {

        try {
            File dir = getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }

    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    /**
     * Application.ActivityLifecycleCallbacks methods
     */
    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

        Log.v("paras","paras activity created");

    }

    @Override
    public void onActivityStarted(Activity activity) {

        Log.v("paras","paras activity started");

        if(Utils.isOnLine==false) {

            try {
                PrefHandler pref = new PrefHandler(activity);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                start_Time = sdf.format(new Date());

                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String date  = sdf2.format(new Date());

                startDate = new Date();

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "makemeonline"));
                params.add(new Pair<>("userid", pref.getuId()));
                params.add(new Pair<>("user_name", pref.getName()));
                params.add(new Pair<>("start_time", start_Time));
                params.add(new Pair<>("end_time", start_Time));
                params.add(new Pair<>("duration", "0"));
                params.add(new Pair<>("date", date));

                new AsyncHttpsRequest("", activity, params, activity, 111, false).execute(Utils.MakeMeOnline_URL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {

        Log.v("paras","paras activity resumed");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        try {
            Log.v("paras","paras activity stopped");

            boolean foreground = new ForegroundCheckTask().execute(getApplicationContext()).get();
            if(!foreground) {
                //App is in Background - do what you want
                if(Utils.isOnLine) {
                    try {
                        Utils.isOnLine=false;


                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        end_Time = sdf.format(new Date());

                        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        String date  = sdf2.format(new Date());

                        endDate = new Date();
                        String duration=printDifference(startDate,endDate);


                        PrefHandler pref = new PrefHandler(activity);

                        List<Pair<String, String>> params = new ArrayList<>();
                        params.add(new Pair<>("tag", "makemeoffline"));
                        params.add(new Pair<>("userid", pref.getuId()));

                        params.add(new Pair<>("user_name", pref.getName()));
                        params.add(new Pair<>("start_time", start_Time));
                        params.add(new Pair<>("end_time", end_Time));
                        params.add(new Pair<>("duration", duration));
                        params.add(new Pair<>("date", date));

                        new AsyncHttpsRequest("", activity, params, activity, 222, false).execute(Utils.MakeMeOffline_URL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.v("paras","paras activity paused");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        Log.v("paras","paras activity destroyed");
    }

    //1 minute = 60 seconds
//1 hour = 60 x 60 = 3600
//1 day = 3600 x 24 = 86400
    public String printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        String diff = ""+elapsedHours+" : "+ elapsedMinutes+" : "+ elapsedSeconds;
        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        return diff;
    }

    class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0];
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }



    public ChatManager getChatManager() {
        return mChatManager;
    }

    private void initConfig() {
        SharedPreferences pref = PrefManager.getPreferences(getApplicationContext());
        mGlobalConfig.setVideoDimenIndex(pref.getInt(Constants.PREF_RESOLUTION_IDX, Constants.DEFAULT_PROFILE_IDX));

        boolean showStats = pref.getBoolean(Constants.PREF_ENABLE_STATS, false);
        mGlobalConfig.setIfShowVideoStats(showStats);
        mStatsManager.enableStats(showStats);

        mGlobalConfig.setMirrorLocalIndex(pref.getInt(Constants.PREF_MIRROR_LOCAL, 0));
        mGlobalConfig.setMirrorRemoteIndex(pref.getInt(Constants.PREF_MIRROR_REMOTE, 0));
        mGlobalConfig.setMirrorEncodeIndex(pref.getInt(Constants.PREF_MIRROR_ENCODE, 0));

        mConfig = new Config(getApplicationContext());
        mGlobal = new com.fairtok.openduo.agora.Global();

        initEngine();
    }

    public EngineConfig engineConfig() {
        return mGlobalConfig;
    }

    public RtcEngine rtcEngine() {
        return mRtcEngine;
    }

    public StatsManager statsManager() {
        return mStatsManager;
    }

    public void registerEventHandler(EventHandler handler) {
        mHandler.addHandler(handler);
    }

    public void removeEventHandler(EventHandler handler) {
        mHandler.removeHandler(handler);
    }



    private void initEngine() {
        String appId = getString(R.string.private_app_id);
        if (TextUtils.isEmpty(appId)) {
            throw new RuntimeException("NEED TO use your App ID, get your own ID at https://dashboard.agora.io/");
        }

        mEventListener = new EngineEventListener();
        try {
            mRtcEngine = RtcEngine.create(getApplicationContext(), appId, mEventListener);
            mRtcEngine.setChannelProfile(io.agora.rtc.Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
            mRtcEngine.enableDualStreamMode(true);
            mRtcEngine.enableVideo();
            mRtcEngine.setLogFile(com.fairtok.openduo.utils.FileUtil.rtmLogFile(getApplicationContext()));

            mRtmClient = RtmClient.createInstance(getApplicationContext(), appId, mEventListener);
            mRtmClient.setLogFile(com.fairtok.openduo.utils.FileUtil.rtmLogFile(getApplicationContext()));

            if (Config.DEBUG) {
                mRtcEngine.setParameters("{\"rtc.log_filter\":65535}");
                mRtmClient.setParameters("{\"rtm.log_filter\":65535}");
            }

            rtmCallManager = mRtmClient.getRtmCallManager();
            rtmCallManager.setEventListener(mEventListener);

            String accessToken = getString(R.string.agora_access_token);
            if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "<#YOUR ACCESS TOKEN#>"))
            {
                accessToken = null;
            }
            mRtmClient.login(accessToken, mConfig.getUserId(), new ResultCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.i(TAG, "rtm client login success");
                }

                @Override
                public void onFailure(ErrorInfo errorInfo) {
                    Log.i(TAG, "rtm client login failed:" + errorInfo.getErrorDescription());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public RtmClient rtmClient() {
        return mRtmClient;
    }

    public void registerEventListener(IEventListener listener) {
        mEventListener.registerEventListener(listener);
    }

    public void removeEventListener(IEventListener listener) {
        mEventListener.removeEventListener(listener);
    }

    public RtmCallManager rtmCallManager() {
        return rtmCallManager;
    }

    public Config config() {
        return mConfig;
    }

    public com.fairtok.openduo.agora.Global global() {
        return mGlobal;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        destroyEngine();
    }

    private void destroyEngine() {
        RtcEngine.destroy();

        mRtmClient.logout(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i(TAG, "rtm client logout success");
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i(TAG, "rtm client logout failed:" + errorInfo.getErrorDescription());
            }
        });
    }
}
