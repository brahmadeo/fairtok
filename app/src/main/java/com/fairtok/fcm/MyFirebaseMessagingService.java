package com.fairtok.fcm;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.RingtoneManager;
import android.net.Uri;
import android.opengl.Visibility;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.utils.Const;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.liveCall.LiveCallListActivity;
import com.fairtok.view.video.PlayerActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.i("tokennnn121 ", s);
    }

    private static final String TAG = "MyFMService";
    String sender_id, message, sender_name, sender_image, msg_id, strid, type = "", username="", liveId="",
            userImage="", thumbnail="", videoLink = "";
    private static final int MAX_NOTIFICATIONS = 10;
    private static final int notificationId = 0;
    public static int counter = 0;
    PrefHandler prefHandler;

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "FCM Message Id: " + remoteMessage.getMessageId());
        Log.d(TAG, "FCM Data Message: " + remoteMessage.getData());


        /*if (remoteMessage.getData()!=null){
            sendLiveNotification("my test message");
        }*/


        try {
            Log.i("messagessss ", remoteMessage.getData().toString());
            Log.i("messagessss1 ", remoteMessage.getData().get("message"));
            Log.i("messagessss2 ", remoteMessage.getData().get("type"));

            if (remoteMessage.getNotification() != null){
                Log.i("messagessssNoti ", remoteMessage.getData().toString());
                Log.d(TAG, "FCM Notification Message: " + remoteMessage.getNotification().getBody());
            }


            if (remoteMessage.getData()!=null) {
                JSONObject jsonObject2 = new JSONObject(remoteMessage.getData());
                type = jsonObject2.getString("type");
                username = jsonObject2.getString("userName");
                userImage = jsonObject2.getString("userImage");
                if (remoteMessage.getData().containsKey("userName") && type.equals("1")){

                    // me get liveId and channel name
                    JSONObject jsonObject1 = new JSONObject(remoteMessage.getData().get("data"));

                    thumbnail = jsonObject1.getString("thumbnail");
                } else if (remoteMessage.getData().containsKey("userName") && type.equals("2")){
                    username = remoteMessage.getData().get("senderId");
                }

                if (remoteMessage.getData().containsKey("userName") && type.equals("4")){

                    // me get liveId and channel name
                    JSONObject jsonObject1 = new JSONObject(remoteMessage.getData().get("data"));

                    thumbnail = jsonObject1.getString("images");
                    videoLink = jsonObject1.getString("link");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        prefHandler = new PrefHandler(this);
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

        if (remoteMessage.getData().containsKey("sender_id") &&
                remoteMessage.getData().containsKey("msg") && remoteMessage.getData().containsKey("sender_name")) {

            sender_id = remoteMessage.getData().get("sender_id");
            message = remoteMessage.getData().get("msg");
            sender_name = remoteMessage.getData().get("sender_name");
            msg_id = remoteMessage.getData().get("msg_id");
            strid = remoteMessage.getData().get("strid");


            Log.d("msg_id", "msg_id = " + msg_id);
            Log.d("sender ID", sender_id);
            Log.d("message", message);
            Log.d("sender_name", sender_name);
            Log.d("strid", "strid = " + strid);

            if (remoteMessage.getData().containsKey("sender_img")) {
                sender_image = remoteMessage.getData().get("sender_img");
            }
            // eg. Server Send Structure data:{"'msg_id'=>'12345','msg'=>'hello this is test message','sender_id'=>'12','sender_name'=>'paras','sender_img'=>'http://websitename.com/users/paras_img.png'"}


            if (Utils.isChatScreenOpen && Utils.CURRENT_RECEIVER_ID.equalsIgnoreCase(sender_id)) {
                broadcastIntent(MyFirebaseMessagingService.this, message);
            } else {
                counter++;
                sendChatNotification(message + " - " + sender_name);

            }
        } else if (remoteMessage.getData().containsKey("message")) {
            Log.d(TAG, "onMessageReceived: " + remoteMessage.getData().get("data"));
            String msg = remoteMessage.getData().get("message");

            if (msg != null && msg.equalsIgnoreCase("delete")) {
                new SessionManager(getApplicationContext()).clear();
            } else {
                //sendNotification(msg);
                sendLiveNotification(msg);
            }
            //Toast.makeText(getApplicationContext(), remoteMessage.getData().get("message"), Toast.LENGTH_SHORT).show();
        } else {
            try {
                if (remoteMessage.getNotification() != null) {
                    if (remoteMessage.getNotification().getBody().contains("party")) {
                        String msg = remoteMessage.getNotification().getBody();
                        sendNotification(msg);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void broadcastIntent(Context context, String message) {
        Intent intent = new Intent();
        intent.setAction("com.fairtok.push");
        intent.putExtra("message", message);
        intent.putExtra("sender_id", sender_id);
        intent.putExtra("sender_name", sender_name);
        intent.putExtra("msg_id", msg_id);
        intent.putExtra("strid", strid);
        // We should use LocalBroadcastManager when we want INTRA app communication
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    private void sendNotification(String msg) {
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo_horizontal);

        int requestID = (int) System.currentTimeMillis();

        Intent intent;
        Bundle bundle = new Bundle();
        //Live
        if (type.equals("1")){
            intent = new Intent(this, LiveUsersActivity.class);
            bundle.putString("fcmNotificationType", type);
            bundle.putString("username", username);
            bundle.putString("liveId", liveId);
            //bundle.putString("from", "notification");
        }
        else if (type.equals("2")){
            intent = new Intent(this, LiveCallListActivity.class);
            bundle.putString("fcmNotificationType", type);
            bundle.putString("liveId", username);
        }
        else {
            intent = new Intent(this, MainActivity.class);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        bundle.putBoolean("needToGoToChatScreen", false);
        bundle.putString("msg", msg);
        intent.putExtras(bundle);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT
                | PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "01";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(username)
                .setContentText(msg)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                .setContentInfo("FairTok")
                .setLargeIcon(icon)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setLights(Color.RED, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.logo_horizontal)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg));

//        try {
//            URL url = new URL(data.get("picture_url"));
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream inputStream = connection.getInputStream();
//
//            Bitmap bigPicture = BitmapFactory.decodeStream(inputStream);
//            notificationBuilder.setStyle(
//                    new NotificationCompat.BigPictureStyle().bigPicture(bigPicture)
//            );
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //CharSequence name = getString(R.string.channel_name);

            String name = "Channel_001";
            String description = "Channel Description";
            //String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            //NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }

    private void sendChatNotification(String message) {
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo_horizontal);
        int requestID = (int) System.currentTimeMillis();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Bundle bundle = new Bundle();
        bundle.putBoolean("needToGoToChatScreen", true);
        intent.putExtras(bundle);

        String ticker = "You have " + counter + " new chat message";

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID, intent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "01";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                .setContentInfo(ticker)
                .setLargeIcon(icon)
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setLights(Color.RED, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setSmallIcon(R.drawable.logo_horizontal);


        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //CharSequence name = getString(R.string.channel_name);

            String name = "Channel_001";
            String description = "Channel Description";
            //String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            //NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0, notificationBuilder.build());
        }
    }

    private void sendLiveNotification(String msg) {
        try{
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logo_horizontal);

            int requestID = (int) System.currentTimeMillis();

            Intent intent;
            Bundle bundle = new Bundle();
            //Live
            if (type.equals("1")){
                intent = new Intent(this, LiveUsersActivity.class);
                bundle.putString("fcmNotificationType", type);
                bundle.putString("username", username);
                bundle.putString("liveId", liveId);
                bundle.putString("from", "notification");
            }
            else if (type.equals("2")){
                intent = new Intent(this, LiveCallListActivity.class);
                bundle.putString("fcmNotificationType", type);
                bundle.putString("liveId", username);
            }
            else if (type.equals("4")){
                Log.i("testMessage ", "fcm fired");
                Bundle bundle1 = new Bundle();
                bundle1.putString("from", "notification");
                bundle1.putString("video", videoLink);
               // bundle1.putString("video", "https://bucket-store-tok-videos2.s3.ap-south-1.amazonaws.com/fairtok_1608741221062.mp4");
                intent = new Intent(this, PlayerActivity.class).putExtras(bundle1);
            }else {
                intent = new Intent(this, MainActivity.class);
            }

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            bundle.putBoolean("needToGoToChatScreen", false);
            bundle.putString("msg", msg);
            intent.putExtras(bundle);

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID, intent, PendingIntent.FLAG_UPDATE_CURRENT
                    | PendingIntent.FLAG_ONE_SHOT);
            String CHANNEL_ID = "01";

            @SuppressLint("RemoteViewLayout") RemoteViews lay_one = new RemoteViews(getPackageName(), R.layout.custom_noti_layout);
            @SuppressLint("RemoteViewLayout") RemoteViews lay_two = new RemoteViews(getPackageName(), R.layout.custom_noti_layout_big);

            String url = !userImage.equals(Utils.DEFAULT_PROFILE_URL) && !userImage.equals("")?  Const.BASE_URL_IMAGE+userImage : Const.ITEM_BASE_URL+Utils.DEFAULT_PROFILE_URL;
            String liveImage = thumbnail!=null && !thumbnail.equals("")?  Const.BASE_URL_IMAGE+thumbnail : Const.ITEM_BASE_URL+Utils.DEFAULT_PROFILE_URL;
            Bitmap image=null, liveImg=null, liveImg1=null;
            //Log.i("profileeee ", url+" "+liveImage);
            try {
                URL url1 = new URL(url);
                URL liveUrl = new URL(liveImage);
                image = BitmapFactory.decodeStream(url1.openConnection().getInputStream());
                liveImg = BitmapFactory.decodeStream(liveUrl.openConnection().getInputStream());
                liveImg1 = BitmapFactory.decodeStream(liveUrl.openConnection().getInputStream());

            } catch(IOException e) {
                //Log.i("printingggggg1 ", e.getMessage());
                System.out.println("printingggggg "+e);
            }

            lay_one.setTextViewText(R.id.title, username);
            lay_one.setTextViewText(R.id.description, msg);
            lay_one.setImageViewBitmap(R.id.image, getCircleBitmap(liveImg));
            if (userImage.equals(Utils.DEFAULT_PROFILE_URL)){
                lay_one.setViewVisibility(R.id.imgProfile, View.GONE);
            }else {
                lay_one.setViewVisibility(R.id.imgProfile, View.VISIBLE);
            }
            lay_one.setImageViewBitmap(R.id.imgProfile,  getRoundedCornerBitmap(image, 20));

            lay_two.setTextViewText(R.id.title, username);
            lay_two.setTextViewText(R.id.description, msg);
            lay_two.setImageViewBitmap(R.id.image, image);
            lay_two.setImageViewBitmap(R.id.image_banner, liveImg);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setContent(lay_one)
                    .setCustomContentView(lay_one)
                    //.setCustomBigContentView(lay_two)
                    .setSmallIcon(R.drawable.logo_horizontal)
                    .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                    .setPriority(Notification.PRIORITY_DEFAULT);
        /*.setCustomContentView(lay_one)
                .setCustomBigContentView(lay_two)*/


            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //CharSequence name = getString(R.string.channel_name);

                String name = "Channel_001";
                String description = "Channel Description";
                //String description = getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;

                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                //NotificationManager notificationManager = getSystemService(NotificationManager.class);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(channel);
                }
            }

            //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notificationBuilder.build());
            }
        }catch (Exception e){
            Log.i("notificationError ", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {

        Intent restartServiceTask = new Intent(getApplicationContext(), this.getClass());
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);

        super.onTaskRemoved(rootIntent);
    }
    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap,int roundPixelSize) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = roundPixelSize;
        paint.setAntiAlias(true);
        canvas.drawRoundRect(rectF,roundPx,roundPx, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
}