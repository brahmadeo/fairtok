package com.fairtok.openlive.bottomsheet;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.openlive.model.GiftInfo;
import com.fairtok.openlive.utils.GiftUtil;

import java.util.ArrayList;
import java.util.List;


public class GiftActionSheet extends AbstractActionSheet implements View.OnClickListener {
    public interface GiftActionSheetListener {
        void onActionSheetGiftSend(String name, int id, int value);
    }

    private static final int SPAN_COUNT = 4;
    private static final String ASSET_PREFIX = "gifts";

    private GiftActionSheetListener mListener;
    private RecyclerView mRecycler;
    private String[] mGiftNames;
    private String mValueFormat;
    private int mSelected = -1;
    List<GiftInfo> mGiftInfoList;

    public GiftActionSheet(Context context) {
        super(context);
        initGiftList(context);
        init();
    }

    public GiftActionSheet(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGiftList(context);
        init();
    }

    public GiftActionSheet(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initGiftList(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.action_gift, this, true);

        mGiftNames = getResources().getStringArray(R.array.gift_names);
        mValueFormat = getResources().getString(R.string.live_room_gift_action_sheet_value_format);

        mRecycler = findViewById(R.id.live_room_action_sheet_gift_recycler);
        mRecycler.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT));
        mRecycler.setAdapter(new GiftAdapter());

        ImageView ivClose = findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                removeAllViews();

            }
        });
        AppCompatTextView sendBtn = findViewById(R.id.live_room_action_sheet_gift_send_btn);
        sendBtn.setOnClickListener(this);

        // The first gift is selected by default
        if (mRecycler.getAdapter().getItemCount() > 0) {
            mSelected = 0;
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.live_room_action_sheet_gift_send_btn) {
            if (mListener != null && mSelected != -1)
                mListener.onActionSheetGiftSend(getGiftList().get(mSelected).getGiftName(),
                        mSelected, 0);
        }
    }

    private class GiftAdapter extends RecyclerView.Adapter {
        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new GiftViewHolder(LayoutInflater.from(getContext()).
                    inflate(R.layout.action_gift_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            GiftInfo info = getGiftList().get(position);
            GiftViewHolder giftViewHolder = (GiftViewHolder) holder;
            giftViewHolder.name.setText(info.getGiftName());
            giftViewHolder.value.setText(String.format(mValueFormat, info.getPoint()));
            giftViewHolder.setPosition(position);
            giftViewHolder.itemView.setActivated(mSelected == position);
            giftViewHolder.icon.setImageResource(info.getRes());
        }

        @Override
        public int getItemCount() {
            return GiftUtil.GIFT_ICON_RES.length;
        }
    }

    private class GiftViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView icon;
        AppCompatTextView name;
        AppCompatTextView value;
        int position;

        GiftViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.live_room_action_sheet_gift_item_icon);
            name = itemView.findViewById(R.id.live_room_action_sheet_gift_item_name);
            value = itemView.findViewById(R.id.live_room_action_sheet_gift_item_value);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelected = position;
                    mRecycler.getAdapter().notifyDataSetChanged();
                }
            });
        }

        void setPosition(int position) {
            this.position = position;
        }
    }

    @Override
    public void setActionSheetListener(AbsActionSheetListener listener) {
        if (listener instanceof GiftActionSheetListener) {
            mListener = (GiftActionSheetListener) listener;
        }
    }


    public void initGiftList(Context context) {

        String[] mGiftNames = context.getResources().getStringArray(R.array.gift_names);
        int[] mGiftValues = context.getResources().getIntArray(R.array.gift_values);
        mGiftInfoList = new ArrayList<>();
        for (int i = 0; i < mGiftNames.length; i++) {
            GiftInfo info = new GiftInfo(i, mGiftNames[i], GiftUtil.GIFT_ICON_RES[i], mGiftValues[i]);
            mGiftInfoList.add(i, info);
        }
    }

    public List<GiftInfo> getGiftList() {
        return mGiftInfoList;
    }
}
