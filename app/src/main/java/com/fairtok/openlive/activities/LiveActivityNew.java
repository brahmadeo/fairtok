package com.fairtok.openlive.activities;

import static com.fairtok.utils.BindingAdapters.loadMediaImage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.bumptech.glide.Glide;
import com.camerakit.CameraKit;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.AdapterBeauty;
import com.fairtok.adapter.AdapterCohostReqList;
import com.fairtok.adapter.AdapterMask;
import com.fairtok.adapter.AdapterParticipants1;
import com.fairtok.adapter.AdapterPremiumGift;
import com.fairtok.adapter.AdapterViewers;
import com.fairtok.adapter.FanPagerAdapter;
import com.fairtok.adapter.MyPerformanceArrayAdapter;
import com.fairtok.beauty.Camera.MainActivity;
import com.fairtok.beauty.Camera.textmodule.adapter.RecyclerItemClickListener;
import com.fairtok.beauty.CameraGallery.FilterAdapter;
import com.fairtok.chat.LoadAds;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.bean.ChatListPOJO;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.customview.BottomSheetListView;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.model.FanModel;
import com.fairtok.model.ServiceData;
import com.fairtok.model.ViewerModel;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.openduo.agora.Config;
import com.fairtok.openlive.adapter.ViewerAdapter;
import com.fairtok.openlive.bottomsheet.GiftAnimWindow;
import com.fairtok.openlive.chatting.adapter.MessageAdapter;
import com.fairtok.openlive.chatting.adapter.MessageAdapter1;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.chatting.model.MessageListBean;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.stats.LocalStatsData;
import com.fairtok.openlive.stats.RemoteStatsData;
import com.fairtok.openlive.stats.StatsData;
import com.fairtok.openlive.ui.VideoGridContainer;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.services.ServiceCallback;
import com.fairtok.services.TimeSpendinAppService;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.GlobalApi;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.TrakConstant;
import com.fairtok.view.media.BottomSheetImagePicker;
import com.fairtok.view.preview.PreviewActivity;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.gson.Gson;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannel;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmMessageType;
import io.agora.rtm.RtmStatusCode;
import io.alterac.blurkit.BlurLayout;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LiveActivityNew extends RtcBaseActivity implements View.OnClickListener, CompleteTaskListner, ViewerAdapter.ClickViewerInterface,
        BillingProcessor.IBillingHandler, PurchasesUpdatedListener, ServiceCallback, AdapterBeauty.BeautyListener,
        AdapterParticipants1.ParticipantListener, AdapterCohostReqList.CohostRequestListener, AdapterPremiumGift.PremiumListener {

    ImageView  /*ivMuteAudio, ivBeautification, ivSwitchCamera, ivGift, ivBigImage,ivMuteVideo,*/ ivChat, ivProfile, ivChatHost,
            ivAdd, ivCoin, ivProfile1, ivProfile2, ivProfile3, ivInstagram, imgVideoOff, ivCloseReq, imgLock;
    RelativeLayout relSwitchCamera, rel_filter, relShare, relInsta, relGift, relWatchFollow;

    EditText etMessage;

    TextView tvViewers, tvName, tvGoldCoin, tvTotalDiamonds, tvSilverCoin, tvParticipantsTitle, tvStaticTime, tvCallReqHostName, tvCohostReqCoin;

    public static TextView tvUserCoin, tvStartedWatch;

    LinearLayout llMessage, llBottomAudience, llHostBottom, llTopViewers, linDiamonds, llUserCoin, linParty;

    RelativeLayout rlTop;

    boolean isBroadcaster = false, showInvite = false, mIsPeerToPeerMode = false, isActivityStop = false;

    SessionManager sessionManager;

    private String mUserId = "", mPeerId = "", mChannelName = "", profilePic = "", liveId = "", name = "",
            join_id = "", instaProfileUrl = "", coinPanId = "";

    RecyclerView rvMessageList, rvAddCoin, filter_listView;

    ArrayList<HashMap<String, String>> arrayListAddCoin = new ArrayList<>();

    private VideoGridContainer videoGrid;

    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;

    ArrayList<ChatListPOJO> arrayList;
    private final List<MessageBean> mMessageBeanList = new ArrayList<>();
    private ArrayList<HashMap<String, String>> cohostRequestList = new ArrayList<>();
    List<RtmChannelMember> memberList = new ArrayList<>();

    private int mChannelMemberCount = 0, currentWalletBalance = 0;

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;

    private MessageAdapter1 mMessageAdapter;
    private AdapterCohostReqList mCohostReqAdapter;

    PrefHandler prefHandler;
    String FPaymentId, FTransactionId, FPaymentMode, FTransactionStatus, FAmount, FUserName, FEmail, FPhone, FMessage;
    JSONObject TrakNPayResponse;
    BillingProcessor bp;
    BillingClient billingClient;

    String coins = "0", hostName;
    private int is_filter_open = 0;
    ServiceCallback serviceCallback;

    private final CompositeDisposable disposable = new CompositeDisposable();
    public ObservableBoolean isLoading = new ObservableBoolean(false);

    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public MutableLiveData<RestResponse> purchase = new MutableLiveData<>();
    CoinPurchaseViewModel viewModel;
    private RelativeLayout layout_filter_layer, rlMain;
    private FilterAdapter mAdapter;
    private int left_h;
    private int bottom_hardware_height;
    private int height,width;
    private Long startTime;
    private ConstraintLayout clMask, clParticipants, clInviteParticipants, clCohostRequest;
    private CardView cvParticipants, cvEmptyParticipants;
    private RecyclerView rvParticipants, rvRequestList;

    private AlertDialog alertDialogInvitation;
    private boolean isAudienceMuted = false;
    private AppCompatButton btnRequestCohost, btnContinueCohost;
    private int coinsCohost = 20;
    private String request_id="", requestStatus="";
    private boolean isPurchased = false;
    private String currentLiveCoin="0";
    private String currentLiveTime="00:00:00";
    private String total_spend_time="00:00:00";
    private String cohostingCoins;
    private String premiumCoinsToGift;
    private String premiumCoinSelected;
    private BlurLayout blurLayout;

    private String isPremiumPredecided = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_live);
        prefHandler = new PrefHandler(this);
        sessionManager = new SessionManager(this);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        init();
        initTimer();
        TimeSpendLive();
    }

    private void TimeSpendLive(){
        startTime  = new Date().getTime();
    }

    private void setFilterAdapter(){
        //open_filter();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height =displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        Point p=getRealScreenSize(LiveActivityNew.this);
        Log.e("camsize"," : "+p.x+" : "+p.y);
        bottom_hardware_height=p.y-height;
        height=p.y;
        width=p.x;

        left_h=(height-((width/3)*4)-bottom_hardware_height);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        filter_listView.setLayoutManager(linearLayoutManager);
        mAdapter = new FilterAdapter(this, MainActivity.imageFilter, 0,left_h*46/100);
        filter_listView.setAdapter(this.mAdapter);

        filter_listView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, final int position) {

                onBeautyClicked(position);
            }
        }));
    }
    private void init() {

        filter_listView = findViewById(R.id.filter_listView);
        tvStaticTime = findViewById(R.id.tvStaticTime);
        layout_filter_layer = findViewById(R.id.layout_filter_layer);
        linParty = findViewById(R.id.linParty);
        rvParticipants = findViewById(R.id.rvParticipants);
        tvParticipantsTitle = findViewById(R.id.tvParticipantsTitle);
        cvEmptyParticipants = findViewById(R.id.cvEmptyParticipants);
        cvParticipants = findViewById(R.id.cvParticipants);
        clInviteParticipants = findViewById(R.id.clInviteParticipants);
        ivCloseReq = findViewById(R.id.ivCloseReq);
        clCohostRequest = findViewById(R.id.clCohostRequest);
        imgVideoOff = findViewById(R.id.imgVideoOff);
        btnRequestCohost = findViewById(R.id.btnRequestCohost);
        btnContinueCohost = findViewById(R.id.btnContinueCohost);
        tvCallReqHostName = findViewById(R.id.tvCallReqHostName);
        relWatchFollow = findViewById(R.id.relWatchFollow);
        tvStartedWatch = findViewById(R.id.tvStartedWatch);
        tvCohostReqCoin = findViewById(R.id.tvCohostReqCoin);
        rlMain = findViewById(R.id.rlMain);
        blurLayout = findViewById(R.id.blurLayout);
        imgLock = findViewById(R.id.imgLock);

        clInviteParticipants.setOnClickListener(view -> clInviteParticipants.setVisibility(View.GONE));
        clCohostRequest.setOnClickListener(view -> {
            clCohostRequest.setVisibility(View.GONE);
            btnRequestCohost.setVisibility(View.VISIBLE);
        });
        ivCloseReq.setOnClickListener(view -> {
            clCohostRequest.setVisibility(View.GONE);
            btnRequestCohost.setVisibility(View.VISIBLE);
        });
        btnRequestCohost.setOnClickListener(view -> {
            btnRequestCohost.setVisibility(View.GONE);
            if (sessionManager.getUser().getData().getIs_fan()!=null && sessionManager.getUser().getData().getIs_fan().equals("1")){
                // direct cohost api call
                joinLiveDirectly();
            }else {
                clCohostRequest.setVisibility(View.VISIBLE);
            }

        });

        tvCallReqHostName.setText("Gift "+Utils.hostFullName+" to join");
        currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        btnContinueCohost.setOnClickListener(view -> {
            // send request
            clCohostRequest.setVisibility(View.GONE);
            //RequestCoHost();

            if (cohostingCoins!=null && !cohostingCoins.equals("null")){
                if (currentWalletBalance>=Integer.parseInt(cohostingCoins)){
                    RequestCoHost();
                }else {
                    doAlert();
                }
            }else {
                if (currentWalletBalance>0){
                    RequestCoHost();
                }else {
                    doAlert();
                }
            }
        });

        /*ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("fullname", "Test User");
        hashMap.put("photo", "https://cdn.pixabay.com/photo/2022/01/30/05/59/couple-6979921_960_720.jpg");
        arrayList.add(hashMap);
        rvParticipants.setLayoutManager(new LinearLayoutManager(this));
        rvParticipants.setAdapter(new AdapterParticipants1(arrayList));*/

        //Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
        bp = new BillingProcessor(this, String.valueOf(R.string.play_lic_key), this);
        bp.initialize();

        linParty.setOnClickListener(view -> {
            //showPartylist();
            Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });

        PurchasesUpdatedListener purchasesUpdatedListener = (billingResult, purchases) -> {
            // To be implemented in a later section.

            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                    && purchases != null) {
                ConsumeParams consumeParams =
                        ConsumeParams.newBuilder()
                                .setPurchaseToken(purchases.get(0).getPurchaseToken())
                                .build();

                ConsumeResponseListener listener = (billingResult1, purchaseToken) -> {
                    if (billingResult1.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        // Handle the success of the consume operation.
                    }
                };

                   billingClient.consumeAsync(consumeParams, listener);

                viewModel.purchaseCoin(purchases.get(0).getOrderId(),
                        "Google InApp Purchase", viewModel.coinsAmount,
                        prefHandler.getName(), prefHandler.getuId(),
                        coinPanId, "0");

            }


        };

        billingClient = BillingClient.newBuilder(this)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, (BillingResult b,
                                                                                          List<PurchaseHistoryRecord> list) -> {
                        Log.v("dkjksdq4", String.valueOf(list.size()));

                        for (int i = 0; i < list.size(); i++) {

                            ConsumeParams consumeParams = ConsumeParams.newBuilder()
                                    .setPurchaseToken(list.get(i).getPurchaseToken()).build();
                            ConsumeResponseListener listener = (billingResult1, purchaseToken) -> {
                            };
                            billingClient.consumeAsync(consumeParams, listener);
                        }
                    });

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);
        viewModel.purchase.observe(this, new Observer<RestResponse>() {
            @Override
            public void onChanged(RestResponse purchase) {
                LiveActivityNew.this.showPurchaseResultToast(purchase.getStatus());
            }
        });

        relSwitchCamera = findViewById(R.id.relSwitchCamera);
        rel_filter = findViewById(R.id.rel_filter);
        relShare = findViewById(R.id.relShare);
        relInsta = findViewById(R.id.relInsta);
        relGift = findViewById(R.id.relGift);

        ivChat = findViewById(R.id.ivChat);
        ivProfile = findViewById(R.id.ivProfile);
        ivAdd = findViewById(R.id.ivAdd);
        ivChatHost = findViewById(R.id.ivChatHost);
        ivCoin = findViewById(R.id.ivCoin);
        ivProfile1 = findViewById(R.id.ivProfile1);
        ivProfile2 = findViewById(R.id.ivProfile2);
        ivProfile3 = findViewById(R.id.ivProfile3);
        ivInstagram = findViewById(R.id.ivInstagram);
        rlTop = findViewById(R.id.rlTop);
        llUserCoin = findViewById(R.id.llUserCoin);

        llTopViewers = findViewById(R.id.llTopViewers);
        linDiamonds = findViewById(R.id.linDiamonds);

        Glide.with(this).load(Const.ITEM_BASE_URL + Utils.hostProfilePic).into(ivProfile);

        profilePic = sessionManager.getUser().getData().getUserProfile();
        name = sessionManager.getUser().getData().getFullName();

        llMessage = findViewById(R.id.llMessage);
        clParticipants = findViewById(R.id.clParticipants);
        llBottomAudience = findViewById(R.id.llBottomAudience);
        llHostBottom = findViewById(R.id.llHostBottom);

        tvSilverCoin = findViewById(R.id.tvSilverCoin);
        tvTotalDiamonds = findViewById(R.id.tvTotalDiamonds);
        tvGoldCoin = findViewById(R.id.tvGoldCoin);
        tvViewers = findViewById(R.id.tvViewers);
        tvName = findViewById(R.id.tvName);
        tvUserCoin = findViewById(R.id.tvUserCoin);
        tvName.setText(Utils.hostFullName);
        tvName.setSelected(true);

        setCurrentCoin();

        etMessage = findViewById(R.id.etMessage);

        rvMessageList = findViewById(R.id.rvMessageList);
        rvMessageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvMessageList.setAdapter(mMessageAdapter);

       /* rvParticipants = findViewById(R.id.rvParticipants);
        rvParticipants.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvParticipants.setAdapter(mMessageAdapter);*/

        rvAddCoin = findViewById(R.id.rvAddCoin);
        rvAddCoin.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvAddCoin.setAdapter(new AdapterAddCoin(getResources().getIntArray(R.array.add_coin_values),
                getResources().getIntArray(R.array.add_coin_amount_values)));

        videoGrid = findViewById(R.id.videoGrid);
        videoGrid.setStatsManager(statsManager());

        liveId = getIntent().getStringExtra("liveId");
        isPremiumPredecided = getIntent().getStringExtra("isPremium");
        premiumCoinsToGift = getIntent().getStringExtra("premiumCoins");
        if (getIntent().getExtras()!=null){
            if (getIntent().getExtras().getString("from").equals("video")){
                config().setChannelName(getIntent().getExtras().getString(MessageUtil.INTENT_EXTRA_TARGET_NAME));
            }else if (getIntent().getExtras().getString("from").equals("notification")){
                config().setChannelName(getIntent().getExtras().getString(MessageUtil.INTENT_EXTRA_TARGET_NAME));
            }
        }

        if (getIntent().getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME).matches("@" + sessionManager.getUser().getData().getUserName())) {
            relSwitchCamera.setVisibility(View.VISIBLE);
            rel_filter.setVisibility(View.VISIBLE);
            relShare.setVisibility(View.VISIBLE);
            relGift.setVisibility(View.GONE);
            btnRequestCohost.setVisibility(View.GONE);
            tvStaticTime.setVisibility(View.VISIBLE);

            setFilterAdapter();
            isBroadcaster = true;
            showInvite = true;

            llBottomAudience.setVisibility(View.GONE);
            llHostBottom.setVisibility(View.VISIBLE);
            rvAddCoin.setVisibility(View.GONE);
            becomesHost(false, false);
            llUserCoin.setVisibility(View.GONE);

        }
        //Audience
        else {
            tvParticipantsTitle.setText("You started watching..");
            relSwitchCamera.setVisibility(View.GONE);
            rel_filter.setVisibility(View.GONE);
            relGift.setVisibility(View.GONE);
            btnRequestCohost.setVisibility(View.VISIBLE);
            tvStaticTime.setVisibility(View.GONE);

            isBroadcaster = false;
            showInvite = false;

            //llBottomAudience.setVisibility(View.VISIBLE);
            llHostBottom.setVisibility(View.GONE);
            rvAddCoin.setVisibility(View.VISIBLE);
            llUserCoin.setVisibility(View.VISIBLE);
            //  config().setChannelName(getIntent().getStringExtra("targetName"));
            makeAudienceLiveOnServerNew();
            becomeAudience(true, true);

            Log.v("kkjlksjl1", config().getChannelName());
        }

        hitGetViewersApi("1");
        hitGetHostProfileDataApi();
        hitGetLiveDetailApi();
        hitGetHostListApi();
        initChat();
        mVideoDimension = com.fairtok.openlive.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];

       /* final View activityRootView = findViewById(R.id.rootView);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

              *//*  Rect r = new Rect();

                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);

                Log.d("heightDiff", String.valueOf(heightDiff));

                if (heightDiff > 250) {

                    //enter your code here
                    Log.d("is Opened", "is Opened");

                    llHostBottom.setVisibility(View.GONE);
                    llBottomAudience.setVisibility(View.GONE);

                    llMessage.setVisibility(View.VISIBLE);
                    etMessage.requestFocus();
                    Log.d("check Opened", String.valueOf(llMessage.getVisibility()));

                }
                else
                    {

                    //enter code for hid
                    Log.d("is Closed", "is Closed");

                    llMessage.setVisibility(View.GONE);
                    Log.d("check Opened", String.valueOf(llMessage.getVisibility()));

                    if (isBroadcaster)
                    {
                        llHostBottom.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        llBottomAudience.setVisibility(View.VISIBLE);
                    }

                }
*//*            }
        });*/
    }


    @Override
    protected void onPause() {
        super.onPause();
        stop(); //timer

        Long time = new Date().getTime() - startTime;
        //Log.i("intimeeee111 ", time+"");
        MyApplication.dataTime = new ServiceData(time, Global.ACCESS_TOKEN);
        ApiCallForTimeSpend();
    }
    // timer code start

    int hou=0;
    int min=0;
    int sec=0;
    boolean stopTimer=false;
    private void initTimer()
    {
        Date currentTime = Calendar.getInstance().getTime();
        //hou=currentTime.getHours();
        //min=currentTime.getMinutes();
        sec=currentTime.getSeconds();
        startTimerThread();
    }

    private void startTimerThread()
    {
        new Thread()
        {
            public void run()
            {
                try
                {

                    Thread.sleep(1000);
                }
                catch (InterruptedException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                updateTimer();

            }
        }.start();
    }
    private void updateTimer()
    {
        if(!stopTimer)
        {

            sec=(((++sec)%60)==0)?0:sec;
            min=(sec==0)?(((++min)%60==0)?0:min):min;
            hou=(min==59)?((++hou)):hou;
            /*hou%=12;*/
            //Log.i("UpdateTimer ","hou"+hou+ " min"+min+" sec"+sec);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    String hr = hou<=9?"0"+hou : hou+"";
                    String mins = min<=9?"0"+min : min+"";
                    String secs = sec<=9?"0"+sec : sec+"";
                    currentLiveTime = hr+"h : "+ mins +"m : "+ secs+"s";
                    tvStaticTime.setText(hr+"h : "+ mins +"m : "+ secs+"s");
                }
            });

            startTimerThread();
        }
    }

    public void start()
    {
        initTimer();
    }
    public void stop()
    {
        stopTimer=true;
    }

    // timer code end

    TimeSpendinAppService mService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            TimeSpendinAppService.MyBinder binder = (TimeSpendinAppService.MyBinder) iBinder;
            mService = binder.getService();

            mService.setCallbacks(serviceCallback);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    public static Point getRealScreenSize(Context context)
    {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();

        if (Build.VERSION.SDK_INT >= 17) {
            display.getRealSize(size);
        } else if (Build.VERSION.SDK_INT >= 14) {
            try {
                size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (IllegalAccessException e) {} catch (InvocationTargetException e) {} catch (NoSuchMethodException e) {}
        }

        return size;
    }

    public void open_filter()
    {
        is_filter_open = 1;
        ObjectAnimator animator = ObjectAnimator.ofFloat(layout_filter_layer, "translationY", layout_filter_layer.getHeight(), 0);
        animator.setDuration(200);
        animator.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                //     findViewById(R.id.save).setClickable(false);
                layout_filter_layer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {

            }
        });
        animator.start();
    }

    public void close_filter()
    {
        is_filter_open=0;
        ObjectAnimator animator = ObjectAnimator.ofFloat(layout_filter_layer, "translationY", 0 ,  layout_filter_layer.getHeight());
        animator.setDuration(200L);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                layout_filter_layer.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                layout_filter_layer.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    private void hitGetLiveDetailApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.live_details;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);
            Log.i("payload ", Global.ACCESS_TOKEN+" , "+liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            parseLiveDetail(response);
                            Log.v("postApi-live-detail", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseLiveDetail(JSONObject response) {

        try {
            // {"status":true,"message":"Live details fetched successfully","data":{"live_id":"1487","name":"rakeshsharma31467","isLiveStreaming":"1","start_time":"2021-09-26 22:48:24","end_time":"2022-03-08 09:25:23","CoinsReceived":"0","CoinsReceivedSilver":"36","post_hash_tag":"","total_time":"162 days 10 hours 36 minutes 59 seconds"}}
            if (response.getBoolean("status")) {
                Log.i("coinsGolddd ", response.getJSONObject("data").getString("CoinsReceived")+" , "+response.getJSONObject("data").getString("CoinsReceivedSilver"));
                tvGoldCoin.setText(response.getJSONObject("data").getString("CoinsReceived"));
                tvSilverCoin.setText(response.getJSONObject("data").getString("CoinsReceivedSilver"));
                total_spend_time = response.getJSONObject("data").getString("total_time");
                currentLiveCoin = response.getJSONObject("data").getString("CoinsReceived");

                if (isBroadcaster) {
                    int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                    int updatedCoins = currentCoins + AppUtils.ifEmptyReturn0Int(response.getJSONObject("data").getString("CoinsReceived"));
                    User user = sessionManager.getUser();
                    user.getData().setPaidCoins("" + updatedCoins);
                    sessionManager.saveUser(user);
                } else {
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void hitGetHostProfileDataApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //   //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.user_details;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", isBroadcaster ? Global.USER_ID : Utils.hostUserId)
                    .addUrlEncodeFormBodyParameter("my_user_id", Global.USER_ID)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //         //AppUtils.hideDialog();
                            parseGetHostProfile(response);
                            Log.v("postApi-host-detail", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //          //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGetHostProfile(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                instaProfileUrl = response.getJSONObject("data").getString("insta_url");
                Log.i("hostprofileData ", response.toString());
                if (isBroadcaster) {


                } else {

                    if (response.getJSONObject("data").getString("is_following").equals("0")) {
                        ivAdd.setImageResource(R.drawable.ic_add);
                    } else {
                        ivAdd.setImageResource(R.drawable.ic_star);
                    }

                    tvName.setText(response.getJSONObject("data").getString("full_name"));
                    hostName = response.getJSONObject("data").getString("full_name");
                    cohostingCoins = response.getJSONObject("data").getString("cohostingCoins");
                    if(cohostingCoins!=null){
                        tvCohostReqCoin.setText(cohostingCoins.equals("null")? "0" : cohostingCoins);
                    }

                    Glide.with(LiveActivityNew.this).load(Const.BASE_URL_IMAGE +
                            response.getJSONObject("data").getString("user_profile")).into(ivProfile);
                    tvTotalDiamonds.setText(response.getJSONObject("data").getString("diamondCoins"));
                }

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void joinLiveDirectly() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.joinLiveDirectly;
            Log.i("payload222 ", Global.ACCESS_TOKEN+" , "+liveId +" , "+Global.USER_ID+" , "+ mUserId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // get request id to send in update api
                            //{"status":true,"message":"data saved"}
                            try {
                                if (response.getBoolean("status")){
                                    becomesCoHost(false, false);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            Log.v("direct-cohost", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //Log.i("errorrrApi ", anError.getErrorCode()+"\n"+ anError.getErrorDetail()+"\n"+ anError.getErrorBody());
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void hitCohostReqApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.sendCohostingRequest;
            Log.i("payload111 ", Global.ACCESS_TOKEN+" , "+liveId +" , "+Global.USER_ID+" , "+ mUserId);

            // host-request-response_data
            // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjIwMjItMDItMjYgMTU6NTU6MDUi.fUiYBCpZRAQ1XKUGB9pcNPlrkO2XcKcQ4u6inUpMN-k , 2426 , @brahmadeoprasad1

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("member_id", Global.USER_ID)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // get request id to send in update api
                            //{"status":true,"message":"data saved"}
                            Log.v("host-request-response1", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //Log.i("errorrrApi ", anError.getErrorCode()+"\n"+ anError.getErrorDetail()+"\n"+ anError.getErrorBody());
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void hitCohostUpdateApi(String co_peerId) {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.updateCohostingRequest;
            Log.i("payloadReq ", Global.ACCESS_TOKEN+" , "+liveId+" , "+ request_id);
            // 2463 , 26

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("request_id", request_id)
                    .addUrlEncodeFormBodyParameter("status", requestStatus)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                // {"status":false,"message":"Insufficient coins"}
                                // {"status":true,"message":"data saved"}
                                Log.v("host-request-update", String.valueOf(response));
                                if (response.getBoolean("status")){
                                    // make audience cohost here
                                    if (!co_peerId.equals("")){
                                        InviteAcceptedCoHost(co_peerId);
                                        // get new host list
                                        hitGetHostListApi();
                                        // for coin update
                                        hitGetLiveDetailApi();
                                    }
                                }
                                hitGetCohostReqApi();
                                Toast.makeText(LiveActivityNew.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void hitGetCohostReqApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.cohostingRequest;
            Log.i("payload ", Global.ACCESS_TOKEN+" , "+liveId);

            if (cohostRequestList!=null){
                cohostRequestList.clear();
            }
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            // {"status":true,"message":"data saved","data":[]}
                            // {"status":true,"message":"data saved","data":[{"user_id":"39506","full_name":"Dilip Kumar","user_name":"dilipkumar8873981765","user_email":"dilipkumar8873981765@gmail.com","user_mobile_no":"0","user_profile":"user_default_img.png","gender":"1","language":"ml","login_type":"google","identity":"dilipkumar8873981765@gmail.com","platform":"android","device_token":"eaQTQsj7TnS3qbRHX6DJN_:APA91bFsc8WUGceQ_RQXs4uI5YdgPVNcvY2JcoLfL6pPhzFJUQ-4ZgS3dkKQ3HjTv2N8_NvYe5BsKfMkcqRbrmIh1yLSeAdbxj46InIWiAlgYewqh8fSWxM-RKDnZ1zb5jPrRQomdnYb","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjIwMjItMDMtMDcgMTI6MDQ6NDYi.SyT5rI8K1Qy5QORFJc3xqX6NakSvGWXu8dK4wcTdHJM","is_verify":"0","total_received":"10","total_send":"0","my_wallet":"10","spen_in_app":"0","check_in":"10","upload_video":"0","from_fans":"0","purchased":"0","bio":"","fb_url":"0","insta_url":null,"youtube_url":"0","status":"pending","created_date":"2022-03-07 12:04:46","islive":"0","lastseen":"0","version":"13","version_code":"1.13","fake_user":"0","Creator_Lang":"","isliveStreaming":"0","isGaming":"0","paidCoins":"0","paidRcvdCoins":"0","paidSend":"0","goldCoins":"0","silverCoins":"50","diamondCoins":"0","liveCallAvailable":"1","callRate":"150","referral_code":"test","state":"","country":"","is_fan":"0","fan_start_time":null,"id":"21","party_id":"2458","member_id":"39506","accept_by":null,"created_at":"2022-03-07 12:24:04","updated_at":null,"request_id":"21"}]}
                            Log.v("host-request-response2", String.valueOf(response));
                            try{
                                JSONArray jsonArray = response.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("user_id", jsonObject.getString("user_id"));
                                    hashMap.put("user_name", jsonObject.getString("user_name"));
                                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                                    hashMap.put("request_id", jsonObject.getString("request_id"));
                                    cohostRequestList.add(hashMap);

                                }
                                if (cohostRequestList.size()>0){
                                    rvRequestList.setVisibility(View.VISIBLE);
                                }else {
                                    rvRequestList.setVisibility(View.GONE);
                                }
                                mCohostReqAdapter.updateData(cohostRequestList);

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void hitBuyCohostApi(String purchaseCoin) {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.buyCohosting;
            Log.i("payload ", Global.ACCESS_TOKEN+" , "+purchaseCoin);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("coins", purchaseCoin)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                User user = sessionManager.getUser();
                                if (response.getBoolean("status")){
                                    user.getData().setIs_fan("1");
                                }else {
                                    user.getData().setIs_fan("0");
                                }
                                sessionManager.saveUser(user);
                                Log.v("host-request-response", String.valueOf(response));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void chattingFunction(){
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        etMessage.requestFocus();
        llHostBottom.setVisibility(View.GONE);
        llBottomAudience.setVisibility(View.GONE);

        llMessage.setVisibility(View.VISIBLE);
        //clParticipants.setVisibility(View.VISIBLE);
        //cvParticipants.setVisibility(View.VISIBLE);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    private boolean isRelchat = false, isRelShare = false, isRelFilter = false, isRelInsta = false,
            isRelAdd= false, isRelGift = false;
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivChatHost:
            case R.id.ivChat:

                chattingFunction();
                //llMessage.setVisibility(llMessage.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                break;
            case R.id.rel_filter:
                //layout_filter_layer.setVisibility(View.VISIBLE);
                isRelFilter = !isRelFilter ;
                if (isRelFilter){
                    rel_filter.setBackground(this.getResources().getDrawable(R.drawable.rectangular_bg_gradient));
                }else {
                    rel_filter.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                }
                relShare.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relInsta.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relGift.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relSwitchCamera.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));

                isRelAdd = false;
                isRelShare = false;
                isRelGift = false;
                isRelInsta = false;


                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                if(is_filter_open==0) {
                    open_filter();
                } else {
                    close_filter();
                }
                break;

            case R.id.ivSendMessage:

                sendMessage();

                break;
            case R.id.ivGift:
                showGiftBottomDialog();
                break;

            case R.id.relGift:
                isRelGift = !isRelGift;
                if (isRelGift){
                    relGift.setBackground(this.getResources().getDrawable(R.drawable.rectangular_bg_gradient));
                }
                relShare.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relInsta.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relSwitchCamera.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));

                isRelFilter = false;
                isRelAdd = false;
                isRelShare = false;
                isRelInsta = false;

                showGiftBottomDialog();

                break;
            case R.id.linPremium:
                if (liveType!=null){
                    if (!liveType.equals("premiumStart")){
                        AlertDialogPremiumWithSticker();
                    }
                }else {
                    if (premiumCoinsToGift!=null){
                        // already started
                        Toast.makeText(this, "Already premium started", Toast.LENGTH_SHORT).show();
                    }else {
                        AlertDialogPremiumWithSticker();
                    }
                }

                break;

            case R.id.ivCohost:
                Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
               // clParticipants.setVisibility(View.VISIBLE);
               // clInviteParticipants.setVisibility(View.VISIBLE);
                showParticpantDialog();
                break;

            case R.id.llMessage:

                break;

            case R.id.ivClose:

                onBackPressed();

                break;
            case R.id.linDots:
                AlertDialogSettings();
                break;

/*
            case R.id.rootView:

                AppUtils.hideSoftKeyboard(this);

                break;*/

            case R.id.relShare:
                isRelShare = !isRelShare;
                if (isRelShare){
                    relShare.setBackground(this.getResources().getDrawable(R.drawable.rectangular_bg_gradient));
                }else {
                    relShare.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                }
                rel_filter.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relInsta.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relGift.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relSwitchCamera.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));

                isRelFilter = false;
                isRelAdd = false;
                isRelGift = false;
                isRelInsta = false;

                shareLiveSession1();
                //Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();

                break;

            case R.id.llTopViewers:
               // AlertDialogViewer();
                llTopViewers.setEnabled(false);
                hitGetViewersApi("2");

                break;
            case R.id.btnRequestCohost:
                // open to ask coins
                clCohostRequest.setVisibility(View.VISIBLE);
                btnRequestCohost.setVisibility(View.GONE);

                break;
            case R.id.linDiamonds:
                AlertDialogStatistic("", 0, 0, 0);
                break;

            case R.id.ivAdd:
                //shareLiveSession();
                hitFollowUnfollowApi(Utils.hostUserId, ivAdd);

                break;

            case R.id.relInsta:
                isRelInsta = !isRelInsta;
                if (isRelInsta){
                    relInsta.setBackground(this.getResources().getDrawable(R.drawable.rectangular_bg_gradient));
                }else {
                    relInsta.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                }
                rel_filter.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relShare.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relGift.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));
                relSwitchCamera.setBackground(this.getResources().getDrawable(R.drawable.bg_round_card));

                isRelFilter = false;
                isRelAdd = false;
                isRelShare = false;
                isRelGift = false;

                if (isBroadcaster) {

                    showEnterInstagramProfileDialog();

                } else {
                    openInstaProfile();
                }

                break;
            case R.id.linBattle:
                Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
                //showBattlelist();
                break;

        }

    }
    private Dialog dialogHashTag = null;

    private AlertDialog alertDialogViewer;
    public void AlertDialogViewer() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_viewers, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogViewer = builder.create();
        alertDialogViewer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogViewer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogViewer.setCanceledOnTouchOutside(true);
        alertDialogViewer.setCancelable(true);
        alertDialogViewer.getWindow().setGravity(Gravity.BOTTOM);

        TabLayout tabLayout = layout.findViewById(R.id.tabLayout);
        ViewPager viewPagerViewer = layout.findViewById(R.id.viewPager);

        RecyclerView recyclerviewViewer = layout.findViewById(R.id.recyclerview);
        tabLayout.addTab(tabLayout.newTab().setText("Viewers"));
        tabLayout.addTab(tabLayout.newTab().setText("Statistics"));


        //setViewPager(viewPagerViewer, tabLayout);

        ViewerAdapter adapter = new ViewerAdapter(this);
        recyclerviewViewer.setLayoutManager(new LinearLayoutManager(this,  RecyclerView.VERTICAL, false));
        recyclerviewViewer.setAdapter(adapter);
        alertDialogViewer.show();
    }
    private AlertDialog alertDialogStatic;
    private TextView tvStaticTimer;
    public void AlertDialogStatistic(String type, int hr, int min, int sec) {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_statistic, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogStatic = builder.create();
        alertDialogStatic.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogStatic.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogStatic.setCanceledOnTouchOutside(true);
        alertDialogStatic.setCancelable(true);
        alertDialogStatic.getWindow().setGravity(Gravity.BOTTOM);

        TextView tvActiveViewer = layout.findViewById(R.id.tvActiveViewer);
        TextView tvTotalFollower = layout.findViewById(R.id.tvTotalFollower);
        TextView tvTotalDiamonds = layout.findViewById(R.id.tvTotalDiamonds);
        tvStaticTimer = layout.findViewById(R.id.tvStaticTimer);

        String timeee = Global.getTotalTimeSpent();
        Log.i("timeeeeeeee ", timeee);
        tvStaticTimer.setText(total_spend_time);

        tvActiveViewer.setText(String.valueOf(mChannelMemberCount));
        tvTotalFollower.setText(String.valueOf(sessionManager.getUser().getData().getFollowersCount()));
        tvTotalDiamonds.setText(sessionManager.getUser().getData().getDiamondCoins());

        if (!type.equals("timer")){
            alertDialogStatic.show();
        }
    }
    private AlertDialog alertDialogViewerDetail;
    public void AlertDialogViewerDetails(ArrayList<HashMap<String, String>> viewerData, int position) {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_viewer_details, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogViewerDetail = builder.create();
        alertDialogViewerDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogViewerDetail.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogViewerDetail.setCanceledOnTouchOutside(true);
        alertDialogViewerDetail.setCancelable(true);
        alertDialogViewerDetail.getWindow().setGravity(Gravity.BOTTOM);

        TextView tvName = layout.findViewById(R.id.tvName);
        TextView tvFollowers = layout.findViewById(R.id.tvFollowers);
        TextView tvDiamonds = layout.findViewById(R.id.tvDiamonds);
        TextView tvAgeGender = layout.findViewById(R.id.tvAgeGender);
        TextView tvLikes = layout.findViewById(R.id.tvLikes);

        ImageView ivProfile = layout.findViewById(R.id.ivProfile);

        Glide.with(getApplicationContext()).load(Const.BASE_URL_IMAGE + viewerData.get(position).get("photo")).into(ivProfile);

        tvName.setText(viewerData.get(position).get("fullname"));
        tvFollowers.setText(String.valueOf(viewerData.get(position).get("followers")));
        tvDiamonds.setText(String.valueOf(viewerData.get(position).get("paidCoins")));
        tvAgeGender.setText(viewerData.get(position).get("age")+" , "+(viewerData.get(position).get("gender").equals("1")? "Male" : "Female"));

        ImageView imgDots = layout.findViewById(R.id.imgDots);
        AppCompatButton btnFollow = layout.findViewById(R.id.btnFollow);
        AppCompatButton btnFan = layout.findViewById(R.id.btnFan);

        if (viewerData.get(position).get("ifFollowing") == "0"){
            btnFollow.setVisibility(View.VISIBLE);
            btnFan.setVisibility(View.GONE);
        }else {
            btnFollow.setVisibility(View.GONE);
            btnFan.setVisibility(View.VISIBLE);
        }
        btnFollow.setOnClickListener(view -> {
            //hitFollowUnfollowApi(viewerData.get(position).get("user_id"), null);
            btnFollow.setVisibility(View.GONE);
            btnFan.setVisibility(View.VISIBLE);
        });

        btnFan.setOnClickListener(view -> {
            alertDialogViewerDetail.dismiss();
            showFanBottomDialog(arrayListFan);
        });

        /*if (isBroadcaster){
            layout.findViewById(R.id.btnInvite).setVisibility(View.VISIBLE);
        }else {
            layout.findViewById(R.id.btnInvite).setVisibility(View.GONE);
        }*/
        layout.findViewById(R.id.btnInvite).setOnClickListener(view -> {
            InviteCoHost(viewerData.get(position).get("user_name"));
        });

        imgDots.setOnClickListener(view -> {
            // open settings dialog
            alertDialogViewerDetail.dismiss();
            AlertDialogSettingsuser(viewerData, position);
        });
        alertDialogViewerDetail.show();
    }
    private AlertDialog alertDialogSettingsUser;
    private boolean isMute = true;
    public void AlertDialogSettingsuser(List<HashMap<String, String>> viewerData, int position) {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_settings_user, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogSettingsUser = builder.create();
        alertDialogSettingsUser.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogSettingsUser.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogSettingsUser.setCanceledOnTouchOutside(true);
        alertDialogSettingsUser.setCancelable(true);
        alertDialogSettingsUser.getWindow().setGravity(Gravity.BOTTOM);
        LinearLayout linAdmin = layout.findViewById(R.id.linAdmin);
        LinearLayout linReport = layout.findViewById(R.id.linReport);
        LinearLayout linBlock = layout.findViewById(R.id.linBlock);
        LinearLayout linMuteUnmute = layout.findViewById(R.id.linMuteUnmute);

        ImageView imgMuteUnmute = layout.findViewById(R.id.imgMuteUnmute);
        linAdmin.setOnClickListener(view -> {
            alertDialogSettingsUser.dismiss();
            AlertDialogAdmin(viewerData, position);
        });
        layout.findViewById(R.id.linKickOff).setOnClickListener(view -> {
            kickOffAudience(viewerData.get(position).get("user_name"));
            // kickof api call
            kickOffAudienceOfflineOnServerNew(viewerData.get(position).get("user_id"), "isOut", "0");
        });
        linReport.setOnClickListener(view -> {
            alertDialogSettingsUser.dismiss();
            AlertDialogReport();
        });
        linBlock.setOnClickListener(view -> {
            alertDialogSettingsUser.dismiss();
            AlertDialogBlock();
        });

        linMuteUnmute.setOnClickListener(view -> {
            if(isMute){
                imgMuteUnmute.setImageResource(R.drawable.ic_mic_on);
            }else {
                imgMuteUnmute.setImageResource(R.drawable.ic_mic_off);
                // unmute viewer
            }
            muteUnmuteAudience(viewerData.get(position).get("user_name"));
            // api call
            kickOffAudienceOfflineOnServerNew(viewerData.get(position).get("user_id"), "isMute", isMute? "0": "1");
            isMute = !isMute;

        });
        alertDialogSettingsUser.show();
    }

    private AlertDialog alertDialogSettings;
    private boolean isCameraOff = false;
    public void AlertDialogSettings() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_settings, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogSettings = builder.create();
        alertDialogSettings.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogSettings.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogSettings.setCanceledOnTouchOutside(true);
        alertDialogSettings.setCancelable(true);
        alertDialogSettings.getWindow().setGravity(Gravity.BOTTOM);
        LinearLayout linPremium = layout.findViewById(R.id.linPremium);
        ImageView imgSwitchCamera = layout.findViewById(R.id.imgSwitchCamera);
        ImageView imgOffCamera = layout.findViewById(R.id.imgOffCamera);

        imgOffCamera.setOnClickListener(view -> {
            isCameraOff = !isCameraOff;
            if (isCameraOff){
                imgVideoOff.setVisibility(View.GONE);
                rtcEngine().muteLocalVideoStream(false);
                imgOffCamera.setImageResource(R.drawable.camera_round);
            }else {
                imgVideoOff.setVisibility(View.VISIBLE);
                rtcEngine().muteLocalVideoStream(true);
                imgOffCamera.setImageResource(R.drawable.camera_round_off);
            }

        });

        if(liveType!=null && liveType.equals("premiumStart")){
            linPremium.setVisibility(View.GONE);
        }else if (premiumCoinsToGift!=null){
            linPremium.setVisibility(View.GONE);
        }else {
            linPremium.setVisibility(View.VISIBLE);
        }

        layout.findViewById(R.id.imgMirrorCamera).setOnClickListener(view -> {
            Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });

        imgSwitchCamera.setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            onSwitchCameraClicked(imgSwitchCamera);
        });

        layout.findViewById(R.id.linMask).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            AlertDialogMask();
        });
        layout.findViewById(R.id.linBeauty).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            AlertDialogBeauty();
        });
        layout.findViewById(R.id.linSettings).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            AlertDialogSettingsBattle();
        });
        layout.findViewById(R.id.linSticker).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            showGiftBottomDialog();
        });

        linPremium.setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            // open premuim and gift icon
            AlertDialogPremiumWithSticker();
        });
        layout.findViewById(R.id.linManageAdmin).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            AlertDialogChooseAdmin();
        });
        layout.findViewById(R.id.linChat).setOnClickListener(view -> {
            alertDialogSettings.dismiss();
            chattingFunction();
        });

        alertDialogSettings.show();
    }
    private AlertDialog alertDialogSettingsBattle;
    private int battleTime = 0;
    public void AlertDialogSettingsBattle() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_settings_battle, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogSettingsBattle = builder.create();
        alertDialogSettingsBattle.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogSettingsBattle.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogSettingsBattle.setCanceledOnTouchOutside(true);
        alertDialogSettingsBattle.setCancelable(true);
        alertDialogSettingsBattle.getWindow().setGravity(Gravity.BOTTOM);

        RadioGroup radioGroup = layout.findViewById(R.id.radioGroupBattle);
        ProgressBar progressbar = layout.findViewById(R.id.progressbar);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.radioAnyone:

                        break;
                    case R.id.radioFriend:

                        break;
                    case R.id.radioNoOne:

                        break;
                }
            }
        });
        layout.findViewById(R.id.battleTime1).setOnClickListener(view -> { battleTime = 100/5; progressbar.setProgress(battleTime);});
        layout.findViewById(R.id.battleTime1).setOnClickListener(view -> { battleTime = 100/4; progressbar.setProgress(battleTime);});
        layout.findViewById(R.id.battleTime1).setOnClickListener(view -> { battleTime = 100/3; progressbar.setProgress(battleTime);});
        layout.findViewById(R.id.battleTime1).setOnClickListener(view -> { battleTime = 100/2; progressbar.setProgress(battleTime);});
        layout.findViewById(R.id.battleTime1).setOnClickListener(view -> { battleTime = 100; progressbar.setProgress(battleTime);});

        alertDialogSettingsBattle.show();
    }
    private AlertDialog alertDialogPremiumWithStickr;
    private ImageView imgPremGift;
    private TextView tvPremiumCriteria;
    public void AlertDialogPremiumWithSticker() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_premium_with_sticker, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogPremiumWithStickr = builder.create();
        alertDialogPremiumWithStickr.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogPremiumWithStickr.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogPremiumWithStickr.setCanceledOnTouchOutside(true);
        alertDialogPremiumWithStickr.setCancelable(true);
        alertDialogPremiumWithStickr.getWindow().setGravity(Gravity.BOTTOM);

        ImageView ivClose = layout.findViewById(R.id.ivClose);
        Button btnContinue = layout.findViewById(R.id.btnContinue);
        tvPremiumCriteria = layout.findViewById(R.id.tvPremiumCriteria);
        imgPremGift = layout.findViewById(R.id.imgPremGift);

        btnContinue.setOnClickListener(view -> {
            liveType = "premium";
            if (premiumCoinsToGift!=null){
                //makeHostOfflineOnServerNew();
                hitGoPremiumlLiveApi();
            }else {
                Toast.makeText(LiveActivityNew.this, "Please choose gift.", Toast.LENGTH_SHORT).show();
            }

            //Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });
        ivClose.setOnClickListener(view -> alertDialogPremiumWithStickr.dismiss());

        RecyclerView rvGifts = layout.findViewById(R.id.rvGifts);
        rvGifts.setLayoutManager(new GridLayoutManager(this, 3));
        AdapterPremiumGift adapterGifts = new AdapterPremiumGift(this, getResources().getIntArray(R.array.gift_values));
        rvGifts.setAdapter(adapterGifts);


        alertDialogPremiumWithStickr.show();
    }

    private AlertDialog alertDialogMask;
    public void AlertDialogMask() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_mask, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogMask = builder.create();
        alertDialogMask.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogMask.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogMask.setCanceledOnTouchOutside(true);
        alertDialogMask.setCancelable(true);
        alertDialogMask.getWindow().setGravity(Gravity.BOTTOM);

        ImageView ivClose = layout.findViewById(R.id.ivBack);
        ivClose.setOnClickListener(view -> alertDialogMask.dismiss());

        RecyclerView rvGifts = layout.findViewById(R.id.rvMask);
        rvGifts.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));
        AdapterMask adapterMask = new AdapterMask(this, GiftUtil.MASK_ICON_RES);
        rvGifts.setAdapter(adapterMask);

        alertDialogMask.show();
    }

    private AlertDialog alertDialogBeauty;
    public void AlertDialogBeauty() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_beauty, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogBeauty = builder.create();
        alertDialogBeauty.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogBeauty.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBeauty.setCanceledOnTouchOutside(true);
        alertDialogBeauty.setCancelable(true);
        alertDialogBeauty.getWindow().setGravity(Gravity.BOTTOM);

        ImageView ivClose = layout.findViewById(R.id.ivBack);
        ivClose.setOnClickListener(view -> alertDialogBeauty.dismiss());

        RecyclerView rvGifts = layout.findViewById(R.id.rvMask);
        rvGifts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        AdapterBeauty adapterBeauty = new AdapterBeauty(LiveActivityNew.this, GiftUtil.BEAUTY_ICON_RES);
        rvGifts.setAdapter(adapterBeauty);

        alertDialogBeauty.show();
    }

    private AlertDialog alertDialogAdmin;
    private boolean isAdmin = false;
    public void AlertDialogAdmin(List<HashMap<String, String>> viewerData, int position) {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_viewer_admin, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogAdmin = builder.create();
        alertDialogAdmin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogAdmin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogAdmin.setCanceledOnTouchOutside(true);
        alertDialogAdmin.setCancelable(true);
        alertDialogAdmin.getWindow().setGravity(Gravity.BOTTOM);

        TextView tvName = layout.findViewById(R.id.tvName);
        ImageView ivProfile = layout.findViewById(R.id.ivProfile);
        TextView tvFollowers = layout.findViewById(R.id.tvFollowers);
        TextView tvLikes = layout.findViewById(R.id.tvLikes);
        TextView tvDiamonds = layout.findViewById(R.id.tvDiamonds);
        TextView tvAgeGender = layout.findViewById(R.id.tvAgeGender);

        tvName.setText(viewerData.get(position).get("fullname"));
        tvFollowers.setText(String.valueOf(viewerData.get(position).get("followers")));
        tvDiamonds.setText(String.valueOf(viewerData.get(position).get("diamondCoins")));
        tvAgeGender.setText(viewerData.get(position).get("age")+" , "+(viewerData.get(position).get("gender").equals("1")? "Male" : "Female"));
        Glide.with(getApplicationContext()).load(Const.ITEM_BASE_URL + viewerData.get(position).get("photo")).into(ivProfile);

        AppCompatButton btnFollow = layout.findViewById(R.id.btnFollow);
        AppCompatButton btnFan = layout.findViewById(R.id.btnFan);
        ProgressBar progressbar = layout.findViewById(R.id.progressbar);


        if (viewerData.get(position).get("ifFollowing") == "0"){
            btnFollow.setVisibility(View.VISIBLE);
            btnFan.setVisibility(View.GONE);
        }else {
            btnFollow.setVisibility(View.GONE);
            btnFan.setVisibility(View.VISIBLE);
        }

        btnFollow.setOnClickListener(view -> {
            btnFan.setVisibility(View.VISIBLE);
            btnFollow.setVisibility(View.GONE);
        });
        btnFan.setOnClickListener(view -> {
            alertDialogAdmin.dismiss();
            showFanBottomDialog(arrayListFan);
        });
        LinearLayout linAdmin = layout.findViewById(R.id.linAdmin);
        linAdmin.setOnClickListener(view -> {
            //api call
            progressbar.setVisibility(View.VISIBLE);
            new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    if (isAdmin){
                        linAdmin.setBackgroundResource(R.drawable.bg_btn_gradient);
                    }else {
                        linAdmin.setBackgroundResource(R.drawable.rectangular_transparent_gray_border);
                    }
                }
            }, 2000);
            isAdmin = !isAdmin;
            kickOffAudienceOfflineOnServerNew(viewerData.get(position).get("user_id"), "isAdmin", isAdmin? "0": "1");
            //alertDialogAdmin.dismiss();
        });
        alertDialogAdmin.show();
    }
    private AlertDialog alertDialogChooseAdmin;
    public void AlertDialogChooseAdmin() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_choose_admin, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogChooseAdmin = builder.create();
        alertDialogChooseAdmin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogChooseAdmin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogChooseAdmin.setCanceledOnTouchOutside(true);
        alertDialogChooseAdmin.setCancelable(true);
        //alertDialogAdmin.getWindow().setGravity(Gravity.BOTTOM);
        layout.findViewById(R.id.btnChooseAdmin).setOnClickListener(view -> {
            Toast.makeText(LiveActivityNew.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });
        layout.findViewById(R.id.ivBack).setOnClickListener(view -> alertDialogChooseAdmin.dismiss());
        alertDialogChooseAdmin.show();
    }
    private boolean isSexual = false, isViolent = false, isAbusive = false, isSpam = false, isOther = false;
    public void AlertDialogReport() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_report, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogAdmin = builder.create();
        alertDialogAdmin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogAdmin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogAdmin.setCanceledOnTouchOutside(true);
        alertDialogAdmin.setCancelable(true);
        alertDialogAdmin.getWindow().setGravity(Gravity.BOTTOM);

        RadioButton radioSexual = layout.findViewById(R.id.radioSexual);
        RadioButton radioViolent = layout.findViewById(R.id.radioViolent);
        RadioButton radioAbusive = layout.findViewById(R.id.radioAbusive);
        RadioButton radioSpam = layout.findViewById(R.id.radioSpam);
        RadioButton radioOther = layout.findViewById(R.id.radioOther);

        radioSexual.setOnClickListener(view -> {
            if (isSexual){
                isSexual = !isSexual;
                radioSexual.setChecked(isSexual);
            }else {
                isSexual = !isSexual;
                radioSexual.setChecked(isSexual);
            }
        });
         radioViolent.setOnClickListener(view -> {
            if (isViolent){
                isViolent = !isViolent;
                radioViolent.setChecked(isViolent);
            }else {
                isViolent = !isViolent;
                radioViolent.setChecked(isViolent);
            }
        });
        radioAbusive.setOnClickListener(view -> {
            if (isAbusive){
                isAbusive = !isAbusive;
                radioAbusive.setChecked(isAbusive);
            }else {
                isAbusive = !isAbusive;
                radioAbusive.setChecked(isAbusive);
            }
        });
        radioSpam.setOnClickListener(view -> {
            if (isSpam){
                isSpam = !isSpam;
                radioSpam.setChecked(isSpam);
            }else {
                isSpam = !isSpam;
                radioSpam.setChecked(isSpam);
            }
        });
        radioOther.setOnClickListener(view -> {
            if (isOther){
                isOther = !isOther;
                radioOther.setChecked(isOther);
            }else {
                isOther = !isOther;
                radioOther.setChecked(isOther);
            }
        });

        layout.findViewById(R.id.btnReport).setOnClickListener(view -> {
            //new GlobalApi().reportAudience(isSexual, isViolent, isAbusive, isSpam, isOther);
        });

        alertDialogAdmin.show();
    }

    private AlertDialog alertDialogBattle;
    public void AlertDialogBattle() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_battle, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogBattle = builder.create();
        alertDialogBattle.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogBattle.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBattle.setCanceledOnTouchOutside(true);
        alertDialogBattle.setCancelable(true);
        //alertDialogBattle.getWindow().setGravity(Gravity.BOTTOM);

        alertDialogBattle.show();
    }

    public void AlertDialogBlock() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_block, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogAdmin = builder.create();
        alertDialogAdmin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogAdmin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogAdmin.setCanceledOnTouchOutside(true);
        alertDialogAdmin.setCancelable(true);
        alertDialogAdmin.getWindow().setGravity(Gravity.BOTTOM);

        layout.findViewById(R.id.btnBlock).setOnClickListener(view -> {
            GlobalApi globalApi = new GlobalApi();
            boolean status = globalApi.blockAudience("");
            Toast.makeText(LiveActivityNew.this, status+"", Toast.LENGTH_SHORT).show();
        });
        alertDialogAdmin.show();
    }

    @Override
    public void clickViewer(String type, int position) {
        // open viewerDeatails dialog
        if (alertDialogViewer!=null){
            alertDialogViewer.dismiss();
        }
    }

    @Override
    public void beautyListener(String type, int position) {
        Toast.makeText(LiveActivityNew.this, "yes", Toast.LENGTH_SHORT).show();
    }

    private void shareLiveSession() {

        String uri = Uri.parse(Const.BASE_URL_SHARE)
                .buildUpon()
                .appendPath("liveSession")
                .appendQueryParameter("liveId", liveId)
                .appendQueryParameter("hostId", Global.USER_ID)
                .appendQueryParameter("targetName", getIntent().getStringExtra("targetName").trim())
                .build().toString();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        String shareMessage = "Live session is going on. Please join before it end" + "\n";
        shareMessage = shareMessage + " " + uri;
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        startActivity(Intent.createChooser(shareIntent, "choose one"));
    }
    private void shareLiveSession1() {

        String json = new Gson().toJson(sessionManager.getUser());
        String title = sessionManager.getUser().getData().getFullName();

        Log.i("ShareJson", "Json Object: " + Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile());
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(title)
                .setContentImageUrl(Const.BASE_URL_IMAGE + sessionManager.getUser().getData().getUserProfile())
                .setContentDescription("Hey There, Check This " + getResources().getString(R.string.app_name) + " Live")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("data", "live"));

        LinkProperties lp = new LinkProperties()
                .setFeature("sharing")
                .setCampaign("Content launch")
                .setStage("Live")
                .addControlParameter("custom", "data")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        buo.generateShortUrl(this, lp, (url, error) -> {

            Log.d("PROFILEURL", "shareProfile: " + url);

            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            String shareBody = url + "\nHey, check my live on " + getResources().getString(R.string.app_name) + " App";
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Live Share");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(intent, "Share Live"));
        });

    }

    private void showEnterInstagramProfileDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this, R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_insta_url);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        EditText etInstaProfile = bottomSheetDialog.findViewById(R.id.etInstaProfile);
        if (!instaProfileUrl.equals("0")) etInstaProfile.setText(instaProfileUrl);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();

            if (etInstaProfile.getText().toString().isEmpty())
                AppUtils.showToastSort(LiveActivityNew.this, getString(R.string.pleaseEnterInstaProfile));
            else
                hitUpdateInstagramApi(etInstaProfile.getText().toString().trim());

        });

    }

    private void hitUpdateInstagramApi(String instaProfile) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.update_instagram;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("insta_url", instaProfile)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            try {
                                if (response.getBoolean("status")) instaProfileUrl = instaProfile;

                                Log.v("postApi-resp", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void openInstaProfile() {

        if (instaProfileUrl.equals("0") || instaProfileUrl.isEmpty()) {

            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                    R.style.CustomBottomSheetDialogTheme);
            bottomSheetDialog.setContentView(R.layout.dialog_message);
            bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
            bottomSheetDialog.show();

            TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

            tvMessage.setText(getString(R.string.intstagramProfileNotAvail));

            bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> bottomSheetDialog.dismiss());

        } else {
            Uri uri = Uri.parse("http://instagram.com/_u/" + instaProfileUrl);
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

            likeIng.setPackage("com.instagram.android");

            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/" + instaProfileUrl)));
            }
        }
    }

    private ArrayList<String> arrayListHashTag = new ArrayList<>();
    private String liveType;
    private void hitGoPremiumlLiveApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.normaltopremium;
            Log.v("postApi-url-livepara", Global.ACCESS_TOKEN  +" , "+ liveType +" , "+ premiumCoinSelected+ " , "+ Global.USER_ID +" , "+ Const.BASE_URL_IMAGE+prefHandler.getLiveProfile()+ ", "+
                    arrayListHashTag.toString()
                            .replace("[", "").replace("]", "").replace(" ", ""));

            // alternative
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", Global.USER_ID)
                    .addUrlEncodeFormBodyParameter("premium_coins", premiumCoinSelected)
                    .addUrlEncodeFormBodyParameter("thumbnail", Const.BASE_URL_IMAGE+prefHandler.getLiveProfile())
                    .addUrlEncodeFormBodyParameter("post_hash_tag ", arrayListHashTag.toString()
                            .replace("[", "").replace("]", "").replace(" ", ""))
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            //parseGoLiveJson(response);

                            //makeHostLiveOnServerNew();
                            try {
                                if (response.getBoolean("status")){
                                    liveType = "premiumStart";
                                    Log.v("postApi-premium-live", String.valueOf(response));
                                    // send message to all audience
                                    if (alertDialogPremiumWithStickr!=null){
                                        alertDialogPremiumWithStickr.dismiss();
                                    }
                                    if(premiumCoinsToGift!=null)
                                        imgLock.setVisibility(View.VISIBLE);
                                    else
                                        imgLock.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }

    private void hitGetViewersApi(String type) {

        if (AppUtils.isNetworkAvailable(this)) {

            //if (type.equals("2"))
            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.live_audience;
            Log.v("postApi-url", url);
            Log.v("postApi-data", Global.ACCESS_TOKEN);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseGetViewersJson(response, type);
                            Log.v("postApi-resp-viewer", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private ArrayList<HashMap<String, String>> hostsArrayList;
    private void hitGetHostListApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            if (hostsArrayList!=null){
                hostsArrayList.clear();
            }

            String url = Const.BASE_URL + Const.cohostingList;
            Log.v("postApi-url", url);
            Log.v("postApi-data", Global.ACCESS_TOKEN);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //AppUtils.hideDialog();
                            // {"status":true,"message":"data saved","data":[{"user_id":"2604","full_name":"Anupama Ajayan","user_name":"anupamaajayan","user_email":"anupamaajayan2002@gmail.com","user_mobile_no":"","user_profile":"4968303IMG-20200622-WA00021.jpg","gender":"1","language":"ml","login_type":"google","identity":"anupamaajayan2002@gmail.com","platform":"android","device_token":"f3ysOtEaQeeAyY2ZqknWd4:APA91bFMtoIRuRhGwD-ITMNE5aNrfj_Pmy8Eva-WxayD-pVC-5B1abkFpyWnnr1KMvYFb1iI2AWIn6-px4ROnd6O887dKsDQbe-18wBruljg2GrDNpGNAinUXv7jOqoKPkpDJLe8nLF9","token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjIwMjAtMDgtMDMgMDg6Mzg6NDIi.4GIgJiaWz3SVXXagPOWNtjcysnh5xGI0QJEMBmv_5ac","is_verify":"0","total_received":"0","total_send":"0","my_wallet":"0","spen_in_app":"0","check_in":"0","upload_video":"0","from_fans":"0","purchased":"0","bio":"","fb_url":"","insta_url":"","youtube_url":"","status":"accept","created_date":"2020-08-03 08:38:42","islive":"0","lastseen":"03\/08\/2020 05:00 PM","version":"1.10","version_code":"10","fake_user":"0","Creator_Lang":"","isliveStreaming":"0","isGaming":"0","paidCoins":"0","paidRcvdCoins":"0","paidSend":"0","goldCoins":"10","silverCoins":"9","diamondCoins":"0","liveCallAvailable":"1","callRate":"150","referral_code":null,"state":null,"country":null,"is_fan":"0","fan_start_time":null,"id":"4","party_id":"1487","member_id":"2604","accept_by":"39502","created_at":"2022-03-05 23:53:33","updated_at":"2022-03-06 00:00:52","request_id":"4"}]}
                            try{
                                hostsArrayList = new ArrayList<>();
                                if (response.getBoolean("status")){
                                    JSONArray jsonArray = response.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("user_id", jsonObject.getString("user_id"));
                                        hashMap.put("user_name", jsonObject.getString("user_name"));
                                        hashMap.put("full_name", jsonObject.getString("full_name"));
                                        hashMap.put("user_profile", jsonObject.getString("user_profile"));

                                        hostsArrayList.add(hashMap);
                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            Log.v("postApi-hosts-list", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }

    private ArrayList<HashMap<String, String>> viewerArrayList;
    private void parseGetViewersJson(JSONObject response, String type) {

        try {
            if (response.getBoolean("status")) {

                viewerArrayList = new ArrayList<>();

                try {
                    Log.i("responseee ", response.toString());
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("live_id", jsonObject.getString("live_id"));
                        hashMap.put("user_id", jsonObject.getString("user_id"));
                        hashMap.put("user_name", jsonObject.getString("user_name"));
                        hashMap.put("fullname", jsonObject.getString("fullname"));
                        hashMap.put("photo", jsonObject.getString("photo"));
                        hashMap.put("CoinsSent", jsonObject.getString("CoinsSent"));
                        hashMap.put("ifFollowing", jsonObject.getString("ifFollowing"));
                        hashMap.put("isAdmin", jsonObject.getString("isAdmin"));
                        hashMap.put("isMute", jsonObject.getString("isMute"));
                        hashMap.put("age", jsonObject.getString("age"));
                        hashMap.put("gender", jsonObject.getString("gender"));
                        hashMap.put("diamondCoins", jsonObject.getString("diamondCoins"));
                        hashMap.put("paidCoins", jsonObject.getString("paidCoins"));
                        hashMap.put("followers", jsonObject.getString("followers"));

                        viewerArrayList.add(hashMap);
                    }
                    mChannelMemberCount = viewerArrayList.size();
                    refreshChannelTitle();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (type.equals("1")) {

                    try {
                        JSONArray jsonArray = response.getJSONArray("data");

                        ivProfile1.setImageResource(R.drawable.fake_user_icon);
                        ivProfile2.setImageResource(R.drawable.fake_user_icon);
                        ivProfile3.setImageResource(R.drawable.fake_user_icon);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            if (i == 0) {

                                Glide.with(this).load(Const.BASE_URL_IMAGE + jsonObject.getString("photo"))
                                        .into(ivProfile1);
                            } else if (i == 1) {

                                Glide.with(this).load(Const.BASE_URL_IMAGE + jsonObject.getString("photo"))
                                        .into(ivProfile2);

                            } else if (i == 2) {

                                Glide.with(this).load(Const.BASE_URL_IMAGE + jsonObject.getString("photo"))
                                        .into(ivProfile3);
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else
                    showViewersDialog();

            } /*else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showViewersDialog() {

        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_viewers, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogViewer = builder.create();
        alertDialogViewer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogViewer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogViewer.setCanceledOnTouchOutside(false);
        alertDialogViewer.setCancelable(false);
        alertDialogViewer.getWindow().setGravity(Gravity.BOTTOM);

        TextView tvClose = layout.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(view -> {
            llTopViewers.setEnabled(true);
            alertDialogViewer.dismiss();
        });

        RecyclerView rvViewers = layout.findViewById(R.id.recyclerview);
        rvViewers.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rvViewers.setAdapter(new AdapterViewers(viewerArrayList));

        alertDialogViewer.show();

    }

    private AlertDialog alertDialogParticipant;
    private void showParticpantDialog() {

        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_participant, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogParticipant = builder.create();
        alertDialogParticipant.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogParticipant.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogParticipant.setCanceledOnTouchOutside(true);
        alertDialogParticipant.setCancelable(true);
        alertDialogParticipant.getWindow().setGravity(Gravity.BOTTOM);

        TextView tvParticipantSubtitle = layout.findViewById(R.id.tvParticipantSubtitle);
        if (hostsArrayList!=null){
            if ( hostsArrayList.size()>0){
                tvParticipantSubtitle.setText(R.string.strTopViewers);
            }
        }

        RecyclerView rvViewers = layout.findViewById(R.id.recyclerview);
        rvViewers.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rvViewers.setAdapter(new AdapterParticipants1(this, hostsArrayList, isBroadcaster));

        alertDialogParticipant.show();
    }

    private void hitFollowUnfollowApi(String toUser, ImageView ivFollow) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.followUnfollow;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);
            Log.v("postApi-data", toUser);
            Log.v("postApi-data", Global.ACCESS_TOKEN);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("to_user_id", toUser)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog(); {"status":true,"message":"Follow successful","data":{"type":1}}
                            parseFollowUnfollow(response, ivFollow);
                            sendMessage(); // for start following
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseFollowUnfollow(JSONObject response, ImageView ivFollow) {

        try {
            if (response.getBoolean("status")) {

                if (response.getJSONObject("data").getString("type").equals("1")){
                    if(ivFollow!=null)
                        ivFollow.setImageResource(R.drawable.ic_star);
                } else{
                    hitFanPlanApi();
                    //showFanBottomDialog(arrayListFan);
                    //ivFollow.setImageResource(R.drawable.ic_add);
                }

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void purchaseOfferCoin() {

        coins = "500";

        viewModel.coins = coins;
        viewModel.coinsAmount = "350";

        initializePayment(viewModel.coinsAmount);
    }

    /*Dialog dialogHashTag = null;
    private void showDialogLiveEnd() {
        dialogHashTag = new Dialog(LiveActivityNew.this);
        dialogHashTag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogHashTag.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialogHashTag.setContentView(R.layout.dialog_live_end);
        dialogHashTag.show();

        BlurLayout layout = dialogHashTag.findViewById(R.id.blur_layout);
        layout.startBlur();
    }*/

    private void showSendGiftBottomDialog(String toUserId, String coin, String type, int position) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_send_gift);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvGiftName = bottomSheetDialog.findViewById(R.id.tvGiftName);
        TextView tvCoins = bottomSheetDialog.findViewById(R.id.tvCoins);
        ImageView imgGiftIcon = bottomSheetDialog.findViewById(R.id.imgGiftIcon);

        tvGiftName.setText("Send "+hostName+" a gift");
        tvCoins.setText(coin);
        imgGiftIcon.setImageResource(GiftUtil.getAddCoinAnimRes(position));

        Button btnSend = bottomSheetDialog.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            // send
            currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
            if (currentWalletBalance>= Integer.parseInt(coin)){
                sendBubble(Utils.hostUserId, "" + coin, "2", position );
            }else {
                doAlert();
            }
        });
    }
    private void showGiftBottomDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_gifts);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvCoins = bottomSheetDialog.findViewById(R.id.tvCoins);
        tvCoins.setText(sessionManager.getUser().getData().getPaidCoins());

        TabLayout giftTablayout = bottomSheetDialog.findViewById(R.id.giftTablayout);
        giftTablayout.addTab(giftTablayout.newTab().setText("Classic"));
        giftTablayout.addTab(giftTablayout.newTab().setText("3D"));
        giftTablayout.addTab(giftTablayout.newTab().setText("VIP"));
        giftTablayout.addTab(giftTablayout.newTab().setText("Love"));
        giftTablayout.addTab(giftTablayout.newTab().setText("Moods"));
        giftTablayout.addTab(giftTablayout.newTab().setText("Artist"));
        giftTablayout.addTab(giftTablayout.newTab().setText("Collection"));
        giftTablayout.addTab(giftTablayout.newTab().setText("Family"));

        giftTablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (giftTablayout.getSelectedTabPosition()){
                    case 0:
                        Toast.makeText(LiveActivityNew.this, giftTablayout.getSelectedTabPosition()+"", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(LiveActivityNew.this, giftTablayout.getSelectedTabPosition()+"", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        RecyclerView rvGift = bottomSheetDialog.findViewById(R.id.rvGift);
        rvGift.setLayoutManager(new GridLayoutManager(this, 3));

        bottomSheetDialog.findViewById(R.id.llCoins).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                getCoinPlan();
            }
        });

        rvGift.setAdapter(new AdapterGift(getResources().getIntArray(R.array.gift_values), bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
    }

    private void showPartylist() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_party);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();


        RecyclerView rvParty = bottomSheetDialog.findViewById(R.id.rvParty);

        rvParty.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        rvParty.setAdapter(new AdapterParty(getResources().getIntArray(R.array.gift_values), bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.btnInvite).setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            //AlertDialogJoinParty();
            //AlertDialogInvitation("invite");
        });
        bottomSheetDialog.findViewById(R.id.imgSearchParty).setOnClickListener(view -> {
            bottomSheetDialog.findViewById(R.id.tvHeader).setVisibility(View.GONE);
            bottomSheetDialog.findViewById(R.id.linSearch).setVisibility(View.VISIBLE);
            bottomSheetDialog.findViewById(R.id.imgBack).setVisibility(View.VISIBLE);
        });
        bottomSheetDialog.findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.findViewById(R.id.tvHeader).setVisibility(View.VISIBLE);
                bottomSheetDialog.findViewById(R.id.linSearch).setVisibility(View.GONE);
                bottomSheetDialog.findViewById(R.id.imgBack).setVisibility(View.GONE);
                //bottomSheetDialog.dismiss();
            }
        });
    }
    private void showBattlelist() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_battle);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();


        RecyclerView rvParty = bottomSheetDialog.findViewById(R.id.rvParty);

        rvParty.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        rvParty.setAdapter(new AdapterParty(getResources().getIntArray(R.array.gift_values), bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.btnInvite).setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            AlertDialogBattle();
            //popup notification your request has been sent to successfull person
            // after joinig show both persons images
        });
        bottomSheetDialog.findViewById(R.id.imgSearchParty).setOnClickListener(view -> {
            bottomSheetDialog.findViewById(R.id.tvHeader).setVisibility(View.GONE);
            bottomSheetDialog.findViewById(R.id.linSearch).setVisibility(View.VISIBLE);
            bottomSheetDialog.findViewById(R.id.imgBack).setVisibility(View.VISIBLE);
        });
        bottomSheetDialog.findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.findViewById(R.id.tvHeader).setVisibility(View.VISIBLE);
                bottomSheetDialog.findViewById(R.id.linSearch).setVisibility(View.GONE);
                bottomSheetDialog.findViewById(R.id.imgBack).setVisibility(View.GONE);
                //bottomSheetDialog.dismiss();
            }
        });
    }
    private AlertDialog alertDialogJoinParty;
    public void AlertDialogJoinParty() {
        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_join_party, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogJoinParty = builder.create();
        alertDialogJoinParty.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogJoinParty.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogJoinParty.setCanceledOnTouchOutside(true);
        alertDialogJoinParty.setCancelable(true);
        alertDialogJoinParty.getWindow().setGravity(Gravity.END);
        alertDialogJoinParty.show();
    }

    public void AlertDialogInvitation(String messageType) {
        LayoutInflater m_inflater = LayoutInflater.from(LiveActivityNew.this);

        View layout = m_inflater.inflate(R.layout.custom_dialog_party_invitation, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveActivityNew.this);
        builder.setView(layout);
        alertDialogInvitation = builder.create();
        alertDialogInvitation.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogInvitation.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogInvitation.setCanceledOnTouchOutside(true);
        alertDialogInvitation.setCancelable(true);
        alertDialogInvitation.getWindow().setGravity(Gravity.TOP);

        TextView tvName = layout.findViewById(R.id.tvName);
        ImageView imgAccept = layout.findViewById(R.id.imgAccept);
        ImageView imgReject = layout.findViewById(R.id.imgReject);

        tvName.setText(config().getChannelName());
        imgAccept.setOnClickListener(view -> {
            alertDialogInvitation.dismiss();
            if (messageType.equals("invite")){
                becomesCoHost(false, false);
            }else {
                // send message to audience for become host
                InviteAcceptedCoHost(AcceptedPeerId);
            }

        });
        imgReject.setOnClickListener(view -> {
            alertDialogInvitation.dismiss();
            rejectCoHost();
        });
        alertDialogInvitation.show();
    }

    private ViewPager2 viewPager;
    private int selectedPos=-1;
    private void showFanBottomDialog(ArrayList<HashMap<String, String>> fanPlanList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_fan);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        AppCompatButton btnBecomeFan = bottomSheetDialog.findViewById(R.id.btnBecomeFan);
        ProgressBar progressbar = bottomSheetDialog.findViewById(R.id.progressbar);

        ArrayList<FanModel> fanModelList = new ArrayList<>();

        fanModelList.add(new FanModel("Fan", "499"));
        fanModelList.add(new FanModel("Super Fan", "999"));
        fanModelList.add(new FanModel("Exclusive Fan", "4999"));

        btnBecomeFan.setOnClickListener(view -> {
            progressbar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressbar.setVisibility(View.GONE);
                    bottomSheetDialog.dismiss();
                }
            }, 3000);
            currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
            // 499, 999, 4999
            if(currentWalletBalance>= Integer.parseInt(fanModelList.get(selectedPos).getPrice())){
                // api call for become fan
                hitBuyCohostApi(fanModelList.get(selectedPos).getPrice());
            }else {
                // purchase coinstest
                purchasingCoin(fanPlanList, selectedPos);
            }
        });

        viewPager = bottomSheetDialog.findViewById(R.id.viewPager);
        TabLayout tabLayout = bottomSheetDialog.findViewById(R.id.into_tab_layout);
        FanPagerAdapter adapter = new FanPagerAdapter(this, fanModelList);
        viewPager.setAdapter(adapter);

        Handler slideHandler = new Handler();
        new TabLayoutMediator(tabLayout, viewPager, (tab, position)->{
            //tab.setText("OBJECT " + (position + 1));
        }).attach();

        CompositePageTransformer pageTransformer = new CompositePageTransformer();
        pageTransformer.addTransformer(new MarginPageTransformer(40));
        pageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float r = 1 - Math.abs(position);
                page.setScaleY(0.85f + r*0.15f);
            }
        });
        viewPager.setPageTransformer(pageTransformer);

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                selectedPos = position;
                switch (position){
                    case 0:
                        btnBecomeFan.setText("Become Fan");
                        break;
                    case 1:
                        btnBecomeFan.setText("Become Super Fan");
                        break;
                    case 2:
                        btnBecomeFan.setText("Become Exclusive Fan");
                        break;
                }
                //slideHandler.removeCallbacks(runnable);
                //slideHandler.postDelayed(runnable, 2000);
            }
        });

    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (viewPager.getCurrentItem() == 2){
                viewPager.setCurrentItem(0);
            }else {
                viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
            }
        }
    };

    ArrayList<HashMap<String, String>> arrayListFan = new ArrayList<>();
    private void hitFanPlanApi() {
        if (AppUtils.isNetworkAvailable(this)) {
            //AppUtils.showRequestDialog(this);
            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            if (arrayListFan!=null){
                                arrayListFan.clear();
                            }
                            try {
                                if (response.getBoolean("status")) {

                                    JSONArray jsonArray = response.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                                        hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                                        hashMap.put("coin_amount", jsonObject.getString("coin_amount"));
                                        hashMap.put("playstore_product_id", jsonObject.getString("playstore_product_id"));

                                        arrayListFan.add(hashMap);
                                    }

                                    showFanBottomDialog(arrayListFan);

                                } else
                                    AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void getCoinPlan() {
        hitGetCoinPlanApi();
    }

    private void hitGetCoinPlanApi() {
        if (AppUtils.isNetworkAvailable(this)) {
            //AppUtils.showRequestDialog(this);
            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseCoinPlanJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseCoinPlanJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                    hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                    hashMap.put("coin_amount", jsonObject.getString("coin_amount"));
                    hashMap.put("playstore_product_id", jsonObject.getString("playstore_product_id"));

                    arrayList.add(hashMap);
                }

                showCoinBottomDialog(arrayList);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showCoinBottomDialog(ArrayList<HashMap<String, String>> arrayList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_coins);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet)
                .setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvPrePrice = bottomSheetDialog.findViewById(R.id.tvPrePrice);
        tvPrePrice.setPaintFlags(tvPrePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        RecyclerView rvCoins = bottomSheetDialog.findViewById(R.id.rvCoins);
        rvCoins.setLayoutManager(new GridLayoutManager(this, 3));

        rvCoins.setAdapter(new AdapterCoins(arrayList, bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    public void initializePayment(String PayAmount) {

        Log.d("PayAmount -  ", PayAmount);

        String customerID = sessionManager.getUser().getData().getUserId();
        String orderID = "OrderID" + System.currentTimeMillis() + "-" + customerID;

        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        TrakConstant.PG_ORDER_ID = Integer.toString(n);
        PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey(TrakConstant.PG_API_KEY);
        pgPaymentParams.setAmount(PayAmount);
        pgPaymentParams.setEmail(sessionManager.getUser().getData().getUserEmail());
        pgPaymentParams.setName(sessionManager.getUser().getData().getFullName());
        pgPaymentParams.setPhone(sessionManager.getUser().getData().getUserMobileNo());
        pgPaymentParams.setOrderId(orderID);
        pgPaymentParams.setCurrency(TrakConstant.PG_CURRENCY);
        pgPaymentParams.setDescription(TrakConstant.PG_DESCRIPTION);
        pgPaymentParams.setCity("NA");
        pgPaymentParams.setState(prefHandler.getLanguage());
        pgPaymentParams.setAddressLine1(TrakConstant.PG_ADD_1);
        pgPaymentParams.setAddressLine2(TrakConstant.PG_ADD_2);
        pgPaymentParams.setZipCode("560094");
        pgPaymentParams.setCountry(TrakConstant.PG_COUNTRY);
        pgPaymentParams.setReturnUrl(TrakConstant.PG_RETURN_URL);
        pgPaymentParams.setMode(TrakConstant.PG_MODE);
        pgPaymentParams.setUdf1(TrakConstant.PG_UDF1);
        pgPaymentParams.setUdf2(TrakConstant.PG_UDF2);
        pgPaymentParams.setUdf3(TrakConstant.PG_UDF3);
        pgPaymentParams.setUdf4(TrakConstant.PG_UDF4);
        pgPaymentParams.setUdf5(TrakConstant.PG_UDF5);

        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new
                PaymentGatewayPaymentInitializer(pgPaymentParams, this);
        pgPaymentInitialzer.initiatePaymentProcess();
    }

    private void sendMessage() {

        String msg = etMessage.getText().toString();

        if (!msg.equals("")) {
            RtmMessage message = mRtmClient.createMessage();

            String[] splited = name.split(" ");
            message.setText(splited[0] + " - " + msg);

            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
            message.setRawMessage(profilePic_bytes);

            //TODO
            //MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            MessageBean messageBean = new MessageBean(name, message, true, profilePic);
            mMessageBeanList.add(messageBean);
            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
            sendChannelMessage(message);

        }else {
            String msg1 = getString(R.string.strStartFollow);
            RtmMessage message = mRtmClient.createMessage();

            String[] splited = name.split(" ");
            message.setText(splited[0] + " - " + msg1);

            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
            message.setRawMessage(profilePic_bytes);

            //TODO
            //MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            MessageBean messageBean = new MessageBean(name, message, true, profilePic);
            mMessageBeanList.add(messageBean);

            if (!name.equals(sessionManager.getUser().getData().getFullName())){
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            }
            //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
            sendChannelMessage(message);

        }

        etMessage.setText("");

    }

    private void becomesHost(boolean audioMuted, boolean videoMuted) {

        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    private void becomeAudience(boolean audioMuted, boolean videoMuted) {
        isBroadcaster = false;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        SurfaceView surface = prepareRtcVideo(0, true);
        videoGrid.addUserVideoSurface(0, surface, true);
        /*ivMuteAudio.setActivated(true);*/
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        removeRtcVideo(0, true);
        videoGrid.removeUserVideo(0, true);
        /*ivMuteAudio.setActivated(false);*/
    }

    private void initChat() {
        mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();
        mClientListener = new MyRtmClientListener();
        mChatManager.registerListener(mClientListener);

        Intent intent = getIntent();
        mIsPeerToPeerMode = intent.getBooleanExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        mUserId = intent.getStringExtra(MessageUtil.INTENT_EXTRA_USER_ID);
        String targetName = intent.getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME);

        if (mIsPeerToPeerMode) {
            mPeerId = targetName;

            // load history chat records
            MessageListBean messageListBean = MessageUtil.getExistMessageListBean(mPeerId);
            if (messageListBean != null) {
                mMessageBeanList.addAll(messageListBean.getMessageBeanList());
            }

            // load offline messages since last chat with this peer.
            // Then clear cached offline messages from message pool
            // since they are already consumed.
            MessageListBean offlineMessageBean = new MessageListBean(mPeerId, (List<MessageBean>) mChatManager);
            mMessageBeanList.addAll(offlineMessageBean.getMessageBeanList());
            mChatManager.removeAllOfflineMessages(mPeerId);
        } else {
            mChannelName = targetName;
            mChannelMemberCount = 1;
            createAndJoinChannel();
        }

        rvRequestList = findViewById(R.id.rvRequestList);
        rvRequestList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mCohostReqAdapter = new AdapterCohostReqList(this, cohostRequestList);
        rvRequestList.setAdapter(mCohostReqAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mMessageAdapter = new MessageAdapter1(this, mMessageBeanList, message -> {
            if (message.getMessage().getMessageType() == RtmMessageType.IMAGE) {
                if (!TextUtils.isEmpty(message.getCacheFile())) {
                   /* Glide.with(this).load(message.getCacheFile()).into(ivBigImage);
                    ivBigImage.setVisibility(View.VISIBLE);*/
                } else {
                    ImageUtil.cacheImage(this, mRtmClient, (RtmImageMessage) message.getMessage(), new ResultCallback<String>() {
                        @Override
                        public void onSuccess(String file) {
                            message.setCacheFile(file);
                            runOnUiThread(() -> {
                                /*Glide.with(LiveActivityNew.this).load(file).into(ivBigImage);
                                ivBigImage.setVisibility(View.VISIBLE);*/
                            });
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
        rvMessageList.setAdapter(mMessageAdapter);
       // rvParticipants.setAdapter(mMessageAdapter);

    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
        Log.v("kkjlksjl2", ""+uid);

    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment
        Log.v("kkjlksjl3", ""+uid);
        // showToast("New User Joined");
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.v("kkjlksjl4", uid+"");
                removeRemoteUser(uid);
            }
        });
    }


    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.v("kkjlksjl5", uid+"");
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        videoGrid.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        videoGrid.removeUserVideo(uid, false);
    }

    @Override
    protected void onGlobalLayoutCompleted() {
/*
        RelativeLayout topLayout = findViewById(R.id.rlTop);
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) topLayout.getLayoutParams();
        params.height = mStatusBarHeight + topLayout.getMeasuredHeight();
        topLayout.setLayoutParams(params);
        topLayout.setPadding(0, mStatusBarHeight, 0, 0);
*/
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);

    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        Log.v("lihh", "finish");
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        onBackPressed();
        stop();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(int position) {
        //view.setActivated(!view.isActivated());
        switch (position){
            case 0:

                break;
            case 1:
                rtcEngine().setBeautyEffectOptions(true, com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS);
                break;
            case 2:
                rtcEngine().setBeautyEffectOptions(true, com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS1);
                break;
            case 3:
                rtcEngine().setBeautyEffectOptions(true, com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS2);
                break;
        }

    }

    public void onMoreClicked(View view) {
    }

    public void onPushStreamClicked(View view) {
    }

    public void onMuteAudioClicked(View view) {
        /*  if (!ivMuteVideo.isActivated()) return;*/

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
            //startVideo();
        }
        view.setActivated(!view.isActivated());
        rtcEngine().muteLocalVideoStream(view.isActivated());
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response = " + result);

        if (response_code == 1111) {
            try {
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {
                    Utils.party_id = obj.getString("id");
                } else {
                    Utils.party_id = "0";
                }

                Log.v("paras", "paras Utils.party_id in response = " + Utils.party_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response_code == 222) {

            if (!isActivityStop)
                finish();

        } else if (response_code == 10) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {

                    arrayList = new ArrayList<>();
                    JSONObject data_obj = obj.getJSONObject("post_data");
                    JSONArray jarr = data_obj.getJSONArray("data_arr");

                    for (int i = 0; i < jarr.length(); i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatListPOJO bean = new ChatListPOJO();
                        bean.setOpp_userid(jobj.getString("user_id"));
                        bean.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        bean.setReceiver_photo(jobj.getString("receiver_photo"));
                        bean.setLast_msg(jobj.getString("CoinsSent"));

                        arrayList.add(bean);
                    }
                    Log.v("memberList2", result);
                    mChannelMemberCount = arrayList.size();

                    MyPerformanceArrayAdapter itemsAdapter = new MyPerformanceArrayAdapter(LiveActivityNew.this, arrayList, showInvite);
                    BottomSheetDialog dialog = new BottomSheetDialog(LiveActivityNew.this);
                    dialog.setContentView(R.layout.bottom_sheet_view);
                    BottomSheetListView listView = dialog.findViewById(R.id.listViewBtmSheet);
                    listView.setAdapter(itemsAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
                        }
                    });
                    dialog.show();

                } else {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * API CALL: create and join channel
     */
    private void createAndJoinChannel() {
        // step 1: create a channel instance

        Log.e("channelNameeee ", mChannelName + "");

        mRtmChannel = mRtmClient.createChannel(mChannelName, new MyChannelListener());

        if (mRtmChannel == null) {
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

                Log.i("TAG", "join channel success");
                getChannelMemberList(false);
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG1234", "join channel failed3");
                Log.e("TAG1234", errorInfo.getErrorDescription());
                Log.e("TAG1234", String.valueOf(errorInfo.getErrorCode()));
                Log.e("TAG1234", errorInfo.toString());
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        bp.consumePurchase(productId);
        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);

        Log.v("lkjska1", "qsqsqs");
        Log.v("lkjska1", productId);
        Log.v("lkjska2", details.orderId);
        viewModel.purchaseCoin(details.orderId, "Google InApp Purchase", viewModel.coinsAmount,
                prefHandler.getName(), prefHandler.getuId(),
                coinPanId, FTransactionStatus);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {

        Log.v("lkdhlwq", "ljbdj");
    }

    @Override
    public void ShowResponce(String responce) {
        //Toast.makeText(LiveActivityNew.this, "successfully called", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void participant(String userName, int position) {
        if (alertDialogParticipant!=null){
            alertDialogParticipant.dismiss();
        }
        if (!userName.equals("")){
            //InviteCoHost(userName);
            kickOffCohost("@"+userName);
        }/*else {
            AlertDialogViewerDetails(viewerArrayList, position);
        }*/
    }
    /**
     * API CALLBACK: rtm event listener
     */
    public String AcceptedPeerId;
    @Override
    public void cohostRequestListener(String type, String userName, String req_id, int pos) {
        requestStatus = type;
        request_id = req_id;
        if(type.equals("accept")){
            AcceptedPeerId = "@"+userName;
            // refresh list of request api
            if (cohostRequestList!=null){
                cohostRequestList.clear();
            }
            hitCohostUpdateApi(AcceptedPeerId);
            //InviteAcceptedCoHost("@"+userName);

        }else {
            hitCohostUpdateApi("");
        }
    }

    @Override
    public void listenerPre(int pos) {
        if (imgPremGift!=null){
            imgPremGift.setImageResource(GiftUtil.getGiftAnimRes(pos));
            tvPremiumCriteria.setText(getResources().getIntArray(R.array.gift_values)[pos]+" to enter premium");
            premiumCoinsToGift = String.valueOf(getResources().getIntArray(R.array.gift_values)[pos]);
            premiumCoinSelected = String.valueOf(getResources().getIntArray(R.array.gift_values)[pos]);
        }
    }

    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {
            runOnUiThread(() -> {

                Log.v("paras", "paras on peer's onMessageReceived called");

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                int backg = getMessageColor(peerId);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved1", msg);
                    message.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                    hitGetLiveDetailApi();
                } else if (msg.contains("co-host-invitation")) {
                    Log.i("audienceeeee ", "invitaionnn");
                    //doInvite();
                    AlertDialogInvitation("invite");
                }else if(msg.contains("co-host-request")){
                    // goes to host
                    AcceptedPeerId = peerId;
                    hitGetCohostReqApi();
                    //for(int i=0; i<3; i++){
                        //AlertDialogInvitation("request");
                    //}
                }else if (msg.contains("co-host-accepted")){
                    // audience reciev to be cohost
                    relSwitchCamera.setVisibility(View.VISIBLE);
                    Toast.makeText(LiveActivityNew.this, "accepted", Toast.LENGTH_SHORT).show();
                    becomesCoHost(false, false);
                    // api call to update coins
                } else if(msg.contains("audience-kick-off")){
                    //kickOff();
                    makeAudienceOfflineOnServerNew();
                    Toast.makeText(LiveActivityNew.this, "You are kicked out!", Toast.LENGTH_SHORT).show();
                }else if (msg.contains("host-go-premium")){
                    // alert audience for premium
                    hostStartedPremium();
                } else if(msg.contains("cohost-kick-off")){
                    //kickOff();
                    makeHostOfflineOnServerNew();
                    Toast.makeText(LiveActivityNew.this, "You are kicked out!", Toast.LENGTH_SHORT).show();
                }else if(msg.contains("audience-mute-unmute")){
                    isAudienceMuted = !isAudienceMuted;
                    Log.i("audiencemute ", ""+isAudienceMuted);
                    rtcEngine().muteLocalAudioStream(!isAudienceMuted);
                }

                if (peerId.equals(mPeerId)) {

                    MessageBean messageBean = new MessageBean(peerId, message, false, profilePic);
                    messageBean.setBackground(backg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                   // rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, message);
                }
            });
        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {

            String profilePic = Utils.DEFAULT_PROFILE_URL;

            profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

            int backg = getMessageColor(peerId);
            String msg = rtmImageMessage.getText();
            if (msg.contains("gift$$$$$")) {
                rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                String[] parts = msg.split("-");
                backg = Integer.parseInt(parts[1]);
                animateGiftGif(Integer.parseInt(parts[1]));
                hitGetLiveDetailApi();
            } else if (msg.contains("co-host-invitation")) {
                //doInvite();
               // AlertDialogInvitation("invite");
            }

            String finalProfilePic = profilePic;
            int finalBackg = backg;
            runOnUiThread(() -> {
                if (peerId.equals(mPeerId)) {
                    MessageBean messageBean = new MessageBean(peerId, rtmImageMessage, false, finalProfilePic);
                    messageBean.setBackground(finalBackg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                   // rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, rtmImageMessage);
                }
            });
        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }
    }

    /**
     * API CALLBACK: rtm channel event listener
     */
    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                String account = fromMember.getUserId();

                int backg = getMessageColor(account);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved2", msg);

                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);

                    animateGiftGif(Integer.parseInt(parts[1]));

                    message.setText(parts[2] + " " + getResources().getString(R.string.live_message_gift_send));
                    hitGetLiveDetailApi();
                }

                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + message);
                MessageBean messageBean = new MessageBean(account, message, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

                String account = rtmChannelMember.getUserId();

                int backg = getMessageColor(account);
                String msg = rtmImageMessage.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved3", msg);
                    rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }

                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + rtmImageMessage);
                MessageBean messageBean = new MessageBean(account, rtmImageMessage, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {
            runOnUiThread(() -> {
                Log.v("ljhljkhl2", String.valueOf(member.getUserId())+" , "+ member.getChannelId());
                hitGetViewersApi("1");
                hitGetHostListApi();
                mChannelMemberCount++;
                refreshChannelTitle();
                showStartedWatching(member);
                if (!isBroadcaster)
                    becomeAudience(true, true);

                //showToast(member.getUserId() + " has joined the party");
            });
        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {
            runOnUiThread(() -> {
                mChannelMemberCount--;
                refreshChannelTitle();
                Log.v("ljhljkhl3", String.valueOf(member.getUserId()));
                hitGetViewersApi("1");
                hitGetHostListApi();
                if (mChannelName.equalsIgnoreCase(member.getUserId())) {
                    //openDialog("Host has ended the party");
                    hostEndParty();
                    //showDialogLiveEnd();
                } //else
                //showToast(member.getUserId() + " has left the party");
            });
        }
    }
    private void showStartedWatching(RtmChannelMember member){
        relWatchFollow.setVisibility(View.VISIBLE);
        tvStartedWatch.setText(member.getUserId().substring(1));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                relWatchFollow.setVisibility(View.GONE);
            }
        }, 10000);
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(LiveActivityNew.this, text, Toast.LENGTH_SHORT).show());
    }

    private int getMessageColor(String account) {
        for (int i = 0; i < mMessageBeanList.size(); i++) {
            if (account.equals(mMessageBeanList.get(i).getAccount())) {
                return mMessageBeanList.get(i).getBackground();
            }
        }
        return MessageUtil.COLOR_ARRAY[MessageUtil.RANDOM.nextInt(MessageUtil.COLOR_ARRAY.length)];
    }

    public void animateGiftGif(int giftId) {

        GiftAnimWindow window = new GiftAnimWindow(LiveActivityNew.this, R.style.gift_anim_window);
        window.setAnimResource(GiftUtil.getGiftAnimRes(giftId));
        window.show();
    }

    private void kickOff() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveActivityNew.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(config().getChannelName() + " has warn you to leave the party.");
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                makeAudienceOfflineOnServerNew();
            }
        });
        builder.show();
    }

    private void becomesCoHost(boolean audioMuted, boolean videoMuted) {

//        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    public void InviteCoHost(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-invitation");

        mPeerId = mChannelName.substring(1);
        Log.i("peeeerrrr ", mChannelName+" , "+mPeerId);
        sendPeerMessage(message, "@"+peerId);
    }
    public void InviteAcceptedCoHost(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-accepted");

        mPeerId = mChannelName.substring(1);
        Log.i("peeeerrrr ", mChannelName+" , "+mPeerId);
        sendPeerMessage(message, peerId);
    }
    public void RequestCoHost() {
        hitCohostReqApi();
        //hitGetCohostReqApi();
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-request");

        mPeerId = mChannelName.substring(1);
        Log.i("peeeerrrr ", mChannelName+" , "+mPeerId);
        sendPeerMessage(message, "@"+mPeerId);
    }

    public void rejectCoHost() {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-rejected");

        MessageBean messageBean = new MessageBean(mChannelName.substring(1), message, true, "default.png");
        mMessageBeanList.add(messageBean);
        mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
        rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
        //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);
        mPeerId = mChannelName.substring(1);
        sendPeerMessage(message, mPeerId);
    }
    public void kickOffAudience(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("audience-kick-off");

        mPeerId = mChannelName.substring(1);
        sendPeerMessage(message, "@"+peerId);
    }
    public void hostGoPremium(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("host-go-premium");

        mPeerId = mChannelName.substring(1);
        sendPeerMessage(message, "@"+peerId);
    }
    public void kickOffCohost(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("cohost-kick-off");

        mPeerId = mChannelName.substring(1);
        sendPeerMessage(message, peerId);
    }
    public void muteUnmuteAudience(String peerId) {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("audience-mute-unmute");
        sendPeerMessage(message, "@"+peerId);
    }

    /**
     * API CALL: get channel member list
     */
    private void getChannelMemberList(boolean showMemberList) {
        memberList = new ArrayList<>();
        mRtmChannel.getMembers(new ResultCallback<List<RtmChannelMember>>() {
            @Override
            public void onSuccess(final List<RtmChannelMember> responseInfo) {
                runOnUiThread(() -> {

                    Log.v("ljhljkhl1", String.valueOf(responseInfo.size()));
                   /* Log.v("memberList1", String.valueOf(responseInfo.get(0).getChannelId()));
                    Log.v("memberList1", String.valueOf(responseInfo.get(0).getUserId()));*/
                    memberList = responseInfo;

                    mChannelMemberCount = responseInfo.size();
                    refreshChannelTitle();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "failed to get channel members, err: " + errorInfo.getErrorCode());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void refreshChannelTitle() {
        int count = mChannelMemberCount;
        tvViewers.setText(count>0? count+"" : "0");
    }

    public void openDialog(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LiveActivityNew.this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void hostEndParty() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_alert_host_exit_party, null);
        dialogBuilder.setView(dialogView);
        AppCompatButton btnYes = dialogView.findViewById(R.id.btnYes);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            finish();
        });
    }
    private void hostStartedPremium() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_alert_host_exit_party, null);
        dialogBuilder.setView(dialogView);
        AppCompatButton btnYes = dialogView.findViewById(R.id.btnYes);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            finish();
        });
    }

    /**
     * API CALL: send message to peer
     */
    private void sendPeerMessage(final RtmMessage message, String peerId) {
        mChatManager.enableOfflineMessage(true);
        mRtmClient.sendMessageToPeer(peerId, message, mChatManager.getSendMessageOptions(), new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // do nothing
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.PeerMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_PEER_UNREACHABLE:
                            showToast("User is offline");
                            break;
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_CACHED_BY_SERVER:
                            showToast("cached msg");
                            break;
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // for time spent
        MyApplication.dataTime = new ServiceData(0l, Global.ACCESS_TOKEN);
        startTime = new Date().getTime();
        // for time spent end

        isActivityStop = false;
        if (!isBroadcaster) {
            //makeHostLiveOnServer("1");
            //makingHost online on LiveUSerActivity
        } else {
            //makeHostLiveOnServer("0");
            makeHostLiveOnServerNew();
        }
    }

    private void makeHostLiveOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.join_live;
            Log.v("postApi-url", liveId +" , "+Global.ACCESS_TOKEN);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseJoinLiveJson(response);
                            Log.v("postApi-resp-again", String.valueOf(response));
                            if(!isPremiumPredecided.equals(""))
                                imgLock.setVisibility(View.VISIBLE);
                            else
                                imgLock.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }
    private void parseJoinLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                join_id = response.getJSONObject("data").getString("join_id");
            } else {
                AppUtils.showToastSort(LiveActivityNew.this, response.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void makeAudienceLiveOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.joinLiveUser;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("user_id", Global.USER_ID)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.v("postApi-audience-live", String.valueOf(response));
                            try {
                                if (response.getBoolean("status")){
                                    if (premiumCoinsToGift!=null){
                                        makeAudiencePremiumLive();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }
    private void makeAudiencePremiumLive() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.premiumLiveUsers;
            Log.v("postApi-url-prem", Global.ACCESS_TOKEN +" , "+ liveId + " , "+ Global.USER_ID+ " , "+premiumCoinsToGift);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("user_id", Global.USER_ID)
                    .addUrlEncodeFormBodyParameter("premiumGiftCoins", premiumCoinsToGift)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.v("postApi-resp-premium", String.valueOf(response));

                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }

    public void makeHostLiveOnServer(String isHost) {
        try {
            PrefHandler pref = new PrefHandler(this);
            SessionManager sessionManager = new SessionManager(this);

            Log.v("paras", "paras making the host live user id = " + pref.getuId());

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeonline"));
            params.add(new Pair<>("userid", pref.getuId()));
            params.add(new Pair<>("isGaming", "0"));
            params.add(new Pair<>("username", sessionManager.getUser().getData().getUserName()));

            params.add(new Pair<>("fullname", sessionManager.getUser().getData().getFullName()));
            params.add(new Pair<>("photo", sessionManager.getUser().getData().getUserProfile()));
            params.add(new Pair<>("isHost", isHost));
            params.add(new Pair<>("party_id", Utils.party_id));

            Log.v("kgdkqwjgsd", String.valueOf(params));
//            party_id

            new AsyncHttpsRequest("", this, params, this, 1111, false).execute(Utils.MakeHostOnline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {

        //if (isBroadcaster) {
        leaveAndReleaseChannel();
        mChatManager.unregisterListener(mClientListener);

        //}

        mRtmClient.logout(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void unused) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {

            }
        });

        super.onDestroy();
    }

    //private MyInterface myInterface;
    private void makeHostOfflineOnServerNew() {
        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.STOP_LIVE;
            Log.v("postApi-url", url);
            Log.v("postApi-url", liveId);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getBoolean("status")) {
                                    if (liveType!=null && liveType.equals("premium")){
                                        //hitGoPremiumlLiveApi();
                                    }else {
                                        if (!isActivityStop) {
                                            finish();
                                        }
                                        if (loadAds == -1){
                                            LoadAds.loadInterstitialAds(LiveActivityNew.this, getString(R.string.full_screen_ad));
                                            loadAds = 0;
                                        }
                                    }
                                }
                                Log.v("postApi-resp-offline", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void makePremiumHostOfflineOnServer() {
        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.stopPremiumLive;
            Log.v("postApi-url", url);
            Log.v("postApi-url", liveId);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getBoolean("status")) {
                                    Log.v("premiummmm ", "premium live end");
                                    if (liveType!=null && liveType.equals("premium")){
                                        //hitGolLiveApi();
                                    }else {
                                        if (!isActivityStop) {
                                            finish();
                                        }
                                        if (loadAds == -1){
                                            LoadAds.loadInterstitialAds(LiveActivityNew.this, getString(R.string.full_screen_ad));
                                            loadAds = 0;
                                        }
                                    }

                                }
                                Log.v("postApi-resp-offline", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private AlertDialog alertDialogLivEnd;
    private boolean isEndLivePressed = false;
    private void showLiveEndDialog() {

        LayoutInflater m_inflater = LayoutInflater.from(this);

        View layout = m_inflater.inflate(R.layout.dialog_live_coin_earned_new, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogLivEnd = builder.create();
        alertDialogLivEnd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogLivEnd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogLivEnd.setCanceledOnTouchOutside(true);
        alertDialogLivEnd.setCancelable(true);

        AppCompatButton btnEnd = layout.findViewById(R.id.btnEnd);
        AppCompatButton btnCancel = layout.findViewById(R.id.btnCancel);
        TextView tvDiamonds = layout.findViewById(R.id.tvDiamonds);
        TextView tvTime = layout.findViewById(R.id.tvTime);

        tvDiamonds.setText(currentLiveCoin);
        tvTime.setText(currentLiveTime);

        btnCancel.setOnClickListener(view -> alertDialogLivEnd.dismiss());
        btnEnd.setOnClickListener(view -> {
            isEndLivePressed = true;
            if (premiumCoinsToGift!=null){
                makePremiumHostOfflineOnServer();
            }else {
                makeHostOfflineOnServerNew();
            }

        });
        alertDialogLivEnd.show();
    }

    /**
     * API CALL: leave and release channel
     */
    private void leaveAndReleaseChannel() {
        if (mRtmChannel != null) {
            mRtmChannel.leave(null);
            mRtmChannel.release();
            mRtmChannel = null;
        }
    }
    private void ApiCallForTimeSpend(){
        serviceCallback = this;

        TimeSpendinAppService mService = new TimeSpendinAppService(serviceCallback);

        Intent mServiceIntent = new Intent(this.getApplicationContext(), mService.getClass());
        mServiceIntent.setAction("startLive");
        startService(mServiceIntent);

        Intent intent = new Intent(this, TimeSpendinAppService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //blurLayout.pauseBlur();
        stop();
        Log.v("lihh", "stop");
        isActivityStop = true;
        if (isBroadcaster) {
            // makeHostOfflineOnServer();
            if(!isRelShare){
                if (!isEndLivePressed){
                    makeHostOfflineOnServerNew();
                }
            }
        } else {
            //makeAudienceOfflineOnServer();
            makeAudienceOfflineOnServerNew();
        }
    }

    private int loadAds = -1;
    private void makeAudienceOfflineOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.exit_live;
            Log.v("postApi-url", url+" , "+join_id+ " , "+ liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("join_id", join_id)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();

                            try {
                                if (response.getBoolean("status")) {
                                    if (!isActivityStop) {
                                        finish();
                                    }
                                    if (loadAds == -1){
                                        LoadAds.loadInterstitialAds(LiveActivityNew.this, getString(R.string.full_screen_ad));
                                        loadAds = 0;
                                    }

                                }
                                //parseJoinLiveJson(response);
                                Log.v("postApi-resp-viewers", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private void kickOffAudienceOfflineOnServerNew(String userId, String OutAdminMuteType, String kickValue) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.updateLiveUsers;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("user_id", userId)
                    .addUrlEncodeFormBodyParameter("value", kickValue)
                    .addUrlEncodeFormBodyParameter("type", OutAdminMuteType)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("status")) {
                                    Log.v("kickoff-resp", String.valueOf(response));
                                }
                                //parseJoinLiveJson(response);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.v("kickoff-resp", String.valueOf(response));
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    public void makeHostOfflineOnServer() {
        try {
            PrefHandler pref = new PrefHandler(this);

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", Utils.party_id));

            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeHostOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeAudienceOfflineOnServer() {
        try {
            PrefHandler pref = new PrefHandler(this);

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", pref.getuId())); //
            params.add(new Pair<>("party_id", Utils.party_id));
            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeAudienceOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                final String file = resultUri.getPath();
                ImageUtil.uploadImage(this, mRtmClient, file, new ResultCallback<RtmImageMessage>() {
                    @Override
                    public void onSuccess(final RtmImageMessage rtmImageMessage) {
                        runOnUiThread(() -> {

                            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                            rtmImageMessage.setRawMessage(profilePic_bytes);

                            MessageBean messageBean = new MessageBean(mUserId, rtmImageMessage, true, profilePic);
                            messageBean.setCacheFile(file);
                            mMessageBeanList.add(messageBean);
                            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                            //rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);

                            sendChannelMessage(rtmImageMessage);

                        });
                    }

                    @Override
                    public void onFailure(ErrorInfo errorInfo) {

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);
                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                        showToast("Transaction Failed");
                        //transactionIdView.setText("Transaction ID: NIL");
                        //transactionStatusView.setText("Transaction Status: Transaction Error!");
                    } else {
                        JSONObject response = new JSONObject(paymentResponse);
                        TrakNPayResponse = response;

                        FPaymentId = response.getString("transaction_id");
                        FTransactionId = response.getString("transaction_id");
                        FPaymentMode = response.getString("payment_mode");
                        FTransactionStatus = response.getString("response_code");
                        FAmount = response.getString("amount");
                        FMessage = response.getString("response_message");

                        Log.e("TrakNPay", "onActivityResult: " + response);

                        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);
                        if (response.getString("response_code").equals("0")) {
                            viewModel.purchaseCoin(FTransactionId, FPaymentMode, FAmount, prefHandler.getName(),
                                    FMessage, coinPanId, FTransactionStatus);

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                showToast("Transaction Canceled");
            }
        }
    }

    public void purchasedCoin(String FTransactionId, String FPaymentMode, String FAmount, String user_name, String status) {

        /*disposable.add(Global.initRetrofit().purchaseCoin(Global.ACCESS_TOKEN,
                coins, FTransactionId, FPaymentMode, FAmount, user_name, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((purchase, throwable) -> {

                    if (purchase != null) {
                        this.purchase.setValue(purchase);

                    }

                }));*/
    }

    /**
     * API CALL: send message to a channel
     */
    private void sendChannelMessage(RtmMessage message) {
        mRtmChannel.sendMessage(message, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.ChannelMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                    }
                });
            }
        });
    }

    private class AdapterParty extends RecyclerView.Adapter<AdapterParty.MyViewHolder> {

        int[] data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterParty(int[] stringArray, BottomSheetDialog dialog) {

            data = stringArray;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_live_party, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
            holder.rlMain.setOnClickListener(view -> {
                holder.checkLiveUser.setChecked(true);
            });
        }

        @Override
        public int getItemCount() {

            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView ivImage, ivCoin;
            TextView tvCoins;
            RelativeLayout rlMain;
            CheckBox checkLiveUser;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                ivCoin = itemView.findViewById(R.id.ivCoin);
                tvCoins = itemView.findViewById(R.id.tvCoins);
                rlMain = itemView.findViewById(R.id.rlMain);
                checkLiveUser = itemView.findViewById(R.id.checkLiveUser);
            }
        }
    }

    private class AdapterGift extends RecyclerView.Adapter<AdapterGift.MyViewHolder> {

        int[] data;
        int selected = -1;
        BottomSheetDialog bottomSheetDialog;

        public AdapterGift(int[] stringArray, BottomSheetDialog dialog) {

            data = stringArray;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_gifts, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            if (position == 0) holder.ivCoin.setImageResource(R.drawable.coin_silver);

            holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            holder.tvCoins.setText(String.valueOf(data[position]));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    selected = position;
                    int coinsToSend = data[position];

                    currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

                    Log.v("paras", "paras sending gift gift index = " + position);

                    if (currentWalletBalance >= coinsToSend) {
                        bottomSheetDialog.dismiss();
                        //type=2 GoldCoin, 1=silver coin
                        sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position);
                        //  updateGiftCoinsAccount("" + coinsToSend);
                    } else {
                        bottomSheetDialog.dismiss();
                        doAlert();
                    }
                    /*if (position == 0) {
                        bottomSheetDialog.dismiss();
                        //type=2 GoldCoin, 1=silver coin
                        sendBubble(Utils.hostUserId, "" + coinsToSend, "1", position);
                        //  updateGiftCoinsAccount("" + coinsToSend);
                    } else {
                        if (currentWalletBalance >= coinsToSend) {
                            bottomSheetDialog.dismiss();
                            //type=2 GoldCoin, 1=silver coin
                            sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position);
                            //  updateGiftCoinsAccount("" + coinsToSend);
                        } else {
                            bottomSheetDialog.dismiss();
                            doAlert();
                        }
                    }*/
                    notifyDataSetChanged();
                }
            });

            if (selected == position){
                holder.llMain.setBackgroundResource(R.drawable.rectangular_transparent_less_radius);
            }
        }

        @Override
        public int getItemCount() {

            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage, ivCoin;

            TextView tvCoins;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                ivCoin = itemView.findViewById(R.id.ivCoin);

                tvCoins = itemView.findViewById(R.id.tvCoins);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }


    private class AdapterCoins extends RecyclerView.Adapter<AdapterCoins.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterCoins(ArrayList<HashMap<String, String>> arrayList, BottomSheetDialog dialog) {

            data = arrayList;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coins, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            //holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            // holder.ivImage.setImageDrawable(R.drawable.ic_coin);
            if (Integer.parseInt(data.get(position).get("coin_plan_price"))>=5000){
                holder.ivImage.setImageDrawable(getResources().getDrawable(R.drawable.gold_coins));
            }
            holder.tvCoins.setText(data.get(position).get("coin_amount"));
            holder.tvAmount.setText("" + data.get(position).get("coin_plan_price"));

            holder.llMain.setTag(R.string.amount, data.get(position).get("coin_plan_price"));
            holder.llMain.setTag(R.string.coins, data.get(position).get("coin_amount"));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                    coinPanId = data.get(position).get("coin_plan_id");

                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModel.coins = coins;
                    viewModel.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

/*
                    bp.consumePurchase(data.get(position).get("playstore_product_id"));
                    bp.purchase(LiveActivityNew.this,data.get(position).get("playstore_product_id"));
*/
                    // bp.consumePurchase(data.get(position).get("playstore_product_id"));

                    purchasingCoin(data, position);

                    //initializePayment((String) holder.llMain.getTag(R.string.amount));


                }
            });

        }


        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            TextView tvCoins, tvAmount;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);

                tvCoins = itemView.findViewById(R.id.tvCoins);
                tvAmount = itemView.findViewById(R.id.tvAmount);

                llMain = itemView.findViewById(R.id.llMain);
            }
        }
    }

    private void purchasingCoin(ArrayList<HashMap<String, String>> data, int position){
        List<String> skuList = new ArrayList<>();

        skuList.add(data.get(position).get("playstore_product_id")); // product id is which you added on play store console

        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);


        billingClient.querySkuDetailsAsync(params.build(),
                new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(BillingResult billingResult,
                                                     List<SkuDetails> skuDetailsList) {
                        // Process the result.
                        Log.v("dkjksdq1", "sqas");
                        for (SkuDetails skuDetails : skuDetailsList) {
                            Log.v("skuDetailsAsyncSize", "kjxhwxx");
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();
                            Log.v("dkjksdq2", "sqas");
                            BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                    .setSkuDetails(skuDetails)
                                    .build();

                            int responseCode = billingClient.launchBillingFlow(LiveActivityNew.this,
                                    billingFlowParams).getResponseCode();

                            Log.v("dkjksdq3", String.valueOf(responseCode));
                        }
                    }

                });
    }

    private class AdapterAddCoin extends RecyclerView.Adapter<AdapterAddCoin.MyViewHolder> {

        int[] data;
        int[] amountData;

        public AdapterAddCoin(int[] intArray, int[] array) {

            data = intArray;
            amountData = array;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_add_coin, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            /*if (position == 0) {

                holder.tvCoins1.setText(String.valueOf(data[position]));
                holder.tvCoinsPrice.setText("Rs. " + amountData[position]);
                holder.ll1.setVisibility(View.VISIBLE);
                holder.ll2.setVisibility(View.GONE);

            } else {*/
                holder.ivImage.setImageResource(GiftUtil.getAddCoinAnimRes(position ));
                holder.tvCoins2.setText(String.valueOf(amountData[position]));
                holder.ll1.setVisibility(View.GONE);
                holder.ll2.setVisibility(View.VISIBLE);

            /*}*/

            holder.llMain.setTag(R.string.amount, String.valueOf(amountData[position]));
            holder.llMain.setTag(R.string.coins, String.valueOf(data[position]));

            holder.llMain.setOnClickListener(view -> {

/*
                if (position == 0) {
                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModel.coins = coins;
                    viewModel.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

                    initializePayment((String) holder.llMain.getTag(R.string.amount));
                } else {*/

                    currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

                    Log.v("paras", "paras sending gift gift index = " + position);

                    int coinsToSend = data[position];

                    if (currentWalletBalance >= coinsToSend) {

                        //type=2 GoldCoin, 1=silver coin
                        //sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position );

                        // open dialog for send
                        showSendGiftBottomDialog(Utils.hostUserId, "" + coinsToSend, "2", position );

                        //  updateGiftCoinsAccount("" + coinsToSend);
                    } else {
                        //doAlert();
                        showSendGiftBottomDialog(Utils.hostUserId, "" + coinsToSend, "2", position );
                    }

                /*}*/


            });
        }

        @Override
        public int getItemCount() {
            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            LinearLayout llMain, ll1, ll2;

            TextView tvCoins1, tvCoins2, tvCoinsPrice;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                tvCoins1 = itemView.findViewById(R.id.tvCoins1);
                tvCoins2 = itemView.findViewById(R.id.tvCoins2);
                tvCoinsPrice = itemView.findViewById(R.id.tvCoinsPrice);

                llMain = itemView.findViewById(R.id.llMain);

                ll1 = itemView.findViewById(R.id.ll1);
                ll2 = itemView.findViewById(R.id.ll2);

            }
        }
    }

    private class AdapterViewers extends RecyclerView.Adapter<AdapterViewers.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        public AdapterViewers(ArrayList<HashMap<String, String>> arrayList) {

            data = arrayList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_viewers, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            switch (position) {

                case 0:
                    holder.tvPosition.setText("1");
                    break;
                case 1:
                    holder.tvPosition.setText("2");
                    break;
                case 2:
                    holder.tvPosition.setText("3");
                    break;

                default:
                    holder.tvPosition.setVisibility(View.GONE);
                    break;

            }

            holder.tvName.setText(data.get(position).get("fullname"));

            if (data.get(position).get("CoinsSent").equals("0"))
                holder.llCoin.setVisibility(View.GONE);
            else {
                holder.llCoin.setVisibility(View.VISIBLE);
                holder.tvCoinSent.setText(data.get(position).get("CoinsSent"));
            }

            Log.v("jlhjlw1", sessionManager.getUser().getData().getUserId());
            Log.v("jlhjlw2", data.get(position).get("user_id"));

            if (isBroadcaster){
                holder.ivFollow.setVisibility(View.GONE);
            }

            if (data.get(position).get("ifFollowing").equals("0")) {
                holder.ivFollow.setVisibility(View.VISIBLE);
            } else {
                holder.ivFollow.setVisibility(View.GONE);
            }

            if (sessionManager.getUser().getData().getUserId().equals(data.get(position).get("user_id")))
                holder.ivFollow.setVisibility(View.GONE);

            Log.i("profile_pic", Const.ITEM_BASE_URL+ data.get(position).get("photo"));
            Glide.with(getApplicationContext()).load(Const.BASE_URL_IMAGE + data.get(position).get("photo")).into(holder.ivProfile);

            holder.ivFollow.setOnClickListener(v -> hitFollowUnfollowApi(data.get(position).get("user_id"),
                    holder.ivFollow
            ));

            holder.linMain.setOnClickListener(view -> {
                if (!data.get(position).get("user_id").equals(Global.USER_ID))
                    AlertDialogViewerDetails(data, position);
            });
        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvPosition, tvName, tvCoinSent;

            ImageView ivProfile, ivFollow;

            LinearLayout llCoin, linMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                tvPosition = itemView.findViewById(R.id.tvPosition);
                tvName = itemView.findViewById(R.id.tvName);
                tvCoinSent = itemView.findViewById(R.id.tvCoinSent);

                ivProfile = itemView.findViewById(R.id.ivProfile);
                ivFollow = itemView.findViewById(R.id.ivFollow);

                llCoin = itemView.findViewById(R.id.llCoin);
                linMain = itemView.findViewById(R.id.linMain);

            }
        }
    }


    public void sendBubble(String toUserId, String coin, String type, int position) {

        Log.v("ljhhhlk", coin);
        Log.v("ljhhhlk", liveId);
        Log.v("ljhhhlk", Global.ACCESS_TOKEN);

        // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjIwMjItMDItMjYgMTU6NTU6MDUi.fUiYBCpZRAQ1XKUGB9pcNPlrkO2XcKcQ4u6inUpMN-k , 1487 , 2604 , 9 , 2
        Log.i("payload11 ", Global.ACCESS_TOKEN+" , "+liveId+" , "+join_id+" , "+coin+" , "+type);

        if (AppUtils.isNetworkAvailable(this)) {
            //AppUtils.showRequestDialog(this);
            String url = Const.BASE_URL + Const.liveSendCoin;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("join_id", join_id)
                    .addUrlEncodeFormBodyParameter("coins", coin)
                    .addUrlEncodeFormBodyParameter("coin_type", type)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.v("postApi-resp", String.valueOf(response));
                            try {
                                if (response.getBoolean("status")) {

                                    try {

                                        animateGiftGif(position);

                                        String[] splited = name.split(" ");

                                        String msg = "gift$$$$$-" + position + "-" + splited[0];
                                        RtmMessage message = mRtmClient.createMessage();
                                        message.setText(msg);
                                        byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                                        message.setRawMessage(profilePic_bytes);
                                        sendChannelMessage(message);

                                        message.setText(splited[0] + " " + getString(R.string.live_message_gift_send));

                                        MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
                                        messageBean.setBackground(position);
                                        mMessageBeanList.add(messageBean);
                                        mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                                        rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                                       // rvParticipants.scrollToPosition(mMessageBeanList.size() - 1);

                                        int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                                        int updatedCoins = currentCoins + -Integer.parseInt(coin);
                                        User user = sessionManager.getUser();
                                        user.getData().setPaidCoins("" + updatedCoins);
                                        sessionManager.saveUser(user);
                                        setCurrentCoin();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    hitGetLiveDetailApi();
                                } else {
                                    doAlert();
                                    /*final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveActivityNew.this,
                                            R.style.CustomBottomSheetDialogTheme);
                                    bottomSheetDialog.setContentView(R.layout.dialog_message);
                                    bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
                                    bottomSheetDialog.show();

                                    TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

                                    tvMessage.setText(response.getString("message"));

                                    bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> bottomSheetDialog.dismiss());*/
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveActivityNew.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();


/*
        disposable.add(Global.initRetrofit().sendCoinNew(Global.ACCESS_TOKEN, liveId, join_id, coin, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> onLoadMoreComplete.setValue(true))
                .subscribe((coinSend, throwable) -> {
                    if (coinSend != null && coinSend.getStatus() != null) {

                        try {

                            int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                            int updatedCoins = currentCoins + -Integer.parseInt(coin);
                            User user = sessionManager.getUser();
                            user.getData().setPaidCoins("" + updatedCoins);
                            sessionManager.saveUser(user);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    hitGteLiveDtailApi();

                }));
*/
    }

    public void updateGiftCoinsAccount(String coinSent) {
        PrefHandler pref = new PrefHandler(LiveActivityNew.this);

        List<Pair<String, String>> params = new ArrayList<>();

        params.add(new Pair<>("coinSent", coinSent));
        params.add(new Pair<>("senderId", pref.getuId()));
        params.add(new Pair<>("partyId", Utils.party_id));

        new AsyncHttpsRequest("", this, params, this, 1, false).
                execute(Utils.UPDATE_COINS);
    }

    private void doAlert() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_insufficient_coins, null);
        dialogBuilder.setView(dialogView);
        Button btnAddCoin = dialogView.findViewById(R.id.btnAddCoin);
        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        btnAddCoin.setOnClickListener(v -> {
            alertDialog.dismiss();
            btnRequestCohost.setVisibility(View.VISIBLE);
            getCoinPlan();
        });
        btnCancel.setOnClickListener(v -> {
            alertDialog.dismiss();
            btnRequestCohost.setVisibility(View.VISIBLE);
        });
    }
    private void doExit() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_alert_exit_party, null);
        dialogBuilder.setView(dialogView);
        AppCompatButton btnYes = dialogView.findViewById(R.id.btnYes);
        AppCompatButton btnCancel = dialogView.findViewById(R.id.btnCancel);
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();
        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            isActivityStop = false;

            if (isBroadcaster) {
                //makeHostOfflineOnServer();
                //makeHostOfflineOnServerNew();
                showLiveEndDialog();

            } else {
                //makeAudienceOfflineOnServer();
                makeAudienceOfflineOnServerNew();
            }
        });
        btnCancel.setOnClickListener(v -> {
            alertDialog.dismiss();
        });
    }

    @Override
    public void onBackPressed() {
        if (llMessage.getVisibility() == View.VISIBLE) {
            AppUtils.hideSoftKeyboard(this);
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            llMessage.setVisibility(View.GONE);
            etMessage.clearFocus();
            if (isBroadcaster) {
                llHostBottom.setVisibility(View.VISIBLE);
            } else {
                llBottomAudience.setVisibility(View.VISIBLE);
            }

        } else {
            if (isBroadcaster)
                showLiveEndDialog();
            else
                doExit();
        }
    }

    private void showPurchaseResultToast(Boolean status) {


        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModel.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                //((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                onBackPressed();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private void setCurrentCoin() {
        tvUserCoin.setText(sessionManager.getUser().getData().getPaidCoins());
    }

    @Override
    protected void onStart() {
        super.onStart();
        //blurLayout.startBlur();
    }

}