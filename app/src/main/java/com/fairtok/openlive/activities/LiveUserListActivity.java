package com.fairtok.openlive.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.fragment.PopularLiveFragment;
import com.fairtok.openlive.fragment.RankingLiveFragment;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;

public class LiveUserListActivity extends RtcBaseActivity implements View.OnClickListener {

    private ViewPager viewPager;

    private SmartTabLayout tabLayout;

    ImageView ivGoLive;

    SessionManager sessionManager;

    private RtmClient mRtmClient;

    private static final int PERMISSION_REQ_CODE = 1 << 4;

    static boolean mIsInChat = false;

    PrefHandler prefHandler;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_user_list);

        init();

    }

    private void init() {

        viewPager = findViewById(R.id.viewPager);

        tabLayout = findViewById(R.id.tabLayout);

        ivGoLive = findViewById(R.id.ivGoLive);

        ivGoLive.setOnClickListener(this);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setViewPager(viewPager);

        sessionManager = new SessionManager(this);

        prefHandler = new PrefHandler(this);

        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(@NonNull FragmentManager fm) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;

            if (position == 0) {
                fragment = new PopularLiveFragment();
            } else {
                fragment = new RankingLiveFragment();
            }

            return fragment;
        }

        @Override
        public int getCount() {

            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;

            if (position == 0) {
                title = getString(R.string.popular);
            } else {
                title = getString(R.string.ranking);
            }

            return title;
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivGoLive:

                Utils.hostProfilePic = sessionManager.getUser().getData().getUserProfile();

                checkPermission();

                break;

        }

    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                doLogin();
            } else {
                Toast.makeText(this, R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * API CALL: login RTM server
     */
    private void doLogin() {
        mIsInChat = true;

        mRtmClient.login(null, "@" + sessionManager.getUser().getData().getUserName(), new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("TAG", "login success");
                runOnUiThread(() -> {
                    gotoRoleActivity();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("TAG", "login failed: " + errorInfo.getErrorCode());
                Log.i("TAG", "login failed: " + errorInfo.getErrorDescription());
                runOnUiThread(() -> {
                    mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    private void gotoRoleActivity() {

        showHashTagDialog();

        /*config().setChannelName("@"+sessionManager.getUser().getData().getUserName());

        Intent intent=new Intent(this, LiveActivityNew.class);
        intent.putExtra("role", Constants.CLIENT_ROLE_BROADCASTER);
        intent.putExtra("peerMode",false);
        intent.putExtra("targetName", config().getChannelName());
        intent.putExtra("userId","@"+sessionManager.getUser().getData().getUserName());
        startActivity(intent);*/

    }

    private void showHashTagDialog() {

        /*final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveUserListActivity.this, R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_hashtag);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();*/

      /*  edtTxtMine.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String startChar = null;

                try{
                    startChar = Character.toString(s.charAt(start));
                    Log.i(getClass().getSimpleName(), "CHARACTER OF NEW WORD: " + startChar);
                }
                catch(Exception ex){
                    startChar = "";
                }

                if (startChar.equals("#")) {
                    changeTheColor(s.toString().substring(start), start, start + count);
                    hashTagIsComing++;
                }

                if(startChar.equals(" ")){
                    hashTagIsComing = 0;
                }

                if(hashTagIsComing != 0) {
                    changeTheColor(s.toString().substring(start), start, start + count);
                    hashTagIsComing++;
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });*/

        hitGolLiveApi();

    }

    private void hitGolLiveApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.GO_LIVE;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", Global.USER_ID)
                    .addUrlEncodeFormBodyParameter("post_hash_tag ", "#live")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseGoLiveJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Toast.makeText(LiveUserListActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGoLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                gotoLive(response.getJSONObject("data").getString("live_id"));

            } else
                Toast.makeText(this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void gotoLive(String liveId) {

        config().setChannelName("@"+sessionManager.getUser().getData().getUserName());

        /*Intent intent=new Intent(this, LiveActivityNew.class);
        intent.putExtra("role", Constants.CLIENT_ROLE_BROADCASTER);
        intent.putExtra("peerMode",false);
        intent.putExtra("targetName", config().getChannelName());
        intent.putExtra("userId","@"+sessionManager.getUser().getData().getUserName());
        intent.putExtra("liveId",liveId);
        startActivity(intent);*/

        String userId= "@"+sessionManager.getUser().getData().getUserName();
        //config().setChannelName(userId);

        Intent intent = new Intent(getIntent());
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_BROADCASTER);
        intent.setClass(getApplicationContext(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        intent.putExtra("liveId",liveId);
        startActivity(intent);
    }

    /**
     * API CALL: logout from RTM server
     */
    private void doLogout() {
        mRtmClient.logout(null);
        MessageUtil.cleanMessageListBeanList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsInChat) {
            doLogout();
        }
    }


}