package com.fairtok.openlive.activities;

import static com.fairtok.utils.BindingAdapters.loadMediaImage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.util.Size;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.AdapterBeauty;
import com.fairtok.adapter.AdapterPremiumGift;
import com.fairtok.adapter.AdapterViewers;
import com.fairtok.adapter.LiveUserAdapterNew;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivityLiveUsersBinding;
import com.fairtok.model.LiveUserBean;
import com.fairtok.model.ViewerModel;
import com.fairtok.model.user.User;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.BottomSheetDialog;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.notification.NotificationActivity;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;
import com.fairtok.viewmodel.LiveUsersViewModel;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class LiveUsersActivity1 extends BaseActivity implements CompleteTaskListner,
        SurfaceHolder.Callback, Handler.Callback , View.OnClickListener, AdapterBeauty.BeautyListener, AdapterPremiumGift.PremiumListener {

    ActivityLiveUsersBinding binding;

    //fcmNotificationType is a type coming from fcmNotification
    String liveId = "", type = "1", fcmNotificationType = "", username = ""; //liveId used when User will go on live

    ImageView ivCrown, ivHome;

    LinearLayout llHome, llSearch, llRecordVideo, llLiveCall, llProfile, linSearch;

    SwipeRefreshLayout swipeRefresh;

    LiveUsersViewModel viewModel;
    SessionManager sessionManager;
    PrefHandler prefHandler;

    private final int mInterval = 5000; // 30 seconds by default, can be changed later
    private Handler mHandler;

    // Permission request code of any integer value
    private static final int PERMISSION_REQ_CODE = 1 << 4;

    public static boolean isLiveClicked = false;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String mUserId;
    private RtmClient mRtmClient;
    static boolean mIsInChat = false;

    static boolean isBroadCaster = false, isPremium = false;
    static String channelName;
    boolean isGaming = false;
    private static final String TAG = "paras";
    String userLogin;
    boolean isFABOpen = false;

    BottomSheetDialog bottomSheet;
    ArrayList<String> arrayListHashTag = new ArrayList<>();
    ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
    LiveUserAdapterNew liveUserAdapterNew;
    RecyclerView recyclerview;

    CameraManager mCameraManager;
    String[] mCameraIDsList;
    CameraDevice.StateCallback mCameraStateCB;
    CameraDevice mCameraDevice;
    private static final int MSG_CAMERA_OPENED = 1;
    private static final int MSG_SURFACE_READY = 2;
    boolean mSurfaceCreated = true;
    boolean mIsCameraConfigured = false;
    CameraCaptureSession mCaptureSession;

    private Surface mCameraSurface = null;
    private final Handler mHandlerCam = new Handler(this);
    ActivityResultLauncher activityResultLauncher;
    private TabLayout topTabLayout;
    ArrayList<ViewerModel> arrayListViewers = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_live_users1);
        //1 viewModel = ViewModelProviders.of(this, new ViewModelFactory(new LiveUsersViewModel()).createFor()).get(LiveUsersViewModel.class);

        mHandler = new Handler();

        sessionManager = new SessionManager(this);
        prefHandler = new PrefHandler(this);

        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

        if (sessionManager.getUser()!=null){
            userLogin = "@" + sessionManager.getUser().getData().getUserName();
        }

        bottomSheet = new BottomSheetDialog();

        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-message".
        ivHome = findViewById(R.id.ivHome);

        binding.frameNotification.setOnClickListener(view -> {
            startActivity(new Intent(this, NotificationActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

        });


        initView();
        initTopTab();
        initObserve();
        initListeners();

        //getAllLiveUsers();

        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    if(dialogHashTag!= null){
                        dialogHashTag.dismiss();
                    }
                    //Toast.makeText(LiveUsersActivity.this, "close dialog", Toast.LENGTH_SHORT).show();
                }
        );

        recyclerview = findViewById(R.id.recyclerview);
        swipeRefresh = findViewById(R.id.swipeRefresh);

        linSearch = findViewById(R.id.linSearch);
        ivCrown = findViewById(R.id.ivCrown);


        liveUserAdapterNew = new LiveUserAdapterNew(arrayList, LiveUsersActivity1.this, "liveuser");
        recyclerview.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerview.setAdapter(liveUserAdapterNew);


        //binding.setViewmodel(viewModel);

        binding.ivBackCrown.setOnClickListener(view -> {

            binding.topTabLayout.setVisibility(View.VISIBLE);
            binding.loutHeader.setVisibility(View.VISIBLE);
            arrayList.clear();
            liveUserAdapterNew.notifyDataSetChanged();
            hitGetLiveList();
        });

        /*binding.tvRanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "2";
                binding.tvRanking.setBackgroundResource(R.drawable.my_bg_gradient_top_corner_20);
                binding.tvPopular.setBackgroundResource(0);
                binding.tvCelebrities.setBackgroundResource(0);
                binding.tvNew.setBackgroundResource(0);
                binding.tvHot.setBackgroundResource(0);
                binding.tvBattle.setBackgroundResource(0);
                arrayList.clear();
                liveUserAdapterNew.notifyDataSetChanged();
                hitGetLiveList();
            }
        });*/

        binding.ivCrown.setOnClickListener(v -> {
            /*type = "2";
            arrayList.clear();
            liveUserAdapterNew.notifyDataSetChanged();
            binding.topTabLayout.setVisibility(View.INVISIBLE);
            binding.rlCrownHeader.setVisibility(View.VISIBLE);
            binding.loutHeader.setVisibility(View.INVISIBLE);
            hitGetLiveList();*/
            startActivity(new Intent(LiveUsersActivity1.this, RankingHostActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        topTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                arrayList.clear();
                liveUserAdapterNew.notifyDataSetChanged();

                switch (tab.getPosition()){
                    case 0:
                        type = "1";
                        break;
                    case 1:
                        type = "3";
                        break;
                    case 2:
                        type = "4";
                        break;
                    case 3:
                        type = "5";
                        break;
                    case 4:
                        type = "6";
                        break;
                }
                hitGetLiveList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        binding.ivGoLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //CropImage.activity().start(LiveUsersActivity.this);
                showHashTagFullScreenDialog();

                /*Utils.hostProfilePic = sessionManager.getUser().getData().getUserProfile();
                isBroadCaster = true;
                channelName = "@" + sessionManager.getUser().getData().getUserName();

                // bottomSheet.show(getSupportFragmentManager(), "ModalBottomSheet");
                Intent intent = new Intent("fairtok-click-listener");
                intent.putExtra("isGame", false);
                //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
*/
//                onStartBroadcastClicked();

//                if(sessionManager.getUser().getData().getFollowersCount()>=prefHandler.getFollowersThreshold())
//                {
//                    Intent intent = new Intent(view.getContext(), BroadcastActivity.class);
//                    intent.putExtra("isBroadCaster", true);
//                    intent.putExtra("channelName", sessionManager.getUser().getData().getUserName());
//                    //start live stream activity
//                    startActivity(intent);
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(),"You Must have "+prefHandler.getFollowersThreshold()+" followers to start live video",Toast.LENGTH_SHORT).show();
//                }
            }
        });

        binding.fabGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hostProfilePic = sessionManager.getUser().getData().getUserProfile();
                isBroadCaster = true;
                channelName = "@" + sessionManager.getUser().getData().getUserName();
                onStartGameClicked();

//                if(sessionManager.getUser().getData().getFollowersCount()>=prefHandler.getFollowersThreshold())
//                {
//                    Intent intent = new Intent(view.getContext(), BroadcastActivity.class);
//                    intent.putExtra("isBroadCaster", true);
//                    intent.putExtra("channelName", sessionManager.getUser().getData().getUserName());
//                    //start live stream activity
//                    startActivity(intent);
//                }
//                else
//                {
//                    Toast.makeText(getApplicationContext(),"You Must have "+prefHandler.getFollowersThreshold()+" followers to start live video",Toast.LENGTH_SHORT).show();
//                }
            }
        });

        binding.linSearch.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), SearchUserActivity.class).putExtra("type", type));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        swipeRefresh.setOnRefreshListener(() -> {
            swipeRefresh.setRefreshing(false);
            hitGetLiveList();
        });

    }

    private void hitGetLiveList() {
        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.LIVE_LIST;
            Log.v("postApi-token", Global.ACCESS_TOKEN);
            Log.v("postApi-url", url);

            //getAllLiveUsers();
            //testDialog("Authorization : " +Global.ACCESS_TOKEN+ " , "+ type);

            // access_token - eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjIwMjEtMTItMjAgMTI6MzM6NDMi.VBwc3pLvPuAUA20IyaJgLW5wG0wHkmhFks0C1fSwwy8
            // {"status":401,"message":"Unauthorized Access!"}
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("type", type)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.v("postApi-url-list", String.valueOf(response));
                            //AppUtils.hideDialog();
                            parseLiveList(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            //testDialog(anError.getErrorCode()+" == "+anError.getErrorBody()+" === "+anError.getErrorDetail());
                            Log.v("postApi-url", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorBody()));
                            //AppUtils.hideDialog();
                            Toast.makeText(LiveUsersActivity1.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(LiveUsersActivity1.this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    android.app.AlertDialog.Builder mBuilder1 = null;
    private void testDialog(String msg){
        mBuilder1 = new android.app.AlertDialog.Builder(this);
        mBuilder1.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        android.app.AlertDialog alert = mBuilder1.create();
        alert.show();
    }

    private void parseLiveList(JSONObject response) {

        arrayList.clear();

        try {
            if (response.getBoolean("status")) {

                JSONArray jsonArray = response.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("post_hash_tag", jsonObject.getString("post_hash_tag"));
                    hashMap.put("live_start", jsonObject.getString("live_start"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                    hashMap.put("live_thumbnail", jsonObject.getString("live_thumbnail"));
                    hashMap.put("coinsReceived", jsonObject.getString("coinsReceived"));
                    hashMap.put("coinsReceivedSilver", jsonObject.getString("coinsReceivedSilver"));
                    hashMap.put("viewers_count", jsonObject.getString("viewers_count"));

                    arrayList.add(hashMap);

                    //Fcm notification
                    if (type.equals("1") &&  username.equals(jsonObject.getString("user_name"))) {

                        Intent intent = new Intent("custom-message");
                        intent.putExtra("isBroadCaster", false);
                        intent.putExtra("isGaming", isGaming);
                        intent.putExtra("channelName", jsonObject.getString("user_name"));
                        intent.putExtra("live_id", jsonObject.getString("live_id"));

                        Log.i("messagessss4 ", jsonObject.getString("live_id"));
                        //LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        type = "";
                        username = "";
                    }
                }

            } /*else
                Toast.makeText(LiveUsersActivity.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        liveUserAdapterNew.notifyDataSetChanged();

    }


    @Override
    protected void onResume() {
        super.onResume();

        hitGetLiveList();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("custom-message"));

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver2, new IntentFilter("fairtok-click-listener"));

        startRepeatingTask();
        if (mIsInChat) {
            doLogout();
        }
    }


    @Override
    protected void onPause() {
        stopRepeatingTask();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver2);

        super.onPause();
    }


    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                //1viewModel.getLiveUserData("",prefHandler.getuId(),LiveUsersActivity.this,LiveUsersActivity.this);
                //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }


    private void initListeners() {
        //1binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());


        binding.llHome.setOnClickListener(this);
        binding.llSearch.setOnClickListener(this);
        binding.llRecordVideo.setOnClickListener(this);
        binding.llLiveCall.setOnClickListener(this);
        binding.llProfile.setOnClickListener(this);
        ivHome.setOnClickListener(this);
    }

    private void initView() {

        topTabLayout = findViewById(R.id.topTabLayout);
        PrefHandler prefHandler = new PrefHandler(this);
        binding.refreshlout.setEnableRefresh(false);
        /*1 viewModel.adapter.isHashTag = true;
        viewModel.adapter.word = viewModel.hashtag;
        viewModel.getLiveUserData("Wait...",prefHandler.getuId(),this,this);*/



        if (getIntent().getExtras() != null) {

            Intent intent = getIntent();
            fcmNotificationType = intent.getStringExtra("fcmNotificationType");
            username = intent.getStringExtra("username");

        }

    }
    private void initTopTab(){
        topTabLayout.addTab(topTabLayout.newTab().setText("Popular"));
        topTabLayout.addTab(topTabLayout.newTab().setText("Celebrities"));
        topTabLayout.addTab(topTabLayout.newTab().setText("New"));
        //topTabLayout.addTab(topTabLayout.newTab().setText("Hot"));
        topTabLayout.addTab(topTabLayout.newTab().setText("Battle"));
    }

    private void initObserve() {
        //1viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
//        viewModel.video.observe(this, video -> binding.tvVideoCount.setText(Global.prettyCount(video.getPost_count()).concat(" Videos")));
    }

    @Override
    public void completeTask(String result, int response_code) {
        if (response_code == 0) {
            List<LiveUserBean.PostDataBean.DataArrBean> list = new ArrayList<>();
            try {
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {

                    Gson gson = new Gson();
                    LiveUserBean bean = gson.fromJson(result, LiveUserBean.class);
                    list = bean.getPost_data().getData_arr();

                }

                //1viewModel.adapter.updateData(list);
                //1viewModel.adapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (list.size() > 0)
                binding.partyLayout.setVisibility(View.GONE);
            else
                binding.partyLayout.setVisibility(View.VISIBLE);
        } else if (response_code == 1) {
            try {
                //JSONObject obj = new JSONObject(result);
                String room = channelName;
                String userId = userLogin;
                config().setChannelName(room);

                Intent intent = new Intent(getIntent());
                intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_BROADCASTER);
                intent.setClass(getApplicationContext(), SpinWheelActivity.class);
                intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
                intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
                intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
                startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    public void onSettingClicked(View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void onStartBroadcastClicked() {

        isBroadCaster = true;
        isGaming = false;
        checkPermission();
    }

    public void onStartGameClicked() {

        isGaming = true;
        doAlertCost("Host");

    }

    private void doAlertCost(String joinOrHost) {

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveUsersActivity1.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("It will cost you " + prefHandler.getFollowersThreshold() + " Gold Coins to " + joinOrHost + " this game");
        builder.setPositiveButton(joinOrHost, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                if (currentWalletBalance >= prefHandler.getFollowersThreshold()) {
                    checkPermission();
                } else {
                    doAlertCoins();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void doAlertCoins() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveUsersActivity1.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough coins to join.");
        builder.setPositiveButton("Add Coins", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
    private void startBroadcastGolive(){
        Intent intent = new Intent("fairtok-click-listener");
        intent.putExtra("isGame", false);
        //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
        Log.i("profileimageee ", prefHandler.getLiveProfile() +" , "+ Utils.DEFAULT_PROFILE_URL);
        if (prefHandler.getLiveProfile()!=null){
            if (!prefHandler.getLiveProfile().equals(Utils.DEFAULT_PROFILE_URL))
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            else
                Toast.makeText(LiveUsersActivity1.this, "Please upload your live profile image.", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(LiveUsersActivity1.this, "Please upload your live profile image.", Toast.LENGTH_SHORT).show();
        }
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            isBroadCaster = intent.getBooleanExtra("isBroadCaster", true);
            isGaming = intent.getBooleanExtra("isGaming", false);
            channelName = "@" + intent.getStringExtra("channelName");
            liveId = intent.getStringExtra("live_id");
            Log.v("ljhljhl", isBroadCaster + " " + isGaming + " " + channelName+ " "+ liveId);
            onJoinAsAudienceClicked(isGaming, channelName);
        }
    };

    public BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            if (bottomSheet.isVisible())
                bottomSheet.dismiss();

            boolean isGame = intent.getBooleanExtra("isGame", false);

            if (isGame)
                onStartGameClicked();
            else
                onStartBroadcastClicked();


        }
    };

    public void onJoinAsAudienceClicked(boolean isGaming, String channelName) {
        isBroadCaster = false;
        this.isGaming = isGaming;
        LiveUsersActivity1.channelName = channelName;
        if (isGaming) {
            doAlertCost("Join");
        } else {
            checkPermission();
        }

    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                doLogin();
            } else {
                toastNeedPermissions();
            }
        }
    }

    public void gotoRoleActivity() {

        if (isGaming)
            gotoGameActivity();
        else if (isBroadCaster) {
            hitGolLiveApi();
        } else
            gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    @Override
    public void beautyListener(String type, int position) {
        Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void listenerPre(int pos) {
        if (imgPremGift!=null){
            imgPremGift.setImageResource(GiftUtil.getGiftAnimRes(pos));
            tvPremiumCriteria.setText(getResources().getIntArray(R.array.gift_values)[pos]+" to enter premium");
        }
    }

    private class AdapterParty extends RecyclerView.Adapter<AdapterParty.MyViewHolder> {

        int[] data;

        com.google.android.material.bottomsheet.BottomSheetDialog bottomSheetDialog;

        public AdapterParty(int[] stringArray, com.google.android.material.bottomsheet.BottomSheetDialog dialog) {

            data = stringArray;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_live_party, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
            holder.rlMain.setOnClickListener(view -> {
                holder.checkLiveUser.setChecked(true);
            });
        }

        @Override
        public int getItemCount() {

            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView ivImage, ivCoin;
            TextView tvCoins;
            RelativeLayout rlMain;
            CheckBox checkLiveUser;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                ivCoin = itemView.findViewById(R.id.ivCoin);
                tvCoins = itemView.findViewById(R.id.tvCoins);
                rlMain = itemView.findViewById(R.id.rlMain);
                checkLiveUser = itemView.findViewById(R.id.checkLiveUser);
            }
        }
    }

    Dialog dialogHashTag = null;
    private ConstraintLayout clInvite, clPremium;
    private boolean viewerInvite = false;
    private ImageView ivProfile, imgPremGift;
    private TextView tvPremiumCriteria;
    private AppCompatButton btnDone;
    private void showHashTagFullScreenDialog() {

        dialogHashTag = new Dialog(LiveUsersActivity1.this, R.style.DialogBoxFulScreen);
        dialogHashTag.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogHashTag.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialogHashTag.setContentView(R.layout.dialog_full_hashtag);
        dialogHashTag.show();

        EditText etHashTag = dialogHashTag.findViewById(R.id.etHashTag);

        // setup bottom tab
        LinearLayout llPremium = dialogHashTag.findViewById(R.id.llPremium);
        LinearLayout llBattle = dialogHashTag.findViewById(R.id.llBattle);
        LinearLayout llGolive = dialogHashTag.findViewById(R.id.llGolive);
        LinearLayout llParty = dialogHashTag.findViewById(R.id.llParty);
        LinearLayout llSettings = dialogHashTag.findViewById(R.id.llSettings);

        ImageView ivPopular = dialogHashTag.findViewById(R.id.ivPopular);
        ImageView ivSetting = dialogHashTag.findViewById(R.id.ivSetting);
        ImageView ivNew = dialogHashTag.findViewById(R.id.ivNew);
        ImageView ivBattle = dialogHashTag.findViewById(R.id.ivBattle);
        ImageView ivHot = dialogHashTag.findViewById(R.id.ivHot);
        btnDone = dialogHashTag.findViewById(R.id.btnDone);
        ProgressBar progressBar = dialogHashTag.findViewById(R.id.progressbar);

        btnDone.setOnClickListener(view -> {
            changeLiveThumbnail(progressBar, btnDone);
        });
        // premium
        clPremium = dialogHashTag.findViewById(R.id.clPremium);
        ImageView ivClose1 = dialogHashTag.findViewById(R.id.ivClose1);
        ivClose1.setOnClickListener(view -> clPremium.setVisibility(View.GONE));

        dialogHashTag.findViewById(R.id.linBeauty).setOnClickListener(view -> {
            AlertDialogBeauty();
        });

        Button btnContinue = dialogHashTag.findViewById(R.id.btnContinue);
        btnContinue.setOnClickListener(view -> {
            clPremium.setVisibility(View.GONE);
            clInvite.setVisibility(View.VISIBLE);
            dialogHashTag.findViewById(R.id.linBeauty).setVisibility(View.GONE);
            viewerInvite = true;
        });
        setupBottomTab(llPremium, llBattle, llGolive, llParty, llSettings,
                ivPopular, ivSetting, ivNew, ivBattle, ivHot, clPremium);

        SurfaceView surfaceView = dialogHashTag.findViewById(R.id.surfaceView);
        SurfaceHolder mSurfaceHolder = surfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mCameraManager = (CameraManager) LiveUsersActivity1.this.getSystemService(Context.CAMERA_SERVICE);

        try {
            mCameraIDsList = mCameraManager.getCameraIdList();
            for (String id : mCameraIDsList) {
                Log.v(TAG, "CameraID: " + id);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }


        mCameraStateCB = new CameraDevice.StateCallback() {
            @Override
            public void onOpened(CameraDevice camera) {
                //Toast.makeText(getApplicationContext(), "onOpened", Toast.LENGTH_SHORT).show();

                mCameraDevice = camera;
                mHandlerCam.sendEmptyMessage(MSG_CAMERA_OPENED);
            }

            @Override
            public void onDisconnected(CameraDevice camera) {
                //Toast.makeText(getApplicationContext(), "onDisconnected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(CameraDevice camera, int error) {
                //Toast.makeText(getApplicationContext(), "onError", Toast.LENGTH_SHORT).show();
            }
        };

        // eftiaz code

        arrayListViewers.add(new ViewerModel("Red Wolf", "", true));
        arrayListViewers.add(new ViewerModel("Mr king", "", false));
        arrayListViewers.add(new ViewerModel("Tango Official", "", false));

        clInvite = dialogHashTag.findViewById(R.id.clInvite);
        ImageView ivBack = dialogHashTag.findViewById(R.id.ivBack);
        ImageView ivCross = dialogHashTag.findViewById(R.id.ivCross);
        Button btnInvitePremium = dialogHashTag.findViewById(R.id.btnInvitePremium);

        btnInvitePremium.setOnClickListener(view -> {
            Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
        });

        ivCross.setOnClickListener(view -> {
            clInvite.setVisibility(View.GONE);
            dialogHashTag.findViewById(R.id.linBeauty).setVisibility(View.VISIBLE);
            viewerInvite = false;
        });
        ivBack.setOnClickListener(view -> {
            dialogHashTag.findViewById(R.id.linBeauty).setVisibility(View.VISIBLE);
            clInvite.setVisibility(View.GONE);
            viewerInvite = false;
        });

        RecyclerView rvViewers = dialogHashTag.findViewById(R.id.rvViewers);
        rvViewers.setLayoutManager(new LinearLayoutManager(this));
        AdapterViewers adapterViewers = new AdapterViewers(arrayListViewers);
        rvViewers.setAdapter(adapterViewers);

        imgPremGift = dialogHashTag.findViewById(R.id.imgPremGift);
        tvPremiumCriteria = dialogHashTag.findViewById(R.id.tvPremiumCriteria);
        RecyclerView rvGifts = dialogHashTag.findViewById(R.id.rvGifts);

        tvPremiumCriteria.setText(getResources().getIntArray(R.array.gift_values)[19]+" to enter premium");
        rvGifts.setLayoutManager(new GridLayoutManager(this, 3));
        AdapterPremiumGift adapterGifts = new AdapterPremiumGift( this, getResources().getIntArray(R.array.gift_values));
        rvGifts.setAdapter(adapterGifts);


        int permissionCheck = ContextCompat.checkSelfPermission(LiveUsersActivity1.this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LiveUsersActivity1.this, Manifest.permission.CAMERA)) {

            } else {
                ActivityCompat.requestPermissions(LiveUsersActivity1.this, new String[]{Manifest.permission.CAMERA}, 1);
                Toast.makeText(getApplicationContext(), "request permission", Toast.LENGTH_SHORT).show();
            }
        } else {
            //Toast.makeText(getApplicationContext(), "PERMISSION_ALREADY_GRANTED", Toast.LENGTH_SHORT).show();
            try {
                mCameraManager.openCamera(mCameraIDsList[1], mCameraStateCB, new Handler());
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

/*
        CameraKitView cameraKitView = dialog.findViewById(R.id.camera);

        cameraKitView.onStart();*/

        dialogHashTag.findViewById(R.id.ivClose).setOnClickListener(v -> dialogHashTag.dismiss());

        dialogHashTag.setOnDismissListener(dialog1 -> {

            try {
                if (mCaptureSession != null) {
                    mCaptureSession.stopRepeating();
                    mCaptureSession.close();
                    mCaptureSession = null;
                }

                mIsCameraConfigured = false;
            } catch (final CameraAccessException | IllegalStateException e) {
                e.printStackTrace();
            } finally {
                if (mCameraDevice != null) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                    mCaptureSession = null;
                }
            }
            //cameraKitView.onStop();
        });

        // live profile start
        Log.i("liveprofileeee ", prefHandler.getLiveProfile());
        ivProfile = dialogHashTag.findViewById(R.id.ivProfile);
        dialogHashTag.findViewById(R.id.rel_Live_profile).setOnClickListener(view -> {
            CropImage.activity().start(this);
            /*BottomSheetImagePicker bottomSheetImagePicker = new BottomSheetImagePicker();
            bottomSheetImagePicker.setOnDismiss(uri -> {
                if (!uri.isEmpty()) {
                     loadMediaImage(ivProfile, uri, false);
                     selectedProfileUri = uri;
                    btnDone.setVisibility(View.VISIBLE);
                }
            });
            bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());*/
        });

        Glide.with(this).load(Const.BASE_URL_IMAGE + prefHandler.getLiveProfile()).into(ivProfile);
        // live profile end

        AdapterHashTag adapterHashTag = new AdapterHashTag(arrayListHashTag);

        RecyclerView rvHashTag = dialogHashTag.findViewById(R.id.rvHashTag);
        rvHashTag.setLayoutManager(new FlexboxLayoutManager(this));
        rvHashTag.setAdapter(adapterHashTag);

        etHashTag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().endsWith(" ") && !s.toString().trim().isEmpty()) {

                    if (s.toString().trim().startsWith("#")) {
                        arrayListHashTag.add(s.toString().trim());
                    } else {
                        arrayListHashTag.add("#" + s.toString().trim());
                    }

                    adapterHashTag.notifyDataSetChanged();
                    etHashTag.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    /*private CompositeDisposable disposable = new CompositeDisposable();
    public RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }
    public RequestBody toRequestBodyFile(String value) {
        return RequestBody.create(MediaType.parse("image/*"), value);
    }
    private void profilePhoto(){
        HashMap<String, RequestBody> hashMap = new HashMap<>();
        hashMap.put("full_name", toRequestBody("fullName"));
        MultipartBody.Part body = null;
       *//* if (imageUri != null && !imageUri.isEmpty()) {

            File file = new File(imageUri);
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("user_profile", file.getName(), requestFile);
            String fileName="FairtokUserPic_"+prefHandler.getuId()+".jpg";
            hashMap.put("user_profile", toRequestBodyFile(fileName));
        }*//*

        disposable.add(Global.initRetrofit().updateUser(Global.ACCESS_TOKEN, hashMap, body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((updateUser, throwable) -> {
                    if (updateUser != null && updateUser.getStatus()) {
                        *//*user = updateUser;
                        if(!isImageUpload)
                            updateProfile.setValue(true);

                        deleteCache(context);
                        hiddProgress();*//*
                        //toast.setValue("Your Profile has been updated successfully");
                    }
                }));
    }*/
    private AlertDialog alertDialogBeauty;
    public void AlertDialogBeauty() {
        LayoutInflater m_inflater = LayoutInflater.from(this);
        View layout = m_inflater.inflate(R.layout.custom_dialog_beauty, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialogBeauty = builder.create();
        alertDialogBeauty.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogBeauty.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialogBeauty.setCanceledOnTouchOutside(true);
        alertDialogBeauty.setCancelable(true);
        alertDialogBeauty.getWindow().setGravity(Gravity.BOTTOM);

        ImageView ivClose = layout.findViewById(R.id.ivBack);
        ivClose.setOnClickListener(view -> alertDialogBeauty.dismiss());

        RecyclerView rvGifts = layout.findViewById(R.id.rvMask);
        rvGifts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        AdapterBeauty adapterBeauty = new AdapterBeauty(LiveUsersActivity1.this, GiftUtil.BEAUTY_ICON_RES);
        rvGifts.setAdapter(adapterBeauty);

        alertDialogBeauty.show();
    }

    private void setupBottomTab(LinearLayout llPremium, LinearLayout llBattle, LinearLayout llGolive, LinearLayout llParty, LinearLayout llSettings,
                                ImageView ivPopular, ImageView ivSetting, ImageView ivNew, ImageView ivBattle,
                                ImageView ivHot, ConstraintLayout clPremium){
        llPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "1";
                isPremium = true;
                clPremium.setVisibility(View.VISIBLE);
                Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
                llPremium.setBackgroundResource(R.drawable.rectangular_bg_gradient_less_radius);
                llBattle.setBackgroundResource(0);
                llGolive.setBackgroundResource(0);
                llParty.setBackgroundResource(0);
                llSettings.setBackgroundResource(0);

                ivPopular.setVisibility(View.VISIBLE);
                ivSetting.setVisibility(View.GONE);
                ivNew.setVisibility(View.GONE);
                ivBattle.setVisibility(View.GONE);
                ivHot.setVisibility(View.GONE);
            }
        });

        llBattle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPremium = false;
                Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
                clPremium.setVisibility(View.GONE);
                llBattle.setBackgroundResource(R.drawable.rectangular_bg_gradient_less_radius);
                llPremium.setBackgroundResource(0);
                llGolive.setBackgroundResource(0);
                llParty.setBackgroundResource(0);
                llSettings.setBackgroundResource(0);

                ivPopular.setVisibility(View.GONE);
                ivSetting.setVisibility(View.GONE);
                ivNew.setVisibility(View.GONE);
                ivBattle.setVisibility(View.VISIBLE);
                ivHot.setVisibility(View.GONE);
            }
        });
        llGolive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPremium = false;
                clPremium.setVisibility(View.GONE);
                llGolive.setBackgroundResource(R.drawable.rectangular_bg_gradient_less_radius);
                llPremium.setBackgroundResource(0);
                llBattle.setBackgroundResource(0);
                llParty.setBackgroundResource(0);
                llSettings.setBackgroundResource(0);

                ivPopular.setVisibility(View.GONE);
                ivSetting.setVisibility(View.GONE);
                ivNew.setVisibility(View.VISIBLE);
                ivBattle.setVisibility(View.GONE);
                ivHot.setVisibility(View.GONE);

                Utils.hostProfilePic = sessionManager.getUser().getData().getUserProfile();
                isBroadCaster = true;
                channelName = "@" + sessionManager.getUser().getData().getUserName();

                // bottomSheet.show(getSupportFragmentManager(), "ModalBottomSheet");
                startBroadcastGolive();

            }
        });
        llParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPremium = false;
                Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
                clPremium.setVisibility(View.GONE);
                llParty.setBackgroundResource(R.drawable.rectangular_bg_gradient_less_radius);
                llPremium.setBackgroundResource(0);
                llBattle.setBackgroundResource(0);
                llGolive.setBackgroundResource(0);
                llSettings.setBackgroundResource(0);

                ivPopular.setVisibility(View.GONE);
                ivSetting.setVisibility(View.GONE);
                ivNew.setVisibility(View.GONE);
                ivBattle.setVisibility(View.GONE);
                ivHot.setVisibility(View.VISIBLE);
            }
        });
        llSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPremium = false;
                Toast.makeText(LiveUsersActivity1.this, "Coming Soon...", Toast.LENGTH_SHORT).show();
                clPremium.setVisibility(View.GONE);
                llSettings.setBackgroundResource(R.drawable.rectangular_bg_gradient_less_radius);
                llPremium.setBackgroundResource(0);
                llBattle.setBackgroundResource(0);
                llGolive.setBackgroundResource(0);
                llParty.setBackgroundResource(0);

                ivPopular.setVisibility(View.GONE);
                ivSetting.setVisibility(View.VISIBLE);
                ivNew.setVisibility(View.GONE);
                ivBattle.setVisibility(View.GONE);
                ivHot.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean handleMessage(@NonNull Message message) {
        switch (message.what) {
            case MSG_CAMERA_OPENED:
            case MSG_SURFACE_READY:
                // if both surface is created and camera device is opened
                // - ready to set up preview and other things
                if (mSurfaceCreated && (mCameraDevice != null)
                        && !mIsCameraConfigured) {
                    configureCamera();
                }
                break;
        }

        return true;
    }


    private void configureCamera() {
        // prepare list of surfaces to be used in capture requests
        List<Surface> sfl = new ArrayList<Surface>();

        sfl.add(mCameraSurface); // surface for viewfinder preview

        // configure camera with all the surfaces to be ever used
        try {
            mCameraDevice.createCaptureSession(sfl,
                    new CaptureSessionListener(), null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mIsCameraConfigured = true;
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mCameraSurface = surfaceHolder.getSurface();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        mCameraSurface = surfaceHolder.getSurface();
        mSurfaceCreated = true;
        mHandlerCam.sendEmptyMessage(MSG_SURFACE_READY);
        /*for (String cameraId : mCameraIDsList) {
            CameraCharacteristics cameraCharacteristics =
                    null;
            try {
                cameraCharacteristics = mCameraManager.getCameraCharacteristics(cameraId);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            if (cameraCharacteristics.get(cameraCharacteristics.LENS_FACING) ==
                    CameraCharacteristics.LENS_FACING_BACK) {
                Log.i(TAG, "Found a back-facing camera");
                StreamConfigurationMap info = cameraCharacteristics
                        .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                // Bigger is better when it comes to saving our image
                Size largestSize = Collections.max(
                        Arrays.asList(info.getOutputSizes(ImageFormat.JPEG)),
                        new CompareSizesByArea());
                // Prepare an ImageReader in case the user wants to capture images
                Log.i(TAG, "Capture size: " + largestSize);
                *//*mCaptureBuffer = ImageReader.newInstance(largestSize.getWidth(),
                        largestSize.getHeight(), ImageFormat.JPEG, *//**//*maxImages*//**//*2);
                mCaptureBuffer.setOnImageAvailableListener(
                        mImageCaptureListener, mBackgroundHandler);*//*


                Size optimalSize = chooseBigEnoughSize(
                        info.getOutputSizes(SurfaceHolder.class), i1, i2);
                // Set the SurfaceHolder to use the camera's largest supported size
                Log.i(TAG, "Preview size: " + optimalSize);
                surfaceHolder.setFixedSize(optimalSize.getWidth(),
                        optimalSize.getHeight());

                // prev
                mCameraSurface = surfaceHolder.getSurface();
                mSurfaceCreated = true;

                Log.i("cameraSize ", i1 + " , " + i2);

                mHandlerCam.sendEmptyMessage(MSG_SURFACE_READY);
                // prev
            }
        }*/
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mSurfaceCreated = false;
    }

    static Size chooseBigEnoughSize(Size[] choices, int width, int height) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<Size>();
        for (Size option : choices) {
            if (option.getWidth() <= width && option.getHeight() <= height) {
                bigEnough.add(option);
            }
        }
        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }
    static class CompareSizesByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ivHome:
            case R.id.llHome:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
                break;

            case R.id.llSearch:
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("pageFrom","1"));
                finish();
                break;
            case R.id.llRecordVideo:
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("pageFrom","2"));
                finish();
                break;
            case R.id.llLiveCall:
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("pageFrom","3"));
                finish();
                break;
            case R.id.llProfile:
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("pageFrom","4"));
                finish();
                break;

        }

    }

    private class CaptureSessionListener extends
            CameraCaptureSession.StateCallback {
        @Override
        public void onConfigureFailed(final CameraCaptureSession session) {
            Log.d("CameraPreviewww", "CaptureSessionConfigure failed");
        }

        @Override
        public void onConfigured(final CameraCaptureSession session) {
            Log.d("CameraPreviewww", "CaptureSessionConfigure onConfigured");
            mCaptureSession = session;

            try {
                CaptureRequest.Builder previewRequestBuilder = mCameraDevice
                        .createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                previewRequestBuilder.addTarget(mCameraSurface);
                mCaptureSession.setRepeatingRequest(previewRequestBuilder.build(),
                        null, null);
            } catch (CameraAccessException e) {
                Log.d("CameraPreviewww", "setting up preview failed");
                e.printStackTrace();
            }
        }
    }

    private static class AdapterHashTag extends RecyclerView.Adapter<AdapterHashTag.MyViewHolder> {

        ArrayList<String> data;

        public AdapterHashTag(ArrayList<String> arrayList) {

            data = arrayList;

        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_hash_tags, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

            holder.tvHashTag.setText(data.get(position));

            holder.ivClear.setOnClickListener(v -> {
                data.remove(position);
                notifyItemChanged(position);
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvHashTag;

            ImageView ivClear;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                tvHashTag = itemView.findViewById(R.id.tvHashTag);

                ivClear = itemView.findViewById(R.id.ivClear);
            }
        }
    }


//    public void onJoinAsBroadcaster(View view) {
//        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
//    }

    public void onJoinAsAudience(View view) {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {

        String room = channelName;
        String userId = userLogin;
        config().setChannelName(room);
        //2 , @pk3995092 , @brahmadeoprasad1 , 2553
        // 2 , @pk3995092 , @brahmadeoprasad1 , 2553
        Log.i("userCredentials", role+" , "+config().getChannelName()+" , "+userId+ " , "+ liveId);
        Intent intent = new Intent(getIntent());
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        intent.putExtra("liveId", liveId);
        intent.putExtra("from", "live");

        activityResultLauncher.launch(intent);
        //startActivityForResult(intent, 1000);

    }

    private void gotoGameActivity() {

        //PrefHandler prefHandler = new PrefHandler(BroadcastActivity.this);

        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - prefHandler.getFollowersThreshold();
        Log.v("paras", "paras currentWalletBalance = " + currentWalletBalance);
        Log.v("paras", "paras finalBalane = " + finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins("" + finalBalane);
        sessionManager.saveUser(user);

        updateWallet(LiveUsersActivity1.this, "" + finalBalane);

    }

    public void updateWallet(Context context, String balance) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "update"));
        params.add(new Pair<>("balance", balance));
        params.add(new Pair<>("userId", prefHandler.getuId()));

        new AsyncHttpsRequest("Wait...", context, params, this, 1, false).execute(Utils.UPDATE_BALANCE);
    }

    private void toastNeedPermissions() {
        Toast.makeText(this, R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
    }
    // chat part

    /**
     * API CALL: login RTM server
     */
    private void doLogin() {
        mIsInChat = true;
        mUserId = userLogin;
        AppUtils.showRequestDialog(LiveUsersActivity1.this);

        mRtmClient.login(null, mUserId, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

                AppUtils.hideDialog();
                Log.i("postApi", "login success");
                runOnUiThread(() -> {
                    gotoRoleActivity();
                });

                /*final Runnable setImageRunnable = new Runnable() {
                    public void run() {
                        AppUtils.hideDialog();
                        gotoRoleActivity();
                    }
                };

                TimerTask task = new TimerTask(){
                    public void run() {
                        runOnUiThread(setImageRunnable);
                    }
                };

                Timer timer = new Timer();
                timer.schedule(task, 2000);*/

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
                AppUtils.hideDialog();
                runOnUiThread(() -> {
                    mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {
                            Log.i("ErrorLogin ", errorInfo.getErrorDescription());
                        }
                    });
                }
            }
        });
    }

    /**
     * API CALL: logout from RTM server
     */
    private void doLogout() {
        mRtmClient.logout(null);
        MessageUtil.cleanMessageListBeanList();
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                selectedProfileUri = resultUri.getPath();
                loadMediaImage(ivProfile, selectedProfileUri, false);
                btnDone.setVisibility(View.VISIBLE);
                //Log.i("uriiii ", resultUri.getPath());
            }
        }
    }
    public RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    private CompositeDisposable disposable = new CompositeDisposable();
    private String selectedProfileUri=null;
    private void changeLiveThumbnail(ProgressBar progressBar, AppCompatButton btnDone){
        progressBar.setVisibility(View.VISIBLE);
        if (AppUtils.isNetworkAvailable(this)) {

            MultipartBody.Part body1 = null;
            if ( selectedProfileUri!= null && !selectedProfileUri.isEmpty()) {
                File file = new File(selectedProfileUri);
                RequestBody requestFile2 =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body1 = MultipartBody.Part.createFormData("thumbnail", file.getName(), requestFile2);
                Log.i("filename ", file.getName()+" , "+ selectedProfileUri);
            }

            HashMap<String, RequestBody> hashMap = new HashMap<>();
            hashMap.put("Authorization", toRequestBody(Global.USER_ID));

            disposable.add(Global.initRetrofit().changeThumbnail(Global.ACCESS_TOKEN, body1)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    //.doOnSubscribe(disposable1 -> Log.i("aaaa", disposable1.toString()))
                    //.doOnTerminate()
                    .subscribe((restResponse, throwable) -> {
                        if (restResponse.getStatus()){
                            progressBar.setVisibility(View.GONE);
                            btnDone.setVisibility(View.GONE);
                            //Log.i("response111 ", restResponse.getData().getLive_thumbnail());
                            prefHandler.setLiveProfile(restResponse.getData().getLive_thumbnail());
                            //Utils.LIVE_PROFILE_URL = restResponse.getAll_data().getThumbnail();
                        }

                    }));

            String url = Const.BASE_URL + Const.GO_LIVE;
            Log.v("postApi-url", url);

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }
    private void hitGolLiveApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.GO_LIVE;
            Log.v("postApi-url-livepara", Global.ACCESS_TOKEN  +" , "+ Global.USER_ID +" , "+ Const.BASE_URL_IMAGE+prefHandler.getLiveProfile()+ ", "+
                    arrayListHashTag.toString()
                            .replace("[", "").replace("]", "").replace(" ", ""));

            // alternative
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", Global.USER_ID)
                    .addUrlEncodeFormBodyParameter("thumbnail", Const.BASE_URL_IMAGE+prefHandler.getLiveProfile())
                    .addUrlEncodeFormBodyParameter("post_hash_tag ", arrayListHashTag.toString()
                            .replace("[", "").replace("]", "").replace(" ", ""))
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseGoLiveJson(response);
                            Log.v("postApi-resp-live", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveUsersActivity1.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }
    private void getAllLiveUsers(){
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(Global.initRetrofit().getAllLiveUsers(Global.ACCESS_TOKEN,
                type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> {

                })
                .subscribe((jsonObject, throwable) -> {
                    Log.i("responseee121 ", jsonObject.toString());
                    //parseLiveList(jsonObject);
                }));
    }

    private void parseGoLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                liveId = response.getJSONObject("data").getString("live_id");

                gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);

            } else
                Toast.makeText(this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            //Log.i("erorrrr ", e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (viewerInvite){
            clInvite.setVisibility(View.GONE);
            viewerInvite = false;
        }else {
            finishAffinity();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        /*if (disposable!=null){
            disposable.clear();
        }*/
    }
}