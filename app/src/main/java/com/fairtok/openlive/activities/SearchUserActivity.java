package com.fairtok.openlive.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.LiveUserAdapterNew;
import com.fairtok.chat.PrefHandler;
import com.fairtok.model.user.SearchUser;
import com.fairtok.model.user.User;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;

public class SearchUserActivity extends RtcBaseActivity {

    RecyclerView rvList;

    LiveUserAdapterNew liveUserAdapterNew;

    ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

    EditText etSearch;

    String type = "", channelName="", liveId="", mUserId="", user="";

    static boolean isBroadCaster = true, isGaming = false, mIsInChat=false;

    PrefHandler prefHandler;

    SessionManager sessionManager;

    private RtmClient mRtmClient;

    private static final int PERMISSION_REQ_CODE = 1 << 4;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        inits();

    }

    private void inits() {

        etSearch = findViewById(R.id.etSearch);

        rvList = findViewById(R.id.rvList);
        rvList.setLayoutManager(new GridLayoutManager(this, 2));

        liveUserAdapterNew = new LiveUserAdapterNew(arrayList, SearchUserActivity.this, "search");
        rvList.setAdapter(liveUserAdapterNew);

        type = getIntent().getStringExtra("type");

        sessionManager = new SessionManager(this);

        user = "@" + sessionManager.getUser().getData().getUserName();

        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("custom-message"));

        findViewById(R.id.ivBack).setOnClickListener(view -> onBackPressed());

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                hitSearchApi();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void hitSearchApi() {

        if (AppUtils.isNetworkAvailable(this)) {

          //  //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.search_live_users;
            Log.v("postApi-token", Global.ACCESS_TOKEN);
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("type", type)
                    .addUrlEncodeFormBodyParameter("keyword", etSearch.getText().toString().trim())
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.v("postApi-url", String.valueOf(response));
                       //     //AppUtils.hideDialog();
                            parseLiveList(response);

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-url", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorBody()));
                         //   //AppUtils.hideDialog();
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(getApplicationContext(), "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseLiveList(JSONObject response) {

        arrayList.clear();

        try {
            if (response.getBoolean("status")) {

                JSONArray jsonArray = response.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("post_hash_tag", jsonObject.getString("post_hash_tag"));
                    hashMap.put("live_start", jsonObject.getString("live_start"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                    hashMap.put("coinsReceived", jsonObject.getString("coinsReceived"));
                    hashMap.put("coinsReceivedSilver", jsonObject.getString("coinsReceivedSilver"));
                    hashMap.put("viewers_count", jsonObject.getString("viewers_count"));

                    arrayList.add(hashMap);
                }

            } /*else
                Toast.makeText(getApplicationContext(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        liveUserAdapterNew.notifyDataSetChanged();

   }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent

            isBroadCaster = intent.getBooleanExtra("isBroadCaster", true);
            isGaming = intent.getBooleanExtra("isGaming", false);
            channelName = "@" + intent.getStringExtra("channelName");
            liveId = intent.getStringExtra("live_id");
            Log.v("ljhljhl", isBroadCaster + " " + isGaming + " " + channelName);
            onJoinAsAudienceClicked(isGaming, channelName);
        }
    };

    public void onJoinAsAudienceClicked(boolean isGaming, String channelName) {
        isBroadCaster = false;
        this.isGaming = isGaming;
        LiveUsersActivity.channelName = channelName;
        if (isGaming) {
            doAlertCost("Join");
        } else {
            checkPermission();
        }
    }

    private void doAlertCost(String joinOrHost) {

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchUserActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("It will cost you " + prefHandler.getFollowersThreshold() + " Gold Coins to " + joinOrHost + " this game");
        builder.setPositiveButton(joinOrHost, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                if (currentWalletBalance >= prefHandler.getFollowersThreshold()) {
                    checkPermission();
                } else {
                    doAlertCoins();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void doAlertCoins() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SearchUserActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough coins to join.");
        builder.setPositiveButton("Add Coins", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    private void doLogin() {
        mIsInChat = true;
        mUserId = user;
       // AppUtils.showRequestDialog(SearchUserActivity.this);

        mRtmClient.login(null, mUserId, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

         //       //AppUtils.hideDialog();
                Log.i("postApi", "login success");
                runOnUiThread(() -> {
                    gotoRoleActivity();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
             //   //AppUtils.hideDialog();
                runOnUiThread(() -> {
                    mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    public void gotoRoleActivity() {

        if (isGaming)
            gotoGameActivity();
        else if (isBroadCaster) {
            //hitGolLiveApi();

        } else
            gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoGameActivity() {

        //PrefHandler prefHandler = new PrefHandler(BroadcastActivity.this);

        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - prefHandler.getFollowersThreshold();
        Log.v("paras", "paras currentWalletBalance = " + currentWalletBalance);
        Log.v("paras", "paras finalBalane = " + finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins("" + finalBalane);
        sessionManager.saveUser(user);

       // updateWallet(SearchUserActivity.this, "" + finalBalane);

    }

    private void gotoLiveActivity(int role) {

        String room = channelName;
        String userId = user;
        config().setChannelName(room);

        Intent intent = new Intent(getIntent());
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        intent.putExtra("liveId", liveId);

        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsInChat) {
            doLogout();
        }
    }

    private void doLogout() {
        mRtmClient.logout(null);
        MessageUtil.cleanMessageListBeanList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}