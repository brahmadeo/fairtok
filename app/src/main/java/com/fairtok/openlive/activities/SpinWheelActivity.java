package com.fairtok.openlive.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.MyPerformanceArrayAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.bean.ChatListPOJO;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.customview.BottomSheetListView;
import com.fairtok.model.LiveUserBean;
import com.fairtok.model.user.User;
import com.fairtok.openlive.bottomsheet.AbstractActionSheet;
import com.fairtok.openlive.bottomsheet.GiftActionSheet;
import com.fairtok.openlive.bottomsheet.GiftAnimWindow;
import com.fairtok.openlive.chatting.adapter.MessageAdapter;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.chatting.model.MessageListBean;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.stats.LocalStatsData;
import com.fairtok.openlive.stats.RemoteStatsData;
import com.fairtok.openlive.stats.StatsData;
import com.fairtok.openlive.ui.VideoGridContainer;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannel;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmMessageType;
import io.agora.rtm.RtmStatusCode;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import rubikstudio.library.LuckyWheelView;
import rubikstudio.library.model.LuckyItem;

public class SpinWheelActivity extends RtcBaseActivity implements CompleteTaskListner, GiftActionSheet.GiftActionSheetListener, AbstractActionSheet.AbsActionSheetListener {
    private static final String TAG = SpinWheelActivity.class.getSimpleName();

    private VideoGridContainer mVideoGridContainer;
    private ImageView mMuteAudioBtn;
    private ImageView mMuteVideoBtn,switchCameraBtn,giftBtn;

    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;

    //https://github.com/AgoraIO/RTM/tree/master/Agora-RTM-Tutorial-Android  For Chat

    private EditText mMsgEditText;
    private ImageView mBigImage;
    private RecyclerView mRecyclerView;
    private List<MessageBean> mMessageBeanList = new ArrayList<>();
    private MessageAdapter mMessageAdapter;

    private boolean mIsPeerToPeerMode = false;
    private String mUserId = "";
    private String mPeerId = "";
    private String mChannelName = "";
    private int mChannelMemberCount = 1;

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;
    TextView totalViewers;
    String profilePic,name;
    SessionManager sessionManager;

    ArrayList<ChatListPOJO> arrayList;

    CircleImageView hostImage;

    int[] mGiftValues;
    int currentWalletBalance=0;

    List<RtmChannelMember> memberList = new ArrayList<>();
    List<LiveUserBean.PostDataBean.DataArrBean> list;
    LuckyWheelView luckyWheelView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spin_wheel);

        totalViewers = findViewById(R.id.tvViewers);
        sessionManager = new SessionManager(this);
        profilePic = sessionManager.getUser().getData().getUserProfile();
        name = sessionManager.getUser().getData().getFullName();
        mGiftValues = getResources().getIntArray(R.array.gift_values);
        currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getMyWallet());

        initUI();
        initSpinWheel();
    }

    List<LuckyItem> data = new ArrayList<>();

    private void initSpinWheel()
    {
        luckyWheelView = (LuckyWheelView) findViewById(R.id.luckyWheel);

        LuckyItem luckyItem1 = new LuckyItem();
        luckyItem1.topText = "100";
        luckyItem1.icon = R.drawable.test1;
        luckyItem1.color = 0xffFFF3E0;
        data.add(luckyItem1);

        LuckyItem luckyItem2 = new LuckyItem();
        luckyItem2.topText = "200";
        luckyItem2.icon = R.drawable.test2;
        luckyItem2.color = 0xffFFE0B2;
        data.add(luckyItem2);

        LuckyItem luckyItem3 = new LuckyItem();
        luckyItem3.topText = "300";
        luckyItem3.icon = R.drawable.test3;
        luckyItem3.color = 0xffFFCC80;
        data.add(luckyItem3);

        //////////////////
        LuckyItem luckyItem4 = new LuckyItem();
        luckyItem4.topText = "400";
        luckyItem4.icon = R.drawable.test4;
        luckyItem4.color = 0xffFFF3E0;
        data.add(luckyItem4);

        LuckyItem luckyItem5 = new LuckyItem();
        luckyItem5.topText = "500";
        luckyItem5.icon = R.drawable.test5;
        luckyItem5.color = 0xffFFE0B2;
        data.add(luckyItem5);

        LuckyItem luckyItem6 = new LuckyItem();
        luckyItem6.topText = "600";
        luckyItem6.icon = R.drawable.test6;
        luckyItem6.color = 0xffFFCC80;
        data.add(luckyItem6);
        //////////////////

        //////////////////////
        LuckyItem luckyItem7 = new LuckyItem();
        luckyItem7.topText = "700";
        luckyItem7.icon = R.drawable.test7;
        luckyItem7.color = 0xffFFF3E0;
        data.add(luckyItem7);

        LuckyItem luckyItem8 = new LuckyItem();
        luckyItem8.topText = "800";
        luckyItem8.icon = R.drawable.test8;
        luckyItem8.color = 0xffFFE0B2;
        data.add(luckyItem8);


        LuckyItem luckyItem9 = new LuckyItem();
        luckyItem9.topText = "900";
        luckyItem9.icon = R.drawable.test9;
        luckyItem9.color = 0xffFFCC80;
        data.add(luckyItem9);
        ////////////////////////

        LuckyItem luckyItem10 = new LuckyItem();
        luckyItem10.topText = "1000";
        luckyItem10.icon = R.drawable.test10;
        luckyItem10.color = 0xffFFE0B2;
        data.add(luckyItem10);

        LuckyItem luckyItem11 = new LuckyItem();
        luckyItem11.topText = "2000";
        luckyItem11.icon = R.drawable.test10;
        luckyItem11.color = 0xffFFE0B2;
        data.add(luckyItem11);

        LuckyItem luckyItem12 = new LuckyItem();
        luckyItem12.topText = "3000";
        luckyItem12.icon = R.drawable.test10;
        luckyItem12.color = 0xffFFE0B2;
        data.add(luckyItem12);

        /////////////////////

        luckyWheelView.setData(data);
        luckyWheelView.setRound(5);

        /*luckyWheelView.setLuckyWheelBackgrouldColor(0xff0000ff);
        luckyWheelView.setLuckyWheelTextColor(0xffcc0000);
        luckyWheelView.setLuckyWheelCenterImage(getResources().getDrawable(R.drawable.icon));
        luckyWheelView.setLuckyWheelCursorImage(R.drawable.ic_cursor);*/

        luckyWheelView.setTouchEnabled(false);


        findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLiveUserData();

            }
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                Toast.makeText(getApplicationContext(), data.get(index).topText, Toast.LENGTH_SHORT).show();
                // make current user out with won amount call and turn to next player
                if(isBroadcaster) {
                    String msg = list.get(Utils.playerNo-1).getFullname() + " has won " + data.get(index).topText + " Coins.";
                    sendChatMsg(msg);
                    animateGiftGif(7);
                    sendChatMsg("RRRRRR");
                    if(Utils.playerNo<list.size()) {
                        turnOutUser(data.get(index).topText, list.get(Utils.playerNo-1).getUser_id());
                    }
                    else
                    {
                        updateWonAmount(data.get(index).topText, list.get(Utils.playerNo-1).getUser_id());
                        sendChatMsg("Game is completed. Please host a new game again if you would like to play another round");
                        Utils.playerNo=0;
                    }

                }
            }
        });
    }

    public void spinWheel()
    {
        if(Utils.playerNo<list.size()) {

            if(Utils.playerNo==0){
                spinNow();
            }
            else {

                sendChatMsg("Next round of spinning the wheel will be started in 10 seconds");

                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        spinNow();
                    }
                }, 10000);
            }

        }
        else
        {
            sendChatMsg("Game is completed. Please host a new game again if you would like to play another round");
            Utils.playerNo=0;
        }
    }

    private void spinNow()
    {
        int index = getRandomIndex();
        sendChatMsg("$SpinWheel ," + index);
        luckyWheelView.startLuckyWheelWithTargetIndex(index);

        sendChatMsg("Spinning the wheel for " + list.get(Utils.playerNo).getFullname());

        if (Utils.playerNo < list.size())
            Utils.playerNo++;
    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(data.size() - 1) + 0;
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }

    static boolean isBroadcaster=false;

    public void sendChatMsg(String msg)
    {
        RtmMessage message = mRtmClient.createMessage();
        message.setText(msg);

        try {
            byte[] profilePic_bytes = profilePic.getBytes("UTF-8");
            message.setRawMessage(profilePic_bytes);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        if(message.getText().matches("$SpinWheel") || message.getText().matches("RRRRRR")){

        }
        else {
            MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            mMessageBeanList.add(messageBean);
            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
            mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
        }
        sendChannelMessage(message);
    }

    private void initUI() {
        TextView roomName = findViewById(R.id.live_room_name);
        roomName.setText(config().getChannelName());
        roomName.setSelected(true);
        
        initUserIcon();

        hostImage = findViewById(R.id.live_name_board_icon);
        Glide.with(this).load(Const.ITEM_BASE_URL+Utils.hostProfilePic).into(hostImage);

        mMuteVideoBtn = findViewById(R.id.live_btn_mute_video);
        mMuteVideoBtn.setActivated(isBroadcaster);

        mMuteAudioBtn = findViewById(R.id.live_btn_mute_audio);
        mMuteAudioBtn.setActivated(isBroadcaster);

        ImageView beautyBtn = findViewById(R.id.live_btn_beautification);
        beautyBtn.setActivated(true);
        rtcEngine().setBeautyEffectOptions(beautyBtn.isActivated(), com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS);

        mVideoGridContainer = findViewById(R.id.live_video_grid_layout);
        mVideoGridContainer.setStatsManager(statsManager());
        switchCameraBtn = findViewById(R.id.live_btn_switch_camera);
        giftBtn = findViewById(R.id.selection_img_btn);

        if(config().getChannelName().matches("@"+sessionManager.getUser().getData().getUserName()))
        {
            isBroadcaster = true;
            becomesHost(false,false);

            mMuteVideoBtn.setVisibility(View.VISIBLE);
            mMuteAudioBtn.setVisibility(View.VISIBLE);
            beautyBtn.setVisibility(View.VISIBLE);
            switchCameraBtn.setVisibility(View.VISIBLE);
            giftBtn.setVisibility(View.GONE);
        }
        else
        {
            isBroadcaster = false;

            mMuteVideoBtn.setVisibility(View.GONE);
            mMuteAudioBtn.setVisibility(View.GONE);
            beautyBtn.setVisibility(View.GONE);
            switchCameraBtn.setVisibility(View.GONE);
            giftBtn.setVisibility(View.VISIBLE);

            becomeAudience();
        }

        mMuteVideoBtn.setActivated(isBroadcaster);

        init();

        findViewById(R.id.liveUsers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getPartyMembers();
            }

        });

        mVideoDimension = com.fairtok.openlive.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];

    }


    private void becomesHost(boolean audioMuted, boolean videoMuted) {

        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalVideoStream(true);
        startBroadcast();
    }


    private void becomeAudience() {
        findViewById(R.id.play).setVisibility(View.GONE);
        isBroadcaster = false;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalVideoStream(true);
        startBroadcast();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isBroadcaster)
        {
            makeHostLiveOnServer("1");
        }
        else
        {
            makeHostLiveOnServer("0");
        }
    }

    public void makeHostLiveOnServer(String isHost)
    {
        try {
            PrefHandler pref = new PrefHandler(this);
            SessionManager sessionManager = new SessionManager(this);

            Log.v("paras","paras making the host live user id = "+pref.getuId());

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeonline"));
            params.add(new Pair<>("userid", pref.getuId()));
            params.add(new Pair<>("isGaming", "1"));
            params.add(new Pair<>("username", sessionManager.getUser().getData().getUserName()));

            params.add(new Pair<>("fullname", sessionManager.getUser().getData().getFullName()));
            params.add(new Pair<>("photo", sessionManager.getUser().getData().getUserProfile()));
            params.add(new Pair<>("isHost", isHost));
            params.add(new Pair<>("party_id", Utils.party_id));

            new AsyncHttpsRequest("", this, params, this, 111, false).execute(Utils.MakeHostOnline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void makeHostOfflineOnServer() {
        try {
            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", Utils.party_id));

            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeHostOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPartyMembers()
    {
        PrefHandler pref = new PrefHandler(SpinWheelActivity.this);

        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("party_id",Utils.party_id));

        new AsyncHttpsRequest("", this, params, this, 10, false).execute(Utils.GET_PARTY_MEMBERS);
    }

    private void initUserIcon() {
        Bitmap origin = BitmapFactory.decodeResource(getResources(), R.drawable.fake_user_icon);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), origin);
        drawable.setCircular(true);
        ImageView iconView = findViewById(R.id.live_name_board_icon);
        iconView.setImageDrawable(drawable);
    }

    @Override
    protected void onGlobalLayoutCompleted() {
        RelativeLayout topLayout = findViewById(R.id.live_room_top_layout);
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) topLayout.getLayoutParams();
        params.height = mStatusBarHeight + topLayout.getMeasuredHeight();
        topLayout.setLayoutParams(params);
        topLayout.setPadding(0, mStatusBarHeight, 0, 0);
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        SurfaceView surface = prepareRtcVideo(0, true);
        mVideoGridContainer.addUserVideoSurface(0, surface, true);
        mMuteAudioBtn.setActivated(true);
        removeRtcVideo(0, true);
        mVideoGridContainer.removeUserVideo(0, true);
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        removeRtcVideo(0, true);
        mVideoGridContainer.removeUserVideo(0, true);
        mMuteAudioBtn.setActivated(false);
    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment

       // showToast("New User Joined");
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        mVideoGridContainer.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        mVideoGridContainer.removeUserVideo(uid, false);
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        finish();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(), com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS);
    }


    @Override
    protected void onStop() {
        super.onStop();
        makeHostOfflineOnServer();
    }

    public void onMoreClicked(View view) {
        // Do nothing at the moment
    }

    public void onPushStreamClicked(View view) {
        // Do nothing at the moment
    }

    public void onMuteAudioClicked(View view) {
        if (!mMuteVideoBtn.isActivated()) return;

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
            //startVideo();
        }
        view.setActivated(!view.isActivated());
        rtcEngine().muteLocalVideoStream(view.isActivated());
    }


    // Chat code start

    private void init() {
        mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();
        mClientListener = new MyRtmClientListener();
        mChatManager.registerListener(mClientListener);

        Intent intent = getIntent();
        mIsPeerToPeerMode = intent.getBooleanExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        mUserId = intent.getStringExtra(MessageUtil.INTENT_EXTRA_USER_ID);
        String targetName = intent.getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME);


        if (mIsPeerToPeerMode) {
            mPeerId = targetName;


            // load history chat records
            MessageListBean messageListBean = MessageUtil.getExistMessageListBean(mPeerId);
            if (messageListBean != null) {
                mMessageBeanList.addAll(messageListBean.getMessageBeanList());
            }

            // load offline messages since last chat with this peer.
            // Then clear cached offline messages from message pool
            // since they are already consumed.
            MessageListBean offlineMessageBean = new MessageListBean(mPeerId, (List<MessageBean>) mChatManager);
            mMessageBeanList.addAll(offlineMessageBean.getMessageBeanList());
            mChatManager.removeAllOfflineMessages(mPeerId);
        } else {
            mChannelName = targetName;
            mChannelMemberCount = 1;
            createAndJoinChannel();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mMessageAdapter = new MessageAdapter(this, mMessageBeanList, message -> {
            if (message.getMessage().getMessageType() == RtmMessageType.IMAGE) {
                if (!TextUtils.isEmpty(message.getCacheFile())) {
                    Glide.with(this).load(message.getCacheFile()).into(mBigImage);
                    mBigImage.setVisibility(View.VISIBLE);
                } else {
                    ImageUtil.cacheImage(this, mRtmClient, (RtmImageMessage) message.getMessage(), new ResultCallback<String>() {
                        @Override
                        public void onSuccess(String file) {
                            message.setCacheFile(file);
                            runOnUiThread(() -> {
                                Glide.with(SpinWheelActivity.this).load(file).into(mBigImage);
                                mBigImage.setVisibility(View.VISIBLE);
                            });
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
        mRecyclerView = findViewById(R.id.message_list);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mMessageAdapter);

        mMsgEditText = findViewById(R.id.message_edittiext);
        mBigImage = findViewById(R.id.big_image);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        leaveAndReleaseChannel();
        mChatManager.unregisterListener(mClientListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                final String file = resultUri.getPath();
                ImageUtil.uploadImage(this, mRtmClient, file, new ResultCallback<RtmImageMessage>() {
                    @Override
                    public void onSuccess(final RtmImageMessage rtmImageMessage) {
                        runOnUiThread(() -> {


                            try {
                                byte[] profilePic_bytes = profilePic.getBytes("UTF-8");
                                rtmImageMessage.setRawMessage(profilePic_bytes);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }


                            MessageBean messageBean = new MessageBean(mUserId, rtmImageMessage, true,profilePic);
                            messageBean.setCacheFile(file);
                            mMessageBeanList.add(messageBean);
                            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                            mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);

                            sendChannelMessage(rtmImageMessage);

                        });
                    }

                    @Override
                    public void onFailure(ErrorInfo errorInfo) {

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.selection_chat_btn:
                String msg = mMsgEditText.getText().toString();
                if (!msg.equals("")) {
                    RtmMessage message = mRtmClient.createMessage();
                    message.setText(msg);

                    try {
                        byte[] profilePic_bytes = profilePic.getBytes("UTF-8");
                        message.setRawMessage(profilePic_bytes);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    MessageBean messageBean = new MessageBean(mUserId, message, true,profilePic);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
                    sendChannelMessage(message);

                }
                mMsgEditText.setText("");
                break;
            case R.id.selection_img_btn:
//                CropImage.activity().start(this);

                showActionSheetDialog(123, false, true, this);
                break;
            case R.id.big_image:
                mBigImage.setVisibility(View.GONE);
                break;
        }
    }

    public void onClickFinish(View v) {
        finish();
    }

    /**
     * API CALL: create and join channel
     */
    private void createAndJoinChannel() {
        // step 1: create a channel instance
        mRtmChannel = mRtmClient.createChannel(mChannelName, new MyChannelListener());
        if (mRtmChannel == null) {
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        Log.e("channel", mRtmChannel + "");

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i(TAG, "join channel success");
                getChannelMemberList();
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e(TAG, "join channel failed4");
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });
    }

    /**
     * API CALL: get channel member list
     */
    private void getChannelMemberList() {
        memberList = new ArrayList<>();
        mRtmChannel.getMembers(new ResultCallback<List<RtmChannelMember>>() {
            @Override
            public void onSuccess(final List<RtmChannelMember> responseInfo) {
                runOnUiThread(() -> {
                    memberList = responseInfo;
                    mChannelMemberCount = responseInfo.size();
                    refreshChannelTitle();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e(TAG, "failed to get channel members, err: " + errorInfo.getErrorCode());
            }
        });
    }

    /**
     * API CALL: send message to a channel
     */
    private void sendChannelMessage(RtmMessage message) {
        mRtmChannel.sendMessage(message, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.ChannelMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                    }
                });
            }
        });
    }

    /**
     * API CALL: leave and release channel
     */
    private void leaveAndReleaseChannel() {
        if (mRtmChannel != null) {
            mRtmChannel.leave(null);
            mRtmChannel.release();
            mRtmChannel = null;
        }
    }

    public void turnOutUser(String amount,String userId)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag","update"));
        params.add(new Pair<>("party_id",Utils.party_id));
        params.add(new Pair<>("amount",amount));
        params.add(new Pair<>("userId",userId));
        new AsyncHttpsRequest("Loading...", SpinWheelActivity.this, params, SpinWheelActivity.this, 1, false).execute(Utils.UPDATE_WON_AMOUNT);
    }

    public void updateWonAmount(String amount,String userId)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag","update"));
        params.add(new Pair<>("party_id",Utils.party_id));
        params.add(new Pair<>("amount",amount));
        params.add(new Pair<>("userId",userId));
        new AsyncHttpsRequest("", SpinWheelActivity.this, params, SpinWheelActivity.this, 11, false).execute(Utils.UPDATE_WON_AMOUNT);
    }

    public void getLiveUserData()
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("party_id",Utils.party_id));
        new AsyncHttpsRequest("Loading...", SpinWheelActivity.this, params, SpinWheelActivity.this, 0, false).execute(Utils.GET_MEMBER_LIST);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras","paras response = "+result);

        if(response_code==0) {

            list = new ArrayList<>();
            int listOfIcon[] = {R.drawable.test1, R.drawable.test2, R.drawable.test3, R.drawable.test4, R.drawable.test5,R.drawable.test6, R.drawable.test7, R.drawable.test8, R.drawable.test9, R.drawable.test10};
            Random rand = new Random();
// nextInt as provided by Random is exclusive of the top value so you need to add 1
            int randomNum = rand.nextInt((10 - 1) + 1) + 1;

            try {
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {

                    Gson gson = new Gson();
                    LiveUserBean bean = gson.fromJson(result, LiveUserBean.class);
                    list = bean.getPost_data().getData_arr();

                    for(LiveUserBean.PostDataBean.DataArrBean model: list)
                    {
                        String name;
                        String input=model.getUser_name();
                        if (input.length() > 5)
                        {
                            name = input.substring(0, 5);
                        }
                        else
                        {
                            name = input;
                        }
                    }

                    if(isBroadcaster) {
                        sendChatMsg("Game host has started the game");
                        findViewById(R.id.play).setVisibility(View.GONE);
                        spinWheel();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(response_code==1)
        {
            if(Utils.playerNo<list.size())
            {
                spinWheel();
            }
            else
            {
                sendChatMsg("Game is completed. Please host a new game again if you would like to play another round");
                Utils.playerNo=0;
            }
        }
        else if(response_code==111)
        {
            try{
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {
                    Utils.party_id = obj.getString("id");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if (response_code == 10) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {


                    arrayList = new ArrayList<>();
                    JSONObject data_obj = obj.getJSONObject("post_data");
                    JSONArray jarr = data_obj.getJSONArray("data_arr");

                    for(int i=0;i<jarr.length();i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatListPOJO bean = new ChatListPOJO();
                        bean.setOpp_userid(jobj.getString("user_id"));
                        bean.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        bean.setReceiver_photo(jobj.getString("receiver_photo"));

                        arrayList.add(bean);
                    }

                    MyPerformanceArrayAdapter itemsAdapter = new MyPerformanceArrayAdapter(SpinWheelActivity.this,arrayList,false);
                    BottomSheetDialog dialog = new BottomSheetDialog(SpinWheelActivity.this);
                    dialog.setContentView(R.layout.bottom_sheet_view);
                    BottomSheetListView listView = (BottomSheetListView) dialog.findViewById(R.id.listViewBtmSheet);
                    listView.setAdapter(itemsAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        }
                    });
                    dialog.show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




    @Override
    public void onActionSheetGiftSend(String name, int index, int value) {
        dismissActionSheetDialog();
//        SendGiftRequest request = new SendGiftRequest()
//        here send coins to the creators

        currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        Log.v("paras","paras sending gift gift index = "+index);
        int coinsToSend=mGiftValues[index];

        if(currentWalletBalance>=coinsToSend) {
            String msg = "gift$$$$$-" + index;
            RtmMessage message = mRtmClient.createMessage();
            message.setText(msg);
            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
            message.setRawMessage(profilePic_bytes);
            sendChannelMessage(message);

            message.setText(getString(R.string.live_message_gift_send));
            MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            messageBean.setBackground(index);
            mMessageBeanList.add(messageBean);
            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
            mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);

            sendBubble(Utils.hostUserId, "" + coinsToSend);
            animateGiftGif(index);
        }
        else
        {
            doAlert();
        }
    }


    private void doAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(SpinWheelActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough gold coins to send this gift.");
        builder.setPositiveButton("Add Coins", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private CompositeDisposable disposable = new CompositeDisposable();
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();

    public void sendBubble(String toUserId, String coin) {

        disposable.add(Global.initRetrofit().sendCoin(Global.ACCESS_TOKEN, coin, toUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> onLoadMoreComplete.setValue(true))
                .subscribe((coinSend, throwable) -> {
                    if (coinSend != null && coinSend.getStatus() != null) {

                        try {
                            int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                            int updatedCoins = currentCoins + - Integer.parseInt(coin);
                            User user = sessionManager.getUser();
                            user.getData().setPaidCoins("" + updatedCoins);
                            sessionManager.saveUser(user);
                        }
                        catch (NumberFormatException e)
                        {
                            e.printStackTrace();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                }));
    }

    /**
     * API CALLBACK: rtm event listener
     */
    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                try {
                    profilePic = new String(message.getRawMessage(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                int backg = getMessageColor(peerId);
                String msg = message.getText();
                if(msg.contains("gift$$$$$"))
                {
                    message.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }

                if(msg.contains("$SpinWheel"))
                {

                    List<String> elephantList = Arrays.asList(msg.split(","));
                    int index = Integer.parseInt(elephantList.get(1));
                    luckyWheelView.startLuckyWheelWithTargetIndex(index);
                }
                else if (peerId.equals(mPeerId)) {

                    MessageBean messageBean = new MessageBean(peerId, message, false,profilePic);
                    messageBean.setBackground(backg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, message);
                }
            });
        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {

            String profilePic = Utils.DEFAULT_PROFILE_URL;

            try {
                profilePic = new String(rtmImageMessage.getRawMessage(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            int backg = getMessageColor(peerId);
            String msg = rtmImageMessage.getText();
            if(msg.contains("gift$$$$$"))
            {
                rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                String[] parts = msg.split("-");
                backg = Integer.parseInt(parts[1]);
                animateGiftGif(Integer.parseInt(parts[1]));
            }

            String finalProfilePic = profilePic;
            int finalBackg = backg;
            runOnUiThread(() -> {
                if (peerId.equals(mPeerId)) {
                    MessageBean messageBean = new MessageBean(peerId, rtmImageMessage, false, finalProfilePic);
                    messageBean.setBackground(finalBackg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, rtmImageMessage);
                }
            });
        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }


    }

    public void animateGiftGif(int giftId)
    {

        GiftAnimWindow window = new GiftAnimWindow(SpinWheelActivity.this, R.style.gift_anim_window);
        window.setAnimResource(GiftUtil.getGiftAnimRes(giftId));
        window.show();
    }

    /**
     * API CALLBACK: rtm channel event listener
     */
    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                try {
                    profilePic = new String(message.getRawMessage(), "UTF-8");


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String account = fromMember.getUserId();

                int backg = getMessageColor(account);
                String msg = message.getText();
                if(msg.matches("Game host has started the game"))
                {
                    getLiveUserData();
                }
                else if(msg.contains("gift$$$$$"))
                {
                    message.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }
                else if(msg.matches("RRRRRR"))
                {
                    animateGiftGif(7);
                }
                else if(msg.contains("$SpinWheel"))
                {
                    Log.v("paras","paras msg = "+msg);
                    String[] animalsArray = msg.split("\\s*,\\s*");
                    int index = Integer.parseInt(animalsArray[1]);
                    luckyWheelView.startLuckyWheelWithTargetIndex(index);
                }
                else {

                    Log.i(TAG, "onMessageReceived account = " + account + " msg = " + message);
                    MessageBean messageBean = new MessageBean(account, message, false, profilePic);
                    messageBean.setBackground(backg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
                }
            });
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                try {
                    profilePic = new String(rtmImageMessage.getRawMessage(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                String account = rtmChannelMember.getUserId();

                int backg = getMessageColor(account);
                String msg = rtmImageMessage.getText();
                if(msg.contains("gift$$$$$"))
                {
                    rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }

                Log.i(TAG, "onMessageReceived account = " + account + " msg = " + rtmImageMessage);
                MessageBean messageBean = new MessageBean(account, rtmImageMessage, false,profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                mRecyclerView.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {
            runOnUiThread(() -> {
                mChannelMemberCount++;
                refreshChannelTitle();

                showToast(member.getUserId() +" has joined the Game");
                String msg = member.getUserId() +" has joined the Game";
                sendChatMsg(msg);
            });
        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {
            runOnUiThread(() -> {
                mChannelMemberCount--;
                refreshChannelTitle();

                if(mChannelName.equalsIgnoreCase(member.getUserId())) {
                    openDialog("Host has ended the Game");
                }
                else {
                    showToast(member.getUserId() + " has left the Game");

                    String msg = member.getUserId() +" has left the Game";
                    sendChatMsg(msg);

                }

            });
        }
    }

    public void openDialog(String msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int arg1) {
                                dialog.dismiss();
                                goToMainActivity();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void goToMainActivity() {
        Intent intent = new Intent(SpinWheelActivity.this,
                MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }

    private int getMessageColor(String account) {
        for (int i = 0; i < mMessageBeanList.size(); i++) {
            if (account.equals(mMessageBeanList.get(i).getAccount())) {
                return mMessageBeanList.get(i).getBackground();
            }
        }
        return MessageUtil.COLOR_ARRAY[MessageUtil.RANDOM.nextInt(MessageUtil.COLOR_ARRAY.length)];
    }

    private void refreshChannelTitle() {
        String titleFormat = getString(R.string.channel_title);
        String title = String.format(titleFormat, mChannelName, mChannelMemberCount);
        TextView totalViewers = findViewById(R.id.tvViewers);
        totalViewers.setText(""+mChannelMemberCount);
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(SpinWheelActivity.this, text, Toast.LENGTH_SHORT).show());
    }
}
