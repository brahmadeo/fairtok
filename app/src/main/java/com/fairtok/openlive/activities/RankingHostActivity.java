package com.fairtok.openlive.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.openlive.adapter.HostRankAdapter;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.PaginationListener;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RankingHostActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private RecyclerView recyclerview;
    private HostRankAdapter hostRankAdapter;
    private ImageView imgBack, imgProfileTop, imgProfile;
    private TextView tvMyUsername, tvUserName, tvMyrank, tvRank, tvCoins;
    private String type;
    private ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
    private JSONArray jsonArray = new JSONArray();
    private PrefHandler prefHandler;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int page_number = 1;
    private int item_count = 10;
    private int total_record = 0;
    private int last_page = 0;
    private String tabSelected = "";
    private List<HashMap<String, String>> rankUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking_host);

        initView();
        setupViewPager();
    }
    private void initView(){
        tabLayout = findViewById(R.id.topTabLayout);
        imgBack = findViewById(R.id.imgBack);
        imgProfileTop = findViewById(R.id.imgProfileTop);
        imgProfile = findViewById(R.id.imgProfile);
        tvUserName = findViewById(R.id.tvUserName);
        tvMyUsername = findViewById(R.id.tvMyUsername);
        tvMyrank = findViewById(R.id.tvMyrank);
        tvRank = findViewById(R.id.tvRank);
        tvCoins = findViewById(R.id.tvCoins);

        //tabLayout.addTab(tabLayout.newTab().setText("Top Creators"));
        tabLayout.addTab(tabLayout.newTab().setText("Top Spenders"));
        tabLayout.addTab(tabLayout.newTab().setText("Top Earners"));

        prefHandler = new PrefHandler(this);

        tvUserName.setText(prefHandler.getName());
        Glide.with(this).load(Const.BASE_URL_IMAGE+ prefHandler.getLiveProfile()).into(imgProfileTop);
        //Glide.with(this).load(Const.BASE_URL_IMAGE+ prefHandler.getLiveProfile()).into(imgProfile);

        recyclerview = findViewById(R.id.recyclerview);
        imgBack.setOnClickListener(view -> {
            onBackPressed();
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);
        hostRankAdapter = new HostRankAdapter(this, rankUserList);
        recyclerview.setAdapter(hostRankAdapter);

        recyclerview.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                page_number++;
                Log.i("page_number ", page_number+"");
                hitGetLiveList();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        type = "1";
        hitGetLiveList();
    }
    private void setupViewPager(){

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                arrayList.clear();
                //hostRankAdapter.notifyDataSetChanged();
                tabSelected = "yes";
                page_number = 0;
                switch (tab.getPosition()){
                    case 0:
                        type = "1";
                        break;
                    case 1:
                        type = "2";
                        break;
                   /* case 2:
                        type = "3";
                        break;*/
                }
                hitGetLiveList();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    private class RankPagerAdapter extends FragmentPagerAdapter{


        public RankPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return 0;
        }
    }
    private void hitGetLiveList() {
        if (AppUtils.isNetworkAvailable(this)) {

            if (!tabSelected.equals(""))
                recyclerview.setVisibility(View.GONE);
            else
                recyclerview.setVisibility(View.VISIBLE);
            //AppUtils.showRequestDialog(this);
            tabSelected = "";
            String url = Const.BASE_URL + Const.LEADERBOARD;
            Log.v("postApi-token", Global.ACCESS_TOKEN);
            Log.v("postApi-url", url);

            //getAllLiveUsers();
            //testDialog("Authorization : " +Global.ACCESS_TOKEN+ " , "+ type);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("page", String.valueOf(page_number))
                    .addUrlEncodeFormBodyParameter("type", type)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.v("postApi-leader", String.valueOf(response));
                            //AppUtils.hideDialog();
                            recyclerview.setVisibility(View.VISIBLE);

                            if (rankUserList!=null){
                                rankUserList.clear();
                            }
                            /*"status": true,
                                    "message": "leaderboard list fetched",
                                    "data": [
                            {
                                "user_id": "37832",
                                    "full_name": "Ab Guru",
                                    "user_name": "abdulkidwai1994",
                                    "user_email": "abdulkidwai1994@gmail.com",
                                    "user_mobile_no": "0",
                                    "user_profile": "FairtokUserPic_37832.jpg",
                                    "coins": "54179"
                            },
                            */
                            try {


                                if (response.getBoolean("status")) {
                                    // get userdat
                                    String profileUrl = response.getJSONObject("myRankData").getString("user_profile");
                                    Glide.with(RankingHostActivity.this).load(Const.BASE_URL_IMAGE+ profileUrl).into(imgProfile);
                                    tvMyrank.setText(response.getJSONObject("myRankData").getString("rank"));
                                    tvMyUsername.setText(response.getJSONObject("myRankData").getString("full_name"));
                                    tvRank.setText(response.getJSONObject("myRankData").getString("rank"));
                                    tvCoins.setText(response.getJSONObject("myRankData").getString("coins"));

                                    JSONArray jsonArray = response.getJSONArray("data");
                                    for (int i=0; i<jsonArray.length(); i++){
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        hashMap.put("full_name", jsonObject.getString("full_name"));
                                        hashMap.put("user_profile", jsonObject.getString("user_profile"));
                                        rankUserList.add(hashMap);
                                    }

                                    total_record = Integer.parseInt(String.valueOf(response.get("count")));
                                    if (total_record % 20 == 0){
                                        last_page = total_record/20;
                                    }else {
                                        last_page = total_record/20 + 1;
                                    }


                                    //Log.i("page_number", page_number+"");
                                    if (isLoading){
                                        hostRankAdapter.loadMore(rankUserList);
                                    }else {
                                        hostRankAdapter.updateData(rankUserList);
                                    }

                                }
                                // check weather is last page or not
                                if (page_number < last_page) {
                                    isLastPage = false;
                                } else {
                                    isLastPage = true;
                                }
                                isLoading = false;

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //parseLiveList(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            //testDialog(anError.getErrorCode()+" == "+anError.getErrorBody()+" === "+anError.getErrorDetail());
                            Log.v("postApi-url", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorBody()));
                            //AppUtils.hideDialog();
                            Toast.makeText(RankingHostActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(RankingHostActivity.this, "No internet available", Toast.LENGTH_SHORT).show();

    }
    private String getRankString(String rank){
        if (rank.equals("1")){
            return "st";
        }else if (rank.equals("2")){
            return "nd";
        }else if (rank.equals("3")){
            return "rd";
        }
        return "";
    }
    private void parseLiveList(JSONObject response) {

        arrayList.clear();

        try {
            if (response.getBoolean("status")) {

                jsonArray = response.getJSONArray("data");
                //hostRankAdapter.updateData(jsonArray);
                /*for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("post_hash_tag", jsonObject.getString("post_hash_tag"));
                    hashMap.put("live_start", jsonObject.getString("live_start"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                    hashMap.put("coinsReceived", jsonObject.getString("coinsReceived"));
                    hashMap.put("coinsReceivedSilver", jsonObject.getString("coinsReceivedSilver"));
                    hashMap.put("viewers_count", jsonObject.getString("viewers_count"));

                    arrayList.add(hashMap);

                }*/

            } /*else
                Toast.makeText(LiveUsersActivity.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

        hostRankAdapter.notifyDataSetChanged();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}