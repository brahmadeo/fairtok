package com.fairtok.openlive.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.model.user.User;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;

import java.util.ArrayList;
import java.util.List;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;

public class BroadcastActivity extends BaseActivity implements CompleteTaskListner {
    private static final String TAG = BroadcastActivity.class.getSimpleName();
    private static final int MIN_INPUT_METHOD_HEIGHT = 200;
    private static final int ANIM_DURATION = 200;



    private Rect mVisibleRect = new Rect();
    private int mLastVisibleHeight = 0;
    private RelativeLayout mBodyLayout;
    private int mBodyDefaultMarginTop;
    private EditText mTopicEdit,user_edit;
    private TextView mStartBtn;
    TextView tvFollowersNote;

    // Permission request code of any integer value
    private static final int PERMISSION_REQ_CODE = 1 << 4;

    private String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String mUserId;
    private RtmClient mRtmClient;
    private boolean mIsInChat = false;

    SessionManager sessionManager;
    PrefHandler prefHandler;

    static boolean isBroadCaster=true;
    static String channelName;
    boolean isGaming=false;



    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            // Do nothing
        }

        @Override
        public void afterTextChanged(Editable editable) {
            mStartBtn.setEnabled(!TextUtils.isEmpty(editable));
        }
    };

    private ViewTreeObserver.OnGlobalLayoutListener mLayoutObserverListener =
            new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    checkInputMethodWindowState();
                }
            };

    private void checkInputMethodWindowState() {
        getWindow().getDecorView().getRootView().getWindowVisibleDisplayFrame(mVisibleRect);
        int visibleHeight = mVisibleRect.bottom - mVisibleRect.top;
        if (visibleHeight == mLastVisibleHeight) return;

        boolean inputShown = mDisplayMetrics.heightPixels - visibleHeight > MIN_INPUT_METHOD_HEIGHT;
        mLastVisibleHeight = visibleHeight;

        // Log.i(TAG, "onGlobalLayout:" + inputShown +
        //        "|" + getWindow().getDecorView().getRootView().getViewTreeObserver());

        // There is no official way to determine whether the
        // input method dialog has already shown.
        // This is a workaround, and if the visible content
        // height is significantly less than the screen height,
        // we should know that the input method dialog takes
        // up some screen space.
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);

        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

        tvFollowersNote = findViewById(R.id.tvFollowersNote);

        sessionManager = new SessionManager(this);
        prefHandler = new PrefHandler(this);

        isBroadCaster = getIntent().getBooleanExtra("isBroadCaster",true);
        isGaming = getIntent().getBooleanExtra("isGaming",false);
        channelName = getIntent().getStringExtra("channelName");

        initUI();

    }

    private void initUI() {
        mBodyLayout = findViewById(R.id.middle_layout);
        mTopicEdit = findViewById(R.id.topic_edit);
        mTopicEdit.addTextChangedListener(mTextWatcher);
        user_edit = findViewById(R.id.user_edit);
        mStartBtn = findViewById(R.id.start_broadcast_button);
        mTopicEdit.setText("@"+channelName);
        user_edit.setText("@"+sessionManager.getUser().getData().getUserName());
        tvFollowersNote.setText("You must have "+prefHandler.getFollowersThreshold()+" followers to host live streaming");

        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(isBroadCaster)
        {
            findViewById(R.id.brodcasterLayout).setVisibility(View.VISIBLE);
            findViewById(R.id.audienceLayout).setVisibility(View.GONE);
        }
        else
        {
            findViewById(R.id.brodcasterLayout).setVisibility(View.GONE);
            findViewById(R.id.audienceLayout).setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onGlobalLayoutCompleted() {
        //adjustViewPositions();
    }

    private void adjustViewPositions() {
        // Setting btn move downward away the status bar
//        ImageView settingBtn = findViewById(R.id.setting_button);
//        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) settingBtn.getLayoutParams();
//        param.topMargin += mStatusBarHeight;
//        settingBtn.setLayoutParams(param);
//
//        // Logo is 0.48 times the screen width
//        // ImageView logo = findViewById(R.id.main_logo);
//        param = (RelativeLayout.LayoutParams) mLogo.getLayoutParams();
//        int size = (int) (mDisplayMetrics.widthPixels * 0.48);
//        param.width = size;
//        param.height = size;
//        mLogo.setLayoutParams(param);
//
//        // Bottom margin of the main body should be two times it's top margin.
//        param = (RelativeLayout.LayoutParams) mBodyLayout.getLayoutParams();
//        param.topMargin = (mDisplayMetrics.heightPixels -
//                mBodyLayout.getMeasuredHeight() - mStatusBarHeight) / 3;
//        mBodyLayout.setLayoutParams(param);
//        mBodyDefaultMarginTop = param.topMargin;
//
//        // The width of the start button is roughly 0.72
//        // times the width of the screen
//        mStartBtn = findViewById(R.id.start_broadcast_button);
//        param = (RelativeLayout.LayoutParams) mStartBtn.getLayoutParams();
//        param.width = (int) (mDisplayMetrics.widthPixels * 0.72);
//        mStartBtn.setLayoutParams(param);
    }

    public void onSettingClicked(View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void onStartBroadcastClicked(View view) {

            isGaming = false;
            checkPermission();

    }

    public void onStartGameClicked(View view) {

            isGaming = true;
            doAlertCost("Host");

    }

    private void doAlertCost(String joinOrHost) {

        AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("It will cost you "+prefHandler.getFollowersThreshold()+" Paid Coins to "+joinOrHost+" this game");
        builder.setPositiveButton(joinOrHost, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                if(currentWalletBalance>=prefHandler.getFollowersThreshold()) {
                    checkPermission();
                }
                else
                {
                    doAlertCoins();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void doAlertCoins() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BroadcastActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough coins to join.");
        builder.setPositiveButton("Add Coins", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void onJoinAsAudienceClicked(View view) {
        if(isGaming) {
            doAlertCost("Join");
        }
        else
        {
            checkPermission();
        }

    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            resetLayoutAndForward();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                resetLayoutAndForward();
            } else {
                toastNeedPermissions();
            }
        }
    }

    private void resetLayoutAndForward() {
        closeImeDialogIfNeeded();
        doLogin();
    }

    private void closeImeDialogIfNeeded() {
        InputMethodManager manager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(mTopicEdit.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void gotoRoleActivity() {

        if(isGaming)
            gotoGameActivity();
        else if(isBroadCaster)
            gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
        else
            gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    public void onJoinAsBroadcaster(View view) {
        gotoLiveActivity(Constants.CLIENT_ROLE_BROADCASTER);
    }

    public void onJoinAsAudience(View view) {
        gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE);
    }

    private void gotoLiveActivity(int role) {

        String room = mTopicEdit.getText().toString();
        String userId=user_edit.getText().toString();
        config().setChannelName(room);

        Intent intent = new Intent(getIntent());
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, role);
        intent.setClass(getApplicationContext(), LiveActivity.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    private void gotoGameActivity() {

        //PrefHandler prefHandler = new PrefHandler(BroadcastActivity.this);

        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - prefHandler.getFollowersThreshold();
        Log.v("paras","paras currentWalletBalance = "+currentWalletBalance);
        Log.v("paras","paras finalBalane = "+finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins(""+finalBalane);
        sessionManager.saveUser(user);

        updateWallet(BroadcastActivity.this,""+finalBalane);

    }

    public void updateWallet(Context context, String balance) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "update"));
        params.add(new Pair<>("balance", balance));
        params.add(new Pair<>("userId", prefHandler.getuId()));

        new AsyncHttpsRequest("Wait...", context, params, this, 0, false).execute(Utils.UPDATE_BALANCE);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if(response_code == 0)
        {
            try
            {
                //JSONObject obj = new JSONObject(result);
                String room = mTopicEdit.getText().toString();
                String userId=user_edit.getText().toString();
                config().setChannelName(room);

                Intent intent = new Intent(getIntent());
                intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_BROADCASTER);
                intent.setClass(getApplicationContext(), SpinWheelActivity.class);
                intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
                intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, config().getChannelName());
                intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
                startActivity(intent);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }

    }


    private void toastNeedPermissions() {
        Toast.makeText(this, R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetUI();
        registerLayoutObserverForSoftKeyboard();

        mStartBtn.setEnabled(true);
        if (mIsInChat) {
            doLogout();
        }
    }

    private void resetUI() {
        resetLogo();
        closeImeDialogIfNeeded();
    }

    private void resetLogo() {
        mBodyLayout.setY(mBodyDefaultMarginTop);
    }

    private void registerLayoutObserverForSoftKeyboard() {
        View view = getWindow().getDecorView().getRootView();
        ViewTreeObserver observer = view.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(mLayoutObserverListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeLayoutObserverForSoftKeyboard();
    }

    private void removeLayoutObserverForSoftKeyboard() {
        View view = getWindow().getDecorView().getRootView();
        view.getViewTreeObserver().removeOnGlobalLayoutListener(mLayoutObserverListener);
    }

    // chat part
    /**
     * API CALL: login RTM server
     */
    private void doLogin() {
        mIsInChat = true;
        mUserId = user_edit.getText().toString();
        mRtmClient.login(null, mUserId, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i(TAG, "login success");
                runOnUiThread(() -> {
                    gotoRoleActivity();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i(TAG, "login failed: " + errorInfo.getErrorCode());
                runOnUiThread(() -> {
                    mStartBtn.setEnabled(true);
                    mIsInChat = false;
                    showToast("Joining Failed");
                });
                if(errorInfo.getErrorCode()==8)
                {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    /**
     * API CALL: logout from RTM server
     */
    private void doLogout() {
        mRtmClient.logout(null);
        MessageUtil.cleanMessageListBeanList();
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
