package com.fairtok.openlive.utils;


import com.fairtok.R;

public class GiftUtil {
    public static final int[] GIFT_ICON_RES = {
            R.drawable.gift_01_balloon,
            R.drawable.gift_11_hello,
            R.drawable.gift_27_popcorn,
            R.drawable.gift_21_mask,
            R.drawable.gift_34_rose,
            R.drawable.gift_19_kiss,
            R.drawable.gift_36_star,
            R.drawable.gift_23_wand,
            R.drawable.gift_13_heart,
            R.drawable.gift_38_teddy,
            R.drawable.gift_05_chocolate,
            R.drawable.gift_32_rainbow,
            R.drawable.gift_17_ice_cream,
            R.drawable.gift_15_hug,
            R.drawable.gift_25_money,
            R.drawable.gift_07_crown,
            R.drawable.gift_09_gems,
            R.drawable.gift_03_car,
            R.drawable.gift_30_rocket,
            R.drawable.gift_29_ring

           /* R.drawable.gift_02_balloon,
            R.drawable.gift_04_car,
            R.drawable.gift_06_chocolate,
            R.drawable.gift_08_crown,
            R.drawable.gift_10_gems,
            R.drawable.gift_12_hello,
            R.drawable.gift_14_heart,
            R.drawable.gift_16_hug,
            R.drawable.gift_18_ice_cream,
            R.drawable.gift_20_kiss,
            R.drawable.gift_22_mask,
            R.drawable.gift_24_wand,
            R.drawable.gift_26_money,
            R.drawable.gift_28_popcorn,
            R.drawable.gift_31_rocket,
            R.drawable.gift_33_rainbow,
            R.drawable.gift_35_rose,
            R.drawable.gift_37_star,
            R.drawable.gift_39_teddy*/
    };

    public static final int[] MASK_ICON_RES = {
            R.drawable.mask4,
            R.drawable.mask1,
            R.drawable.mask1,
            R.drawable.mask4
    };
    public static int getMaskAnimRes(int id) {
        return MASK_ICON_RES[id];
    }
    public static final int[] BEAUTY_ICON_RES = {
            R.drawable.beauty1,
            R.drawable.beauty2,
            R.drawable.beauty3,
    };
    public static int getBeautyAnimRes(int id) {
        return BEAUTY_ICON_RES[id];
    }

    public static final int[] GIFT_ANIM_RES = {
            R.drawable.gift_01_balloon,
            R.drawable.gift_11_hello,
            R.drawable.gift_27_popcorn,
            R.drawable.gift_21_mask,
            R.drawable.gift_34_rose,
            R.drawable.gift_19_kiss,
            R.drawable.gift_36_star,
            R.drawable.gift_23_wand,
            R.drawable.gift_13_heart,
            R.drawable.gift_38_teddy,
            R.drawable.gift_05_chocolate,
            R.drawable.gift_32_rainbow,
            R.drawable.gift_17_ice_cream,
            R.drawable.gift_15_hug,
            R.drawable.gift_25_money,
            R.drawable.gift_07_crown,
            R.drawable.gift_09_gems,
            R.drawable.gift_03_car,
            R.drawable.gift_30_rocket,
            R.drawable.gift_29_ring

           /* R.drawable.gift_01_balloon,
            R.drawable.gift_02_balloon,
            R.drawable.gift_03_car,
            R.drawable.gift_04_car,
            R.drawable.gift_05_chocolate,
            R.drawable.gift_06_chocolate,
            R.drawable.gift_07_crown,
            R.drawable.gift_08_crown,
            R.drawable.gift_09_gems,
            R.drawable.gift_10_gems,
            R.drawable.gift_11_hello,
            R.drawable.gift_12_hello,
            R.drawable.gift_13_heart,
            R.drawable.gift_14_heart,
            R.drawable.gift_15_hug,
            R.drawable.gift_16_hug,
            R.drawable.gift_17_ice_cream,
            R.drawable.gift_18_ice_cream,
            R.drawable.gift_19_kiss,
            R.drawable.gift_20_kiss,
            R.drawable.gift_21_mask,
            R.drawable.gift_22_mask,
            R.drawable.gift_23_wand,
            R.drawable.gift_24_wand,
            R.drawable.gift_25_money,
            R.drawable.gift_26_money,
            R.drawable.gift_27_popcorn,
            R.drawable.gift_28_popcorn,
            R.drawable.gift_29_ring,
            R.drawable.gift_30_rocket,
            R.drawable.gift_31_rocket,
            R.drawable.gift_32_rainbow,
            R.drawable.gift_33_rainbow,
            R.drawable.gift_34_rose,
            R.drawable.gift_35_rose,
            R.drawable.gift_36_star,
            R.drawable.gift_37_star,
            R.drawable.gift_38_teddy,
            R.drawable.gift_39_teddy*/
    };
    public static final int[] GIFT_ADD_COIN_ANIM_RES = {
            R.drawable.gift_02_balloon,
            R.drawable.gift_12_hello,
            R.drawable.gift_28_popcorn,
            R.drawable.gift_22_mask,
            R.drawable.gift_35_rose,
            R.drawable.gift_20_kiss,
            R.drawable.gift_37_star,
            R.drawable.gift_24_wand,
            R.drawable.gift_14_heart,
            R.drawable.gift_39_teddy
    };

    public static int getGiftAnimRes(int id) {
        return GIFT_ANIM_RES[id];
    }
    public static int getAddCoinAnimRes(int id) {
        return GIFT_ADD_COIN_ANIM_RES[id];
    }
}
