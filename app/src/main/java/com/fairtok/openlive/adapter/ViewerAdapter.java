package com.fairtok.openlive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.model.videos.Video;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import java.util.ArrayList;
import java.util.List;

public class ViewerAdapter extends RecyclerView.Adapter<ViewerAdapter.SingingViewHolder> {

    public interface ClickViewerInterface{
        void clickViewer(String type, int pos);
    }
    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;
    Context context;
    ClickViewerInterface clickViewerInterface;
    public ViewerAdapter(Context context){
        this.context = context;
        clickViewerInterface = (ClickViewerInterface) context;
    }
    @NonNull
    @Override
    public SingingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_viewers, parent, false);
        return new ViewerAdapter.SingingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingingViewHolder holder, int position) {
        //holder.setModel(position);

        holder.tvName.setOnClickListener(view -> {
            clickViewerInterface.clickViewer("", position);
        });
        holder.imgUser.setOnClickListener(view -> {
            clickViewerInterface.clickViewer("", position);
        });
        holder.imgFollow.setOnClickListener(view -> {
            clickViewerInterface.clickViewer("follow", position);
        });
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class SingingViewHolder extends RecyclerView.ViewHolder{
        private CardView card_viewer;
        private ImageView imgFollow, imgUser;
        private TextView tvName;
        public SingingViewHolder(@NonNull View itemView) {
            super(itemView);
            card_viewer = itemView.findViewById(R.id.card_viewer);
            imgFollow = itemView.findViewById(R.id.imgFollow);
            tvName = itemView.findViewById(R.id.tvName);
            imgUser = itemView.findViewById(R.id.imgUser);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<Video.Data> list) {
        mList = (ArrayList<Video.Data>) list;
        notifyDataSetChanged();
    }

    public void loadMore(List<Video.Data> data) {
        for (int i = 0; i < data.size(); i++) {
            mList.add(data.get(i));
            notifyItemInserted(mList.size() - 1);
        }

    }

}
