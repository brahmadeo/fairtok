package com.fairtok.openlive.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Const;
import com.fairtok.viewmodel.RecommendFamilyViewModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HostRankAdapter extends RecyclerView.Adapter<HostRankAdapter.SingingViewHolder> {

    public interface ClickViewerInterface{
        void clickViewer();
    }
    public ArrayList<Video.Data> mList = new ArrayList<>();
    public RecommendFamilyViewModel recommendFamilyViewModel;
    Context context;
    ClickViewerInterface clickViewerInterface;
    private ArrayList<HashMap<String, String>> arrayList;
    private JSONArray jsonArray;
    private List<HashMap<String, String >> userData = new ArrayList<>();
    public HostRankAdapter(Context context, List<HashMap<String, String>> userData){
        this.context = context;
        this.userData.addAll(userData);
        //clickViewerInterface = (ClickViewerInterface) context;
    }
    @NonNull
    @Override
    public SingingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_host_rank, parent, false);
        return new HostRankAdapter.SingingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingingViewHolder holder, int position) {
        //holder.setModel(position);
        /*if (position == 1){
            holder.tvLive.setVisibility(View.VISIBLE);
            holder.imgFollow.setVisibility(View.GONE);
        }else {
            holder.tvLive.setVisibility(View.GONE);
            holder.imgFollow.setVisibility(View.VISIBLE);
        }*/
        try {
            holder.tvName.setText(userData.get(position).get("full_name"));
            holder.tvRank.setText(String.valueOf(position+1));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String profile_url = userData.get(position).get("user_profile").split("_")[0];
            if (userData.get(position).get("user_profile")!=null
                    && !userData.get(position).get("user_profile").equals("user_default_img.png")){
                Glide.with(context).load(Const.BASE_URL_IMAGE + userData.get(position).get("user_profile")).into(holder.imgProfile);
            }
            if (profile_url.equals("FairtokUserPic")) {
                Glide.with(context).load(Const.ITEM_BASE_URL + userData.get(position).get("user_profile")).into(holder.imgProfile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* holder.tvLive.setOnClickListener(view -> {
            holder.tvLive.setBackground(context.getResources().getDrawable(R.drawable.rectangular_solid_pink));
        });*/
    }

    @Override
    public int getItemCount() {
        return userData.size();
    }

    public class SingingViewHolder extends RecyclerView.ViewHolder{
        private TextView tvLive, tvName, tvRank;
        private ImageView imgProfile;

        public SingingViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRank = itemView.findViewById(R.id.tvRank);
            tvName = itemView.findViewById(R.id.tvName);
            imgProfile = itemView.findViewById(R.id.imgProfile);
        }

        public void setModel(int position){

        }
    }

    public void updateData(List<HashMap<String, String>> hashMaps) {
        if (userData!=null){
            userData.clear();
        }
        userData.addAll(hashMaps);
        notifyDataSetChanged();
    }

    public void loadMore(List<HashMap<String, String>> data) {
        userData.addAll(data);
        notifyDataSetChanged();
        /*for (int i = 0; i < data.size(); i++) {
            userData.add(data.get(i));
            notifyItemInserted(userData.size() - 1);
        }*/
    }
}
