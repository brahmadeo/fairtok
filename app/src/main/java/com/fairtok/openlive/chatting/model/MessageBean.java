package com.fairtok.openlive.chatting.model;

import io.agora.rtm.RtmMessage;

public class MessageBean {
    private String account;
    private RtmMessage message;
    private String cacheFile;
    private int background;
    private boolean beSelf;
    private String profilePic;
    private String name;

    //String name,
    public MessageBean(String account, RtmMessage message, boolean beSelf,String profilePic) {
        this.account = account;
        //this.name = name;
        this.message = message;
        this.beSelf = beSelf;
        this.profilePic = profilePic;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

   /* public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    public RtmMessage getMessage() {
        return message;
    }

    public void setMessage(RtmMessage message) {
        this.message = message;
    }

    public String getCacheFile() {
        return cacheFile;
    }

    public void setCacheFile(String cacheFile) {
        this.cacheFile = cacheFile;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public boolean isBeSelf() {
        return beSelf;
    }

    public void setBeSelf(boolean beSelf) {
        this.beSelf = beSelf;
    }
}
