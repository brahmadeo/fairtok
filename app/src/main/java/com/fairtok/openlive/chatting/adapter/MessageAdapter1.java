package com.fairtok.openlive.chatting.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.SessionManager;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.agora.rtm.RtmMessage;


public class MessageAdapter1 extends RecyclerView.Adapter<MessageAdapter1.MyViewHolder> {

    private List<MessageBean> messageBeanList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;
    Context context;
    private boolean isLiked = false;
    private SessionManager sessionManager;

    public MessageAdapter1(Context context, List<MessageBean> messageBeanList, @NonNull OnItemClickListener listener) {
        this.inflater = ((Activity) context).getLayoutInflater();
        this.messageBeanList = messageBeanList;
        this.listener = listener;
        this.context = context;
        this.sessionManager = new SessionManager(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.inflate_chat, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        setupView(holder, position);
    }

    @Override
    public int getItemCount() {
        return messageBeanList.size();
    }

    private void setupView(MyViewHolder holder, int position) {
        MessageBean bean = messageBeanList.get(position);

        //Log.d("username",bean.toString());

       // String name = bean.getName();
        //String[] splited = name.split(" ");
        //holder.textViewOtherName.setText(splited[0]);
        //holder.textViewOtherName.setText(bean.getAccount());
        RtmMessage rtmMessage = bean.getMessage();

  //      RequestOptions options = new RequestOptions().placeholder(R.drawable.user_default_img).error(R.drawable.user_default_img);
//        Glide.with(context).load(Const.ITEM_BASE_URL+bean.getProfilePic()).apply(options).into(holder.profilePic);


        if(bean.getMessage().getText().contains(context.getString(R.string.live_message_gift_send)))
        {
            holder.imgGift.setVisibility(View.VISIBLE);
            holder.imgGift.setImageResource(GiftUtil.GIFT_ICON_RES[bean.getBackground()]);
        } else {
            //holder.imgGift.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(v -> {
            if (listener != null) listener.onItemClick(bean);
        });

        /*holder.ivFavourite.setOnClickListener(view -> {
            isLiked = !isLiked;
            if (isLiked){
                holder.ivFavourite.setBackgroundResource(R.drawable.heart_on);
            }else {
                holder.ivFavourite.setBackgroundResource(R.drawable.heart_off);
            }

        });*/

        holder.textViewOtherMsg.setText(rtmMessage.getText());

    }

    public interface OnItemClickListener {
        void onItemClick(MessageBean message);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewOtherName;
        private TextView textViewOtherMsg;
        private ImageView imgGift, ivFavourite;
        private CircleImageView profilePic;
        RelativeLayout rlMain;

        MyViewHolder(View itemView) {
            super(itemView);

            textViewOtherName = itemView.findViewById(R.id.item_name_l);
            textViewOtherMsg = itemView.findViewById(R.id.item_msg_l);
            profilePic = itemView.findViewById(R.id.profilePic);
            imgGift = itemView.findViewById(R.id.imgGift);
            ivFavourite = itemView.findViewById(R.id.ivFavourite);
            rlMain = itemView.findViewById(R.id.rlMain);
        }
    }
}
