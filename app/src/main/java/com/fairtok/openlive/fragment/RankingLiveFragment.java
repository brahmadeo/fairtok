package com.fairtok.openlive.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fairtok.R;
import com.fairtok.view.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class RankingLiveFragment extends BaseFragment implements View.OnClickListener{

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable
            ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_ranking_live, container, false);

        return view;
    }

    @Override
    public void onClick(View view) {

    }
}
