package com.fairtok.openlive.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.openlive.activities.LiveActivity;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.openlive.activities.LiveUserListActivity;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.view.base.BaseFragment;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;

public class PopularLiveFragment extends BaseFragment implements View.OnClickListener{

    RecyclerView rvList;

    ArrayList<HashMap<String, String>> arrayList=new ArrayList<>();

    Adapter adapter;

    String username = "", mUserId="";

    private static final int PERMISSION_REQ_CODE = 1 << 4;

    static boolean mIsInChat = false;

    private RtmClient mRtmClient;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_popular_live, container, false);

        init(view);

        return view;
    }

    private void init(View view) {

        rvList = view.findViewById(R.id.rvList);
        rvList.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        adapter = new Adapter(arrayList);
        rvList.setAdapter(adapter);

        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

    }

    private void hitGetLiveListApi() {

        if (AppUtils.isNetworkAvailable(getActivity())) {

           // AppUtils.showRequestDialog(getActivity());

            String url = Const.BASE_URL + Const.LIVE_LIST;
            Log.v("postApi-token", Global.ACCESS_TOKEN);
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.v("postApi-url", String.valueOf(response));
                            //AppUtils.hideDialog();
                            parseLiveList(response);

                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-url", String.valueOf(anError.getErrorDetail()));
                            //AppUtils.hideDialog();
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(getActivity(), "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseLiveList(JSONObject response) {

        arrayList.clear();

        try {
            if (response.getBoolean("status")) {

                JSONArray jsonArray=response.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++){

                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap=new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("post_hash_tag", jsonObject.getString("post_hash_tag"));
                    hashMap.put("live_start", jsonObject.getString("live_start"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));

                    arrayList.add(hashMap);
                }

            } else
                Toast.makeText(getActivity(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {

    }

    private class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        public Adapter(ArrayList<HashMap<String, String>> arrayList) {

            data = arrayList;
        }

        @NonNull
        @Override
        public Adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_live, viewGroup, false);
            return new Adapter.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull Adapter.MyViewHolder holder, final int position) {

            holder.tvName.setText(data.get(position).get("user_full_name"));
            holder.tvHashTag.setText(data.get(position).get("post_hash_tag"));

            AppUtils.loadPicassoImage(Const.ITEM_BASE_URL+data.get(position).get("user_profile"), holder.ivProfile);
            AppUtils.loadPicassoImage(Const.ITEM_BASE_URL+data.get(position).get("user_profile"), holder.ivProfileSmall);

            holder.rlMain.setOnClickListener(view -> {

                username  = "@"+data.get(position).get("user_name");
                mUserId  = data.get(position).get("user_id");
                Utils.hostProfilePic =data.get(position).get("user_profile");

                checkPermission();
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            RelativeLayout rlMain;

            TextView tvViewers, tvDiamonds, tvName, tvHashTag;

            ImageView ivProfile, ivProfileSmall;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                rlMain = itemView.findViewById(R.id.rlMain);

                ivProfile = itemView.findViewById(R.id.ivProfile);
                ivProfileSmall = itemView.findViewById(R.id.ivProfileSmall);

                tvViewers = itemView.findViewById(R.id.tvViewers);
                tvDiamonds = itemView.findViewById(R.id.tvDiamonds);
                tvName = itemView.findViewById(R.id.tvName);
                tvHashTag = itemView.findViewById(R.id.tvHashTag);

            }
        }
    }

    private void gotoLive() {

        Intent intent = new Intent();
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_AUDIENCE);
        intent.setClass(getActivity(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME,username);
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, "@"+sessionManager.getUser().getData().getUserName());
        startActivity(intent);
    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                doLogin();
            } else {
                Toast.makeText(getActivity(), R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * API CALL: login RTM server
     */
    private void doLogin() {
        mIsInChat = true;
        mRtmClient.login(null, "@" + sessionManager.getUser().getData().getUserName(), new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("postApi", "login success");
                getActivity().runOnUiThread(() -> {
                    gotoLive();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
                getActivity().runOnUiThread(() -> {
                    mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        hitGetLiveListApi();
    }
}
