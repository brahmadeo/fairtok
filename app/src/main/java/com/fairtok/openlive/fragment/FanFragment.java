package com.fairtok.openlive.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.model.FanModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FanFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private Serializable mParam2;

    public FanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FanFragment newInstance(String param1, List<FanModel> param2) {
        FanFragment fragment = new FanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, (Serializable) param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getSerializable(ARG_PARAM2);
        }
    }

    private TextView tvName, tvFanType, tvPrice;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fan, container, false);
        Log.i("parcelable ", mParam2.toString());
        List<FanModel> listtt = (List<FanModel>)mParam2;
        //Toast.makeText(getContext(), "Position "+mParam1+" , "+ listtt.get(0).getName(), Toast.LENGTH_SHORT).show();
        tvName = view.findViewById(R.id.tvName);
        tvFanType = view.findViewById(R.id.tvFanType);
        tvPrice = view.findViewById(R.id.tvPrice);

        tvName.setText(Utils.hostFullName);
        tvFanType.setText(listtt.get(Integer.parseInt(mParam1)).getName());
        tvPrice.setText(listtt.get(Integer.parseInt(mParam1)).getPrice()+"/Month");
        return view;
    }
}