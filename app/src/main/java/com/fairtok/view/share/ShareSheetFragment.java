package com.fairtok.view.share;

import android.Manifest;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.egl.filter.GlSwirlFilter;
import com.daasuu.gpuv.egl.filter.GlVibranceFilter;
import com.daasuu.gpuv.egl.filter.GlWatermarkFilter;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.downloader.request.DownloadRequest;
import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.FragmentShareSheetBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.home.ForUFragment;
import com.fairtok.view.home.ReportSheetFragment;
import com.fairtok.viewmodel.ShareSheetViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Calendar;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;

public class ShareSheetFragment extends BottomSheetDialogFragment {


    private static final int MY_PERMISSIONS_REQUEST = 101;
    FragmentShareSheetBinding binding;
    ShareSheetViewModel viewModel;
    private CustomDialogBuilder customDialogBuilder;
    private String TAG = "ShareSheetFragment";
    private Uri mInvitationUrl;

    public ShareSheetFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        bottomSheetDialog.setOnShowListener(dialog1 -> {
            BottomSheetDialog dialog = (BottomSheetDialog) dialog1;
            dialog.setCanceledOnTouchOutside(false);

        });

        return bottomSheetDialog;

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_share_sheet, container, false);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new ShareSheetViewModel()).createFor()).get(ShareSheetViewModel.class);
        customDialogBuilder = new CustomDialogBuilder(getActivity());
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initListeners();
        initObserve();
    }

    private void initView() {
        Shader textShader=new LinearGradient(0, 0, 0, 20,
                new int[]{getResources().getColor(R.color.pink_dark), getResources().getColor(R.color.yellow)},
                new float[]{0, 1}, Shader.TileMode.CLAMP);
        binding.tvTitle.getPaint().setShader(textShader);

        binding.setViewModel(viewModel);
        if (getArguments() != null && getArguments().getString("video") != null) {
            viewModel.video = new Gson().fromJson(getArguments().getString("video"), Video.Data.class);
        }
        createVideoShareLink();
        //createDynamicLink();

    }


    private void initListeners() {
        binding.btnCopy.setOnClickListener(v -> {
            if (getActivity() != null) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Video link", viewModel.shareUrl);
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity(), "Copied Clipboard To Successfully", Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.btnDownload.setOnClickListener(view -> initPermission());
        binding.btnReport.setOnClickListener(view -> {
            ReportSheetFragment fragment = new ReportSheetFragment();
            Bundle args = new Bundle();
            args.putString("postid", viewModel.video.getPostId());
            args.putInt("reporttype", 1);
            fragment.setArguments(args);
            if (getParentFragment() != null) {
                fragment.show(getParentFragment().getChildFragmentManager(), fragment.getClass().getSimpleName());
            }
            dismiss();
        });
        binding.btnBlock.setOnClickListener(view -> {
            ReportSheetFragment fragment = new ReportSheetFragment();
            Bundle args = new Bundle();
            args.putString("postid", viewModel.video.getPostId());
            args.putInt("reporttype", 2);
            fragment.setArguments(args);
            if (getParentFragment() != null) {
                fragment.show(getParentFragment().getChildFragmentManager(), fragment.getClass().getSimpleName());
            }
            dismiss();
        });
    }
    private void createDynamicLink(){
        String videoUrl = Const.ITEM_BASE_URL + viewModel.video.getPostVideo();
        String link = "https://fairtok.com/Post="+videoUrl;
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://fairtok.page.link")
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder("com.example.ios")
                                .setAppStoreId("123456789")
                                .setMinimumVersion("1.0.1")
                                .build())
                .buildShortDynamicLink()
                .addOnSuccessListener(new OnSuccessListener<ShortDynamicLink>() {
                    @Override
                    public void onSuccess(ShortDynamicLink shortDynamicLink) {
                        mInvitationUrl = shortDynamicLink.getShortLink();
                        Log.i(TAG, mInvitationUrl.toString());
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, e.getMessage());
                    }
                });


    }

    private void initObserve() {
        viewModel.onItemClick.observe(this, type -> {
            String directPath = getPath() +"/Shared/";
            String videoUrl = Const.ITEM_BASE_URL + viewModel.video.getPostVideo();
            Global.Show_determinent_loader(getActivity(),false,false);
            PRDownloader.initialize(getActivity().getApplicationContext());
            DownloadRequest prDownloader= PRDownloader.download(videoUrl, directPath,viewModel.video.getPostVideo())
                    .build()
                    .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                        @Override
                        public void onStartOrResume() {

                        }
                    })
                    .setOnPauseListener(new OnPauseListener() {
                        @Override
                        public void onPause() {

                        }
                    })
                    .setOnCancelListener(new OnCancelListener() {
                        @Override
                        public void onCancel() {

                        }
                    })
                    .setOnProgressListener(new OnProgressListener() {
                        @Override
                        public void onProgress(Progress progress) {

                            int prog=(int)((progress.currentBytes*100)/progress.totalBytes);
                            Global.Show_loading_progress(prog/2);

                        }
                    });


            prDownloader.start(new OnDownloadListener() {
                @Override
                public void onDownloadComplete() {
                    File videoFile = new File(directPath,viewModel.video.getPostVideo());
                    applyWaterMark(videoFile,true,type);
                    Global.cancel_determinent_loader();

                }

                @Override
                public void onError(Error error) {
                    Log.i("errorrr ", error.toString());
                    TestDialog(error.toString()+"==="+error.getServerErrorMessage());
                    Toast.makeText(getActivity(), "Error"+error.getServerErrorMessage(), Toast.LENGTH_SHORT).show();
                    Global.cancel_determinent_loader();
                    dismiss();
                }

            });

        });
    }


    private void initPermission() {
        if (getActivity() != null) {

           /* if (ContextCompat.checkSelfPermission(
                    CONTEXT, Manifest.permission.REQUESTED_PERMISSION) ==
                    PackageManager.PERMISSION_GRANTED) {
                // You can use the API that requires the permission.
            } else if (shouldShowRequestPermissionRationale(...)) {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected. In this UI,
                // include a "cancel" or "no thanks" button that allows the user to
                // continue using your app without granting the permission.
            } else {
                // You can directly ask for the permission.
                requestPermissions(CONTEXT,
                        new String[] { Manifest.permission.REQUESTED_PERMISSION },
                        REQUEST_CODE);
            }*/

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
            } else {
                startDownload();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startDownload();
            }
        }
    }

    private void startDownload() {
        Log.d("DOWNLOAD", "startDownload: ");
        PRDownloader.download(Const.ITEM_BASE_URL + viewModel.video.getPostVideo(), Utils.getCreatedAppDirectory(getActivity()).getAbsolutePath(), viewModel.video.getPostVideo())
                .build()
                .setOnStartOrResumeListener(() -> customDialogBuilder.showLoadingDialog())
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {

                        File videoFile = new File(Utils.getCreatedAppDirectory(getActivity()).getAbsolutePath(), viewModel.video.getPostVideo());

                        applyWaterMark(videoFile,false,0);

                    }

                    @Override
                    public void onError(Error error) {
                        customDialogBuilder.hideLoadingDialog();
                        Log.d("DOWNLOAD", "onError:  error downloading video");
                    }
                });
    }
    private void TestDialog(String msg){
        Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.live_users_popup);


        TextView text = (TextView) dialog.findViewById(R.id.text_message_incoming);
        text.setText(msg);
    }

    public File getPath() {
        if (getActivity() != null) {
            String state = Environment.getExternalStorageState();
            File filesDir;
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                // We can read and write the media
                filesDir = getActivity().getExternalFilesDir(null);
            } else {
                // Load another directory, probably local memory
                filesDir = getActivity().getFilesDir();
            }
            return filesDir;
        }
        return new File(Environment.getRootDirectory().getAbsolutePath());
    }



    private void createVideoShareLink() {
        String json = new Gson().toJson(viewModel.video);
        String title = viewModel.video.getPostDescription();

        Log.i("ShareJson", "Json Object: " + json);
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(title)
                .setContentImageUrl(Const.ITEM_BASE_URL + viewModel.video.getPostImage())
                .setContentDescription(viewModel.video.getPostDescription())
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("data", json));

        LinkProperties lp = new LinkProperties()
                .setFeature("sharing")
                .setCampaign("Content launch")
                .setStage("Video")
                .addControlParameter("custom", "data")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        if (getActivity() != null) {
            buo.generateShortUrl(getActivity(), lp, (url, error) -> {
                Log.d("VIDEO_URL", "shareProfile: " + url);
                viewModel.shareUrl = url;
            });
        }

    }

    private Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    private String temp = "";
    private void applyWaterMark(File videoPath,boolean isShare,int type) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.water_mark_layout, null);
        TextView mUsername = view.findViewById(R.id.waterrmark_username);
        mUsername.setText("@" + viewModel.video.getUserName());

        //second, set the width and height of inflated view
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        temp = videoPath.getAbsolutePath();
        String last = temp.substring(temp.lastIndexOf("/") + 1, temp.lastIndexOf("."));

        // if (temp.contains("append") || temp.contains("finally"))
        {
//            temp = temp.replace("append", "append1");
            temp = temp.replace(last, last.concat("1"));
//        Bitmap myLogo = ((BitmapDrawable) getResources().getDrawable(R.drawable.logo)).getBitmap();
            Bitmap myLogo = getImageFromLayout(view);
            Bitmap bitmap_resize = resize(myLogo, 125, 125);
            GlWatermarkFilter filter = new GlWatermarkFilter(bitmap_resize, GlWatermarkFilter.Position.RIGHT_TOP);
            //GlWatermarkFilter filter1 = new GlWatermarkFilter(bitmap_resize, GlWatermarkFilter.Position.RIGHT_BOTTOM);
            customDialogBuilder.showLoadingDialog();
            new GPUMp4Composer(String.valueOf(videoPath), temp)
                    .filter(filter)
                    .listener(new GPUMp4Composer.Listener() {
                        @Override
                        public void onProgress(double progress) {

                        }

                        @Override
                        public void onCompleted() {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    customDialogBuilder.hideLoadingDialog();
                                    Delete_file_no_watermark(videoPath.getPath());
                                    //Toast.makeText(getActivity(), "Saved Successfully", Toast.LENGTH_SHORT).show();
                                    if(isShare){
                                        File file = new File(temp);
                                        Share(file,type);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onCanceled() {
                            customDialogBuilder.hideLoadingDialog();
                        }

                        @Override
                        public void onFailed(Exception exception) {
                            Log.d("TEST199", exception.getMessage().toString());
                            customDialogBuilder.hideLoadingDialog();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Failed "+exception.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    }).start();
        }
    }

    private void Share(File file,int type){
        try {
            Intent share = new Intent(Intent.ACTION_SEND);
            switch (type) {
                case 1:     // Instagram
                    share.setPackage("com.instagram.android");
                    break;
                case 2:   // facebook
                    share.setPackage("com.facebook.katana");
                    break;
                case 3:   // whatsapp
                    share.setPackage("com.whatsapp");
                    break;
                // other
                case 4:
                    break;
            }

            String shareBody = "Watch this amazing video on " + getActivity().getResources().getString(R.string.app_name) + " App";
//            File file = fileznew File(directPath+viewModel.video.getPostVideo());
            Uri uri = FileProvider.getUriForFile(
                    getActivity(),
                    "com.fairtok", //(use your app signature + ".provider" )
                    file);
            share.setType("video/*");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            //share.putExtra(Intent.EXTRA_SUBJECT, "Share Video");
            share.putExtra(Intent.EXTRA_TEXT, shareBody+" "+viewModel.shareUrl);
            startActivity(Intent.createChooser(share, "Share Video"));
            dismiss();
        }
        catch (Exception e)
        {
            Global.cancel_determinent_loader();
            Log.d("TEST199",e.getMessage().toString());
            dismiss();
            Toast.makeText(getActivity(), "Error: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap getImageFromLayout(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void Delete_file_no_watermark(String videoPath) {
        File file = new File(videoPath);
        if (file.exists()) {
            file.delete();
        }
    }
}