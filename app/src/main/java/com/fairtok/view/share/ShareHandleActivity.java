package com.fairtok.view.share;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.Utils;
import com.fairtok.model.user.User;
import com.fairtok.model.videos.Video;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.s3.Util;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.search.FetchUserActivity;
import com.fairtok.view.video.PlayerActivity;
import com.google.gson.Gson;

import org.json.JSONException;

import java.util.ArrayList;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.branch.referral.Branch;

public class ShareHandleActivity extends BaseActivity {

    private CustomDialogBuilder customDialogBuilder;

    private static final int PERMISSION_REQ_CODE = 1 << 4;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private RtmClient mRtmClient;

    String targetName="", liveId="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_handle);

        getDataFromBranch();
        customDialogBuilder = new CustomDialogBuilder(this);
        customDialogBuilder.showLoadingDialog();
    }

    private void getDataFromBranch() {
        Branch branch = Branch.getInstance();

        // Branch init
        branch.initSession((referringParams, error) -> {
            if (error == null) {
                if (referringParams != null) {
                    Log.i("BRANCH_SDK", referringParams.toString());
                }

                try {
                    if (referringParams != null && referringParams.has("data")) {
                        customDialogBuilder.hideLoadingDialog();

                        String data = referringParams.getString("data");

                        Log.i("dattaaaa ", data);
                        if (data.equals("live")){
                            startActivity(new Intent(ShareHandleActivity.this, LiveUsersActivity.class));
                        }else {
                            User user = new Gson().fromJson(data, User.class);
                            Video.Data video = new Gson().fromJson(data, Video.Data.class);
                            if (user != null && user.getData() != null) {
                                Intent intent = new Intent(ShareHandleActivity.this, FetchUserActivity.class);
                                intent.putExtra("isBranch", true);
                                intent.putExtra("userid", user.getData().getUserId());
                                startActivity(intent);
                            } else if (video != null) {
                                Intent intent = new Intent(ShareHandleActivity.this, PlayerActivity.class);
                                ArrayList<Video.Data> mList = new ArrayList<>();
                                mList.add(video);
                                intent.putExtra("video_list", new Gson().toJson(mList));
                                intent.putExtra("position", 0);
                                intent.putExtra("type", 5);
                                startActivity(intent);
                            } else {
                                startActivity(new Intent(ShareHandleActivity.this, MainActivity.class));
                            }
                        }
                        finish();

                    }
                    else {

                        if (referringParams!=null && referringParams.has("+non_branch_link")){

                            String data = null;
                            try {
                                data = referringParams.getString("+non_branch_link");
                                Uri uri = Uri.parse(data);
                                liveId = uri.getQueryParameter("liveId");
                                targetName = uri.getQueryParameter("targetName");
                                Utils.hostUserId = uri.getQueryParameter("hostId");

                                ChatManager mChatManager = MyApplication.the().getChatManager();
                                mRtmClient = mChatManager.getRtmClient();

                                onJoinAsAudienceClicked();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Log.i("BRANCH_SDK", error.getMessage());
            }
        }, this.getIntent().getData(), this);
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    public void onJoinAsAudienceClicked() {

        checkPermission();

    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }

    private void doLogin() {

        mRtmClient.login(null, "@" + sessionManager.getUser().getData().getUserName(), new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

                Log.i("postApi", "login success");
                runOnUiThread(() -> {
                    gotoLiveActivity();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
                //AppUtils.hideDialog();
                runOnUiThread(() -> {

                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    private void gotoLiveActivity() {
        Intent intent = new Intent();
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_AUDIENCE);
        intent.setClass(getApplicationContext(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, targetName);
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, "@" + sessionManager.getUser().getData().getUserName());
        intent.putExtra("liveId", liveId);
        startActivity(intent);
        finish();
    }
}