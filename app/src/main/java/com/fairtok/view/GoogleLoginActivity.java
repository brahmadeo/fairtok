package com.fairtok.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fairtok.utils.Global.RC_SIGN_IN;

public class GoogleLoginActivity extends BaseActivity implements CompleteTaskListner {

    private ProgressBar progressbar;
    private SessionManager sessionManager;
    PrefHandler prefHandler;
    private CompositeDisposable disposable = new CompositeDisposable();
    private LocationManager locationManager;
    private RelativeLayout btnSubmit;
    private String countryName="";
    private String stateName="";
    int LOCATION_REFRESH_TIME = 15000; // 15 seconds to update
    int LOCATION_REFRESH_DISTANCE = 500; // 500 meters to update

    ActivityResultLauncher activityResultLauncher;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_login_new);

        progressbar = findViewById(R.id.progressbar);

        // dytnZ7J-QwWrHMWOzXtYAf:APA91bH7mS-DSOU770VJl5sZ-pSUayHuZfgnchwFPzib1Pua6_a_0G4-d8PN7ZiEXHbUruJaEIq-72RYPSP-vMsbMV236bfayZ6Bl6NSCWNZhPJUaBNw0xyQ222KDowS8Mt6PLWz5s8V
        // d8qLwbkaTAaSQyibSacC23:APA91bE1HB6wibxrXCCMMJai8tVZAi1PaDIvXrNgWfdzZj4RGXj5Qv4zBqroThVH-QD7mrABgdmAalOaGxVrOeXx6Jr2ZsTdZ5COgTqhLq0Ta26B5bpNtTJRmPDM524npPuYsRBy31z

        // geolocation
        getLocation();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        sessionManager = new SessionManager(this);
        MyApplication.sessionManager = sessionManager;
        prefHandler = new PrefHandler(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if(prefHandler.isLoggedIn())
        {
            finish();
            //startActivity(new Intent(GoogleLoginActivity.this,MainActivity.class));
            startActivity(new Intent(GoogleLoginActivity.this, MainActivity.class));
        }

        findViewById(R.id.btnSignin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GoogleLoginActivity.this, SigninActivity.class));
                //Log.e("Google login", "handleSignInResult: " + account.getEmail());
            }
        });
        findViewById(R.id.tvTerms).setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.terms_conditions)));
            startActivity(browserIntent);
        });
        findViewById(R.id.tvPolicy).setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy)));
            startActivity(browserIntent);
//            Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
//            startActivity(intent);
        });


        CountryCodePicker ccp= findViewById(R.id.ccp);

        Button btnOtp = findViewById(R.id.btnOtp);
        TextInputEditText mobile_number = findViewById(R.id.mobile_number);

        btnOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mobile_number.getText().toString().isEmpty()){
                    sendVerificationCode("+"+ccp.getSelectedCountryCode(), mobile_number.getText().toString().trim());
                }else {
                    Toast.makeText(GoogleLoginActivity.this,"Please enter the mobile Number.",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    progressbar.setVisibility(View.VISIBLE);
                    btnSubmit.setClickable(false);
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                    //activityResultLauncher.launch(signInIntent);
                }
                catch(NullPointerException e){
                    btnSubmit.setClickable(true);
                    progressbar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
                catch (ClassCastException e)
                {
                    btnSubmit.setClickable(true);
                    progressbar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    btnSubmit.setClickable(true);
                    progressbar.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

        });

    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);
        if(response_code == 3)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if(success==1)
                {
                    prefHandler.setIsMobileVerified(true);
                }
                else
                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode == RESULT_OK && requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else {
            btnSubmit.setClickable(true);
            progressbar.setVisibility(View.GONE);
        }
    }

    private void sendVerificationCode(String countryCode, String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                countryCode + mobile,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks);

        //openOtpScreen(context);
    }
    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                //editTextCode.setText(code);
                //verifying the code
                //verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            //mVerificationId = s;
        }
    };

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("device_token", Global.FIREBASE_DEVICE_TOKEN);
                hashMap.put("user_email", account.getEmail());
                hashMap.put("full_name", account.getDisplayName());
                hashMap.put("login_type", Const.GOOGLE_LOGIN);
                hashMap.put("user_name", Objects.requireNonNull(account.getEmail()).split("@")[0]);
                hashMap.put("identity", account.getEmail());
               // hashMap.put("mobile", prefHandler.getMobile());
                hashMap.put("language", prefHandler.getLanguage());
                hashMap.put("referal_code", prefHandler.getReferralUrl());
                hashMap.put("country", countryName);
                hashMap.put("state", stateName);
                registerUser1(hashMap, GoogleLoginActivity.this,GoogleLoginActivity.this);
                Log.e("Google login", "handleSignInResult: " + account.getEmail());
            }
            // Signed in successfully, show authenticated UI.

        } catch (ApiException e) {
            btnSubmit.setClickable(true);
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Google login", "signInResult:failed code=" + e.getStatusCode());
        }
    }


    public void registerUser1(HashMap<String, Object> hashMap, Context context, AppCompatActivity activity) {
        Log.i("hashmapp ", hashMap.toString());
        disposable.add(Global.initRetrofit().registrationUser(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((user, throwable) -> {
                    btnSubmit.setClickable(true);
                    progressbar.setVisibility(View.GONE);
                    if (user != null && user.getStatus()) {
                        sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                        sessionManager.saveUser(user);
                        Global.ACCESS_TOKEN = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getToken() : "";
                        Global.USER_ID = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserId() : "";
                        sessionManager.saveBooleanValue("notification", true);

                        prefHandler.setuId(Global.USER_ID);
                        prefHandler.setName(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserName() : "");
                        prefHandler.setEmail(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserEmail() : "");
                        prefHandler.setProfile(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserProfile() : "");

                        //startActivity(new Intent(GoogleLoginActivity.this,LanguageSelection.class));
                        if(prefHandler.isLoggedIn())
                        {
                            Intent i = new Intent(this, MainActivity.class);
                            startActivity(i);
                        }else {
                            startActivity(new Intent(GoogleLoginActivity.this,LanguageSelection.class));
                        }
                        //Log.i("logedinnn ", prefHandler.isLoggedIn()+"");
                        prefHandler.setIsLoggedIn(true);
                        finish();
                    }
                }));
    }

    private void getLocation(){
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            Dexter.withActivity(GoogleLoginActivity.this).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted())
                    {
                        try {
                            LocationListener locationListener = new MyLocationListener();
                            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, locationListener);
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                    //progressbar.setVisibility(View.GONE);

                }
            }).check();
        } else {
            LocationListener locationListener = new MyLocationListener();
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(GoogleLoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(GoogleLoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, locationListener);

        }
    }
    public class MyLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            try {

                Geocoder geocoder = new Geocoder(GoogleLoginActivity.this, Locale.getDefault());
                //currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                //currentLatLng = new LatLng(22.594793, 88.408670);
                //String locationName = "";
                List<Address> addresses1 = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                //List<Address> addresses = geocoder.getFromLocationName(locationName, 1);

                Address address = addresses1.get(0);
                countryName = address.getCountryName();
                Global.COUNTRY = countryName;
                stateName = address.getAdminArea();
                Global.STATE = stateName;

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.i("onStatusChanged ", s);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.i("onProviderEnabled ", s);
        }

        @Override
        public void onProviderDisabled(String s)
        {
            //progressbar.setVisibility(View.GONE);
            Toast.makeText(GoogleLoginActivity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
