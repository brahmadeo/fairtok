package com.fairtok.view.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.widget.ImageView;

import com.fairtok.R;

public class NotificationActivity extends AppCompatActivity {

    private CardView cardComments, cardLikes;
    private ImageView imgBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initView();
    }
    private void initView(){
        cardLikes = findViewById(R.id.cardLikes);
        cardComments = findViewById(R.id.cardComments);
        imgBack = findViewById(R.id.imgBack);

        imgBack.setOnClickListener(view -> {
            onBackPressed();
        });
        cardLikes.setOnClickListener(view -> {

        });
        cardComments.setOnClickListener(view -> {

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}