package com.fairtok.view.notification;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.databinding.FragmentNotificationBinding;
import com.fairtok.viewmodel.MainViewModel;
import com.fairtok.viewmodel.NotificationViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

public class NotificationFragment extends Fragment {

    FragmentNotificationBinding binding;
    NotificationViewModel viewModel;
    private MainViewModel parentViewModel;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            parentViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new NotificationViewModel()).createFor()).get(NotificationViewModel.class);
        initView();
        initListeners();
        initObserve();
        if (viewModel != null) {
            binding.setViewModel(viewModel);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void initView() {

        binding.refreshlout.setEnableRefresh(false);
        viewModel.fetchNotificationData(false);

    }


    private void initListeners() {
        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.fetchNotificationData(true));
    }

    private void initObserve() {
        parentViewModel.selectedPosition.observe(this, position -> {

            if (position != null && position == 2) {
                viewModel.start = 0;
                viewModel.fetchNotificationData(false);

            }
        });
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
    }
}
