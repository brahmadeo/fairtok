package com.fairtok.view.video;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.VideoFullAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivityPlayerBinding;
import com.fairtok.databinding.ItemVideoListBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.utils.GlobalApi;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.CommentSheetFragment;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.home.ReportSheetFragment;
import com.fairtok.view.home.SoundVideosActivity;
import com.fairtok.view.preview.PreviewActivity;
import com.fairtok.view.recordvideo.DuetCameraActivity;
import com.fairtok.view.search.FetchUserActivity;
import com.fairtok.view.search.HashTagActivity;
import com.fairtok.view.share.ShareSheetFragment;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;
import com.fairtok.viewmodel.VideoPlayerViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

public class PlayerActivity extends BaseActivity implements Player.EventListener, CompleteTaskListner {

    private ActivityPlayerBinding binding;
    private VideoPlayerViewModel viewModel;
    private SimpleExoPlayer player;
    private SimpleExoPlayer playerSingle;
    private int lastPosition = -1;
    private ItemVideoListBinding playerBinding;
    private CompositeDisposable disposable = new CompositeDisposable();

    private CustomDialogBuilder customDialogBuilder;
    private static final int MY_PERMISSIONS_REQUEST = 101;
    Video.Data current_model;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparentFlag();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_player);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new VideoPlayerViewModel()).createFor()).get(VideoPlayerViewModel.class);
        initView();
        initAds();
        initListener();
        initIntent();
        initObserver();
        binding.setViewModel(viewModel);
    }

    private void initObserver() {
        viewModel.onCommentSuccess.observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                playerBinding.getModel().setPostCommentsCount(playerBinding.getModel().getPostCommentsCount() + 1);
                playerBinding.tvCommentCount.setText(Global.prettyCount(playerBinding.getModel().getPostCommentsCount()));
                binding.etComment.setText("");
                closeKeyboard();
            }
        });

        viewModel.onShareSuccess.observe(this, isSuccess -> {
            if (isSuccess != null && isSuccess) {
                try {
                    playerBinding.getModel().setPostShareCount(playerBinding.getModel().getPostShareCount() + 1);
                }
                catch(NullPointerException e){
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }


            }
        });

        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
        viewModel.coinSend.observe(this, coinSend -> showSendResult(coinSend.getStatus()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initListener() {
        binding.imgBack.setOnClickListener(v -> {
            if (viewModel.type == 5) {
                startActivity(new Intent(this, MainActivity.class));
                finishAffinity();
            } else {
                onBackPressed();
            }
        });

        binding.playerView1.setOnTouchListener(new View.OnTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(PlayerActivity.this, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if (playerSingle != null) {
                        if (playerSingle.isPlaying()) {
                            playerSingle.setPlayWhenReady(false);
                        } else {
                            playerSingle.setPlayWhenReady(true);
                        }
                    }
                    return super.onSingleTapConfirmed(e);
                }
            });
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });

        binding.refreshlout.setEnableRefresh(false);
        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());
        viewModel.adapter.onRecyclerViewItemClick = new VideoFullAdapter.OnRecyclerViewItemClick() {
            @Override
            public void onItemClick(Video.Data model, int position, int type, ItemVideoListBinding binding) {

                current_model = model;

                switch (type) {
                    case 10:
                        binding.newFollowBtn.setVisibility(View.GONE);
                        handleButtonClick(model.getUserId());
                        String currentUserId=model.getUserId();
                        Log.v("Current","paras current userid = "+currentUserId);
                        handleButtonClick(model.getUserId());

                        for(int i=0; i<viewModel.adapter.mList.size(); i++)
                        {
                            try
                            {
                                if(viewModel.adapter.mList.get(i).getUserId().equalsIgnoreCase(currentUserId))
                                {
                                    viewModel.adapter.mList.get(i).setIsFollow(1);
                                }
                            }
                            catch(Exception e){e.printStackTrace();}
                        }

                        viewModel.adapter.notifyDataSetChanged();


                        break;
                    // Send to FetchUser Activity
                    case 1:
                        Intent intent = new Intent(PlayerActivity.this, FetchUserActivity.class);
                        intent.putExtra("userid", model.getUserId());
                        startActivity(intent);
                        break;
                    // Play/Pause video
                    case 2:
                        if (player != null) {
                            if (player.isPlaying()) {
                                player.setPlayWhenReady(false);
                            } else {
                                player.setPlayWhenReady(true);
                            }
                        }

                        break;
                    // Send Bubble to creator
                    case 3:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            showSendBubblePopUp(model.getUserId());
                        } else {
                            initLogin(PlayerActivity.this, () -> showSendBubblePopUp(model.getUserId()));

                        }
                        break;
                    // On like btn click
                    case 4:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            viewModel.likeUnlikePost(model.getPostId());
                        }
                        break;
                    // On Comment Click
                    case 5:
                        CommentSheetFragment fragment = new CommentSheetFragment();
                        fragment.onDismissListener = count -> {
                            model.setPostCommentsCount(count);
                            binding.tvCommentCount.setText(Global.prettyCount(count));

                        };
                        Bundle args = new Bundle();
                        args.putString("postid", model.getPostId());
                        args.putInt("commentCount", model.getPostCommentsCount());
                        fragment.setArguments(args);
                        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
                        break;
                    // On Share Click
                    case 6:
                        handleShareClick(model);
                        break;
                    // On Sound Disk Click
                    case 7:
                        if (Global.ACCESS_TOKEN.isEmpty()) {
                            initLogin(PlayerActivity.this, () -> viewModel.likeUnlikePost(model.getPostId()));

                        } else {
                            Intent intent1 = new Intent(PlayerActivity.this, SoundVideosActivity.class);
                            intent1.putExtra("soundid", model.getSoundId());
                            intent1.putExtra("sound", model.getSound());
                            startActivity(intent1);
                        }
                        break;
                    // On Long Click (Report Video)
                    case 8:
                        new CustomDialogBuilder(PlayerActivity.this).showSimpleDialog("Report this post", "Are you sure you want to\nreport this post?", "Cancel", "Yes, Report", new CustomDialogBuilder.OnDismissListener() {
                            @Override
                            public void onPositiveDismiss() {
                                reportPost(model);
                            }

                            @Override
                            public void onNegativeDismiss() {

                            }
                        });

                        break;

                    case 9:
                        playVideo(Const.ITEM_BASE_URL + model.getPostVideo(), binding);
                        break;


                    case 22:
                        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                            initPermission();
                        } else {
                            Toast.makeText(getApplicationContext(),"You need to login first in order to create duet",Toast.LENGTH_SHORT).show();
                        }

                        break;
                }
            }

            @Override
            public void onHashTagClick(String hashTag) {
                Intent intent = new Intent(PlayerActivity.this, HashTagActivity.class);
                intent.putExtra("hashtag", hashTag);
                startActivity(intent);
            }
        };

        binding.rvVideos.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (binding.rvVideos.getLayoutManager() instanceof LinearLayoutManager) {
                        int position = ((LinearLayoutManager) binding.rvVideos.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                        if (position != -1 && lastPosition != position) {
                            Animation animation = AnimationUtils.loadAnimation(binding.getRoot().getContext(), R.anim.slow_rotate);
                            if (viewModel.adapter.mList.get(position) != null) {
                                if (binding.rvVideos.getLayoutManager() != null) {
                                    View view = binding.rvVideos.getLayoutManager().findViewByPosition(position);
                                    if (view != null) {
                                        lastPosition = position;
                                        ItemVideoListBinding binding1 = null, binding3 = null;
                                        if(position != 4){
                                            binding1 = DataBindingUtil.bind(view);
                                        }
                                        if (binding1 != null) {
                                            if (position < viewModel.adapter.mList.size()) {
                                                new GlobalApi().increaseView(binding1.getModel().getPostId());
                                                viewModel.postId = viewModel.adapter.mList.get(position).getPostId();
                                                binding1.imgSound.startAnimation(animation);
                                                binding.loutAddComment.setVisibility(View.VISIBLE);
                                                playVideo(Const.ITEM_BASE_URL + viewModel.adapter.mList.get(position).getPostVideo(), binding1);
                                            }
                                        }else {
                                            if (player != null) {
                                                player.setPlayWhenReady(false);
                                                player.stop();
                                                player.release();
                                                player = null;
                                                lastPosition = position;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if (player != null) {
                                    binding.loutAddComment.setVisibility(View.GONE);
                                    player.setPlayWhenReady(false);
                                    player.stop();
                                    player.release();
                                    player = null;
                                    lastPosition = position;
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void handleButtonClick(String userId) {

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            followUnfollow(userId);
        } else {
            Toast.makeText(PlayerActivity.this, "You have to login first", Toast.LENGTH_SHORT).show();
        }
    }

    public void followUnfollow(String userId) {

        disposable.add(Global.initRetrofit().followUnFollow(Global.ACCESS_TOKEN, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((followRequest, throwable) -> {
//                    if (followRequest != null && followRequest.getStatus() != null) {
//                        if (isMyAccount.get() == 1) {
//                            isMyAccount.set(2);
//                        } else {
//                            isMyAccount.set(1);
//                        }
//                        followApi.setValue(followRequest);
//                    }

                }));
    }

    private void handleShareClick(Video.Data model) {
        upDateShareCount(model);

        ShareSheetFragment fragment = new ShareSheetFragment();
        Bundle args = new Bundle();
        args.putString("video", new Gson().toJson(model));
        fragment.setArguments(args);
        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
    }

    public void upDateShareCount(Video.Data model)
    {
        PrefHandler prefHandler = new PrefHandler(PlayerActivity.this);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("user_id",prefHandler.getuId()));
        params.add(new Pair<>("post_id", model.getPostId()));
        new AsyncHttpsRequest("", this, params, this, 0, false).execute(Utils.UPDATE_SHARE_COUNT);
    }

    private void reportPost(Video.Data model) {
        ReportSheetFragment fragment = new ReportSheetFragment();
        Bundle args = new Bundle();
        args.putString("postid", model.getPostId());
        args.putInt("reporttype", 1);
        fragment.setArguments(args);
        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
    }

    private void showSendBubblePopUp(String userId) {

        new CustomDialogBuilder(this).showSendCoinDialogue(new CustomDialogBuilder.OnCoinDismissListener() {
            @Override
            public void onCancelDismiss() {

            }

            @Override
            public void on5Dismiss() {
                viewModel.sendBubble(userId, "500");
            }

            @Override
            public void on10Dismiss() {
                viewModel.sendBubble(userId, "1000");
            }

            @Override
            public void on20Dismiss() {
                viewModel.sendBubble(userId, "5000");
            }
        });
    }

    private void showSendResult(boolean success) {
        new CustomDialogBuilder(this).showSendCoinResultDialogue(success, success1 -> {
            if (!success1) {
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
    }

    String field="draft";

    private void initIntent() {
        String from = getIntent().getStringExtra("from");
        if (from != null && from.equals("notification"))
        {
            String video = getIntent().getStringExtra("video");
            Log.i("videoUrllll ", video);
            //playSingleVideo(Const.VIDEO_ITEM_BASE_URL + video);
            /*if (binding.rvVideos.getLayoutManager() instanceof LinearLayoutManager) {
                int position = ((LinearLayoutManager) binding.rvVideos.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                if (position != -1) {
                    Animation animation = AnimationUtils.loadAnimation(binding.getRoot().getContext(), R.anim.slow_rotate);
                    if (binding.rvVideos.getLayoutManager() != null) {
                        View view = binding.rvVideos.getLayoutManager().findViewByPosition(position);
                        if (view != null) {
                            ItemVideoListBinding binding3 = DataBindingUtil.bind(view);
                            playSingleVideo(video, binding3);
                        }
                    }
                }
            }*/
            playSingleVideo1(video);

            String videoStr = getIntent().getStringExtra("video_list");
            int position = getIntent().getIntExtra("position", 0);

            viewModel.type = getIntent().getIntExtra("type", 0);
            viewModel.handleType(getIntent());

            if (videoStr != null && !videoStr.isEmpty())
            {
                viewModel.list = new Gson().fromJson(videoStr, new TypeToken<ArrayList<Video.Data>>() {
                }.getType());


                //Collections.shuffle(viewModel.list);

                viewModel.adapter.postId = viewModel.list.get(position).getPostId();
                viewModel.position = position;
                viewModel.adapter.itemToPlay = viewModel.position;
                viewModel.start = viewModel.list.size();
                viewModel.adapter.updateData(viewModel.list);
                binding.rvVideos.scrollToPosition(position);
            }

        }
        else {
            binding.loutAddComment.setVisibility(View.VISIBLE);
            String videoStr = getIntent().getStringExtra("video_list");
            int position = getIntent().getIntExtra("position", 0);

            viewModel.type = getIntent().getIntExtra("type", 0);
            viewModel.handleType(getIntent());



            if (videoStr != null && !videoStr.isEmpty())
            {
                viewModel.list = new Gson().fromJson(videoStr, new TypeToken<ArrayList<Video.Data>>() {}.getType());

                //Collections.shuffle(viewModel.list);

                viewModel.adapter.postId = viewModel.list.get(position).getPostId();
                viewModel.position = position;
                viewModel.adapter.itemToPlay = viewModel.position;
                viewModel.start = viewModel.list.size();
                viewModel.adapter.updateData(viewModel.list);
                binding.rvVideos.scrollToPosition(position);



                if((viewModel.type==2 || viewModel.type==3) && Utils.isFromOwnProfile==true)
                {
                    Utils.hideViewCounts=true;
                    Utils.isFromOwnProfile=false;
                    binding.btPublish.setVisibility(View.VISIBLE);

//                    if(viewModel.type==2)
//                    {
//                        binding.btnEdit.setVisibility(View.VISIBLE);
//                    }
//                    else
//                    {
//                        binding.btnEdit.setVisibility(View.GONE);
//                    }

                    binding.btnEdit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(viewModel.adapter.mList.get(position).getIsDuet())
                            {
                                Toast.makeText(getApplicationContext(),"Sorry You can't edit duet videos. Instead you can delete this draft and create new one.",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                showConfirmAlert("What you want to edit? Video itself or just video description?",viewModel.list.get(position));
                            }
                        }
                    });

                    binding.btPublish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(viewModel.type==2)
                                field="draft";
                            else
                                field="private_video";

                            new CustomDialogBuilder(PlayerActivity.this).showSimpleDialog("", "You want to\nPublish this post?", "Cancel", "Yes, Publish", new CustomDialogBuilder.OnDismissListener() {
                                @Override
                                public void onPositiveDismiss() {
                                    publishPost(viewModel.adapter.postId,field);
                                }

                                @Override
                                public void onNegativeDismiss() {

                                }
                            });


                        }
                    });
                }
                else
                {
                    binding.btPublish.setVisibility(View.GONE);
                    binding.btnEdit.setVisibility(View.GONE);
                }

            }
        }
    }

    private void publishPost(String videoId, String field)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "publish"));
        params.add(new Pair<>("videoId",videoId));
        params.add(new Pair<>("field",field));

        new AsyncHttpsRequest("Wait...!", this, params, this, 1, false).execute(Utils.Publish_Draft_Private);
    }

    private void initView() {
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(binding.rvVideos);

        customDialogBuilder = new CustomDialogBuilder(PlayerActivity.this);
    }

    private void initAds() {

        AdLoader.Builder builder = new AdLoader.Builder(this, getResources().getString(R.string.admobe_native_ad_id));
        builder.forUnifiedNativeAd(unifiedNativeAd -> {
            // You must call destroy on old ads when you are done with them,
            // otherwise you will have a memory leak.
            if (viewModel.adapter.unifiedNativeAd != null) {
                viewModel.adapter.unifiedNativeAd.destroy();
            }
            viewModel.adapter.unifiedNativeAd = unifiedNativeAd;

        });

        VideoOptions videoOptions = new VideoOptions.Builder()
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder()
                .setVideoOptions(videoOptions)
                .build();

        builder.withNativeAdOptions(adOptions);
        AdLoader adLoader = builder.build();
        adLoader.loadAd(new AdRequest.Builder().build());
        NativeAd nativeAd = new NativeAd(this, getResources().getString(R.string.admobe_native_ad_id));

        nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                if (nativeAd == null || nativeAd != ad) {
                    return;
                }
                viewModel.adapter.facebookNativeAd = nativeAd;
                Log.d(TAG, "Native ad is loaded and ready to be displayed!");
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        nativeAd.loadAd();

    }

    private void playVideo(String videoUrl, ItemVideoListBinding binding) {
        if (player != null) {
            player.removeListener(this);
            player.setPlayWhenReady(false);
            player.release();
        }
        playerBinding = binding;
        player = new SimpleExoPlayer.Builder(this).build();
        SimpleCache simpleCache = MyApplication.simpleCache;
        CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(Util.getUserAgent(this, getResources().getString(R.string.app_name)))
                , CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

        ProgressiveMediaSource progressiveMediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));
        binding.playerView.setPlayer(player);
        player.setPlayWhenReady(true);
        player.seekTo(0, 0);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.addListener(this);
        binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
        player.prepare(progressiveMediaSource, true, false);
    }
    private void playSingleVideo1(String videoUrl) {
        try {
            if (playerSingle != null) {
                playerSingle.removeListener(this);
                playerSingle.setPlayWhenReady(false);
                playerSingle.release();
            }
            binding.playerView1.setVisibility(View.VISIBLE);
            binding.loutAddComment.setVisibility(View.GONE);

          /* View view = binding.rvVideos.getLayoutManager().findViewByPosition(0);
           playerBinding = DataBindingUtil.bind(view);*/

            playerSingle = new SimpleExoPlayer.Builder(this).build();
            SimpleCache simpleCache = MyApplication.simpleCache;
            CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(Util.getUserAgent(this, getResources().getString(R.string.app_name)))
                    , CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

            ProgressiveMediaSource progressiveMediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));
            binding.playerView1.setPlayer(playerSingle);
            playerSingle.setPlayWhenReady(true);
            playerSingle.seekTo(0, 0);
            playerSingle.setRepeatMode(Player.REPEAT_MODE_ALL);
            playerSingle.addListener(this);
            binding.playerView1.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
            playerSingle.prepare(progressiveMediaSource, true, false);
        }
        catch (Exception e)
        {
            Log.i("exceptionnnn ", e.getMessage()+" pp");
            e.printStackTrace();
        }
    }

    private void playSingleVideo(String videoUrl) {
       try {
           if (player != null) {
               player.removeListener(this);
               player.setPlayWhenReady(false);
               player.release();
           }

          /* View view = binding.rvVideos.getLayoutManager().findViewByPosition(0);
           playerBinding = DataBindingUtil.bind(view);*/

           player = new SimpleExoPlayer.Builder(this).build();
           SimpleCache simpleCache = MyApplication.simpleCache;
           CacheDataSourceFactory cacheDataSourceFactory = new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(Util.getUserAgent(this, getResources().getString(R.string.app_name)))
                   , CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

           ProgressiveMediaSource progressiveMediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));
           //binding3.playerView.setPlayer(player);
           player.setPlayWhenReady(true);
           player.seekTo(0, 0);
           player.setRepeatMode(Player.REPEAT_MODE_ALL);
           player.addListener(this);
           //binding3.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
           player.prepare(progressiveMediaSource, true, false);
       }
       catch (Exception e)
       {
           Log.i("exceptionnnn ", e.getMessage()+" pp");
           e.printStackTrace();
       }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            viewModel.loadingVisibility.set(View.VISIBLE);
        } else if (playbackState == Player.STATE_READY) {
            viewModel.loadingVisibility.set(View.GONE);
        }
    }

    @Override
    public void onResume() {
        if (player != null) {
            player.setPlayWhenReady(true);
        }
        if (playerSingle != null) {
            playerSingle.setPlayWhenReady(true);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        if (playerSingle != null) {
            playerSingle.setPlayWhenReady(false);
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        if (playerSingle != null) {
            playerSingle.setPlayWhenReady(false);
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        Utils.hideViewCounts=false;
        if (player != null) {
            player.setPlayWhenReady(false);
            player.stop();
            player.release();
        }
        super.onDestroy();
    }

    @Override
    public void completeTask(String result, int response_code) {

        Log.v("paras","paras response= "+ result);

        if(response_code==1)
        {
            try {
                JSONObject obj = new JSONObject(result);
                String msg = obj.getString("sucess_msg");
                showAlert(msg);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else {
            viewModel.onShareSuccess.setValue(true);
        }
    }


    public void showAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PlayerActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void initPermission() {

            if (ActivityCompat.checkSelfPermission(PlayerActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(PlayerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST);
                }
                else
                {
                    startDownload();
                }
            } else {
                startDownload();
            }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startDownload();
            }
        }
    }


    public void showConfirmAlert(String msg, Video.Data video) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PlayerActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Video", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }
        });

        builder.setPositiveButton("Video Description Only", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();

                Intent intent = new Intent(PlayerActivity.this, PreviewActivity.class);
                intent.putExtra("post_video", Const.ITEM_BASE_URL+video.getPostVideo());
                intent.putExtra("post_image", video.getPostImage());
                intent.putExtra("is_front", false);
                if (video.getSoundId() != null && !video.getSoundId().isEmpty()) {
                    intent.putExtra("soundId", video.getSoundId());
                }
                intent.putExtra("post_sound", video.getSound());
                intent.putExtra("sound_image", video.getSoundImage());
                startActivityForResult(intent, 101);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void startDownload() {
        Log.d("DOWNLOAD", "startDownload: ");
        PRDownloader.download(Const.ITEM_BASE_URL + current_model.getPostVideo(), Utils.getCreatedAppDirectory(PlayerActivity.this).getAbsolutePath(), current_model.getPostVideo())
                .build()
                .setOnStartOrResumeListener(() -> customDialogBuilder.showLoadingDialog())
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        customDialogBuilder.hideLoadingDialog();
                        Toast.makeText(getApplicationContext(), "Loaded Successfully", Toast.LENGTH_SHORT).show();
                        File videoFile = new File(Utils.getCreatedAppDirectory(PlayerActivity.this).getAbsolutePath(), current_model.getPostVideo());
                        Utils.downloadedFile = videoFile.getPath();
                        Utils.VideoCreaterUserName=current_model.getUserName();
                        Utils.VideoCreaterName = current_model.getFullName();
                        Utils.VideoCreaterId = current_model.getUserId();
                        Utils.VideoId=current_model.getPostId();
                        Log.v("paras","paras download path = "+ Utils.downloadedFile);
                        startActivity(new Intent(PlayerActivity.this, DuetCameraActivity.class));
                    }

                    @Override
                    public void onError(Error error) {
                        customDialogBuilder.hideLoadingDialog();
                        Log.d("DOWNLOAD", "onError: " + error.getConnectionException().getMessage());
                    }
                });
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}