package com.fairtok.view.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.LoadAds;
import com.fairtok.databinding.ActivitySpinWinRewardBinding;
import com.fairtok.model.user.User;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.wallet.RedeemActivity;
import com.fairtok.view.wallet.WalletActivity;
import com.fairtok.viewmodel.SpinWinViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class SpinWinRewardActivity extends AppCompatActivity {

    ActivitySpinWinRewardBinding binding;
    private SpinWinViewModel viewModel;
    private TextView tvBack;
    private Uri mInvitationUrl;
    private String TAG = "SpinWinActivity";
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_spin_win_reward);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new SpinWinViewModel()).createFor()).get(SpinWinViewModel.class);

        sessionManager = new SessionManager(this);
        initView();
        createDynamicLink();
        initObserver();
    tvBack = findViewById(R.id.tvBack);
        tvBack.setOnClickListener(view -> {
            finish();
        });
    }
    private void initView(){
        viewModel.fetchMyWallet();
        viewModel.fetchCoinRate();
        viewModel.fetchImage();

        LinearLayout llAdview = binding.getRoot().findViewById(R.id.llAdview);
        LoadAds.loadAdmob(llAdview.getContext(),llAdview,getString(R.string.banner_ad_id));
    }
    @SuppressLint("SetTextI18n")
    private void initObserver(){
        viewModel.myWallet.observe(this, wallet ->{
            Log.i("wallettt ", wallet.getData().getTotalSilver());
            binding.tvGoldCoin.setText("Gold Coins  " + wallet.getData().getPaidCoins());
            binding.tvDiamondCoin.setText("Diamonds  " + wallet.getData().getDiamondCoins());

            User user = sessionManager.getUser();
            user.getData().setPaidCoins(wallet.getData().getPaidCoins());
            sessionManager.saveUser(user);
        });

        viewModel.spinWin.observe(this, spinWinModel -> {
            Glide.with(this).load(Const.BASE_URL_IMAGE+spinWinModel.getData().getValue()).into(binding.spinWinImg);
        });

        binding.setOnConvertGoldClick(view -> {
            if (viewModel.myWallet.getValue() != null) {

                Log.v("ljjhqjksh", String.valueOf(Integer.parseInt(viewModel.myWallet.getValue().getData().getPaidCoins())));

                if (Integer.parseInt(viewModel.myWallet.getValue().getData().getPaidCoins()) >
                        Integer.parseInt(viewModel.coinRate.getValue().getData().getMinGold())) {
                    showRedeemDialog("1");
                } else {
                    Toast.makeText(this, "Minimum " + viewModel.coinRate.getValue().getData().getMinGold() + " Gold required to convert & redeem", Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.setOnRedeemDiamondClick(view -> {
            Intent intent = new Intent(SpinWinRewardActivity.this, RedeemActivity.class);
            startActivity(intent);
        });

        binding.setOnShareViaWhatsapp(view -> {
            Intent sendInt = new Intent(Intent.ACTION_SEND);
            sendInt.setPackage("com.whatsapp");
            sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            sendInt.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_message) + "\n"+mInvitationUrl);
            sendInt.setType("text/plain");
            startActivity(Intent.createChooser(sendInt, "Share"));
        });
    }
    private void showRedeemDialog(String type) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SpinWinRewardActivity.this, R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_redeem);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);
        Button btnRedeem = bottomSheetDialog.findViewById(R.id.btnRedeem);
        tvMessage.setText(type.equals("1")? getString(R.string.goldCoin) : getString(R.string.diamondCoins));
        btnRedeem.setText(type.equals("1")? getString(R.string.strConvert) : getString(R.string.redeem));

        EditText etCoin = bottomSheetDialog.findViewById(R.id.etCoin);

        bottomSheetDialog.findViewById(R.id.btnRedeem).setOnClickListener(view -> {

            if (etCoin.getText().toString().trim().isEmpty()) {
                AppUtils.showToastSort(SpinWinRewardActivity.this, getString(R.string.pleaseEnterCoin));
            } else {
                bottomSheetDialog.dismiss();
                hitRedeemApi(type, etCoin.getText().toString().trim());
            }

        });
    }
    private void hitRedeemApi(String type, String coin) {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = type.equals("1") ? Const.BASE_URL + Const.convert_gold_coins : Const.BASE_URL + Const.convert_silver_coins;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("coins", coin)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            parseRedeem(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(SpinWinRewardActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseRedeem(JSONObject response) {
        try {
            showMessageDialog(response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showMessageDialog(String message) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SpinWinRewardActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_message);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            viewModel.fetchMyWallet();
        });
    }
    private void createDynamicLink(){
        String link = "https://fairtok.com/User="+Global.USER_ID;
        String imgUrl = Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile();
        URL url;
        Uri uri = null;
        try {
            url = new URL(imgUrl);
            uri = Uri.parse( url.toURI().toString() );
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://fairtok.page.link")
                //.setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder().setTitle("title").setImageUrl(uri).build())
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder("com.example.ios")
                                .setAppStoreId("123456789")
                                .setMinimumVersion("1.0.1")
                                .build())
                .buildShortDynamicLink()
                .addOnSuccessListener(new OnSuccessListener<ShortDynamicLink>() {
                    @Override
                    public void onSuccess(ShortDynamicLink shortDynamicLink) {
                        mInvitationUrl = shortDynamicLink.getShortLink();
                        Log.i(TAG, mInvitationUrl.toString());
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, e.getMessage());
                    }
                });

    }
}