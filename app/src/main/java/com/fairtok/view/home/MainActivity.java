package com.fairtok.view.home;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.adapter.MainViewPagerAdapter;
import com.fairtok.chat.ChatListActivity;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivityMainBinding;
import com.fairtok.databinding.BtnAddLytBinding;
import com.fairtok.databinding.BtnLiveLytBinding;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.openlive.activities.LiveUsersActivity1;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.GlobalApi;
import com.fairtok.utils.TimerTask;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.liveCall.LiveCallListActivity;
import com.fairtok.view.recordvideo.CameraActivity;
import com.fairtok.viewmodel.MainViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.fairtok.utils.Global.RC_SIGN_IN;

public class MainActivity extends BaseActivity implements CompleteTaskListner {

    private static final int CAMERA = 101;
    private static final int LIVE = 102;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
    private ActivityMainBinding binding;
    private MainViewModel viewModel;
    private TimerTask timerTask;
    String version;
    int verCode;
    PrefHandler prefHandler;
    private String token;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTransparentFlag();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new MainViewModel()).createFor()).get(MainViewModel.class);

        prefHandler = new PrefHandler(this);

        /*FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (task.isSuccessful()){
                    token = task.getResult();
                }

            }
        });*/
//        Uri uri = getIntent().getData();
//        if(uri != null)
//        {
//            List<String> params = uri.getPathSegments();
//            String id = params.get(params.size()-1);
//            Toast.makeText(this,id,Toast.LENGTH_LONG).show();
//        }


        startReceiver();
        initView();
        initTabLayout();
        //initFaceBook();


        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
            verCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            version = "1";
            verCode = 1;
            e.printStackTrace();
        }

        rewardDailyCheckIn();

        Intent intent = getIntent();
        handleIntentData(intent);


//        if(Utils.isFromAdPosting == true)
//        {
//            Utils.isFromAdPosting = false;
//        }
//        else


        //binding.splashLayout.setVisibility(View.VISIBLE);
/*

        new Handler().postDelayed(() -> {
            binding.splashLayout.setVisibility(View.GONE);
        }, 1000);
*/


        Log.d("TOKEN", "onCreate: " + Global.FIREBASE_DEVICE_TOKEN);
        binding.setViewModel(viewModel);
    }


    public void registerDeviceToken2(Context context, AppCompatActivity activity) {
        PrefHandler prefHandler = new PrefHandler(context);

        Log.d("paras", "FCM Token: " + token);

        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "register_device"));
        params.add(new Pair<>("userid", prefHandler.getuId()));
        params.add(new Pair<>("token", token));
        params.add(new Pair<>("device_type", "android"));

        params.add(new Pair<>("version", version));
        params.add(new Pair<>("version_code", "" + verCode));

        new AsyncHttpsRequest("", context, params, activity, 1, false).execute(Utils.DEVICE_REGISTER_URL);
    }

    private void initTabLayout() {
        MainViewPagerAdapter adapter = new MainViewPagerAdapter(MainActivity.this, getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        binding.viewPager.setAdapter(adapter);
        //binding.viewPager.setOffscreenPageLimit(4);
//        binding.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            public void onPageSelected(int pageNumber) {
//                // Just define a callback method in your fragment and call it like this!
//                adapter.getItem(pageNumber).imVisible();
//
//            }
//
//            public void onPageScrolled(int arg0, float arg1, int arg2) {
//                // TODO Auto-generated method stub
//
//            }
//
//            public void onPageScrollStateChanged(int arg0) {
//                // TODO Auto-generated method stub
//
//            }
//        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntentData(intent);
    }

    public void handleIntentData(Intent intent) {
        try {

            if (intent.hasExtra("pageFrom")) {

                if (intent.getStringExtra("pageFrom").equals("1"))
                    binding.tabLout.selectTab(binding.tabLout.getTabAt(1));
                else if (intent.getStringExtra("pageFrom").equals("2"))
                    binding.tabLout.selectTab(binding.tabLout.getTabAt(2));
                else if (intent.getStringExtra("pageFrom").equals("3"))
                    binding.tabLout.selectTab(binding.tabLout.getTabAt(3));
                else if (intent.getStringExtra("pageFrom").equals("4"))
                    binding.tabLout.selectTab(binding.tabLout.getTabAt(4));


            } else {

                if (intent.getBooleanExtra("needToGoToChatScreen", false)) {
                    if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                        startActivity(new Intent(MainActivity.this, ChatListActivity.class));
                    } else {
                        initLogin(MainActivity.this, () -> {

                        });
                    }
                }

                Log.v("paras", "paras msg = " + intent.getStringExtra("msg"));

                if (intent.getStringExtra("msg").contains("party")) {
                    if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                        startActivity(new Intent(MainActivity.this, LiveUsersActivity.class));
                    } else {
                        initLogin(MainActivity.this, () -> {

                        });
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        if (!Global.ACCESS_TOKEN.isEmpty()) {
            timerTask = new TimerTask();
        }
        for (int i = 0; i <= 4; i++) {
            switch (i) {
                case 0:
                    binding.tabLout.addTab(binding.tabLout.newTab().setIcon(R.drawable.a_home));
                    TabLayout.Tab tab = binding.tabLout.getTabAt(0);
                    if (tab != null && tab.getIcon() != null) {
                        tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.white));
                    }
                    break;
                case 1:
                    binding.tabLout.addTab(binding.tabLout.newTab().setIcon(R.drawable.a_explore));
                    break;

                case 2:
                    BtnAddLytBinding addLytBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.btn_add_lyt, null, false);
                    binding.tabLout.addTab(binding.tabLout.newTab().setCustomView(addLytBinding.getRoot()));
                    break;

                /*case 3:
                    binding.tabLout.addTab(binding.tabLout.newTab().setIcon(R.drawable.live_tv));
                    break;*/

                case 3:
                    BtnLiveLytBinding addLytBindingLive = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.btn_live_lyt, null, false);
                    binding.tabLout.addTab(binding.tabLout.newTab().setCustomView(addLytBindingLive.getRoot()));
                    break;

                case 4:
                    binding.tabLout.addTab(binding.tabLout.newTab().setIcon(R.drawable.a_profile));
                    break;
            }
        }


        binding.tabLout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab != null) {
                    if (tab.getPosition() == 0) {
                        viewModel.onStop.postValue(false);
                    } else {
                        viewModel.onStop.postValue(true);
                    }

                    switch (tab.getPosition()) {
                        case 0:
                            setStatusBarTransparentFlag();
                            viewModel.selectedPosition.setValue(0);
                            binding.viewPager.setCurrentItem(0);
                            tab.setIcon(R.drawable.a_home);
                            if (tab.getIcon() != null)
                                tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.white));
                            break;
                        case 1:
                            removeStatusBarTransparentFlag();
                            viewModel.loadingVisibility.set(View.GONE);
                            viewModel.selectedPosition.setValue(1);
                            binding.viewPager.setCurrentItem(1);
                            tab.setIcon(R.drawable.a_explore);
                            if (tab.getIcon() != null)
                                tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.white));
                            break;
                        case 2:
                            viewModel.loadingVisibility.set(View.GONE);
                            if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                                startActivityForResult(new Intent(MainActivity.this, CameraActivity.class), CAMERA);
                            } else {
                                initLogin(MainActivity.this, () -> {
//                                    initPhoneVerification(BroadcastActivity.this,()->{
                                    TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                                    if (tab1 != null) {
                                        tab1.select();
                                    }
//                                    });

                                });
                            }
                            break;
                        /*case 3:

                            if (tab.getIcon() != null)
                                tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.white));

                            if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {

                                //startActivityForResult(new Intent(MainActivity.this, LiveUserListActivity.class), LIVE);
                                startActivityForResult(new Intent(MainActivity.this, LiveCallListActivity.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        , LIVE);
                            } else {
                                initLogin(MainActivity.this, () -> {

                                    TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                                    if (tab1 != null) {
                                        tab1.select();
                                    }
                                });
                            }

                          *//*  viewModel.loadingVisibility.set(View.GONE);
                            removeStatusBarTransparentFlag();
                            viewModel.selectedPosition.setValue(2);
                            binding.viewPager.setCurrentItem(2);
                            tab.setIcon(R.drawable.a_notification);
                            if (tab.getIcon() != null)
                                tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.colorTheme));*//*
                            break;*/


                        case 3:
                            if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {

                                //startActivityForResult(new Intent(MainActivity.this, LiveUserListActivity.class), LIVE);
                                startActivityForResult(new Intent(MainActivity.this, LiveUsersActivity.class), LIVE);
                            } else {
                                initLogin(MainActivity.this, () -> {

                                    TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                                    if (tab1 != null) {
                                        tab1.select();
                                    }
                                });
                            }

                            break;

                        case 4:
                            viewModel.loadingVisibility.set(View.GONE);
                            removeStatusBarTransparentFlag();
                            if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                                binding.viewPager.setCurrentItem(4);
                                viewModel.selectedPosition.setValue(4);
                                tab.setIcon(R.drawable.a_profile);
                                if (tab.getIcon() != null)
                                    tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.white));
                            } else {
                                initLogin(MainActivity.this, () -> {
//                                    initPhoneVerification(BroadcastActivity.this,()->{
                                    TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                                    if (tab1 != null) {
                                        tab1.select();
                                    }
//                                    });
                                });
                            }
                            break;
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        if (tab.getIcon() != null)
                            tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.gray_light));
                        break;
                    case 1:
                        tab.setIcon(R.drawable.a_explore);
                        if (tab.getIcon() != null)
                            tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.gray_light));
                        break;
                    case 2:
                        //tab.setIcon(R.drawable.ic_home_strock);
                        break;
                    /*case 3:
                        tab.setIcon(R.drawable.live_tv);
                        if (tab.getIcon() != null)
                            tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.gray_light));
                        break;*/

                    case 3:
                        tab.setIcon(R.drawable.live_icon);
                        if (tab.getIcon() != null)
                            tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.gray_light));
                        break;

                    case 4:
                        tab.setIcon(R.drawable.a_profile);
                        if (tab.getIcon() != null)
                            tab.getIcon().setTint(ContextCompat.getColor(MainActivity.this, R.color.gray_light));
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 2) {
                    if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                        startActivityForResult(new Intent(MainActivity.this, CameraActivity.class), CAMERA);
                    } else {
                        initLogin(MainActivity.this, () -> {
                            TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                            if (tab1 != null) {
                                tab1.select();
                            }
                        });
                    }
                } else if (tab.getPosition() == 3) {
                    if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                        startActivityForResult(new Intent(MainActivity.this, LiveUsersActivity.class), LIVE);
                    } else {
                        initLogin(MainActivity.this, () -> {
                            TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                            if (tab1 != null) {
                                tab1.select();
                            }
                        });
                    }
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }


        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode == RESULT_OK && requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else if (requestCode == CAMERA) {
            TabLayout.Tab tab = binding.tabLout.getTabAt(0);
            if (tab != null) {
                tab.select();
            }
            if (requestCode == RESULT_OK) {
                viewModel.videoUpload.setValue(true);
            }
        } else if (requestCode == LIVE) {
            TabLayout.Tab tab = binding.tabLout.getTabAt(0);
            if (tab != null) {
                tab.select();
            }
            if (requestCode == RESULT_OK) {

            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("device_token", Global.FIREBASE_DEVICE_TOKEN);
                hashMap.put("user_email", account.getEmail());
                hashMap.put("full_name", account.getDisplayName());
                hashMap.put("login_type", Const.GOOGLE_LOGIN);
                hashMap.put("user_name", Objects.requireNonNull(account.getEmail()).split("@")[0]);
                hashMap.put("identity", account.getEmail());
                hashMap.put("mobile", prefHandler.getMobile());
                hashMap.put("language", prefHandler.getLanguage());
                hashMap.put("country", Global.COUNTRY);
                hashMap.put("state", Global.STATE);
                registerUser(hashMap, MainActivity.this, MainActivity.this);
                Log.e("Google login", "handleSignInResult: " + account.getEmail());
            }
            // Signed in successfully, show authenticated UI.

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Google login", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void rewardDailyCheckIn() {
        if (sessionManager.getStringValue("intime").isEmpty()) {
            new GlobalApi().rewardUser("2");
            sessionManager.saveStringValue("intime", new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date()));
        } else {
            try {
                simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
                Date date1 = simpleDateFormat.parse(sessionManager.getStringValue("intime"));
                Date date2 = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
                long difference = 0;
                if (date2 != null) {
                    if (date1 != null) {
                        difference = date2.getTime() - date1.getTime();
                    }
                }
                int days = (int) (difference / (1000 * 60 * 60 * 24));
                int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                hours = (hours < 0 ? -hours : hours);
                if (hours >= 24) {
                    new GlobalApi().rewardUser("2");
                    sessionManager.saveStringValue("intime", new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(new Date()));

                }
                Log.i("======= Hours", " :: " + hours + ":" + min);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected void onPause() {
        viewModel.onStop.setValue(true);
        if (timerTask != null) {
            timerTask.stopTimerTask();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        viewModel.onStop.setValue(true);
        if (timerTask != null) {
            timerTask.stopTimerTask();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        unregisterNetworkChanges();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if (binding.tabLout.getSelectedTabPosition() == 0) {
            viewModel.onStop.setValue(false);
        }
        if (binding.tabLout.getSelectedTabPosition() >= 3) {

            viewModel.selectedPosition.setValue(binding.tabLout.getSelectedTabPosition() - 1);
        } else {
            viewModel.selectedPosition.setValue(binding.tabLout.getSelectedTabPosition());
        }
        if (timerTask != null) {
            timerTask.doTimerTask();
        }
        registerDeviceToken2(this, this);
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        if (viewModel.selectedPosition.getValue() != null && viewModel.selectedPosition.getValue() != 0) {
            TabLayout.Tab tab = binding.tabLout.getTabAt(0);
            if (tab != null) {
                tab.select();
            }
            viewModel.selectedPosition.setValue(0);
        } else if (viewModel.selectedPosition.getValue() != null && viewModel.selectedPosition.getValue()
                == 0 && !viewModel.isBack) {
            /*viewModel.isBack = true;
            Toast.makeText(this, "Press Again To Exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> viewModel.isBack = false, 2000);*/
            startActivity(new Intent(this, LiveUsersActivity.class));
            finish();
        } else {


            startActivity(new Intent(this, LiveUsersActivity.class));
            finish();
            //super.onBackPressed();

//                if (!MyApplication.msg)
//                {
//                    super.onBackPressed();
//                }
//                else
//                {
//                   Toast.makeText(this,"Your video uploading in progress...",Toast.LENGTH_LONG).show();
//                }

        }
    }


    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);
        if (response_code == 1) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    String appVersionAdmin = obj.getString("app_version");
                    Log.d("paras", "paras FCM Token registered successfuly ");
                    PrefHandler prefHandler = new PrefHandler(MainActivity.this);
                    prefHandler.setAdEnabled(obj.getString("ad_enable"));
                    prefHandler.setAppVersionAdmin(appVersionAdmin);

                    prefHandler.setFollowersThreshold(obj.getInt("followers_threshold"));

                    try {
                        int appAdminCode = Integer.parseInt(appVersionAdmin);

                        Log.v("Paras", "Paras app version: " + verCode + " \nadminApp Version: " + appAdminCode);

                        if (verCode < appAdminCode) {
                            showUpdateAlert("New version is now available with new awesome features. This is mandatory update in order to use the app.");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (response_code == 0) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {
                    openOtpScreen(MainActivity.this);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (response_code == 2) {
            try {
                prefHandler.setIsMobileVerified(true);
                dismissBottom();

                initLoginAfterMobile(MainActivity.this, () -> {
                    TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                    if (tab1 != null) {
                        tab1.select();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (response_code == 3) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {
                    prefHandler.setIsMobileVerified(true);
                    dismissBottom();

                    initLoginAfterMobile(MainActivity.this, () -> {
                        TabLayout.Tab tab1 = binding.tabLout.getTabAt(0);
                        if (tab1 != null) {
                            tab1.select();
                        }
                    });
                } else
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response_code == 111) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Log.d("paras", "paras user is now online ");
                    Utils.isOnLine = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showUpdateAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                launchPlayStoreApp(MainActivity.this);
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void launchPlayStoreApp(Context context) {

        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}
