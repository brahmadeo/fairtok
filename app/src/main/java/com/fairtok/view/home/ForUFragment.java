package com.fairtok.view.home;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.hardware.display.DisplayManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextPaint;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdsManager;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.LiveUserAdapterNew;
import com.fairtok.adapter.LiveUserAdapterVideo;
import com.fairtok.adapter.VideoFullAdapter;
import com.fairtok.beauty.Camera.textmodule.TextActivity;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.FragmentForUBinding;
import com.fairtok.databinding.ItemFamousCreatorBinding;
import com.fairtok.databinding.ItemVideoListBinding;
import com.fairtok.model.ServiceData;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.videos.Video;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.services.ServiceCallback;
import com.fairtok.services.TimeSpendinAppService;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.utils.GlobalApi;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.VideoPlayerConfig;
import com.fairtok.view.LanguageSelection;
import com.fairtok.view.base.BaseFragment;
import com.fairtok.view.recordvideo.DuetCameraActivity;
import com.fairtok.view.search.FetchUserActivity;
import com.fairtok.view.search.HashTagActivity;
import com.fairtok.view.share.ShareSheetFragment;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;
import com.fairtok.viewmodel.ForUViewModel;
import com.fairtok.viewmodel.HomeViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.agora.rtc.Constants;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmClient;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;
import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;


public class ForUFragment extends BaseFragment implements Player.EventListener, CompleteTaskListner, LiveUserAdapterVideo.Listener {

    private LinearLayoutManager layoutManager1;
    private LinearLayoutManager layoutManager;
    public int lastPosition = -1;
    private SimpleExoPlayer player;
    private FragmentForUBinding binding;
    private ForUViewModel viewModel;
    private HomeViewModel parentViewModel;


    Video.Data current_model;

    private SimpleCache simpleCache;
    private CacheDataSourceFactory cacheDataSourceFactory;
    private ItemFamousCreatorBinding binding2;
    private String type;
    private NativeAd nativeAd;
    private AdLoader adLoader;
    private Boolean isFling = false;
    private CompositeDisposable disposable = new CompositeDisposable();

    SessionManager sessionManager;
    private CustomDialogBuilder customDialogBuilder;
    private static final int MY_PERMISSIONS_REQUEST = 101;
    PrefHandler prefHandler;
    int NUMBER_OF_ADS=5;
    private AlertDialog alertDialog;
    private String user_id;
    ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
    ServiceCallback serviceCallback;

    private static final int PERMISSION_REQ_CODE = 1 << 3;
    private final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private RtmClient mRtmClient;

    public static ForUFragment getNewInstance(String type) {
        ForUFragment fragment = new ForUFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_for_u, container, false);

        if (getActivity() != null) {
            parentViewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);
        }

        customDialogBuilder = new CustomDialogBuilder(getActivity());
        sessionManager = new SessionManager(getActivity());
        prefHandler = new PrefHandler(getActivity());

        Utils.hideViewCounts=false;

        if (sessionManager.getUser()!=null){
            user_id = "@" + sessionManager.getUser().getData().getUserName();
        }

        // Register to receive messages.
        // We are registering an observer (mMessageReceiver) to receive Intents
        // with actions named "custom-message".

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new ForUViewModel()).createFor()).get(ForUViewModel.class);
        initView();
        initTimeSpend();
        initAds();
        hitGetLiveList();
        initListeners();
        initObserve();
        binding.setViewmodel(viewModel);
        return binding.getRoot();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    TimeSpendinAppService mService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            TimeSpendinAppService.MyBinder binder = (TimeSpendinAppService.MyBinder) iBinder;
            mService = binder.getService();

            //mService.setCallbacks(serviceCallback);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

        }
    };

    private Long startTime;
    private void initTimeSpend(){
        startTime = new Date().getTime();
        Log.i("access_token ", Global.ACCESS_TOKEN+"");
        //prefHandler.setInTime(time);
    }

    private void doLogin(String liveId, String channelname) {
        //mIsInChat = true;
        //mUserId = user;
        //AppUtils.showRequestDialog(getContext());

        mRtmClient.login(null, user_id, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

                AppUtils.hideDialog();
                Log.i("postApi", "login success");
                runOnUiThread(() -> {
                    gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE, liveId, channelname);
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
                AppUtils.hideDialog();
                runOnUiThread(() -> {
                   // mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    private void ApiCallForTimeSpend(){
       // serviceCallback = getActivity();

        TimeSpendinAppService mService = new TimeSpendinAppService();

        Intent mServiceIntent = new Intent(getContext().getApplicationContext(), mService.getClass());
        mServiceIntent.setAction("startVideo");
        getContext().startService(mServiceIntent);

        Intent intent = new Intent(getContext(), TimeSpendinAppService.class);
        getContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("activityLifecycle ", "onStop");
        //prefHandler.setInTime(0l);
        if (player != null) {
            player.setPlayWhenReady(false);
            player.stop();
            player.release();
            player = null;
        }
    }


    private NativeAdsManager fbNativeManager;


    private void loadNativeAds() {
        AdLoader.Builder builder = new AdLoader.Builder(getActivity(),getResources().getString(R.string.admobe_native_ad_id));

        adLoader = builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            @Override
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                viewModel.adapter.unifiedNativeAd = unifiedNativeAd;
                viewModel.adapter.nativeAds.add(unifiedNativeAd);
                if (!adLoader.isLoading()) {
                    //insertAdsinMenuItem();
                }
            }
        }).withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                if (!adLoader.isLoading()) {
                    //insertAdsinMenuItem();
                }
            }
        }).build();

        adLoader.loadAds(new AdRequest.Builder().build(),NUMBER_OF_ADS);
    }



    private void initAds() {
        if (getActivity() != null) {

            loadNativeAds();

//            AdLoader.Builder builder = new AdLoader.Builder(getActivity(), getActivity().getResources().getString(R.string.admobe_native_ad_id));
//            builder.forUnifiedNativeAd(unifiedNativeAd -> {
//                // You must call destroy on old ads when you are done with them,
//                // otherwise you will have a memory leak.
//                if (viewModel.adapter.unifiedNativeAd != null) {
//                    viewModel.adapter.unifiedNativeAd.destroy();
//                }
//
//
//                if (adLoader.isLoading()) {
//                    // The AdLoader is still loading ads.
//                    // Expect more adLoaded or onAdFailedToLoad callbacks.
//                } else {
//                    // The AdLoader has finished loading ads.
//                    viewModel.adapter.nativeAds.add(unifiedNativeAd);
//                    viewModel.adapter.unifiedNativeAd = unifiedNativeAd;
//                }
//
//            });
//
//            VideoOptions videoOptions = new VideoOptions.Builder().build();
//            NativeAdOptions adOptions = new NativeAdOptions.Builder().setVideoOptions(videoOptions).build();
//            builder.withNativeAdOptions(adOptions);
//            adLoader = builder.build();
//            //adLoader.loadAd(new AdRequest.Builder().build());
//            adLoader.loadAds(new AdRequest.Builder().build(), 5);

            //Facebook




//            nativeAd = new NativeAd(getActivity(), getActivity().getResources().getString(R.string.fb_native_ad_id));
//
//            nativeAd.setAdListener(new NativeAdListener() {
//                @Override
//                public void onMediaDownloaded(Ad ad) {
//                    // Native ad finished downloading all assets
//                    Log.e(TAG, "Native ad finished downloading all assets.");
//                }
//
//                @Override
//                public void onError(Ad ad, AdError adError) {
//                    // Native ad failed to load
//                    Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
//                }
//
//                @Override
//                public void onAdLoaded(Ad ad) {
//                    if (nativeAd == null || nativeAd != ad) {
//                        return;
//                    }
//                    viewModel.adapter.facebookNativeAd = nativeAd;
//
//
//                    Log.d(TAG, "Native ad is loaded and ready to be displayed!");
//                }
//
//                @Override
//                public void onAdClicked(Ad ad) {
//                    // Native ad clicked
//                    Log.d(TAG, "Native ad clicked!");
//
//                }
//
//                @Override
//                public void onLoggingImpression(Ad ad) {
//                    // Native ad impression
//                    Log.d(TAG, "Native ad impression logged!");
//                }
//            });
//
//            // Request an ad
//            nativeAd.loadAd();


            //initNativeAds();
        }
    }

    public void initNativeAds(){
        viewModel.adapter.nativeAd = new ArrayList<>();

        fbNativeManager = new NativeAdsManager(getActivity(), getActivity().getResources().getString(R.string.fb_native_ad_id),8);
        try {
            fbNativeManager.setListener(new NativeAdsManager.Listener() {
                @Override
                public void onAdsLoaded() {
                    Log.i(TAG, "onAdsLoaded!" + fbNativeManager.getUniqueNativeAdCount());

                    int count = fbNativeManager.getUniqueNativeAdCount();
                    for (int i = 0; i < count; i++) {
                        NativeAd ad = fbNativeManager.nextNativeAd();
                        if(ad != null) {
                            addNativeAd(i, ad);
                        }
                    }
                }

                @Override
                public void onAdError(AdError adError) {

                }
            });
        }
        catch(NullPointerException e){
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        fbNativeManager.loadAds();
    }

    public void addNativeAd(int i, NativeAd ad) {

        try {
            if (ad == null) {
                return;
            }

            if (viewModel.adapter.nativeAd.size() > i && viewModel.adapter.nativeAd.get(i) != null) {
                viewModel.adapter.nativeAd.get(i).unregisterView();
                viewModel.adapter.nativeAd = null;
            }
            viewModel.adapter.nativeAd.add(i, ad);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void initView() {
        ChatManager mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();

        viewModel.language = prefHandler.getLanguage();
        binding.lytSwipe.setProgressViewOffset(true, 0, 220);
        binding.lytSwipe.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.napier_green),
                ContextCompat.getColor(getActivity(), R.color.india_red),
                ContextCompat.getColor(getActivity(), R.color.kellygreen),
                ContextCompat.getColor(getActivity(), R.color.tufs_blue),
                ContextCompat.getColor(getActivity(), R.color.tiffanyblue),
                ContextCompat.getColor(getActivity(), R.color.Sanddtorm),
                ContextCompat.getColor(getActivity(), R.color.salmonpink_1)
        );
        binding.lytSwipe.setOnRefreshListener(() -> {
            viewModel.start = 0;
            viewModel.fetchPostVideos(false);
            hitGetLiveList();
        });
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        binding.recyclerview.setLayoutManager(layoutManager);
        layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        binding.popularRecyclerview.setLayoutManager(layoutManager1);
        binding.refreshlout.setEnableRefresh(false);
        SnapHelper snapHelper = new PagerSnapHelper();
        binding.recyclerview.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(binding.recyclerview);
        SnapHelper snapHelper1 = new PagerSnapHelper();
        binding.popularRecyclerview.setOnFlingListener(null);
        snapHelper1.attachToRecyclerView(binding.popularRecyclerview);

        if (getArguments() != null) {
            type = getArguments().getString("type");
            if (type != null && type.equals("1")) {
                viewModel.postType = "related";
            } else {
                viewModel.postType = "following";
            }
            viewModel.fetchPostVideos(false);
        }
    }

    private void initListeners()
    {
        viewModel.famousAdapter.onRecyclerViewItemClick = (model, position, binding, type) -> {
            if (type == 1) {
                if (parentViewModel.onPageSelect.getValue() != null && parentViewModel.onPageSelect.getValue() == Integer.parseInt(ForUFragment.this.type)) {
                    lastPosition = position;
                    //playVideo(Const.ITEM_BASE_URL + model.getPostVideo(), binding);
                    playVideo(Const.VIDEO_ITEM_BASE_URL + model.getPostVideo(), binding);
                }
            } else {
                Intent intent = new Intent(getContext(), FetchUserActivity.class);
                intent.putExtra("userid", model.getUserId());
                startActivity(intent);
            }
        };

        if (sessionManager.getUser() != null) {
            viewModel.userId = sessionManager.getUser().getData().getUserId();
        }
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(binding.recyclerview);

        binding.recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE)
                {
                    int position = layoutManager.findFirstCompletelyVisibleItemPosition();
                    Animation animation = AnimationUtils.loadAnimation(binding.getRoot().getContext(), R.anim.slow_rotate);

                    if (position != -1 && lastPosition != position)
                    {
                        if (viewModel.adapter.mList.get(position) != null) {
                            if (binding.recyclerview.getLayoutManager() != null) {
                                View view = binding.recyclerview.getLayoutManager().findViewByPosition(position);
                                if (view != null) {
                                    lastPosition = position;
                                    ItemVideoListBinding binding1 = null, binding3 = null;
                                    if(position != 4){
                                        binding1 = DataBindingUtil.bind(view);
                                    }

                                    if (binding1 != null) {
                                        binding1.imgSound.startAnimation(animation);
                                        new GlobalApi().increaseView(binding1.getModel().getPostId());
                                        //playVideo(Const.ITEM_BASE_URL + viewModel.adapter.mList.get(position).getPostVideo(), binding1);
                                        playVideo(Const.VIDEO_ITEM_BASE_URL + viewModel.adapter.mList.get(position).getPostVideo(), binding1);
                                    }else{
                                        if (player != null) {
                                            player.setPlayWhenReady(false);
                                            player.stop();
                                            player.release();
                                            player = null;
                                            lastPosition = position;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (player != null) {
                                player.setPlayWhenReady(false);
                                player.stop();
                                player.release();
                                player = null;
                                lastPosition = position;
                            }
                        }
                    }
                }
            }
        });

        binding.popularRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int position = layoutManager1.findFirstCompletelyVisibleItemPosition();
                    if (position != -1 && lastPosition != position) {
                        if (binding.popularRecyclerview.getLayoutManager() != null) {
                            View view = binding.popularRecyclerview.getLayoutManager().findViewByPosition(position);
                            if (view != null) {
                                lastPosition = position;
                                ItemFamousCreatorBinding binding1 = DataBindingUtil.bind(view);

                                //playVideo(Const.ITEM_BASE_URL + viewModel.famousAdapter.mList.get(position).getPostVideo(), binding1);
                                playVideo(Const.VIDEO_ITEM_BASE_URL + viewModel.famousAdapter.mList.get(position).getPostVideo(), binding1);
                            }
                        }
                    }
                }
            }
        });


        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());

        viewModel.adapter.onRecyclerViewItemClick = new VideoFullAdapter.OnRecyclerViewItemClick() {
            @Override
            public void onItemClick(Video.Data model, int position, int type, ItemVideoListBinding binding) {

                current_model =model;

                switch (type) {
                    case 10:
                        binding.newFollowBtn.setVisibility(View.GONE);
                        handleButtonClick(model.getUserId());
                        String currentUserId=model.getUserId();
                        Log.v("Current","paras current userid = "+currentUserId);

                        List<Video.Data> list = viewModel.adapter.mList;

                        for(int i=0; i<list.size(); i++)
                        {
                            try
                            {
                                if(list.get(i).getUserId().equalsIgnoreCase(currentUserId))
                                {
                                    list.get(i).setIsFollow(1);
                                }
                            }
                            catch(Exception e){e.printStackTrace();}
                        }

                        viewModel.adapter.notifyDataSetChanged();
                        break;
                    // SWIPE LEFT to FetchUser Activity
                    case 0:
                        // Send to FetchUser Activity
                    case 1:
                        Intent swipeIntent = new Intent(getContext(), FetchUserActivity.class);
                        swipeIntent.putExtra("userid", model.getUserId());
                        startActivity(swipeIntent);
                        break;
                    // Play/Pause video
                    case 2:
                        if (player != null) {
                            if (player.isPlaying()) {
                                player.setPlayWhenReady(false);
                            } else {
                                player.setPlayWhenReady(true);
                            }
                        }
                        break;
                    // Send Bubble to creator
                    case 3:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            showSendBubblePopUp(model.getUserId());
                        } else {
                            if (getActivity() != null && getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).initLogin(getActivity(), () -> showSendBubblePopUp(model.getUserId()));
                            }
                        }
                        break;
                    // On like btn click
                    case 4:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            viewModel.likeUnlikePost(model.getPostId());
                        }else {
                            if (getActivity() != null && getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).initLogin(getActivity(), () -> {

                                });
                            }
                        }
                        break;
                    // On Comment Click
                    case 5:
                        CommentSheetFragment fragment = new CommentSheetFragment();
                        fragment.onDismissListener = count -> {
                            model.setPostCommentsCount(count);
                            binding.tvCommentCount.setText(Global.prettyCount(count));

                        };
                        Bundle args = new Bundle();
                        args.putString("postid", model.getPostId());
                        args.putInt("commentCount", model.getPostCommentsCount());
                        fragment.setArguments(args);
                        fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
                        break;
                    // On Share Click
                    case 6:
                        handleShareClick(model);
                        break;
                    // On Sound Disk Click
                    case 7:
                        if (Global.ACCESS_TOKEN.isEmpty()) {
                            if (getActivity() != null && getActivity() instanceof MainActivity) {
                                ((MainActivity) getActivity()).initLogin(getActivity(), () -> viewModel.likeUnlikePost(model.getPostId()));
                            }
                        } else {
                            Intent intent1 = new Intent(getContext(), SoundVideosActivity.class);
                            intent1.putExtra("soundid", model.getSoundId());
                            intent1.putExtra("sound", model.getSound());
                            startActivity(intent1);
                        }
                        break;
                    // On Long Click (Report Video)
                    case 8:
                        new CustomDialogBuilder(getContext()).showSimpleDialog("Report this post", "Are you sure you want to\nreport this post?", "Cancel", "Yes, Report", new CustomDialogBuilder.OnDismissListener() {
                            @Override
                            public void onPositiveDismiss() {
                                reportPost(model);
                            }

                            @Override
                            public void onNegativeDismiss() {

                            }
                        });

                        break;

                    case 9:
                        lastPosition = position;
                        //playVideo(Const.ITEM_BASE_URL + model.getPostVideo(), binding);
                        playVideo(Const.VIDEO_ITEM_BASE_URL + model.getPostVideo(), binding);
                        break;

                    case 20:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            binding.ivLike.setImageResource(R.drawable.ic_heart_gradient);
                            binding.ivLike.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(() -> binding.ivLike.setVisibility(View.INVISIBLE), 500);
                        }
                        break;

                    case 21:
                        if (!Global.ACCESS_TOKEN.isEmpty()) {
                            binding.ivLike.setImageResource(R.drawable.ic_heart);
                            binding.ivLike.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(() -> binding.ivLike.setVisibility(View.INVISIBLE), 500);
                        }
                        break;

                    case 22:
                        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
                            initPermission();
                        } else {
                            Toast.makeText(getActivity(),"You need to login first in order to create duet",Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case 23:
                       startActivity(new Intent(getContext(), SpinWinRewardActivity.class));
                        break;
                    case 24:
                        startActivity(new Intent(getActivity(), LanguageSelection.class));
                        break;
                }

            }

            @Override
            public void onHashTagClick(String hashTag) {
                Intent intent = new Intent(getContext(), HashTagActivity.class);
                intent.putExtra("hashtag", hashTag);
                startActivity(intent);
            }
        };

    }

    private void handleButtonClick(String userId) {

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            followUnfollow(userId);
        } else {
            Toast.makeText(getContext(), "You have to login first", Toast.LENGTH_SHORT).show();
        }
    }
    public void followUnfollow(String userId) {

        disposable.add(Global.initRetrofit().followUnFollow(Global.ACCESS_TOKEN, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((followRequest, throwable) -> {
//                    if (followRequest != null && followRequest.getStatus() != null) {
//                        if (isMyAccount.get() == 1) {
//                            isMyAccount.set(2);
//                        } else {
//                            isMyAccount.set(1);
//                        }
//                        followApi.setValue(followRequest);
//                    }
                    //viewModel.fetchPostVideos(false);
                }));
    }

    private void handleShareClick(Video.Data model) {
        upDateShareCount(model);
        ShareSheetFragment fragment = new ShareSheetFragment();
        Bundle args = new Bundle();
        args.putString("video", new Gson().toJson(model));
        fragment.setArguments(args);
        fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
    }

    public void upDateShareCount(Video.Data model)
    {
        PrefHandler prefHandler = new PrefHandler(getActivity());
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("user_id",prefHandler.getuId()));
        params.add(new Pair<>("post_id", model.getPostId()));
        new AsyncHttpsRequest("", getActivity(), params, this, 0, false).execute(Utils.UPDATE_SHARE_COUNT);
    }

    private void initObserve() {
        parentViewModel.uploadSuccess.observe(getViewLifecycleOwner(), aBoolean -> {
            if (getArguments() != null) {
                type = getArguments().getString("type");
                if (type != null && type.equals("1")) {
                    viewModel.postType = "related";
                } else {
                    viewModel.postType = "following";
                }
                viewModel.fetchPostVideos(false);
            }
        });
        parentViewModel.onStop.observe(getViewLifecycleOwner(), onStop -> {
            if (onStop != null) {
                if (parentViewModel.onPageSelect.getValue() != null && parentViewModel.onPageSelect.getValue() == Integer.parseInt(type) && player != null) {
                    player.setPlayWhenReady(!onStop);
                }
            }
        });
        viewModel.onLoadMoreComplete.observe(getViewLifecycleOwner(), onLoadMore -> {
            binding.refreshlout.finishLoadMore();
            binding.lytSwipe.setRefreshing(false);
        });

        viewModel.coinSend.observe(getViewLifecycleOwner(), coinSend -> coinSendAction(coinSend));

        viewModel.onShareSuccess.observe(getActivity(), isSuccess -> {
            if (isSuccess != null && isSuccess) {
               //paras
            }
        });
    }

    static boolean isDialogShowing=false;

    @Override
    public void liveUserClick(String liveId, String channel) {
        startActivity(new Intent(getContext(), LiveUsersActivity.class));
        //doLogin(liveId, channel);
        //gotoLiveActivity(Constants.CLIENT_ROLE_AUDIENCE, liveId, channel);
    }


    public class ViewDialog {

        private PortraitAnimator mAnimator;
        public void showDialog(Activity activity, String msg){
            isDialogShowing=true;

            if(activity !=null) {
                Dialog dialog = new Dialog(activity, R.style.CustomDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.live_users_popup);

                mAnimator = new PortraitAnimator(
                        dialog.findViewById(R.id.anim_layer_1),
                        dialog.findViewById(R.id.anim_layer_2),
                        dialog.findViewById(R.id.anim_layer_3));


                TextView text = (TextView) dialog.findViewById(R.id.text_message_incoming);
                //text.setText(msg);

                Button dialogButton = (Button) dialog.findViewById(R.id.btnJoin);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        isDialogShowing = false;
                        mAnimator.stop();
                        startActivity(new Intent(getActivity(), LiveUsersActivity.class));
                    }
                });

                dialog.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAnimator.stop();
                        isDialogShowing = false;
                        dialog.dismiss();
                    }
                });

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        isDialogShowing = false;
                    }
                });

                dialog.show();
                isDialogShowing = true;
                mAnimator.start();
                Window window = dialog.getWindow();
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    private boolean isIsDialogShowing = false;
    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(isDialogShowing==false) {
//                ViewDialog alert = new ViewDialog();
//                alert.showDialog(getActivity(), "text to replace");
            }
            if (intent.getBooleanExtra("isPopup", false)){
                if (arrayList!=null && arrayList.size()>0){
                    if (!isIsDialogShowing){
                        AlertDialog();
                    }
                    isIsDialogShowing = true;
                }
                //Toast.makeText(getContext(), "Show popup", Toast.LENGTH_SHORT).show();
            }
        }
    };

    LiveUserAdapterVideo liveUserAdapterNew;
    public void AlertDialog() {
        LayoutInflater m_inflater = LayoutInflater.from(getContext());

        View layout = m_inflater.inflate(R.layout.custom_dialog_live, null);
        RecyclerView recyclerviewLive = layout.findViewById(R.id.recyclerviewLive);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);

        TextView tvTitle = layout.findViewById(R.id.tvTitle);
        TextView tvMore = layout.findViewById(R.id.tvMore);
        TextView tvClose = layout.findViewById(R.id.tvClose);

        tvMore.setOnClickListener(view -> {
            startActivity(new Intent(getContext(), LiveUsersActivity.class));
        });
        tvClose.setOnClickListener(view -> {
            alertDialog.dismiss();
        });
        Shader textShader=new LinearGradient(0, 0, 0, 20,
                new int[]{getResources().getColor(R.color.pink_dark), getResources().getColor(R.color.yellow)},
                new float[]{0, 1}, Shader.TileMode.CLAMP);
        tvTitle.getPaint().setShader(textShader);

        /*TextPaint paint = tvTitle.getPaint();
        float width = paint.measureText("Live Users");
        Shader textShader = new LinearGradient(0, 0, width, tvTitle.getTextSize(),
                new int[]{
                        ContextCompat.getColor(getContext(), R.color.colorPink),
                        ContextCompat.getColor(getContext(), R.color.colorYellow),
                }, null, Shader.TileMode.CLAMP);
        tvTitle.getPaint().setShader(textShader);*/


        liveUserAdapterNew = new LiveUserAdapterVideo(getActivity(), arrayList, ForUFragment.this, "video");
        recyclerviewLive.setLayoutManager(new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false));
        recyclerviewLive.setAdapter(liveUserAdapterNew);
        alertDialog.show();
    }

    private void hitGetLiveList() {

        if (AppUtils.isNetworkAvailable(getContext())) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.LIVE_LIST;
            Log.v("postApi-token", Global.ACCESS_TOKEN);
            Log.v("postApi-url", url);

            //getAllLiveUsers();
            //testDialog("Authorization : " +Global.ACCESS_TOKEN+ " , "+ type);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("type", "1")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.v("postApi-url", String.valueOf(response));
                            //AppUtils.hideDialog();
                            parseLiveList(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            //testDialog(anError.getErrorCode()+" == "+anError.getErrorBody()+" === "+anError.getErrorDetail());
                            Log.v("postApi-url", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorCode()));
                            Log.v("postApi-url", String.valueOf(anError.getErrorBody()));
                            //AppUtils.hideDialog();
                            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(getContext(), "No internet available", Toast.LENGTH_SHORT).show();

    }

    android.app.AlertDialog.Builder mBuilder1 = null;
    private void testDialog(String msg){
        mBuilder1 = new android.app.AlertDialog.Builder(getActivity());
        mBuilder1.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        android.app.AlertDialog alert = mBuilder1.create();
        alert.show();
    }

    private void parseLiveList(JSONObject response) {

        if (arrayList!=null){
            arrayList.clear();
        }

        try {
            if (response.getBoolean("status")) {

                JSONArray jsonArray = response.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("post_hash_tag", jsonObject.getString("post_hash_tag"));
                    hashMap.put("live_start", jsonObject.getString("live_start"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                    hashMap.put("live_thumbnail", jsonObject.getString("live_thumbnail"));
                    hashMap.put("coinsReceived", jsonObject.getString("coinsReceived"));
                    hashMap.put("coinsReceivedSilver", jsonObject.getString("coinsReceivedSilver"));
                    hashMap.put("viewers_count", jsonObject.getString("viewers_count"));

                    arrayList.add(hashMap);
                    isIsDialogShowing = false;
                    //Fcm notification
                    /*if (type.equals("1") &&  username.equals(jsonObject.getString("user_name"))) {

                        Intent intent = new Intent("custom-message");
                        intent.putExtra("isBroadCaster", false);
                        intent.putExtra("isGaming", isGaming);
                        intent.putExtra("channelName", jsonObject.getString("user_name"));
                        intent.putExtra("live_id", jsonObject.getString("live_id"));

                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                        type = "";
                        username = "";

                    }*/
                }

            } /*else
                Toast.makeText(LiveUsersActivity.this, "" + response.getString("message"), Toast.LENGTH_SHORT).show();*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

       // liveUserAdapterNew.notifyDataSetChanged();

    }

    private void gotoLiveActivity(int role, String liveId, String channelName) {
        if (alertDialog!=null){
            alertDialog.dismiss();
        }
        if (player != null) {
            player.removeListener(this);
            player.setPlayWhenReady(false);
            player.release();
        }
        String room = channelName;
        String userId = user_id;
        //config().setChannelName(room);

        Intent intent = new Intent(getContext(), LiveActivityNew.class);
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, role);
        intent.setClass(getContext(), LiveActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, channelName);
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        intent.putExtra("liveId", liveId);
        intent.putExtra("from", "video");


        startActivity(intent);

    }

    private void coinSendAction(RestResponse response)
    {
        if(viewModel.isSent) {
            viewModel.isSent= false;
            showSendResult(response.getStatus());
        }
    }

    private void playVideo(String videoUrl, ItemVideoListBinding binding) {
        if (player != null) {
            player.removeListener(this);
            player.setPlayWhenReady(false);
            player.release();
        }
        if (getActivity() != null) {

            LoadControl loadControl = new DefaultLoadControl.Builder()
                    .setAllocator(new DefaultAllocator(true, 16))
                    .setBufferDurationsMs(VideoPlayerConfig.MIN_BUFFER_DURATION,
                            VideoPlayerConfig.MAX_BUFFER_DURATION,
                            VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
                            VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER)
                    .setTargetBufferBytes(-1)
                    .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();


            player = new SimpleExoPlayer.Builder(getActivity()).setLoadControl(loadControl).build();
            simpleCache = MyApplication.simpleCache;
            cacheDataSourceFactory = new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(Util.getUserAgent(getActivity(), getResources().getString(R.string.app_name)))
                    , CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

            ProgressiveMediaSource progressiveMediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));
            binding.playerView.setPlayer(player);
            if (parentViewModel.onPageSelect.getValue() != null && parentViewModel.onPageSelect.getValue().equals(Integer.parseInt(ForUFragment.this.type))) {
                player.setPlayWhenReady(true);
            }
            player.seekTo(0, 0);
            player.setRepeatMode(Player.REPEAT_MODE_ALL);
            player.addListener(this);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
            player.prepare(progressiveMediaSource, true, false);
        }
    }

    private void playVideo(String videoUrl, ItemFamousCreatorBinding binding) {
        if (player != null) {
            player.removeListener(this);
            player.setPlayWhenReady(false);
            player.release();
        }
        if (binding2 != null) {
            // run scale animation and make it smaller
            Animation anim = AnimationUtils.loadAnimation(binding2.getRoot().getContext(), R.anim.scale_out_tv);
            binding2.getRoot().startAnimation(anim);
            anim.setFillAfter(true);
        }
        binding2 = binding;
        // run scale animation and make it bigger
        Animation anim = AnimationUtils.loadAnimation(binding.getRoot().getContext(), R.anim.scale_in_tv);
        binding.getRoot().startAnimation(anim);
        anim.setFillAfter(true);
        if (getActivity() != null) {
            player = new SimpleExoPlayer.Builder(getActivity()).build();
            simpleCache = MyApplication.simpleCache;
            cacheDataSourceFactory = new CacheDataSourceFactory(simpleCache, new DefaultHttpDataSourceFactory(Util.getUserAgent(getActivity(), getResources().getString(R.string.app_name)))
                    , CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR);

            ProgressiveMediaSource progressiveMediaSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(videoUrl));
            binding.playerView.setPlayer(player);
            player.setPlayWhenReady(true);
            player.seekTo(0, 0);
            player.setRepeatMode(Player.REPEAT_MODE_ALL);
            player.addListener(this);
            binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH);
            player.prepare(progressiveMediaSource, true, false);
//            binding.playerView.setOnTouchListener(new View.OnTouchListener() {
//
//                private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
//
//                    @Override
//                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//                        super.onFling(e1, e2, velocityX, velocityY);
//                        float deltaX = e1.getX() - e2.getX();
//                        float deltaXAbs = Math.abs(deltaX);
//                        // Only when swipe distance between minimal and maximal distance value then we treat it as effective swipe
//                        if((deltaXAbs > 100) && (deltaXAbs < 1000)) {
//                            if(deltaX > 0)
//                            {
//                                //OpenProfile(item,true);
//
//                            }
//                        }
//
//
//                        return true;
//                    }
//
//                    @Override
//                    public boolean onSingleTapUp(MotionEvent e) {
//                        super.onSingleTapUp(e);
//
//                        return true;
//                    }
//
//                    @Override
//                    public void onLongPress(MotionEvent e) {
//                        super.onLongPress(e);
//
//                    }
//
//                    @Override
//                    public boolean onDoubleTap(MotionEvent e) {
//                        return super.onDoubleTap(e);
//                    }
//                });
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    gestureDetector.onTouchEvent(event);
//                    return true;
//                }
//            });
        }
    }

    private void showSendBubblePopUp(String userId) {

        new CustomDialogBuilder(getContext()).showSendCoinDialogue(new CustomDialogBuilder.OnCoinDismissListener() {
            @Override
            public void onCancelDismiss() {

            }

            @Override
            public void on5Dismiss() {
                viewModel.sendBubble(userId, "500");
            }

            @Override
            public void on10Dismiss() {
                viewModel.sendBubble(userId, "1000");
            }

            @Override
            public void on20Dismiss() {
                viewModel.sendBubble(userId, "5000");
            }
        });
    }

    private void showSendResult(boolean success) {
        new CustomDialogBuilder(getContext()).showSendCoinResultDialogue(success, success1 -> {
            if (!success1) {
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
            }
        });
    }

    private void reportPost(Video.Data model) {
        ReportSheetFragment fragment = new ReportSheetFragment();
        Bundle args = new Bundle();
        args.putString("postid", model.getPostId());
        args.putInt("reporttype", 1);
        fragment.setArguments(args);
        fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_BUFFERING) {
            parentViewModel.loadingVisibility.set(View.VISIBLE);
        } else if (playbackState == Player.STATE_READY) {
            parentViewModel.loadingVisibility.set(View.GONE);
        }
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            // Row is swiped from recycler view
            // remove it from adapter
            try {
                Intent swipeIntent = new Intent(getContext(), FetchUserActivity.class);
                swipeIntent.putExtra("userid", viewModel.adapter.mList.get(viewHolder.getAdapterPosition()).getUserId());
                startActivity(swipeIntent);
                //Toast.makeText(getContext(),"Swipe left",Toast.LENGTH_SHORT).show();
            }
            catch(NullPointerException e){
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver, new IntentFilter("live-popup-messagenew"));

        MyApplication.dataTime = new ServiceData(0l, Global.ACCESS_TOKEN);
        startTime = new Date().getTime();
        Log.i("activityLifecycle", "onResume");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("activityLifecycle", "onDestroyView");
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);

        Long time = new Date().getTime() - startTime;
        MyApplication.dataTime = new ServiceData(time, Global.ACCESS_TOKEN);
        ApiCallForTimeSpend();
        //implement your code only if !STATE_OFF - to prevent infinite loop onResume <-> onStop  while screen is off
        DisplayManager dm = (DisplayManager) getContext().getSystemService(Context.DISPLAY_SERVICE);
        for (Display display : dm.getDisplays()){
            if(display.getState() != Display.STATE_OFF){

                //implement your code only if device is not in "locked"  state
                KeyguardManager myKM = (KeyguardManager) getContext().getSystemService(Context.KEYGUARD_SERVICE);
                if( !myKM.inKeyguardRestrictedInputMode()) {
                    //If needed you can call finish() (call onDestroy()) and your logic will restart your activity only once.
                    //ApiCallForTimeSpend();
                    // implement your logic here (your logic probably restarts the activity)
                }
            }
        }
    }

    @Override
    public void completeTask(String result, int response_code) {

        Log.v("paras","paras response= "+ result);
//        viewModel.onShareSuccess.setValue(true);
    }

    private void initPermission() {
        checkPermission();
    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (!Environment.isExternalStorageManager()) {
                    Toast.makeText(getContext(), "Allow permission for storage access!", Toast.LENGTH_SHORT).show();
                    /*Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    startActivity(intent);*/
                    return;
                }
                startDownload();
            }
            startDownload();

        } else {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getContext(), permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }

    private void startDownload() {
        Log.d("DOWNLOAD", "startDownload: ");
        PRDownloader.download(Const.ITEM_BASE_URL + current_model.getPostVideo(), Utils.getCreatedAppDirectory(getActivity()).getAbsolutePath(), current_model.getPostVideo())
                .build()
                .setOnStartOrResumeListener(() -> customDialogBuilder.showLoadingDialog())
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        customDialogBuilder.hideLoadingDialog();
                        //Toast.makeText(getActivity(), "Loaded Successfully", Toast.LENGTH_SHORT).show();
                        File videoFile = new File(Utils.getCreatedAppDirectory(getActivity()).getAbsolutePath(), current_model.getPostVideo());
                        Utils.downloadedFile = videoFile.getPath();
                        Utils.VideoCreaterUserName=current_model.getUserName();
                        Utils.VideoCreaterName = current_model.getFullName();
                        Utils.VideoCreaterId = current_model.getUserId();
                        Utils.VideoId=current_model.getPostId();
                        Log.v("paras","paras download path = "+ Utils.downloadedFile);
                        startActivity(new Intent(getActivity(), DuetCameraActivity.class));
                    }

                    @Override
                    public void onError(Error error) {
                        customDialogBuilder.hideLoadingDialog();
                        Log.d("DOWNLOAD", "onError: " + error.getConnectionException().getMessage());
                    }
                });
    }

    private class PortraitAnimator {
        static final int ANIM_DURATION = 3000;

        private Animation mAnim1;
        private Animation mAnim2;
        private Animation mAnim3;
        private View mLayer1;
        private View mLayer2;
        private View mLayer3;
        private boolean mIsRunning;

        PortraitAnimator(View layer1, View layer2, View layer3) {
            mLayer1 = layer1;
            mLayer2 = layer2;
            mLayer3 = layer3;
            mAnim1 = buildAnimation(0);
            mAnim2 = buildAnimation(1000);
            mAnim3 = buildAnimation(2000);
        }

        private AnimationSet buildAnimation(int startOffset) {
            AnimationSet set = new AnimationSet(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(ANIM_DURATION);
            alphaAnimation.setStartOffset(startOffset);
            alphaAnimation.setRepeatCount(Animation.INFINITE);
            alphaAnimation.setRepeatMode(Animation.RESTART);
            alphaAnimation.setFillAfter(true);

            ScaleAnimation scaleAnimation = new ScaleAnimation(
                    1.0f, 1.3f, 1.0f, 1.3f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimation.setDuration(ANIM_DURATION);
            scaleAnimation.setStartOffset(startOffset);
            scaleAnimation.setRepeatCount(Animation.INFINITE);
            scaleAnimation.setRepeatMode(Animation.RESTART);
            scaleAnimation.setFillAfter(true);

            set.addAnimation(alphaAnimation);
            set.addAnimation(scaleAnimation);
            return set;
        }

        void start() {
            if (!mIsRunning) {
                mIsRunning = true;
                mLayer1.setVisibility(View.VISIBLE);
                mLayer2.setVisibility(View.VISIBLE);
                mLayer3.setVisibility(View.VISIBLE);
                mLayer1.startAnimation(mAnim1);
                mLayer2.startAnimation(mAnim2);
                mLayer3.startAnimation(mAnim3);
            }
        }

        void stop() {
            mLayer1.clearAnimation();
            mLayer2.clearAnimation();
            mLayer3.clearAnimation();
            mLayer1.setVisibility(View.GONE);
            mLayer2.setVisibility(View.GONE);
            mLayer3.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
