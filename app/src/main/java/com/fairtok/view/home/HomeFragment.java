package com.fairtok.view.home;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.fairtok.R;
import com.fairtok.adapter.HomeViewPagerAdapter;
import com.fairtok.databinding.FragmentMainBinding;
import com.fairtok.view.base.BaseFragment;
import com.fairtok.viewmodel.HomeViewModel;
import com.fairtok.viewmodel.MainViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    private FragmentMainBinding binding;
    private MainViewModel parentViewModel;
    private HomeViewModel viewModel;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            parentViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
        viewModel = ViewModelProviders.of(requireActivity(), new ViewModelFactory(new HomeViewModel()).createFor()).get(HomeViewModel.class);
        initView();
        initViewPager();
        initObserve();
        initListener();
    }

    private void initView() {
        parentViewModel.videoUpload.observe(getViewLifecycleOwner(), aBoolean -> {
            viewModel.uploadSuccess.setValue(aBoolean);
        });
        binding.tvFollowing.setTextColor(getResources().getColor(R.color.grey));
    }

    private void initViewPager() {
        if (getArguments()!=null){
            if (getArguments().getString("playVideo").equals("true")){
                HomeViewPagerAdapter homeViewPagerAdapter = new HomeViewPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
                binding.viewPager.setAdapter(homeViewPagerAdapter);
                binding.viewPager.setCurrentItem(1);
                viewModel.onPageSelect.postValue(1);
                binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        viewModel.onPageSelect.postValue(position);
                        viewModel.onStop.setValue(true);
                        if (position == 0) {
                            binding.tvForu.setTextColor(getResources().getColor(R.color.grey));
                            binding.tvFollowing.setTextColor(getResources().getColor(R.color.white));
                        } else {
                            binding.tvFollowing.setTextColor(getResources().getColor(R.color.grey));
                            binding.tvForu.setTextColor(getResources().getColor(R.color.white));
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
        }

    }

    private void initObserve() {
        parentViewModel.onStop.observe(getViewLifecycleOwner(), onStop -> {
            viewModel.onPageSelect.setValue(binding.viewPager.getCurrentItem());
            viewModel.onStop.postValue(onStop);
        });
        viewModel.loadingVisibility = parentViewModel.loadingVisibility;

    }

    private void initListener() {
        binding.tvFollowing.setOnClickListener(v -> {
            //binding.tvForu.setTextColor(getResources().getColor(R.color.grey));
            //binding.tvFollowing.setTextColor(getResources().getColor(R.color.white));
            binding.tvForu.setBackground(null);
            binding.tvFollowing.setBackground(getActivity().getDrawable(R.drawable.bg_btn_gradient));
            binding.viewPager.setCurrentItem(0);
        });
        binding.tvForu.setOnClickListener(v -> {
            //binding.tvFollowing.setTextColor(getResources().getColor(R.color.grey));
            //binding.tvForu.setTextColor(getResources().getColor(R.color.white));
            binding.tvFollowing.setBackground(null);
            binding.tvForu.setBackground(getActivity().getDrawable(R.drawable.bg_btn_gradient));
            binding.viewPager.setCurrentItem(1);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
