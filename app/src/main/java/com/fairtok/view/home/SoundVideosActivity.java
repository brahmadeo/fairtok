package com.fairtok.view.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.databinding.ActivitySoundVideosBinding;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.recordvideo.CameraActivity;
import com.fairtok.viewmodel.SoundActivityViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class SoundVideosActivity extends BaseActivity {


    ActivitySoundVideosBinding binding;
    SoundActivityViewModel viewModel;
    SimpleExoPlayer player;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sound_videos);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new SoundActivityViewModel()).createFor()).get(SoundActivityViewModel.class);
        initView();
        initListeners();
        initObserve();
        binding.setViewmodel(viewModel);
    }

    private void initView() {
        viewModel.soundId = getIntent().getStringExtra("soundid");
        viewModel.soundUrl = getIntent().getStringExtra("sound");
        viewModel.adapter.soundId = viewModel.soundId;
        viewModel.fetchSoundVideos(false);

        player = new SimpleExoPlayer.Builder(this).build();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(Const.ITEM_BASE_URL + viewModel.soundUrl));
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.prepare(videoSource);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale);
        binding.loutShoot.startAnimation(animation);
        binding.tvSoundTitle.setSelected(true);


        if (sessionManager != null && sessionManager.getFavouriteMusic() != null) {
            viewModel.isFavourite.set(sessionManager.getFavouriteMusic().contains(viewModel.soundId));
        }

    }

    private void initListeners() {
        binding.loutFavourite.setOnClickListener(v -> {
            sessionManager.saveFavouriteMusic(viewModel.soundId);
            viewModel.isFavourite.set(!viewModel.isFavourite.get());
        });

        binding.imgPlay.setOnClickListener(v -> {
            if (viewModel.isPlaying.get()) {
                player.setPlayWhenReady(false);
                viewModel.isPlaying.set(false);
            } else {
                player.setPlayWhenReady(true);
                viewModel.isPlaying.set(true);
            }
        });
        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());
        binding.imgBack.setOnClickListener(v -> onBackPressed());
        binding.loutShoot.setOnClickListener(v -> {
            Log.v("Paras","Paras sound id = "+viewModel.soundId);
            Intent intent = new Intent(this, CameraActivity.class);
            intent.putExtra("music_url", viewModel.soundUrl);
            intent.putExtra("music_title", viewModel.soundData.getValue() != null ? viewModel.soundData.getValue().getSoundTitle() : "Sound_Title");
            intent.putExtra("soundId", viewModel.soundId);
            startActivity(intent);
        });
    }

    private void initObserve() {
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
        viewModel.soundData.observe(this, soundData -> {
            binding.setSoundData(soundData);
            binding.tvVideoCount.setText(Global.prettyCount(soundData.getPostVideoCount()).concat(" Videos"));

        });
    }

    @Override
    protected void onPause() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        super.onPause();
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}