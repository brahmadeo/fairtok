package com.fairtok.view.home;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.FragmentCommentSheetBinding;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.search.FetchUserActivity;
import com.fairtok.viewmodel.CommentSheetViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class CommentSheetFragment extends DialogFragment implements CompleteTaskListner {

    public OnDismissListener onDismissListener;
    FragmentCommentSheetBinding binding;
    CommentSheetViewModel viewModel;
    EditText et_comment;

    private void initView() {

        if (getArguments() != null && getArguments().getString("postid") != null) {
            viewModel.postId = getArguments().getString("postid");
            viewModel.commentCount.set(getArguments().getInt("commentCount"));
        }

        if (getActivity() != null) {
            try {
                viewModel.sessionManager = new SessionManager(getActivity());
                if(viewModel.sessionManager.getBooleanValue(Const.IS_LOGIN)==true)
                    viewModel.userId = viewModel.sessionManager.getUser().getData().getUserId();
                else
                    viewModel.userId="0";
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        binding.refreshlout.setEnableRefresh(false);
        viewModel.fetchComments(false);

    }


//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
//        bottomSheetDialog.setOnShowListener(dialog1 -> {
//            BottomSheetDialog dialog = (BottomSheetDialog) dialog1;
//            dialog.setCanceledOnTouchOutside(false);
//
//        });
//
//        return bottomSheetDialog;
//
//    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_comment_sheet, container, false);
        et_comment=binding.etComment;
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CommentSheetViewModel()).createFor()).get(CommentSheetViewModel.class);

        initView();
        initListeners();
        initObserve();
        binding.setViewmodel(viewModel);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        closeKeyboard();
        onDismissListener.onDismissed(viewModel.commentCount.get());
    }

    public void updateCommentCount(String comment_id)
    {
        PrefHandler prefHandler = new PrefHandler(getActivity());
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("user_id",prefHandler.getuId()));
        params.add(new Pair<>("comment_id", comment_id));
        new AsyncHttpsRequest("", getActivity(), params, this, 0, false).execute(Utils.COMMENT_LIKE_UNLIKE);
    }


    private void initListeners() {
        viewModel.adapter.onReplyRecyclerViewItemClick= (comment, type) -> {
            switch (type) {

                case 3:

                    if(comment.getCommentIsLiked()==false) {
                        comment.setCommentIsLiked(1);
                        comment.setCommentLike(comment.getCommentLike()+1);
                    }else {
                        comment.setCommentIsLiked(0);
                        comment.setCommentLike(comment.getCommentLike()-1);
                    }

                    viewModel.adapter.notifyDataSetChanged();
                    updateCommentCount(comment.getCommentsId());
                    break;

                case 4:

                    binding.etComment.requestFocus();
                    binding.etComment.append("@"+comment.getUserName() +" ");
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(binding.etComment, InputMethodManager.SHOW_IMPLICIT);
                    break;

            }
        };
        viewModel.adapter.onRecyclerViewItemClick = (comment, position, type) -> {
            switch (type) {
                // On delete click
                case 1:
                    viewModel.callApitoDeleteComment(comment.getCommentsId(), position);
                    break;
                // On user Profile Click
                case 2:
                    Intent intent = new Intent(getActivity(), FetchUserActivity.class);
                    intent.putExtra("userid", comment.getUserId());
                    startActivity(intent);
                    break;

                case 3:

                    if(comment.getCommentIsLiked()==false) {
                        comment.setCommentIsLiked(1);
                        comment.setCommentLike(comment.getCommentLike()+1);
                    }else {
                        comment.setCommentIsLiked(0);
                        comment.setCommentLike(comment.getCommentLike()-1);
                    }

                    viewModel.adapter.notifyDataSetChanged();
                    updateCommentCount(comment.getCommentsId());
                    break;

                case 4:
                    Global.PARENT_ID=comment.getCommentsId();
                    binding.etComment.requestFocus();
                    binding.etComment.append("@"+comment.getUserName() +" ");
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(binding.etComment, InputMethodManager.SHOW_IMPLICIT);
                    break;

            }
        };
        binding.imgClose.setOnClickListener(v -> dismiss());
        binding.imgSend.setOnClickListener(v -> {
            if (Global.ACCESS_TOKEN.isEmpty()) {
                if (getActivity() != null && getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).initLogin(getActivity(), () -> viewModel.addComment());
                }
                closeKeyboard();
            } else {
                viewModel.addComment();
                binding.etComment.setText("");
            }
        });
        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());

    }

    private void initObserve() {
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> {
            binding.refreshlout.finishLoadMore();
            if (!onLoadMore) {
                binding.etComment.setText("");
                closeKeyboard();
            }
        });

        viewModel.isLoading.observe(this, onLoadComplete -> {

            if (!onLoadComplete) {
                binding.loutNoComments.setVisibility(View.GONE);
            }
            else
            {
                binding.loutNoComments.setVisibility(View.VISIBLE);
            }
        });

        viewModel.isEmpty.observe(this, isEmpty -> {
            if (isEmpty==true)
            {
                binding.myProgressBar.setVisibility(View.GONE);
                binding.loutNoComments.setVisibility(View.VISIBLE);
                binding.empty.setVisibility(View.VISIBLE);

            }
        });

    }

    public void closeKeyboard() {
        if (getDialog() != null) {
            InputMethodManager im = (InputMethodManager) getDialog().getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (im != null && getDialog().getCurrentFocus() != null) {
                im.hideSoftInputFromWindow(getDialog().getCurrentFocus().getWindowToken(), 0);
            }
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        }
    }

    @Override
    public void completeTask(String result, int response_code) {

    }

    public interface OnDismissListener {
        void onDismissed(int count);
    }
}