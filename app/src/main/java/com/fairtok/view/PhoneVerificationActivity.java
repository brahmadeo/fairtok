package com.fairtok.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class PhoneVerificationActivity extends BaseActivity implements CompleteTaskListner {

    private SessionManager sessionManager;
    PrefHandler prefHandler;
    CountryCodePicker ccp;
    TextInputEditText mobileNumber;

    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;
    //firebase auth object
    private FirebaseAuth mAuth;
    boolean flag=false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_phone);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        //initializing objects
        mAuth = FirebaseAuth.getInstance();

        sessionManager = new SessionManager(this);
        MyApplication.sessionManager = sessionManager;
        prefHandler = new PrefHandler(this);
        ccp= (CountryCodePicker) findViewById(R.id.ccp);

        mobileNumber = findViewById(R.id.mobile_number);

        if(prefHandler.isLoggedIn())
        {
            finish();
            startActivity(new Intent(PhoneVerificationActivity.this, MainActivity.class));
        }
        else if(prefHandler.getIsMobileVerified())
        {
            finish();
            startActivity(new Intent(PhoneVerificationActivity.this,GoogleLoginActivity.class));
        }

        findViewById(R.id.btnSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mobile = mobileNumber.getText().toString().trim();

                if (flag == false) {
                    if (mobileNumber.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter the mobile Number.", Toast.LENGTH_SHORT).show();
                    } else {
                        PrefHandler prefHandler = new PrefHandler(PhoneVerificationActivity.this);
                        prefHandler.setMobile(mobile);

//                 if(ccp.getSelectedCountryCode().equalsIgnoreCase("91")) {
//                     isIndian=true;
//                     sendSms(mobile, context, (MainActivity) context);
//                 }
//                 else
                        sendVerificationCode("+" + ccp.getSelectedCountryCode(), mobile, view.getContext());

                        Log.v("Paras", "Paras : " + ccp.getSelectedCountryCode());

                    }
                }
                else
                {
                    if (mobileNumber.getText().toString().trim().isEmpty())
                    {
                        Toast.makeText(getApplicationContext(),"Please enter the OTP.",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
    //                if(isIndian)
    //                    verifyMobile(mobile,context,(MainActivity)context);
    //                else
                            verifyVerificationCode(mobile);
                    }
                }
            }

        });

    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);
        if(response_code == 0)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {
                    openOtpScreen(PhoneVerificationActivity.this);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(response_code == 2)
        {
            try
            {
                prefHandler.setIsMobileVerified(true);
                finish();
                startActivity(new Intent(PhoneVerificationActivity.this,GoogleLoginActivity.class));

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(response_code == 3)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if(success==1)
                {
                    prefHandler.setIsMobileVerified(true);
                }
                else
                    Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

    }


    public void onClose() {
        showAlert("Please complete onetime login process to continue enjoying our app.");
    }

    public void showAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PhoneVerificationActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                initLogin(PhoneVerificationActivity.this, () -> {
                });
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String countryCode, String mobile, Context context) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                countryCode + mobile,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks);

        openOtpScreen(context);
    }

    public void openOtpScreen(Context context)
    {
        flag=true;
        mobileNumber.setText("");
        TextView tvLabel= findViewById(R.id.tvLabel);
        tvLabel.setText("Please enter OTP received in your mobile");
        mobileNumber.setHint("Enter OTP");
        ccp.setVisibility(View.GONE);
        findViewById(R.id.divider).setVisibility(View.GONE);
        Button btnSubmit=findViewById(R.id.btnSubmit);
        btnSubmit.setText("Submit");

    }


    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                mobileNumber.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        try {
            //creating the credential
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
            //signing the user
            signInWithPhoneAuthCredential(credential);
        }
        catch (Exception e){
            Toast toast = Toast.makeText(getApplicationContext(), "Verification Code is wrong", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneVerificationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            dismissBottom();
                            completeTask("Success", 2);

                            String message = "Your Mobile Number is Verified Successfully.";
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
