package com.fairtok.view.recordvideo;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.camera2.CameraManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.ExecuteCallback;
import com.arthenica.mobileffmpeg.FFmpeg;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.coremedia.iso.boxes.Container;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.fairtok.R;
import com.fairtok.beauty.Camera.MainActivity;
import com.fairtok.beauty.Camera.textmodule.adapter.RecyclerItemClickListener;
import com.fairtok.beauty.CameraGallery.FilterAdapter;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivityCameraDuetBinding;
import com.fairtok.databinding.DailogProgressBinding;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.media.BottomSheetImagePicker;
import com.fairtok.view.music.MusicFrameFragment;
import com.fairtok.view.preview.PreviewActivity;
import com.fairtok.viewmodel.DuetCameraViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.slider.Slider;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.controls.Audio;
import com.otaliastudios.cameraview.controls.Facing;
import com.otaliastudios.cameraview.controls.Flash;
import com.otaliastudios.cameraview.controls.Mode;
import com.otaliastudios.cameraview.engine.meter.ExposureMeter;
import com.otaliastudios.cameraview.filter.Filters;
import com.otaliastudios.cameraview.filter.MultiFilter;
import com.otaliastudios.cameraview.filters.BrightnessFilter;
import com.otaliastudios.cameraview.filters.ContrastFilter;
import com.otaliastudios.cameraview.filters.FillLightFilter;
import com.otaliastudios.cameraview.filters.SharpnessFilter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL;
import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS;

public class DuetCameraActivity extends BaseActivity implements CompleteTaskListner, EventListener {

    private static final int MY_PERMISSIONS_REQUEST = 101;
    public DuetCameraViewModel viewModel;
    private ActivityCameraDuetBinding binding;
    private CustomDialogBuilder customDialogBuilder;
    private Dialog mBuilder;
    private DailogProgressBinding progressBinding;
    private boolean isFront = false;
    private int rangeSelectedValue = 0;
    TextView mCountDown;
    int is_filter_open = 0;
    int filterPos=0;
    FilterAdapter mAdapter;
    String videoPath;
    int left_h;
    int bottom_hardware_height;
    int height,width;
    boolean is_camera_front=true;
    boolean foucs = true;
    public static int selected_aspect_ratio=0;
    private CameraManager mCameraManager;
    CameraView camera;
    private String mCameraId;
    boolean isChecked=false;
    File file, audioFile, outputFile,outputFileCompressed;
    File musicFile;
    File duetFileCompressed,duetFile;
    private SimpleExoPlayer player;
    com.google.android.exoplayer2.ui.PlayerView playerView;

    String video_resolution="480x800";

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camera_duet);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new DuetCameraViewModel()).createFor()).get(DuetCameraViewModel.class);
        customDialogBuilder = new CustomDialogBuilder(DuetCameraActivity.this);
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        initView();
        initListener();
        initObserve();
        initProgressDialog();
        binding.setViewModel(viewModel);
        playerView = binding.playerView;

        Utils.isDuetVideo=true;

        binding.music.setVisibility(View.GONE);
        binding.btnTimer.setVisibility(View.GONE);
        binding.tvSelect.setVisibility(View.GONE);

        camera = findViewById(R.id.myGLSurfaceView);
        camera.setLifecycleOwner(this);
        camera.setFacing(Facing.FRONT);
        camera.setExposureCorrection(ExposureMeter.STATE_COMPLETED);


        FillLightFilter fillLightFilter = (FillLightFilter)Filters.FILL_LIGHT.newInstance();//0~1.0
        fillLightFilter.setStrength(0.5f);

        BrightnessFilter brightnessFilter = (BrightnessFilter)Filters.BRIGHTNESS.newInstance();//1.0~2.0
        brightnessFilter.setBrightness(1.3f);

        SharpnessFilter sharpnessFilter = (SharpnessFilter)Filters.SHARPNESS.newInstance();//0~1.0
        sharpnessFilter.setSharpness(0);

        ContrastFilter contrastFilter = (ContrastFilter)Filters.CONTRAST.newInstance();//1.0~2.0
        contrastFilter.setContrast(1.0f);

        MultiFilter multiFilter = new MultiFilter(
                //fillLightFilter,
                brightnessFilter,
                sharpnessFilter,
                contrastFilter
        );

        camera.setFilter(brightnessFilter);


        try{
            camera.setZoom(0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        camera.addCameraListener(new CameraListener() {

            @Override
            public void onZoomChanged(float newValue, @NonNull float[] bounds, @Nullable PointF[] fingers) {

                Log.v("paras","Paras zoom value changed");
                // newValue: the new zoom value
                // bounds: this is always [0, 1]
                // fingers: if caused by touch gestures, these is the fingers position
            }
        });

        duetFile = new File(Utils.downloadedFile);
        GetVideoDurationInMillisecond(duetFile);
        prepareExoPlayerFromFileUri(Utils.downloadedFile);

        binding.myGLSurfaceViewParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.timerLayout.getVisibility() == View.VISIBLE)
                {
                    binding.timerLayout.setVisibility(View.GONE);
                }
                else
                {
                    binding.timerLayout.setVisibility(View.VISIBLE);
                }
            }
        });


        rangeSelectedValue = (int) binding.slider.getValue();
        binding.secondsTv.setText(rangeSelectedValue+"s/15s");
        binding.slider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                rangeSelectedValue = (int) value;
                binding.secondsTv.setText(rangeSelectedValue+"s/15s");
            }
        });

        binding.startShooting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                binding.timerLayout.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(DuetCameraActivity.this);
                View view = LayoutInflater.from(DuetCameraActivity.this).inflate(R.layout.countdown_layout,null);
                mCountDown = view.findViewById(R.id.remaining);
                builder.setView(view);
                AlertDialog alertDialog = builder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alertDialog.show();
                new CountDownTimer(rangeSelectedValue * 1000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        int seconds = (int) (millisUntilFinished / 1000);
                        seconds = seconds + 1;
                        MediaPlayer mp = MediaPlayer.create(getBaseContext(), (R.raw.tick));
                        mp.start();
                        mCountDown.setText(""+seconds);
                    }

                    public void onFinish() {
                        alertDialog.dismiss();
                        //viewModel.onDurationUpdate.setValue((long) (rangeSelectedValue * 1000));
                        if (!viewModel.isRecording.get()) {
                            player.setPlayWhenReady(true);
                            startReCording();
                        }
                    }

                }.start();
            }
        });
    }

    private void prepareExoPlayerFromFileUri(String video){
        player = new SimpleExoPlayer.Builder(this).build();

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(DuetCameraActivity.this, Util.getUserAgent(DuetCameraActivity.this, getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(video));

        player.prepare(videoSource);
        player.setRepeatMode(Player.REPEAT_MODE_ALL);
        player.setPlaybackParameters(PlaybackParameters.DEFAULT);
        binding.playerView.setPlayer(player);
        binding.playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        player.setPlayWhenReady(false);
    }

    public void flashConfig(View view)
    {
        Log.v("Paras","paras flash clicked");

        if(isChecked==false) {
            isChecked=true;
            viewModel.mCameraView.setFlash(Flash.TORCH);
        }
        else {
            isChecked=false;
            viewModel.mCameraView.setFlash(Flash.OFF);
        }

    }


    @SuppressLint("RestrictedApi")
    private void initView()
    {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height =displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        Point p=getRealScreenSize(DuetCameraActivity.this);
        Log.e("camsize"," : "+p.x+" : "+p.y);
        bottom_hardware_height=p.y-height;
        height=p.y;
        width=p.x;
        is_camera_front=true;

        left_h=(height-((width/3)*4)-bottom_hardware_height);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        binding.filterListView.setLayoutManager(linearLayoutManager);
        mAdapter = new FilterAdapter(this, MainActivity.imageFilter2, 0,left_h*46/100);
        binding.filterListView.setAdapter(this.mAdapter);
        binding.filterListView.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            public void onItemClick(View view, final int position) {

                filterPos = position;
                //mCurrentConfig=BroadcastActivity.EFFECT_CONFIGS[position];
                showText1(filterPos, null);
                try {
                    if (filterPos != 0) {
                        binding.seekBar.setVisibility(View.VISIBLE);
                        binding.seekBar.setProgress(80);
                    } else {
                        binding.seekBar.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                }
                try {
                    viewModel.mCameraView.post(new Runnable() {
                        public void run() {
                            try {
//                                if (foucs) {
//                                    viewModel.mCameraView.setFilterWithConfig(BroadcastActivity.EFFECT_CONFIGS[position]);
//                                } else {
//                                    viewModel.mCameraView.setFilterWithConfig(BroadcastActivity.camFilNormal[position]);
//                                }
//                                viewModel.mCameraView.setFilterIntensity(0.8f);

                                setFilter(position);

                            } catch (Exception e) {
                            }
                        }
                    });
                } catch (Exception e2) {
                }
            }
        }));

        String musicUrl = getIntent().getStringExtra("music_url");
        try{Log.v("Paras","Paras musicUrl = "+musicUrl);}catch (Exception e){e.printStackTrace();}
        if (musicUrl != null && !musicUrl.isEmpty()) {
            downLoadMusic(getIntent().getStringExtra("music_url"));
            if (getIntent().getStringExtra("music_title") != null) {
                binding.tvSoundTitle.setText(getIntent().getStringExtra("music_title"));
            }
            if (getIntent().getStringExtra("soundId") != null) {
                viewModel.soundId = getIntent().getStringExtra("soundId");
            }
        }

        binding.ivSelect.setVisibility(View.GONE);

        close_filter();
        binding.seekBar.setVisibility(View.INVISIBLE);

        if (viewModel.onDurationUpdate.getValue() != null) {
            binding.progressBar.enableAutoProgressView(viewModel.onDurationUpdate.getValue());
        }
        binding.progressBar.setDividerColor(Color.WHITE);
        binding.progressBar.setDividerEnabled(true);
        binding.progressBar.setDividerWidth(4);
        binding.progressBar.SetListener(mills -> {
            viewModel.isEnabled.set(mills >= 14500);

            if (mills == viewModel.onDurationUpdate.getValue()) {
                binding.btnCapture.setVisibility(View.GONE);
                binding.btnCapture.setClickable(false);

                binding.startShooting.setClickable(false);
                binding.startShooting.setEnabled(false);
                binding.startShooting.setVisibility(View.GONE);

                stopRecording();
            }
        });

        binding.ivSelect.setOnClickListener(v -> {
            if (!viewModel.isRecording.get())
            {
                //saveData(true); //commented as compression is not required
                PreviewActivity.applyWaterMark=true;
                saveData(true);
            }
        });
        binding.progressBar.setShader(new int[]{ContextCompat.getColor(this, R.color.colorTheme), ContextCompat.getColor(this, R.color.colorTheme), ContextCompat.getColor(this, R.color.colorTheme)});
        viewModel.mCameraView = binding.myGLSurfaceView;

        binding.seekBar.setVisibility(View.GONE);
    }


    void showText1(final int msg, final String msg1)
    {
        Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(200);
        fadeIn.setStartOffset(100);
        final Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeOut.setDuration(1000);
        fadeOut.setStartOffset(1000);
        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation arg0) {
                binding.filterText.startAnimation(fadeOut);
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationStart(Animation arg0) {
                binding.filterText.setVisibility(View.VISIBLE);
                if (msg1 == null) {
                    binding.filterText.setText(FilterAdapter.filterName1(msg));
                } else {
                    binding.filterText.setText(msg1);
                }
            }
        });
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation arg0) {
                binding.filterText.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation arg0) {
            }

            public void onAnimationStart(Animation arg0) {
            }
        });
        binding.filterText.startAnimation(fadeIn);
    }

    public static Point getRealScreenSize(Context context)
    {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();

        if (Build.VERSION.SDK_INT >= 17) {
            display.getRealSize(size);
        } else if (Build.VERSION.SDK_INT >= 14) {
            try {
                size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (IllegalAccessException e) {} catch (InvocationTargetException e) {} catch (NoSuchMethodException e) {}
        }

        return size;
    }

    public void open_filter()
    {
        is_filter_open = 0;
        ObjectAnimator animator = ObjectAnimator.ofFloat(binding.layoutFilterLayer, "translationY", binding.layoutFilterLayer.getHeight(), 0);
        animator.setDuration(200);
        animator.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                //     findViewById(R.id.save).setClickable(false);
                binding.layoutFilterLayer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {

            }
        });
        animator.start();
    }
    public void close_filter()
    {
        is_filter_open=1;
        ObjectAnimator animator = ObjectAnimator.ofFloat(binding.layoutFilterLayer, "translationY", 0 ,  binding.layoutFilterLayer.getHeight());
        animator.setDuration(200L);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                binding.layoutFilterLayer.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationCancel(Animator animator) {
                binding.layoutFilterLayer.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }


    private void initListener()
    {
        binding.btnCapture.setOnClickListener(v -> {

            if (!viewModel.isRecording.get())
            {
                startReCording();
                player.setPlayWhenReady(true);
            } else {
                player.setPlayWhenReady(false);
                stopRecording();
            }
        });
        binding.btnFlip.setOnClickListener(v ->
        {
            viewModel.isFacingFront.set(!viewModel.isFacingFront.get());
            if (viewModel.isFacingFront.get()) {
                is_camera_front=false;
                viewModel.mCameraView.setFacing(Facing.BACK);

            } else {
                is_camera_front=true;
                viewModel.mCameraView.setFacing(Facing.FRONT);
            }
        });
        binding.tvSelect.setOnClickListener(v -> {
            BottomSheetImagePicker bottomSheetImagePicker = BottomSheetImagePicker.Companion.getNewInstance(1);
            bottomSheetImagePicker.setOnDismiss(uri -> {
                if (!uri.isEmpty())
                {
                    File file = new File(uri);

                    String someFilepath=file.getAbsolutePath();
                    String extension = someFilepath.substring(someFilepath.lastIndexOf("."));
                    Log.v("TAG","Hello extension = "+extension);

                    //MatroskaExtractor, FragmentedMp4Extractor, Mp4Extractor, Mp3Extractor, AdtsExtractor,
                    // Ac3Extractor, TsExtractor, FlvExtractor, OggExtractor, PsExtractor, WavExtractor, AmrExtractor, Ac4Extractor
                    if(extension.matches(".mp4") || extension.matches(".flv")||extension.matches(".wav")||extension.matches(".mov"))
                    {
                        // Get length of file in bytes
                        long fileSizeInBytes = file.length();
                        // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                        long fileSizeInKB = fileSizeInBytes / 1024;
                        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                        long fileSizeInMB = fileSizeInKB / 1024;
                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                        retriever.setDataSource(this, Uri.fromFile(new File(uri)));
                        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMilliSec = Long.parseLong(time);
                        if (timeInMilliSec > 60000) {
                            customDialogBuilder.showSimpleDialog("Too long video", "This video is longer than 1 min.\nPlease select onOther..",
                                    "Cancel", "Select onOther", new CustomDialogBuilder.OnDismissListener() {
                                        @Override
                                        public void onPositiveDismiss() {
                                            bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());
                                        }

                                        @Override
                                        public void onNegativeDismiss() {

                                        }
                                    });
                        } else if (fileSizeInMB < 80) {

                            viewModel.videoPaths = new ArrayList<>();
                            viewModel.videoPaths.add(uri);
                            if (fileSizeInMB > 5) {
                                //saveData(true);  // commented as compression is not required
                                PreviewActivity.applyWaterMark=true;
                                saveData(false);

                            } else {
                                PreviewActivity.applyWaterMark=true;
                                customDialogBuilder.showLoadingDialog();
                                saveData(false);
                            }
                        } else {
                            customDialogBuilder.showSimpleDialog("Too long video's size", "This video's size is grater than 60Mb.\nPlease select onOther..",
                                    "Cancel", "Select onOther", new CustomDialogBuilder.OnDismissListener() {
                                        @Override
                                        public void onPositiveDismiss() {
                                            bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());
                                        }

                                        @Override
                                        public void onNegativeDismiss() {

                                        }
                                    });
                        }
                        retriever.release();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Sorry this file format is not supported. Please try diffrent video",Toast.LENGTH_SHORT).show();
                    }


                }
            });
            bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());
        });
        binding.ivClose.setOnClickListener(v -> customDialogBuilder.showSimpleDialog("Are you sure", "Do you really wan to go back ?",
                "No", "Yes", new CustomDialogBuilder.OnDismissListener() {
                    @Override
                    public void onPositiveDismiss() {
                        //1.86
                        onBackPressed();
                    }

                    @Override
                    public void onNegativeDismiss() {

                    }
                }));
    }


    private void recreateCamera()
    {
//        viewModel.mCameraView.cameraInstance().setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//        viewModel.mCameraView.presetRecordingSize(1080, 1920);
//        viewModel.mCameraView.cameraInstance().setPreferPreviewSize(1080,1920);
//        viewModel.mCameraView.setFitFullView(true);
//        viewModel.mCameraView.resumePreview();
    }


    private void initObserve()
    {
        viewModel.parentPath = getPath().getPath();
        viewModel.onItemClick.observe(this, type -> {
            if (type != null) {
                if (type == 1) {
                    MusicFrameFragment frameFragment = new MusicFrameFragment();
                    frameFragment.show(getSupportFragmentManager(), frameFragment.getClass().getSimpleName());
                }
                else if (type == 2)
                {
                    if (binding.timerLayout.getVisibility() == View.VISIBLE)
                    {
                        binding.timerLayout.setVisibility(View.GONE);
                    }
                    else
                    {
                        binding.timerLayout.setVisibility(View.VISIBLE);
                    }
                }
                else if (type == 3)
                {
                    mAdapter.notifyDataSetChanged();
                    if(is_filter_open==1)
                    {
                        open_filter();
                        if (filterPos == 0)
                        {
                            binding.seekBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            binding.seekBar.setVisibility(View.VISIBLE);
                        }

                    }
                    else
                    {
                        close_filter();
                        binding.seekBar.setVisibility(View.INVISIBLE);
                    }
                }
                viewModel.onItemClick.setValue(null);
            }
        });
        viewModel.onSoundSelect.observe(this, sound -> {
            if (sound != null)
            {
                camera.setAudio(Audio.OFF);
                binding.tvSoundTitle.setText(sound.getSoundTitle());
                downLoadMusic(sound.getSound());
            }
        });
        viewModel.onDurationUpdate.observe(this, aLong -> binding.progressBar.enableAutoProgressView(aLong));
    }

    @SuppressLint("RestrictedApi")
    private void stopRecording()
    {
        player.setPlayWhenReady(false);

        binding.btnCapture.clearAnimation();
        if (viewModel.audio != null) {
            viewModel.audio.pause();
        }
        viewModel.count += 1;
        binding.progressBar.pause();
        binding.progressBar.addDivider();
        if(viewModel.mCameraView.isTakingVideo())
        {
            // Later... stop recording. This will trigger onVideoTaken().
            camera.stopVideo();
            viewModel.isRecording.set(false);
            recreateCamera();

//            viewModel.mCameraView.endRecording(new CameraRecordGLSurfaceView.EndRecordingCallback() {
//                @Override
//                public void endRecordingOK() {
//                    viewModel.isRecording.set(false);
//                    recreateCamera();
//
//                }
//            });
        }
    }

    private void initPermission()
    {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_REQUEST);
        } else {
            recreateCamera();
        }
    }

    public void initProgressDialog()
    {
        mBuilder = new Dialog(this);
        mBuilder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mBuilder.setCancelable(false);
        mBuilder.setCanceledOnTouchOutside(false);
        if (mBuilder.getWindow() != null) {
            mBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        mBuilder.setCancelable(false);
        mBuilder.setCanceledOnTouchOutside(false);
        progressBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dailog_progress, null, false);
        progressBinding.progressBar.setShader(new int[]{ContextCompat.getColor(this, R.color.colorTheme), ContextCompat.getColor(this, R.color.colorTheme), ContextCompat.getColor(this, R.color.colorTheme)});

        Animation rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);
        Animation reverseAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate_reverse);
        progressBinding.ivParent.startAnimation(rotateAnimation);
        progressBinding.ivChild.startAnimation(reverseAnimation);
        mBuilder.setContentView(progressBinding.getRoot());
    }

    public void showProgressDialog() {
        if (!mBuilder.isShowing()) {
            mBuilder.show();
        }
    }

    public void hideProgressDialog() {
        try {
            mBuilder.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    onBackPressed();
                }
                recreateCamera();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void downLoadMusic(String endPoint)
    {

        PRDownloader.download(Const.ITEM_BASE_URL + endPoint, getPath().getPath(), "recordSound.aac")
                .build()
                .setOnStartOrResumeListener(() -> customDialogBuilder.showLoadingDialog())
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        customDialogBuilder.hideLoadingDialog();
                        viewModel.isStartRecording.set(true);
                        viewModel.createAudioForCamera();
                    }

                    @Override
                    public void onError(Error error) {
                        customDialogBuilder.hideLoadingDialog();
                    }
                });
    }


    private void startReCording()
    {
        videoPath=getPath().getPath().concat("/video".concat(String.valueOf(viewModel.count)).concat(".mp4"));

        viewModel.isStartRecording.set(true);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.scale);
        binding.btnCapture.startAnimation(animation);

        if (binding.progressBar.getProgressPercent() != 100)
        {

            file = new File(getPath(), "video".concat(String.valueOf(viewModel.count)).concat(".mp4"));

            Log.v("video path","Paras video Path = "+videoPath);

            viewModel.videoPaths.add(getPath().getPath().concat("/video").concat(String.valueOf(viewModel.count)).concat(".mp4"));
            camera.setMode(Mode.VIDEO);

            if (viewModel.audio != null) {
                viewModel.audio.start();
            }

            camera.takeVideoSnapshot(file);

            binding.progressBar.resume();
            viewModel.isRecording.set(true);

        }
        else
        {
            Log.v("Paras","Paras else % = "+binding.progressBar.getProgressPercent());
        }
    }

    public void ffMpegTask()
    {
        try {

            long duration = GetVideoDuration(file);
            String[] cmds = new String[18];
            cmds[0] = "-y";
            cmds[1] = "-i";
            cmds[2] = file.getPath();

            cmds[3] = "-i";
            cmds[4] = audioFile.getPath();
            cmds[5] = "-c:a";
            cmds[6] = "copy";
            cmds[7] = "-c:v";
            cmds[8] = "copy";
            cmds[9] = "-map";
            cmds[10] = "0:v:0";
            cmds[11] = "-map";
            cmds[12] = "1:a:0";
            cmds[13] = "-t";
            cmds[14] = ""+duration;
            cmds[15] = "-shortest";
            cmds[16] = "-an";
            cmds[17] = outputFile.getPath();

            long executionId = FFmpeg.executeAsync(cmds, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int returnCode) {
                    if (returnCode == RETURN_CODE_SUCCESS) {
                        Log.i(Config.TAG, "paras : merging audio & camera video completed successfully.");
                        gotoNextScreen(outputFile,true);

                        //combineBothVideo(outputFile,true);
                    } else if (returnCode == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "paras : merging audio & camera video cancelled by user.");
                    } else {
                        Log.i(Config.TAG, String.format("paras : merging audio & camera video failed with rc=%d.", returnCode));
                    }
                }
            });


        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void saveData(boolean isCompress)
    {

        customDialogBuilder.showLoadingDialog();

        isFront=viewModel.isFacingFront.get();
        try{outputFile = new File(getPath(), "finally".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputFileCompressed = new File(getPath(), "outputFileCompressed".concat(".mp4"));}catch (Exception e){e.printStackTrace();}

        if (isCompress)
        {
            if(viewModel.audio !=null)
            {
                try{audioFile = new File(getPath(), "recordSound.aac");}catch (Exception e){e.printStackTrace();}
                ffMpegTask();
            }
            else
            {
                Log.v("Paras","Paras in else viewModel.audio is null here");
                Log.i("TAG", "onCombineFinished: " + "is original sound");
                withoutExternalMusic();
            }
        }
        else
        {
            galleryVideo(new File(viewModel.videoPaths.get(0)));
        }

    }

    public void withoutExternalMusic()
    {
        try{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Track audio;
                    try {
                        String[] commandArray = new String[]{"-y", "-i", file.getPath(), "-vn","-acodec", "copy", getPath().getPath().concat("/originalSound.aac")};


                        long executionId = FFmpeg.executeAsync(commandArray, new ExecuteCallback() {

                            @Override
                            public void apply(final long executionId, final int rc) {
                                if (rc == RETURN_CODE_SUCCESS) {
                                    Log.i(Config.TAG, "Async command execution completed successfully.");

                                } else if (rc == RETURN_CODE_CANCEL) {
                                    Log.i(Config.TAG, "Async command execution cancelled by user.");
                                } else {
                                    Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                                }
                            }
                        });

//                    Movie m1 = MovieCreator.build(file.getPath());
//                    audio = m1.getTracks().get(1);
//                    Movie m2 = new Movie();
//                    m2.addTrack(audio);
//                    DefaultMp4Builder builder = new DefaultMp4Builder();
//                    Container stdMp4 = builder.build(m2);
//                    FileOutputStream fos = new FileOutputStream(getPath().getPath().concat("/originalSound.aac"));
//                    stdMp4.writeContainer(fos.getChannel());
//                    fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Glide.with(DuetCameraActivity.this)
                            .asBitmap()
                            .load(Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile())
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    File soundImage = new File(getPath(), "soundImage.jpeg");
                                    try {
                                        FileOutputStream stream = new FileOutputStream(soundImage);
                                        resource.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                                        stream.flush();
                                        stream.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    hideProgressDialog();
                                    gotoNextScreen(file,true);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {
                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    super.onLoadFailed(errorDrawable);

                                    gotoNextScreen(file,true);
                                }
                            });

                }
            }).start();

        }catch (Exception e){e.printStackTrace();}
    }


    public void galleryVideo(File videoFile)
    {
        new Thread(() -> {
            Track audio;
            try {
                Movie m1 = MovieCreator.build(videoFile.getPath());
                audio = m1.getTracks().get(1);
                Movie m2 = new Movie();
                m2.addTrack(audio);
                DefaultMp4Builder builder = new DefaultMp4Builder();
                Container stdMp4 = builder.build(m2);
                FileOutputStream fos = new FileOutputStream(getPath().getPath().concat("/originalSound.aac"));
                stdMp4.writeContainer(fos.getChannel());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                File galleryFile = new File(viewModel.videoPaths.get(0));
                gotoNextScreen(galleryFile,false);
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }).start();
    }



    public void gotoNextScreen(File file, boolean isCamera)
    {
        try{
            String[] commandArray
                    = new String[]{"-y", "-i", file.getPath(), "-s", video_resolution, "-r", "25",
                    "-vcodec", "h264","-b:v", "300k", "-b:a", "48000", "-ac", "2", "-ar", "22050", outputFileCompressed.getPath()};



            long executionId = FFmpeg.executeAsync(commandArray, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "paras : resize camera video successfully.");
                        compressDuetVideo(outputFileCompressed,isCamera);

                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "paras : resize camera video cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("paras : resize camera video failed with rc=%d.", rc));
                        compressDuetVideo(file,isCamera);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
            compressDuetVideo(file,isCamera);
        }

    }


    public void compressDuetVideo(File file, boolean isCamera)
    {
        try{

            File duetFile = new File(Utils.downloadedFile);
            duetFileCompressed = new File(getPath(),"duetCompressed.mp4");


            String[] commandArray
                    = new String[]{"-y", "-i", duetFile.getPath(),  "-s", video_resolution, "-r", "25",
                    "-vcodec", "h264","-b:v", "300k", "-b:a", "48000", "-ac", "2", "-ar", "22050", duetFileCompressed.getPath()};


            long executionId = FFmpeg.executeAsync(commandArray, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {
                        Log.i(Config.TAG, "paras : resize duet video successfully.");

                        combineBothVideo(outputFileCompressed,isCamera);


                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "paras : resize duet video cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("paras : resize duet video failed with rc=%d.", rc));
                        combineBothVideo(file,isCamera);

                    }
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
            combineBothVideo(file,isCamera);
        }

    }


    public void combineBothVideo(File file, boolean isCamera)
    {
        //extract the audio

        new Thread(() -> {
            Track audio;
            try {
                Movie m1 = MovieCreator.build(duetFile.getPath());
                audio = m1.getTracks().get(1);
                Movie m2 = new Movie();
                m2.addTrack(audio);
                DefaultMp4Builder builder = new DefaultMp4Builder();
                Container stdMp4 = builder.build(m2);
                FileOutputStream fos = new FileOutputStream(getPath().getPath().concat("/originalSound.aac"));
                stdMp4.writeContainer(fos.getChannel());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();


        try{


            File outputDuetFile = new File(getPath(),"duet.mp4");


            /*

            -i input1.mp4 \
  -i input2.mp4 \
  -filter_complex '[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' \
  -map [vid] \
  -c:v libx264 \
  -crf 23 \
  -preset veryfast \
  output.mp4

   // Original command


            String[] commandArray = new String[]{};
            commandArray = new String[]{
                    "-y",
                    "-i", file.getPath(),
                    "-i", duetFileCompressed.getPath(),
                   -filter_complex", "[1:v][0:v]scale2ref[wm][base];[base][wm]hstack=2",
                    "-map", "1:a",
                    outputDuetFile.getPath()};


             */

            //ffmpeg -i left.mp4 -i right.mp4 -filter_complex hstack output.mp4

            //"-filter_complex", "[1:v][0:v]scale2ref[wm][base];[base][wm]hstack=2",


            String[] commandArray = new String[]{};
            commandArray = new String[]{
                    "-y",
                    "-i", file.getPath(),
                    "-i", duetFileCompressed.getPath(),
                    "-filter_complex", "[1:v][0:v]scale2ref[wm][base];[base][wm]hstack=2",
                    "-map", "1:a",
                    "-c:v", "libx264",
                    "-crf", "23",
                    "-preset", "veryfast",
                    outputDuetFile.getPath()};

            /*
             "-map", "[vid]",
                    "-c:v", "libx264",
                    "-crf", "23",
                    "-preset", "veryfast",
             */

            long executionId = FFmpeg.executeAsync(commandArray, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {
                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        gotoNextAfterCompress(outputDuetFile,isCamera);
                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {
                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));
                        gotoNextAfterCompress(file,isCamera);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();
            gotoNextAfterCompress(file,isCamera);
        }

    }

    public void gotoNextAfterCompress(File file,boolean isCamera)
    {
        Utils.isDuetVideo=true;
        try {

            if(isCamera) {
                File thumbFile = new File(getPath(), "temp.jpg");
                try {
                    FileOutputStream stream = new FileOutputStream(thumbFile);

                    Bitmap bmThumbnail;
                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(),
                            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                    if (bmThumbnail != null) {
                        bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    stream.flush();
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Glide.with(DuetCameraActivity.this)
                        .asBitmap()
                        .load(Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile())
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                File soundImage = new File(getPath(), "soundImage.jpeg");
                                try {
                                    FileOutputStream stream = new FileOutputStream(soundImage);
                                    resource.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                                    stream.flush();
                                    stream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                player.setPlayWhenReady(false);
                                player.stop();
                                customDialogBuilder.hideLoadingDialog();
                                Intent intent = new Intent(DuetCameraActivity.this, PreviewActivity.class);
                                intent.putExtra("post_video", file.getPath());
                                intent.putExtra("post_image", thumbFile.getPath());
                                intent.putExtra("is_front", isFront);
                                if (viewModel.soundId != null && !viewModel.soundId.isEmpty()) {
                                    intent.putExtra("soundId", viewModel.soundId);
                                }
                                intent.putExtra("post_sound", getPath().getPath().concat("/originalSound.aac"));
                                intent.putExtra("sound_image", soundImage.getPath());
                                startActivityForResult(intent, 101);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);

                                player.setPlayWhenReady(false);
                                player.stop();
                                customDialogBuilder.hideLoadingDialog();
                                Intent intent = new Intent(DuetCameraActivity.this, PreviewActivity.class);
                                intent.putExtra("post_video", file.getPath());
                                intent.putExtra("post_image", thumbFile.getPath());
                                intent.putExtra("is_front", isFront);
                                if (viewModel.soundId != null && !viewModel.soundId.isEmpty()) {
                                    intent.putExtra("soundId", viewModel.soundId);
                                }
                                intent.putExtra("post_sound", getPath().getPath().concat("/originalSound.aac"));
                                startActivityForResult(intent, 101);
                            }
                        });
            }
            else
            {
                File thumbFile = new File(getPath(), "temp.jpg");
                try {
                    FileOutputStream stream = new FileOutputStream(thumbFile);

                    Bitmap bmThumbnail;
                    bmThumbnail = ThumbnailUtils.createVideoThumbnail(file.getPath(),
                            MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                    if (bmThumbnail != null) {
                        bmThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    }
                    stream.flush();
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Glide.with(DuetCameraActivity.this)
                        .asBitmap()
                        .load(Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile())
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                File soundImage = new File(getPath(), "soundImage.jpeg");
                                try {
                                    FileOutputStream stream = new FileOutputStream(soundImage);
                                    resource.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                                    stream.flush();
                                    stream.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                customDialogBuilder.hideLoadingDialog();
                                player.setPlayWhenReady(false);
                                player.stop();
                                Intent intent = new Intent(DuetCameraActivity.this, PreviewActivity.class);
                                intent.putExtra("post_video", file.getPath());
                                intent.putExtra("post_image", thumbFile.getPath());
                                intent.putExtra("post_sound", getPath().getPath().concat("/originalSound.aac"));
                                intent.putExtra("sound_image", soundImage.getPath());
                                startActivityForResult(intent, 101);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);
                                customDialogBuilder.hideLoadingDialog();
                                player.setPlayWhenReady(false);
                                player.stop();
                                Intent intent = new Intent(DuetCameraActivity.this, PreviewActivity.class);
                                intent.putExtra("post_video", file.getPath());
                                intent.putExtra("post_image", thumbFile.getPath());
                                intent.putExtra("post_sound", getPath().getPath().concat("/originalSound.aac"));
                                //   intent.putExtra("sound_image", getPath().getPath().concat("/soundImage.jpeg"));
                                startActivityForResult(intent, 101);
                            }
                        });
            }

        }catch (Exception e){e.printStackTrace();}
    }

    public File getPath()
    {
        String state = Environment.getExternalStorageState();
        File filesDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            filesDir = getExternalFilesDir(null);
        } else {
            // Load another directory, probably local memory
            filesDir = getFilesDir();
        }
        if (filesDir != null) {
            viewModel.parentPath = filesDir.getPath();
        }
        return filesDir;
    }


    public long GetVideoDuration(File videoFile)
    {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(DuetCameraActivity.this, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(time));
        retriever.release();
        return seconds;
    }

    public long GetVideoDurationInMillisecond(File videoFile)
    {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(DuetCameraActivity.this, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long milliseconds = TimeUnit.MILLISECONDS.toMillis(Long.parseLong(time));

        if(milliseconds<30000)
            viewModel.onDurationUpdate.setValue(milliseconds);
        else
            viewModel.onDurationUpdate.setValue(30000L);

        retriever.release();
        return milliseconds;
    }

    @Override
    public void onDestroy()
    {
        if (viewModel.isRecording.get()) {
            stopRecording();
        }
        super.onDestroy();
    }

    @Override
    protected void onResume()
    {
        Log.d("TAG", "onResume: ");
        initPermission();
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (viewModel.isRecording.get()) {
            stopRecording();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (viewModel.isRecording.get()) {
            stopRecording();
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            onBackPressed();
        }
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    public void setFilter(int position)
    {
        switch (position)
        {
            case 0:
                camera.setFilter(Filters.NONE.newInstance());
                break;

            case 2:
                camera.setFilter(Filters.AUTO_FIX.newInstance());
                break;

            case 3:
                camera.setFilter(Filters.BLACK_AND_WHITE.newInstance());
                break;

            case 4:
                camera.setFilter(Filters.BRIGHTNESS.newInstance());
                break;

            case 5:
                camera.setFilter(Filters.CONTRAST.newInstance());
                break;

            case 6:
                camera.setFilter(Filters.CROSS_PROCESS.newInstance());
                break;

            case 7:
                camera.setFilter(Filters.DOCUMENTARY.newInstance());
                break;

            case 8:
                camera.setFilter(Filters.DUOTONE.newInstance());
                break;

            case 9:
                camera.setFilter(Filters.FILL_LIGHT.newInstance());
                break;

            case 10:
                camera.setFilter(Filters.GAMMA.newInstance());
                break;

            case 11:
                camera.setFilter(Filters.GRAIN.newInstance());
                break;

            case 12:
                camera.setFilter(Filters.GRAYSCALE.newInstance());
                break;

            case 13:
                camera.setFilter(Filters.HUE.newInstance());
                break;

            case 14:
                camera.setFilter(Filters.INVERT_COLORS.newInstance());
                break;

            case 15:
                camera.setFilter(Filters.LOMOISH.newInstance());
                break;

            case 16:
                camera.setFilter(Filters.POSTERIZE.newInstance());
                break;

            case 17:
                camera.setFilter(Filters.SATURATION.newInstance());
                break;

            case 18:
                camera.setFilter(Filters.SEPIA.newInstance());
                break;

            case 19:
                camera.setFilter(Filters.SHARPNESS.newInstance());
                break;

            case 20:
                camera.setFilter(Filters.TEMPERATURE.newInstance());
                break;

            case 21:
                camera.setFilter(Filters.TINT.newInstance());
                break;

            case 22:
                camera.setFilter(Filters.VIGNETTE.newInstance());
                break;

            default:
                camera.setFilter(Filters.NONE.newInstance());
                break;

        }
    }

    @Override
    public void completeTask(String result, int response_code) {

    }

    public void correctVideoAudioTiming(Movie m)
    {
        LinkedList<Track> videoTracks = new LinkedList<>();
        LinkedList<Track> audioTracks = new LinkedList<>();
        double[] audioDuration = {0}, videoDuration = {0};

        {
            for (Track t : m.getTracks()) {
                if (t.getHandler().equals("soun")) {
                    for (long a : t.getSampleDurations()) audioDuration[0] += ((double) a) / t.getTrackMetaData().getTimescale();
                    audioTracks.add(t);
                } else if (t.getHandler().equals("vide")) {
                    for (long v : t.getSampleDurations()) videoDuration[0] += ((double) v) / t.getTrackMetaData().getTimescale();
                    videoTracks.add(t);
                }
            }

            adjustDurations(videoTracks, audioTracks, videoDuration, audioDuration);
        }
    }


    private void adjustDurations(LinkedList<Track> videoTracks, LinkedList<Track> audioTracks, double[] videoDuration, double[] audioDuration) {
        double diff = audioDuration[0] - videoDuration[0];

        //nothing to do
        if (diff == 0) {
            return;
        }

        //audio is longer
        LinkedList<Track> tracks = audioTracks;

        //video is longer
        if (diff < 0) {
            tracks = videoTracks;
            diff *= -1;
        }

        Track track = tracks.getLast();
        long[] sampleDurations = track.getSampleDurations();
        long counter = 0;
        for (int i = sampleDurations.length - 1; i > -1; i--) {
            if (((double) (sampleDurations[i]) / track.getTrackMetaData().getTimescale()) > diff) {
                break;
            }
            diff -= ((double) (sampleDurations[i]) / track.getTrackMetaData().getTimescale());
            audioDuration[0] -= ((double) (sampleDurations[i]) / track.getTrackMetaData().getTimescale());
            counter++;
        }

        if (counter == 0) {
            return;
        }

        track = new CroppedTrack(track, 0, track.getSamples().size() - counter);

        //update the original reference
        tracks.removeLast();
        tracks.addLast(track);
    }

    public void getVideoResolution()
    {
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(Utils.downloadedFile);
        String height = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String width = metaRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        //viewModel.mCameraView.setVideoSize();
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}
