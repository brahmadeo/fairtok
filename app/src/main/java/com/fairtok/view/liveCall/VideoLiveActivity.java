package com.fairtok.view.liveCall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.openduo.Constants;
import com.fairtok.openduo.activities.BaseCallActivity;
import com.fairtok.openduo.activities.VideoActivity;
import com.fairtok.openduo.agora.EngineEventListener;
import com.fairtok.openlive.bottomsheet.GiftAnimWindow;
import com.fairtok.openlive.chatting.adapter.MessageAdapter;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.chatting.model.MessageListBean;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.TrakConstant;
import com.fairtok.utils.WebServices;
import com.fairtok.utils.WebServicesCallback;
import com.fairtok.view.wallet.WalletActivity;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.agora.rtm.ErrorInfo;
import io.agora.rtm.RemoteInvitation;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannel;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmMessageType;
import io.agora.rtm.RtmStatusCode;

public class VideoLiveActivity extends BaseCallActivity implements CompleteTaskListner, View.OnClickListener {

    private static final String TAG = VideoActivity.class.getSimpleName();

    private FrameLayout mLocalPreviewLayout;
    private FrameLayout mRemotePreviewLayout;
    private AppCompatImageView mMuteBtn;
    private String mChannel;
    private int mPeerUid;
    PrefHandler prefHandler;

    long startTime;
    TextView tvTimer;

    String profilePic = "", coinPanId = "", coins = "";

    ImageView ivProfile, ivAdd, ivChat, ivSendMessage, ivGift;

    LinearLayout llBottom;

    EditText etMessage;

    LinearLayout llMessage, llCoinPerMinute;

    TextView tvName, tvCoinPerMinute;

    String start_Time, end_Time;

    String charges, callCharge;

    RecyclerView rvMessageList;
    int currentWalletBalance = 0, callTimeLeft = 60 * 1000, totalSecondsOfCall = 0;

    private MessageAdapter mMessageAdapter;

    CoinPurchaseViewModel viewModel;

    boolean isBroadcaster = false, showInvite = false, mIsPeerToPeerMode = false, isActivityStop = false;

    private String mUserId = "";
    private String mPeerId = "";
    private String mChannelName = "";
    private final String liveId = "";
    private final String name = "";
    private final String join_id = "";
    private final String instaProfileUrl = "";


    SessionManager sessionManager;

    private final List<MessageBean> mMessageBeanList = new ArrayList<>();

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;
    private EngineEventListener mEventListener;

    boolean isHost;

    CountDownTimer countDownTimer;

    Chronometer tvChronometer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_live);

        initUI();
        initVideo();

        tvTimer = findViewById(R.id.tvTimer);
        tvChronometer = findViewById(R.id.tvChronometer);
        startTime = SystemClock.elapsedRealtime();
        prefHandler = new PrefHandler(this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        start_Time = sdf.format(new Date());

        //TextView textGoesHere = (TextView) findViewById(R.id.tvTime);

        if (Utils.isCaller)
        {
            tvTimer.setVisibility(View.VISIBLE);
            tvChronometer.setVisibility(View.GONE);
            tvCoinPerMinute.setText(Utils.LIVE_CALL_RATE);
            llCoinPerMinute.setVisibility(View.VISIBLE);
            startCountDown();
        }
        else {
            tvTimer.setVisibility(View.GONE);
            tvChronometer.setVisibility(View.VISIBLE);
            llCoinPerMinute.setVisibility(View.GONE);
            startChronometer();
        }

    }

    private void  startChronometer() {

       tvChronometer.setBase(SystemClock.elapsedRealtime());
        tvChronometer.start();
    }

    private void startCountDown() {

        if (countDownTimer!=null) countDownTimer.cancel();
        Log.v("kjjkgh1", String.valueOf(callTimeLeft));

        countDownTimer = new CountDownTimer(callTimeLeft, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                totalSecondsOfCall++;
                @SuppressLint("DefaultLocale") String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));
                tvTimer.setText(hms);
                checkBalance();

                if (Utils.isCaller) {

                    if ((millisUntilFinished / 1000) == 20) {

                        showLowMinutesDialog(getString(R.string.youHaveTwentySecLeft));
                    } else if ((millisUntilFinished / 1000) == 40) {
                        showLowMinutesDialog(getString(R.string.youHaveFourtySecLeft));
                    }
                }


            }

            @Override
            public void onFinish() {
                finish();

            }
        };

        countDownTimer.start();

    }

    private void showLowMinutesDialog(String message) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoLiveActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_low_minutes);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);

        bottomSheetDialog.findViewById(R.id.tvCancel).setOnClickListener(v -> bottomSheetDialog.dismiss());

        bottomSheetDialog.findViewById(R.id.btnAddMinutes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hitAddMinutesApi();
                bottomSheetDialog.dismiss();

            }
        });

    }

    private void hitAddMinutesApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("liveId", Utils.LIVE_CALL_LIVE_ID);
        hashMap.put("callId", Utils.LIVE_CALL_CALL_ID);
        WebServices.postApiHeader(this,
                Const.BASE_URL + Const.purchaseMinute, hashMap, true, false,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                        try {
                            if (response.getBoolean("status")){
                                Log.v("kjjkgh2", String.valueOf(callTimeLeft));
                                Log.v("kjjkgh2", String.valueOf(totalSecondsOfCall));
                                callTimeLeft = (callTimeLeft-(totalSecondsOfCall*1000))+(60*1000);
                                startCountDown();

                            }
                            else
                                showMessageDialog(response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    private void showMessageDialog(String message) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoLiveActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_message);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();

        });

    }


    private void initUI() {

        mLocalPreviewLayout = findViewById(R.id.local_preview_layout);
        mRemotePreviewLayout = findViewById(R.id.remote_preview_layout);

        llCoinPerMinute = findViewById(R.id.llCoinPerMinute);

        sessionManager = new SessionManager(this);

        llMessage = findViewById(R.id.llMessage);

        tvCoinPerMinute = findViewById(R.id.tvCoinPerMinute);

        llBottom = findViewById(R.id.llBottom);

        rvMessageList = findViewById(R.id.rvMessageList);
        rvMessageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvMessageList.setAdapter(mMessageAdapter);

        etMessage = findViewById(R.id.etMessage);

        ivSendMessage = findViewById(R.id.ivSendMessage);
        ivGift = findViewById(R.id.ivGift);

        ivProfile = findViewById(R.id.ivProfile);
        ivAdd = findViewById(R.id.ivAdd);
        ivChat = findViewById(R.id.ivChat);

        tvName = findViewById(R.id.tvName);

        mMuteBtn = findViewById(R.id.btn_mute);
        mMuteBtn.setActivated(true);

        ivAdd.setOnClickListener(this);
        ivChat.setOnClickListener(this);
        ivSendMessage.setOnClickListener(this);
        ivGift.setOnClickListener(this);

        mMuteBtn.performClick();

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);
        viewModel.purchase.observe(this, new Observer<RestResponse>() {
            @Override
            public void onChanged(RestResponse purchase) {
                VideoLiveActivity.this.showPurchaseResultToast(purchase.getStatus());
            }
        });



    }

    private void setMessageAdapter() {


        Intent intent = getIntent();
        mIsPeerToPeerMode = intent.getBooleanExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        mUserId = intent.getStringExtra(MessageUtil.INTENT_EXTRA_USER_ID);
        String targetName = intent.getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME);

        if (mIsPeerToPeerMode) {
            mPeerId = targetName;

            // load history chat records
            MessageListBean messageListBean = MessageUtil.getExistMessageListBean(mPeerId);
            if (messageListBean != null) {
                mMessageBeanList.addAll(messageListBean.getMessageBeanList());
            }

            // load offline messages since last chat with this peer.
            // Then clear cached offline messages from message pool
            // since they are already consumed.
            MessageListBean offlineMessageBean = new MessageListBean(mPeerId, (List<MessageBean>) mChatManager);
            mMessageBeanList.addAll(offlineMessageBean.getMessageBeanList());
            mChatManager.removeAllOfflineMessages(mPeerId);
        } else {
            mChannelName = targetName;
            //mChannelMemberCount = 1;
            //   doLogin();

        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);

        mMessageAdapter = new MessageAdapter(this, mMessageBeanList, message -> {
            if (message.getMessage().getMessageType() == RtmMessageType.IMAGE) {
                if (!TextUtils.isEmpty(message.getCacheFile())) {
                   /* Glide.with(this).load(message.getCacheFile()).into(ivBigImage);
                    ivBigImage.setVisibility(View.VISIBLE);*/
                } else {
                    ImageUtil.cacheImage(this, mRtmClient, (RtmImageMessage) message.getMessage(), new ResultCallback<String>() {
                        @Override
                        public void onSuccess(String file) {
                            message.setCacheFile(file);
                            runOnUiThread(() -> {
                                /*Glide.with(LiveActivityNew.this).load(file).into(ivBigImage);
                                ivBigImage.setVisibility(View.VISIBLE);*/
                            });
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
        rvMessageList.setAdapter(mMessageAdapter);

    }

    private void doLogin() {
        mRtmClient.login(null, mChannel, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {

                Log.i("postApi", "login success");
                runOnUiThread(() -> {

                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());

                runOnUiThread(() -> {

                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }


    @Override
    protected void onGlobalLayoutCompleted() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLocalPreviewLayout.getLayoutParams();
        params.topMargin += statusBarHeight;
        mLocalPreviewLayout.setLayoutParams(params);

        RelativeLayout buttonLayout = findViewById(R.id.button_layout);
        params = (RelativeLayout.LayoutParams) buttonLayout.getLayoutParams();
        params.bottomMargin = displayMetrics.heightPixels / 8;
        params.leftMargin = displayMetrics.widthPixels / 6;
        params.rightMargin = displayMetrics.widthPixels / 6;
        buttonLayout.setLayoutParams(params);
    }

    private void initVideo() {
        Intent intent = getIntent();
        mChannel = intent.getStringExtra(Constants.KEY_CALLING_CHANNEL);

        try {
            mPeerUid = Integer.parseInt(intent.getStringExtra(Constants.KEY_CALLING_PEER));

            Log.v("ljhjlh1", mChannel);
            Log.v("ljhjlh2", String.valueOf(mPeerUid));
            Log.v("ljhjlh3", Global.USER_ID);

            hitGetHostProfileDataApi();

        } catch (NumberFormatException e) {
            Toast.makeText(this, R.string.message_wrong_number,
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        rtcEngine().setClientRole(io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
        setVideoConfiguration();
        setupLocalPreview();
        Log.v("kjhsq1", config().getUserId());
        Log.v("kjhsq2", mChannel);
        joinRtcChannel(mChannel, "", Integer.parseInt(config().getUserId()));


        mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();
        createAndJoinChannel();
        mClientListener = new MyRtmClientListener();
        mChatManager.registerListener(mClientListener);
        setMessageAdapter();
    }

    private void setupLocalPreview() {
        SurfaceView surfaceView = setupVideo(Integer.parseInt(config().getUserId()), true);
        surfaceView.setZOrderOnTop(true);
        mLocalPreviewLayout.addView(surfaceView);
    }

    @Override
    public void onUserJoined(final int uid, int elapsed) {
        Log.v("ljlkhlhkws1", String.valueOf(mPeerUid));
        Log.v("ljlkhlhkws2", String.valueOf(uid));
        if (uid != mPeerUid)
            return;

        runOnUiThread(() -> {
            Log.v(" fddbsdfvsdvsv", "1");
            if (mRemotePreviewLayout.getChildCount() == 0) {
                SurfaceView surfaceView = setupVideo(uid, false);
                mRemotePreviewLayout.addView(surfaceView);
            }
        });
    }

    //if (uid != mPeerUid) return;
    @Override
    public void onUserOffline(int uid, int reason) {
        Log.v("paras", "paras user went offline uid = " + uid);
        Log.v("paras", "paras mPeerUid = " + mPeerUid);
        finish();
    }

    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.btn_mute:
                rtcEngine().muteLocalAudioStream(mMuteBtn.isActivated());
                mMuteBtn.setActivated(!mMuteBtn.isActivated());
                break;
            case R.id.btn_switch_camera:
                rtcEngine().switchCamera();
                break;
        }
    }

    @Override
    public void finish() {

        hitStopLiveCallApi();

        if (countDownTimer != null)
            countDownTimer.cancel();
        if (Utils.isCaller)
            showElapsedTime();
        else {

            leaveChannel();

            gotoLiveActivity();
            //startActivity(new Intent(this, LiveCallActivityNew.class));
            super.finish();

        }
    }

    //makingUserLiveAgain
    private void gotoLiveActivity() {

        hitGolLiveApi();
        //finish();

    }

    private void hitGolLiveApi() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.goLiveCall;
            Log.v("postApi-url", url);
            Log.v("postApi-url", Global.ACCESS_TOKEN);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            AppUtils.hideDialog();
                            parseGoLiveJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(getApplicationContext(), "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGoLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                Utils.LIVE_HOST_CALL_ID = response.getJSONObject("data").getString("liveId");

                String room = "@" + sessionManager.getUser().getData().getUserName();
                String userId = "@" + sessionManager.getUser().getData().getUserName();

                Intent intent = new Intent(getIntent());
                intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
                intent.setClass(getApplicationContext(), LiveCallActivityNew.class);
                intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
                intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, room);
                intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
                intent.putExtra("liveId", Utils.LIVE_HOST_CALL_ID);
                startActivity(intent);

            } else
                Toast.makeText(getApplicationContext(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void hitStopLiveCallApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("liveId", Utils.LIVE_CALL_LIVE_ID);
        WebServices.postApiHeader(this,
                Const.BASE_URL + Const.stopLiveCall, hashMap, false, false,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    @Override
    public void onRemoteInvitationReceived(RemoteInvitation remoteInvitation) {
        // Do not respond to any other calls
        Log.i(TAG, "Ignore remote invitation from " +
                remoteInvitation.getCallerId() + " while in calling");
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    private void showElapsedTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        end_Time = sdf.format(new Date());

        long elapsedMillis = totalSecondsOfCall * 1000L;
//        Toast.makeText(getApplicationContext(), "Call duration: " + millisecondsToTime(elapsedMillis), Toast.LENGTH_SHORT).show();
        leaveChannel();

        charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
        int finalBalane = currentWalletBalance - chargeInt;
        Log.v("paras", "paras currentWalletBalance = " + currentWalletBalance);
        Log.v("paras", "paras finalBalane = " + finalBalane);
        User user = sessionManager.getUser();
        user.getData().setPaidCoins("" + finalBalane);
        sessionManager.saveUser(user);

        String duration = millisecondsToTime(elapsedMillis);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Your dialog code.

                showAlert("Call duration: " + duration + "\n" + charges, "" + finalBalane, duration);
            }
        });

    }

    public void showAlert(String msg, String balance, String duration) {

        //updateWallet(getApplicationContext(), balance, duration);

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", (dialog, which) -> {

            startActivity(new Intent(VideoLiveActivity.this, LiveCallListActivity.class));

        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private String millisecondsToTime(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;
        String secondsStr = Long.toString(seconds);
        String secs;
        if (secondsStr.length() >= 2) {
            secs = secondsStr.substring(0, 2);
        } else {
            secs = "0" + secondsStr;
        }

        return minutes + ":" + secs;
    }

    private String getCharges(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes = minutes + 1;
        }

        int charges = (int) (Integer.parseInt(Utils.LIVE_CALL_RATE) * minutes);

        callCharge = "" + charges;
        return "Call charges: " + charges;
    }

    private int getChargesInt(long milliseconds) {
        long minutes = (milliseconds / 1000) / 60;
        long seconds = (milliseconds / 1000) % 60;

        if (seconds > 0) {
            minutes = minutes + 1;
        }

        //int charges = (int) (prefHandler.getVideoCharges() * minutes);
        int charges = (int) (Integer.parseInt(Utils.LIVE_CALL_RATE) * minutes);
        return charges;
    }

    private void checkBalance() {
        long elapsedMillis = totalSecondsOfCall * 1000L;
        String charges = getCharges(elapsedMillis);
        int chargeInt = getChargesInt(elapsedMillis);
        SessionManager sessionManager = new SessionManager(this);
        int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

        Log.v("dwdwqq1", String.valueOf(chargeInt));
        Log.v("dwdwqq2", String.valueOf(currentWalletBalance));
        if (chargeInt >= currentWalletBalance)
            showElapsedTime();
    }

    public void updateWallet(Context context, String balance, String duration) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "update"));
        params.add(new Pair<>("balance", balance));
        params.add(new Pair<>("userId", prefHandler.getuId()));
        params.add(new Pair<>("callerName", prefHandler.getName()));
        params.add(new Pair<>("calleeId", Utils.CURRENT_RECEIVER_ID));
        params.add(new Pair<>("calleeName", Utils.CURRENT_RECEIVER_NAME));
        params.add(new Pair<>("duration", duration));
        params.add(new Pair<>("startTime", start_Time));
        params.add(new Pair<>("endTime", end_Time));
        params.add(new Pair<>("charge", callCharge));
        params.add(new Pair<>("callType", "Video"));

        new AsyncHttpsRequest("Wait...", context, params, this, 0, false).execute(Utils.UPDATE_BALANCE);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if (response_code == 0) {
            try {


                //JSONObject obj = new JSONObject(result);
                //super.finish();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ivAdd:

                hitFollowUnfollowApi(Utils.hostUserId, ivAdd);

                break;

            case R.id.ivChat:

                etMessage.requestFocus();

                llMessage.setVisibility(View.VISIBLE);
                llBottom.setVisibility(View.GONE);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                break;

            case R.id.ivSendMessage:

                sendMessage();

                break;

            case R.id.ivGift:

                showGiftBottomDialog();

                break;

        }
    }

    private void showGiftBottomDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoLiveActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_gifts);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvCoins = bottomSheetDialog.findViewById(R.id.tvCoins);
        tvCoins.setText(sessionManager.getUser().getData().getPaidCoins());

        RecyclerView rvGift = bottomSheetDialog.findViewById(R.id.rvGift);
        rvGift.setLayoutManager(new GridLayoutManager(this, 4));

        bottomSheetDialog.findViewById(R.id.llCoins).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                getCoinPlan();
            }
        });

        rvGift.setAdapter(new AdapterGift(getResources().getIntArray(R.array.gift_values), bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

    }


    private void sendMessage() {

        String msg = etMessage.getText().toString();

        if (!msg.equals("")) {
            ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
            RtmMessage message = mRtmClient.createMessage();

            String[] splited = sessionManager.getUser().getData().getFullName().split(" ");
            message.setText(splited[0] + " - " + msg);

            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
            message.setRawMessage(profilePic_bytes);

            //TODO
            MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            //MessageBean messageBean = new MessageBean(sessionManager.getUser().getData().getFullName(), message, true, profilePic);
            mMessageBeanList.add(messageBean);
            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            sendChannelMessage(message);

        }

        etMessage.setText("");

    }


    private void hitGetHostProfileDataApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //   //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.user_details;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", String.valueOf(mPeerUid))
                    .addUrlEncodeFormBodyParameter("my_user_id", Global.USER_ID)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //         //AppUtils.hideDialog();
                            parseGetHostProfile(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //          //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                final String file = resultUri.getPath();
                ImageUtil.uploadImage(this, mRtmClient, file, new ResultCallback<RtmImageMessage>() {
                    @Override
                    public void onSuccess(final RtmImageMessage rtmImageMessage) {
                        runOnUiThread(() -> {

                            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                            rtmImageMessage.setRawMessage(profilePic_bytes);

                            MessageBean messageBean = new MessageBean(mUserId, rtmImageMessage, true, profilePic);
                            messageBean.setCacheFile(file);
                            mMessageBeanList.add(messageBean);
                            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);

                            sendChannelMessage(rtmImageMessage);

                        });
                    }

                    @Override
                    public void onFailure(ErrorInfo errorInfo) {

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);
                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                        showToast("Transaction Failed");
                        //transactionIdView.setText("Transaction ID: NIL");
                        //transactionStatusView.setText("Transaction Status: Transaction Error!");
                    } else {
                        JSONObject response = new JSONObject(paymentResponse);
//                        TrakNPayResponse = response;
//
//                        FPaymentId = response.getString("transaction_id");
//                        FTransactionId = response.getString("transaction_id");
//                        FPaymentMode = response.getString("payment_mode");
//                        FTransactionStatus = response.getString("response_code");
//                        FAmount = response.getString("amount");
//                        FMessage = response.getString("response_message");
//
//                        Log.e("TrakNPay", "onActivityResult: " + response);
//
//                        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);
//                        viewModel.purchaseCoin(FTransactionId, FPaymentMode, FAmount, prefHandler.getName(),
//                                FMessage, coinPanId, FTransactionStatus);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                showToast("Transaction Canceled");
            }
        }
    }

    private void parseGetHostProfile(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                JSONObject jsonObject = response.getJSONObject("data");

                tvName.setText(jsonObject.getString("full_name"));

                Glide.with(this).load(Const.ITEM_BASE_URL + jsonObject.getString("user_profile")).into(ivProfile);
                profilePic = jsonObject.getString("user_profile");

                if (jsonObject.getString("is_following").equals("0")) {

                    ivAdd.setImageResource(R.drawable.ic_add);
                } else {
                    ivAdd.setImageResource(R.drawable.ic_star);
                }

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void hitFollowUnfollowApi(String toUser, ImageView ivFollow) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.followUnfollow;
            Log.v("postApi-url", url);
            Log.v("postApi-data", toUser);
            Log.v("postApi-data", Global.ACCESS_TOKEN);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("to_user_id", toUser)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseFollowUnfollow(response, ivFollow);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseFollowUnfollow(JSONObject response, ImageView ivFollow) {

        try {
            if (response.getBoolean("status")) {

                if (response.getJSONObject("data").getString("type").equals("1"))
                    ivFollow.setImageResource(R.drawable.ic_star);
                else
                    ivFollow.setImageResource(R.drawable.ic_add);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                //onBackPressed();
                llMessage.setVisibility(View.GONE);
                llBottom.setVisibility(View.VISIBLE);
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        Log.v("dwdq", "2");
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {
            runOnUiThread(() -> {

                Log.v("paras", "paras on peer's onMessageReceived called");

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                int backg = getMessageColor(peerId);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved1", msg);
                    message.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                    // hitGetLiveDetailApi();
                } else if (msg.contains("co-host-invitation")) {
                    // doInvite();
                }

                if (peerId.equals(String.valueOf(mPeerUid))) {

                    MessageBean messageBean = new MessageBean(peerId, message, false, profilePic);
                    messageBean.setBackground(backg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, message);
                }
            });
        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {

            String profilePic = Utils.DEFAULT_PROFILE_URL;

            profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

            int backg = getMessageColor(peerId);
            String msg = rtmImageMessage.getText();
            if (msg.contains("gift$$$$$")) {
                rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                String[] parts = msg.split("-");
                backg = Integer.parseInt(parts[1]);
                animateGiftGif(Integer.parseInt(parts[1]));
                //hitGetLiveDetailApi();
            } else if (msg.contains("co-host-invitation")) {
                // doInvite();
            }

            String finalProfilePic = profilePic;
            int finalBackg = backg;
            runOnUiThread(() -> {
                if (peerId.equals(String.valueOf(mPeerUid))) {
                    MessageBean messageBean = new MessageBean(peerId, rtmImageMessage, false, finalProfilePic);
                    messageBean.setBackground(finalBackg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, rtmImageMessage);
                }
            });
        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }
    }

    private int getMessageColor(String account) {
        for (int i = 0; i < mMessageBeanList.size(); i++) {
            if (account.equals(mMessageBeanList.get(i).getAccount())) {
                return mMessageBeanList.get(i).getBackground();
            }
        }
        return MessageUtil.COLOR_ARRAY[MessageUtil.RANDOM.nextInt(MessageUtil.COLOR_ARRAY.length)];
    }

    public void animateGiftGif(int giftId) {

        GiftAnimWindow window = new GiftAnimWindow(VideoLiveActivity.this, R.style.gift_anim_window);
        window.setAnimResource(GiftUtil.getGiftAnimRes(giftId));
        window.show();
    }

    private void sendChannelMessage(RtmMessage message) {
        mRtmChannel.sendMessage(message, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.ChannelMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    Log.d("errorCode", String.valueOf(errorCode));
                    switch (errorCode) {
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                    }
                });
            }
        });
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show());
    }

    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                String account = fromMember.getUserId();

                int backg = getMessageColor(account);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved2", msg);

                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);

                    animateGiftGif(Integer.parseInt(parts[1]));

                    message.setText(parts[2] + " " + getResources().getString(R.string.live_message_gift_send));

                }


                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + message);
                MessageBean messageBean = new MessageBean(account, message, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

                String account = rtmChannelMember.getUserId();

                int backg = getMessageColor(account);
                String msg = rtmImageMessage.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved3", msg);
                    rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }

                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + rtmImageMessage);
                MessageBean messageBean = new MessageBean(account, rtmImageMessage, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {

        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {

        }
    }


    /**
     * API CALL: create and join channel
     */
    private void createAndJoinChannel() {
        // step 1: create a channel instance

        mRtmChannel = mRtmClient.createChannel(mChannel, new MyChannelListener());

        if (mRtmChannel == null) {
            Log.e("TAG", "join channel failed6");
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        Log.e("channel", mRtmChannel + "");

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("TAG", "join channel success");
                //getChannelMemberList(false);
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "join channel failed7");
                Log.e("TAG", errorInfo.getErrorDescription());
                Log.e("TAG", String.valueOf(errorInfo.getErrorCode()));
                Log.e("TAG", errorInfo.toString());
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });
    }

    private class AdapterGift extends RecyclerView.Adapter<AdapterGift.MyViewHolder> {

        int[] data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterGift(int[] stringArray, BottomSheetDialog dialog) {

            data = stringArray;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_gifts, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            if (position == 0) holder.ivCoin.setImageResource(R.drawable.coin_silver);

            holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            holder.tvCoins.setText(String.valueOf(data[position]));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int coinsToSend = data[position];

                    currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

                    Log.v("paras", "paras sending gift gift index = " + position);

                    if (position == 0) {

                        bottomSheetDialog.dismiss();

                        //type=2 GoldCoin, 1=silver coin
                        sendBubble(Utils.hostUserId, "" + coinsToSend, "1", position);

                        //  updateGiftCoinsAccount("" + coinsToSend);

                    } else {
                        if (currentWalletBalance >= coinsToSend) {
                            bottomSheetDialog.dismiss();

                            //type=2 GoldCoin, 1=silver coin
                            sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position);

                            //  updateGiftCoinsAccount("" + coinsToSend);
                        } else {
                            bottomSheetDialog.dismiss();
                            doAlert();
                        }
                    }
                }
            });

        }

        @Override
        public int getItemCount() {

            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage, ivCoin;

            TextView tvCoins;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                ivCoin = itemView.findViewById(R.id.ivCoin);

                tvCoins = itemView.findViewById(R.id.tvCoins);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }

    private void getCoinPlan() {

        hitGetCoinPlanApi();

    }

    private void hitGetCoinPlanApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseCoinPlanJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseCoinPlanJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                    hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                    hashMap.put("coin_amount", jsonObject.getString("coin_amount"));

                    arrayList.add(hashMap);
                }

                showCoinBottomDialog(arrayList);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showCoinBottomDialog(ArrayList<HashMap<String, String>> arrayList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoLiveActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_coins);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet)
                .setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        RecyclerView rvCoins = bottomSheetDialog.findViewById(R.id.rvCoins);
        rvCoins.setLayoutManager(new GridLayoutManager(this, 3));

        rvCoins.setAdapter(new AdapterCoins(arrayList, bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    private class AdapterCoins extends RecyclerView.Adapter<AdapterCoins.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterCoins(ArrayList<HashMap<String, String>> arrayList, BottomSheetDialog dialog) {

            data = arrayList;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coins, viewGroup, false);
            return new MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

            //holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            // holder.ivImage.setImageDrawable(R.drawable.ic_coin);
            holder.tvCoins.setText(data.get(position).get("coin_amount"));
            holder.tvAmount.setText("Rs. " + data.get(position).get("coin_plan_price"));

            holder.llMain.setTag(R.string.amount, data.get(position).get("coin_plan_price"));
            holder.llMain.setTag(R.string.coins, data.get(position).get("coin_amount"));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bottomSheetDialog.dismiss();
                    coinPanId = data.get(position).get("coin_plan_id");

                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModel.coins = coins;
                    viewModel.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

                    initializePayment((String) holder.llMain.getTag(R.string.amount));

                }
            });
        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            TextView tvCoins, tvAmount;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);

                tvCoins = itemView.findViewById(R.id.tvCoins);
                tvAmount = itemView.findViewById(R.id.tvAmount);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }

    public void sendBubble(String toUserId, String coin, String type, int position) {

        Log.v("ljhhhlk", coin);
        Log.v("ljhhhlk", Global.ACCESS_TOKEN);
        Log.v("ljhhhlk", Utils.LIVE_CALL_LIVE_ID);
        Log.v("ljhhhlk", Utils.LIVE_CALL_CALL_ID);

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.liveCallSendCoin;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("liveId", Utils.LIVE_CALL_LIVE_ID)
                    .addUrlEncodeFormBodyParameter("callId", Utils.LIVE_CALL_CALL_ID)
                    .addUrlEncodeFormBodyParameter("coins", coin)
                    .addUrlEncodeFormBodyParameter("coinType", type)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.v("postApi-resp", String.valueOf(response));
                            try {
                                if (response.getBoolean("status")) {

                                    try {

                                        animateGiftGif(position);

                                        String[] splited = sessionManager.getUser().getData().getFullName().split(" ");

                                        String msg = "gift$$$$$-" + position + "-" + splited[0];
                                        RtmMessage message = mRtmClient.createMessage();
                                        message.setText(msg);
                                        byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                                        message.setRawMessage(profilePic_bytes);
                                        sendChannelMessage(message);

                                        message.setText(splited[0] + " " + getString(R.string.live_message_gift_send));

                                        MessageBean messageBean = new MessageBean(String.valueOf(mPeerUid), message, true, profilePic);
                                        messageBean.setBackground(position);
                                        mMessageBeanList.add(messageBean);
                                        mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                                        rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);

                                        int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                                        int updatedCoins = currentCoins + -Integer.parseInt(coin);
                                        User user = sessionManager.getUser();
                                        user.getData().setPaidCoins("" + updatedCoins);
                                        sessionManager.saveUser(user);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(VideoLiveActivity.this,
                                            R.style.CustomBottomSheetDialogTheme);
                                    bottomSheetDialog.setContentView(R.layout.dialog_message);
                                    bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
                                    bottomSheetDialog.show();

                                    TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

                                    tvMessage.setText(response.getString("message"));

                                    bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> bottomSheetDialog.dismiss());
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();


    }

    private void doAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(VideoLiveActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough gold coins to send this gift.");
        builder.setPositiveButton("Add Coins", (dialog, which) -> {
            dialog.dismiss();


            getCoinPlan();
           /* CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
            fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());*/
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void initializePayment(String PayAmount) {

        Log.d("PayAmount -  ", PayAmount);

        String customerID = sessionManager.getUser().getData().getUserId();
        String orderID = "OrderID" + System.currentTimeMillis() + "-" + customerID;

        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        TrakConstant.PG_ORDER_ID = Integer.toString(n);
        PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey(TrakConstant.PG_API_KEY);
        pgPaymentParams.setAmount(PayAmount);
        pgPaymentParams.setEmail(sessionManager.getUser().getData().getUserEmail());
        pgPaymentParams.setName(sessionManager.getUser().getData().getFullName());
        pgPaymentParams.setPhone(sessionManager.getUser().getData().getUserMobileNo());
        pgPaymentParams.setOrderId(orderID);
        pgPaymentParams.setCurrency(TrakConstant.PG_CURRENCY);
        pgPaymentParams.setDescription(TrakConstant.PG_DESCRIPTION);
        pgPaymentParams.setCity("NA");
        pgPaymentParams.setState(prefHandler.getLanguage());
        pgPaymentParams.setAddressLine1(TrakConstant.PG_ADD_1);
        pgPaymentParams.setAddressLine2(TrakConstant.PG_ADD_2);
        pgPaymentParams.setZipCode("560094");
        pgPaymentParams.setCountry(TrakConstant.PG_COUNTRY);
        pgPaymentParams.setReturnUrl(TrakConstant.PG_RETURN_URL);
        pgPaymentParams.setMode(TrakConstant.PG_MODE);
        pgPaymentParams.setUdf1(TrakConstant.PG_UDF1);
        pgPaymentParams.setUdf2(TrakConstant.PG_UDF2);
        pgPaymentParams.setUdf3(TrakConstant.PG_UDF3);
        pgPaymentParams.setUdf4(TrakConstant.PG_UDF4);
        pgPaymentParams.setUdf5(TrakConstant.PG_UDF5);

        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, this);
        pgPaymentInitialzer.initiatePaymentProcess();
    }

    private void showPurchaseResultToast(Boolean status) {


        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModel.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        leaveAndReleaseChannel();
        mChatManager.unregisterListener(mClientListener);
    }

    private void leaveAndReleaseChannel() {
        if (mRtmChannel != null) {
            mRtmChannel.leave(null);
            mRtmChannel.release();
            mRtmChannel = null;
        }
    }

}