package com.fairtok.view.liveCall;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.openduo.activities.DialerActivity;
import com.fairtok.openlive.activities.BaseActivity;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.stats.LocalStatsData;
import com.fairtok.openlive.stats.RemoteStatsData;
import com.fairtok.openlive.stats.StatsData;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.VerticalViewPager;
import com.fairtok.utils.WebServices;
import com.fairtok.utils.WebServicesCallback;
import com.fairtok.view.home.MainActivity;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodel.ProfileViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcChannelEventHandler;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcChannel;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.models.ChannelMediaOptions;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmStatusCode;

public class LiveCallListActivity extends BaseActivity implements View.OnClickListener,PurchasesUpdatedListener {

    LinearLayout llGoLive;

   // VideoGridContainer videoGrid;

    private String mUserId = "", liveId = "", coinPanId = "", coins = "0", fcmNotificationType = "", notificationLiveId = "";
    static boolean mIsInChat = false;

    private static final int PERMISSION_REQ_CODE = 1 << 4;
    private static final int PERMISSION_REQ_FORWARD = 1 << 3;

    SessionManager sessionManager;

    FrameLayout containerF;

    RtcEngine mRtcEngine;
    private ChatManager mChatManager;
    private RtmClientListener mClientListener;

    ViewPagerAdapter viewPagerAdapter;

    private final String[] PERMISSIONS = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    VerticalViewPager viewPager;

    ArrayList<FrameLayout> arrayListVideoGrid=new ArrayList<>();

    ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

    private RtmClient mRtmClient;

    public ProfileViewModel viewModel;

    String userIdForCall = "", nameForCall = "", profileForCall = "";

    boolean isLive = false; //True=GoLive, false= Call
    BillingClient billingClient;
    CoinPurchaseViewModel viewModelCoin;

    PrefHandler prefHandler;

   /* private VideoEncoderConfiguration.VideoDimensions mVideoDimension;

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;*/



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_live_call_list);

        inits();

    }

    private void inits() {
        prefHandler = new PrefHandler(this);
        llGoLive = findViewById(R.id.llGoLive);
        /*videoGrid = findViewById(R.id.videoGrid);

        videoGrid.setStatsManager(statsManager());
*/
        viewPager = findViewById(R.id.viewPager);

        llGoLive.setOnClickListener(this);

        sessionManager = new SessionManager(this);

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new ProfileViewModel()).createFor()).get(ProfileViewModel.class);
        mUserId = "@" + sessionManager.getUser().getData().getUserName();

        viewModelCoin = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);
        viewModelCoin.purchase.observe(this, new Observer<RestResponse>() {
            @Override
            public void onChanged(RestResponse purchase) {
                LiveCallListActivity.this.showPurchaseResultToast(purchase.getStatus());
            }
        });
      //  mVideoDimension = com.fairtok.openlive.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];

        PurchasesUpdatedListener purchasesUpdatedListener = (billingResult, purchases) -> {
            // To be implemented in a later section.

            Log.v("dkjksdq", "sqas");

            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                    && purchases != null) {
                ConsumeParams consumeParams =
                        ConsumeParams.newBuilder()
                                .setPurchaseToken(purchases.get(0).getPurchaseToken())
                                .build();

                ConsumeResponseListener listener = (billingResult1, purchaseToken) -> {
                    if (billingResult1.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        // Handle the success of the consume operation.
                    }
                };

                   billingClient.consumeAsync(consumeParams, listener);

                viewModelCoin.purchaseCoin(purchases.get(0).getOrderId(),
                        "Google InApp Purchase", viewModelCoin.coinsAmount,
                        prefHandler.getName(), prefHandler.getuId(),
                        coinPanId, "0");

            }


        };

        billingClient = BillingClient.newBuilder(this)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    billingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, (BillingResult b,
                                                                                          List<PurchaseHistoryRecord> list) -> {
                        Log.v("dkjksdq4", String.valueOf(list.size()));

                        for (int i = 0; i < list.size(); i++) {

                            ConsumeParams consumeParams = ConsumeParams.newBuilder()
                                    .setPurchaseToken(list.get(i).getPurchaseToken()).build();
                            ConsumeResponseListener listener = (billingResult1, purchaseToken) -> {
                            };
                            billingClient.consumeAsync(consumeParams, listener);

                        }


                    });

                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });


        if (getIntent().getExtras() != null) {

            Intent intent = getIntent();
            fcmNotificationType = intent.getStringExtra("fcmNotificationType");
            notificationLiveId = intent.getStringExtra("liveId");

        }
/*
        mRtmClient.login(null, mUserId, new ResultCallback<Void>() {@Override
            public void onSuccess(Void responseInfo) { Log.i("postApi", "login success");runOnUiThread(() -> { hitGetChatListApi(); }); }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());

                runOnUiThread(() -> { mIsInChat = false; });
                if (errorInfo.getErrorCode() == 8) { mRtmClient.logout(new ResultCallback<Void>() {
                        @Override public void onSuccess(Void aVoid) {
                            mRtmClient.login(null, mUserId, new ResultCallback<Void>() {@Override
                            public void onSuccess(Void responseInfo) { Log.i("postApi", "login success");
                            runOnUiThread(LiveCallListActivity.this::hitGetChatListApi); }
                                @Override
                                public void onFailure(ErrorInfo errorInfo) { }
                            });

                        }
                        @Override public void onFailure(ErrorInfo errorInfo) { }
                    });
                }
            }
        });
*/

        viewPager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {

                        mRtcEngine.leaveChannel();
                        arrayListVideoGrid.get(viewPager.getCurrentItem()).removeAllViews();
                        initAgoraEngineAndJoinChannel();
                        initAgoraRtcChannelEngineAndJoinChannel();

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                }
        );
    }

    private void initAgoraEngineAndJoinChannel() {
        initializeAgoraEngine();
        setupVideoProfile();
        //setupLocalVideo();
        joinChannel();
    }

    private void initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.private_app_id), mRtcEventHandler);
        } catch (Exception e) {
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    private IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {

        @Override
        public void onUserOffline(int uid, int reason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    onRemoteUserLeft();
                }
            });
        }

        @Override
        public void onUserJoined(final int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setupRemoteVideo(uid);
                }
            });
        }
    };

    private void setupRemoteVideo(int uid) {
       // FrameLayout container = (FrameLayout) findViewById(R.id.videoGrid);

        if (arrayListVideoGrid.get(viewPager.getCurrentItem()).getChildCount() >= 1) {
            return;
        }

        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        arrayListVideoGrid.get(viewPager.getCurrentItem()).addView(surfaceView);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, uid));

        surfaceView.setTag(uid); // for mark purpose
    }

    private void onRemoteUserLeft() {
       // FrameLayout container = (FrameLayout) findViewById(R.id.videoGrid);
        arrayListVideoGrid.get(viewPager.getCurrentItem()).removeAllViews();
    }

    private void setupVideoProfile() {
        //mRtcEngine.enableVideo();
        mRtcEngine.disableAudio();

        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }

    private void joinChannel() {
        mRtcEngine.joinChannel(null, "@"+arrayList.get(viewPager.getCurrentItem()).get("user_name"), "Extra Optional Data", 0);

    }

    private void initAgoraRtcChannelEngineAndJoinChannel(){
        RtcChannel rtcChannel = mRtcEngine.createRtcChannel("@"+arrayList.get(viewPager.getCurrentItem()).
                get("user_name"));

        rtcChannel.setRtcChannelEventHandler(new IRtcChannelEventHandler(){
            @Override
            // Listen for the onJoinChannelSuccess callback.
            // This callback occurs when the local user successfully joins the channel.
            public void onJoinChannelSuccess(RtcChannel rtcChannel, int uid, int elapsed) {
                super.onJoinChannelSuccess(rtcChannel, uid, elapsed);
                Log.i("TAG", String.format("onJoinChannelSuccess channel %s uid %d", "demoChannel2", uid));

            }
            @Override
            // Listen for the onUserJoinedcallback.
            // This callback occurs when a remote host joins a channel.
            public void onUserJoined(RtcChannel rtcChannel, final int uid, int elapsed) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //setupRemoteVideo2(uid);
                    }
                });
                super.onUserJoined(rtcChannel, uid, elapsed);
            }

        });

        ChannelMediaOptions option = new ChannelMediaOptions();
        option.autoSubscribeVideo = true;
        option.autoSubscribeAudio = true;

        //rtcChannel.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcChannel.joinChannel(null, " ", 0, option);
        rtcChannel.publish();
    }


    private void hitGetChatListApi() {

        WebServices.getApi(LiveCallListActivity.this,
                Const.BASE_URL + Const.liveCallUsers, false, true,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                        parseList(response);

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    private void parseList(JSONObject response) {

        arrayList.clear();

        try {
            if (response.getBoolean("status")) {

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("live_id", jsonObject.getString("live_id"));
                    hashMap.put("user_id", jsonObject.getString("user_id"));
                    hashMap.put("user_full_name", jsonObject.getString("user_full_name"));
                    hashMap.put("user_name", jsonObject.getString("user_name"));
                    hashMap.put("user_profile", jsonObject.getString("user_profile"));
                    hashMap.put("callRate", jsonObject.getString("callRate"));
                    hashMap.put("coinsReceived", jsonObject.getString("coinsReceived"));
                    hashMap.put("coinsReceivedSilver", jsonObject.getString("coinsReceivedSilver"));
                    hashMap.put("status", jsonObject.getString("status"));
                    hashMap.put("ifFollowing", jsonObject.getString("ifFollowing"));

                    arrayList.add(hashMap);

                    if (jsonObject.getString("status").equals("1") && notificationLiveId.
                            equals(jsonObject.getString("live_id"))) {
                        profileForCall = Const.ITEM_BASE_URL + jsonObject.getString("user_profile");
                        isLive = false;

                        Utils.LIVE_CALL_LIVE_ID = jsonObject.getString("live_id");
                        Utils.LIVE_CALL_RATE = jsonObject.getString("callRate");

                        handleVideoCallClick(jsonObject.getString("user_id"),
                                jsonObject.getString("user_full_name"));

                    } else {
                        showMessageDialog(jsonObject.getString("user_full_nam") + " " + getString(R.string.currentlyBusy));
                    }
                }

            }
            if (response.getString("liveCallAvailable").equals("1")) {

                llGoLive.setVisibility(View.VISIBLE);
            } else {
                llGoLive.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setupViewPager();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.llGoLive:

                isLive = true;
                mChatManager = MyApplication.the().getChatManager();
                mRtmClient = mChatManager.getRtmClient();
                mClientListener = new MyRtmClientListener();
                mChatManager.registerListener(mClientListener);

                onStartLiveClicked();

                break;

        }
    }

    private void setupViewPager() {

        viewPagerAdapter = new ViewPagerAdapter(this, arrayList);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(arrayList.size());
        if (arrayList.size()>0){

            mRtcEngine.leaveChannel();
            arrayListVideoGrid.get(viewPager.getCurrentItem()).removeAllViews();
            initAgoraEngineAndJoinChannel();
            initAgoraRtcChannelEngineAndJoinChannel();
        }
    }

    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {

    }

    public class ViewPagerAdapter extends PagerAdapter {

        private final Context context;
        private final ArrayList<HashMap<String, String>> data;

        public ViewPagerAdapter(Context context, ArrayList<HashMap<String, String>> arrayList) {
            this.context = context;
            data = arrayList;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.custom_layout, container, false);

            //VideoGridContainer videoGrid;

            ImageView ivImage, ivCall, ivAdd;
            CardView cvLive;
            ivImage = view.findViewById(R.id.ivImage);
            ivCall = view.findViewById(R.id.ivCall);
            ivAdd = view.findViewById(R.id.ivAdd);

            containerF = view.findViewById(R.id.frameLayout);
            cvLive = view.findViewById(R.id.cvLive);
            arrayListVideoGrid.add(containerF);
            initAgoraEngineAndJoinChannel();
            initAgoraRtcChannelEngineAndJoinChannel();
            /*videoGrid = view.findViewById(R.id.videoGrid);

            videoGrid.setStatsManager(statsManager());*/

            Log.v("wdwdwdw1", String.valueOf(arrayListVideoGrid.size()));
            Log.v("ljhq", String.valueOf(position));
            //setLiveVideo(data.get(position), position);

            TextView tvName, tvCoinPerMinute;
            tvName = view.findViewById(R.id.tvName);
            tvCoinPerMinute = view.findViewById(R.id.tvCoinPerMinute);

            AppUtils.loadPicassoImage(Const.ITEM_BASE_URL + data.get(position).get("user_profile"), ivImage);

            tvName.setText(data.get(position).get("user_full_name"));
            tvCoinPerMinute.setText(data.get(position).get("callRate"));

            if (data.get(position).get("ifFollowing").equals("0")) {

                ivAdd.setImageResource(R.drawable.ic_add);
            } else {
                ivAdd.setImageResource(R.drawable.ic_star);
            }

            if (data.get(position).get("status").equals("1")) {

                ////videoGrid.setVisibility(View.VISIBLE);
                ivAdd.setVisibility(View.VISIBLE);
                cvLive.setVisibility(View.VISIBLE);
            }
            else {
                //videoGrid.setVisibility(View.GONE);
                ivAdd.setVisibility(View.GONE);
                cvLive .setVisibility(View.GONE);
            }

            ivCall.setOnClickListener(view1 -> {

                if (data.get(position).get("status").equals("1")) {
                    profileForCall = Const.ITEM_BASE_URL + data.get(position).get("user_profile");
                    isLive = false;

                    Utils.LIVE_CALL_LIVE_ID = data.get(position).get("live_id");
                    Utils.LIVE_CALL_RATE = data.get(position).get("callRate");

                    handleVideoCallClick(data.get(position).get("user_id"),
                            data.get(position).get("user_full_name"));

                } else {
                    showMessageDialog(data.get(position).get("user_full_nam") + " " + getString(R.string.currentlyBusy));
                }
            });
            ivAdd.setOnClickListener(view2 -> {

                hitFollowUnfollowApi(data.get(position).get("user_id"), position, ivAdd);
            });
            ViewPager vp = (ViewPager) container;
            vp.addView(view, 0);
            return view;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            ViewPager vp = (ViewPager) container;
            View view = (View) object;
            vp.removeView(view);

        }
    }

    private void hitFollowUnfollowApi(String user_id, int position, ImageView ivAdd) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("to_user_id", user_id);

        WebServices.postApiHeader(LiveCallListActivity.this,
                Const.BASE_URL + Const.followUnfollow, hashMap, true, true,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                        parseFollowUnfollow(response, position, ivAdd);

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    private void parseFollowUnfollow(JSONObject response, int position, ImageView ivAdd) {

        try {
            if (response.getBoolean("status")) {

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("live_id", arrayList.get(position).get("live_id"));
                hashMap.put("user_id", arrayList.get(position).get("user_id"));
                hashMap.put("user_full_name", arrayList.get(position).get("user_full_name"));
                hashMap.put("user_name", arrayList.get(position).get("user_name"));
                hashMap.put("user_profile", arrayList.get(position).get("user_profile"));
                hashMap.put("callRate", arrayList.get(position).get("callRate"));
                hashMap.put("coinsReceived", arrayList.get(position).get("coinsReceived"));
                hashMap.put("coinsReceivedSilver", arrayList.get(position).get("coinsReceivedSilver"));
                hashMap.put("status", arrayList.get(position).get("status"));

                if (response.getJSONObject("data").getString("type").equals("1")) {
                    hashMap.put("ifFollowing", "1");
                    ivAdd.setImageResource(R.drawable.ic_star);
                } else {
                    hashMap.put("ifFollowing", "0");
                    ivAdd.setImageResource(R.drawable.ic_add);

                }

                arrayList.set(position, hashMap);


            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setLiveVideo(HashMap<String, String> hashMap, int position) {


    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
        Log.v("wdsdwdw1", channel);
    }
    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment
        Log.v("wdsdwdw2", String.valueOf(uid));
        // showToast("New User Joined");
    }

    private void createAndJoinChannel(String target) {
        // step 1: create a channel instance
      /*  mRtmChannel = mRtmClient.createChannel(target, new MyChannelListener());
        if (mRtmChannel == null) {
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        Log.e("channel", mRtmChannel + "");

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("TAG", "join channel success");

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "join channel failed3");
                Log.e("TAG", errorInfo.getErrorDescription());
                Log.e("TAG", String.valueOf(errorInfo.getErrorCode()));
                Log.e("TAG", errorInfo.toString());
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });*/

    }

    private void showToast(String text) {
        runOnUiThread(() -> Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show());
    }

    private void handleVideoCallClick(String userId, String fullName) {
        PrefHandler prefHandler = new PrefHandler(LiveCallListActivity.this);

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {

            if (sessionManager.getUser() != null) {
                viewModel.user.setValue(sessionManager.getUser());
                viewModel.fetchUserById(sessionManager.getUser().getData().getUserId());
                viewModel.userId = sessionManager.getUser().getData().getUserId();
                viewModel.isBackBtn.set(false);
            }

            userIdForCall = userId;
            nameForCall = fullName;

            Utils.CURRENT_RECEIVER_ID = userId;
            Utils.CURRENT_RECEIVER_NAME = fullName;
            checkPermissionForCall();
            /*int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

            if (currentWalletBalance >= prefHandler.getVideoCharges())
                checkPermissionForCall();
            else
                showAlertWallet("You don't have sufficient balance in your wallet to call." +
                        " Please purchase coins to make this call.");
*/
        }
    }

    private void checkPermissionForCall() {

        if (!permissionArrayGranted(null)) {
            requestPermissions(PERMISSION_REQ_FORWARD);
        } else {
            onStartLiveClicked();
        }
    }

    private void onStartLiveClicked() {

        checkPermission();

    }

    private void checkPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            doLogin();
        } else {
            requestPermissions(PERMISSION_REQ_CODE);
        }
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getApplicationContext(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(int code) {
        ActivityCompat.requestPermissions(LiveCallListActivity.this, PERMISSIONS, code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            boolean granted = true;
            for (int result : grantResults) {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }

            if (granted) {
                doLogin();
            } else {
                Toast.makeText(getApplicationContext(), R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == PERMISSION_REQ_FORWARD) {
            boolean granted = permissionArrayGranted(permissions);
            if (granted) {

                //If not empty it means user click on call button
                if (!nameForCall.isEmpty()){
                    gotoDialerActivity();
                }

            } else {
                Toast.makeText(getApplicationContext(), R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void gotoDialerActivity() {

        showOkCancelAlert("Video call will charge you " + Utils.LIVE_CALL_RATE + " coins/min from your wallet.");

        /*showOkCancelAlert("Video call will charge you " + prefHandler.getVideoCharges() +
                " coins, Audio call will charge " + prefHandler.getAudioCharges() + " coins from your wallet.");*/
    }

    private void doLogin() {
        mIsInChat = true;

        AppUtils.showRequestDialog(LiveCallListActivity.this);

        if (mRtmClient==null){
            mChatManager = MyApplication.the().getChatManager();
            mRtmClient = mChatManager.getRtmClient();
            mClientListener = new MyRtmClientListener();
            mChatManager.registerListener(mClientListener);
        }

        mRtmClient.login(null, mUserId, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {


                AppUtils.hideDialog();
                Log.i("postApi", "login success");
                runOnUiThread(() -> {
                    if (isLive)
                        hitGolLiveApi();
                    else
                        gotoDialerActivity();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.i("postApi", "login failed: " + errorInfo.getErrorCode());
                Log.i("postApi", "login failed: " + errorInfo.getErrorDescription());
                AppUtils.hideDialog();
                runOnUiThread(() -> {
                    mIsInChat = false;
                    //showToast("Joining Failed");
                });
                if (errorInfo.getErrorCode() == 8) {
                    mRtmClient.logout(new ResultCallback<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            doLogin();
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
    }

    private void hitGolLiveApi() {

        if (AppUtils.isNetworkAvailable(getApplicationContext())) {

            AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.goLiveCall;
            Log.v("postApi-url", url);
            Log.v("postApi-url", Global.ACCESS_TOKEN);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            AppUtils.hideDialog();
                            parseGoLiveJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(getApplicationContext(), "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGoLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                liveId = response.getJSONObject("data").getString("liveId");
                Utils.LIVE_HOST_CALL_ID = liveId;

                gotoLiveActivity();

            } else
                Toast.makeText(getApplicationContext(), "" + response.getString("message"), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void gotoLiveActivity() {

        String room = "@" + sessionManager.getUser().getData().getUserName();
        String userId = "@" + sessionManager.getUser().getData().getUserName();
        // config().setChannelName(room);
        //Log.v("jhjh", config().getChannelName());
        Intent intent = new Intent(getIntent());
        intent.putExtra(com.fairtok.openlive.Constants.KEY_CLIENT_ROLE, Constants.CLIENT_ROLE_BROADCASTER);
        intent.setClass(getApplicationContext(), LiveCallActivityNew.class);
        intent.putExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        intent.putExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME, room);
        intent.putExtra(MessageUtil.INTENT_EXTRA_USER_ID, userId);
        intent.putExtra("liveId", liveId);

        startActivity(intent);

    }

    public void showAlertWallet(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LiveCallListActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK",  new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();

                hitGetCoinPlanApi();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean permissionArrayGranted(@Nullable String[] permissions) {
        String[] permissionArray = permissions;
        if (permissionArray == null) {
            permissionArray = PERMISSIONS;
        }

        boolean granted = true;
        for (String per : permissionArray) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }
        return granted;
    }

    public void showOkCancelAlert(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LiveCallListActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                openDialerActivity();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void openDialerActivity() {

        hitInitiateCallApi();
    }

    private void hitInitiateCallApi() {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("liveId", Utils.LIVE_CALL_LIVE_ID);

        WebServices.postApiHeader(LiveCallListActivity.this,
                Const.BASE_URL + Const.initiateLiveCall, hashMap, true, true,
                new WebServicesCallback() {

                    @Override
                    public void OnJsonSuccess(JSONObject response) {

                        parseInitiateCal(response);

                    }

                    @Override
                    public void OnFail(String response) {

                    }
                });
    }

    private void parseInitiateCal(JSONObject response) {

        try {

            if (response.getBoolean("status")) {

                Utils.LIVE_CALL_CALL_ID = response.getJSONObject("data").getString("callId");

                AppUtils.hideDialog();
                mRtcEngine.leaveChannel();
                Intent intent = new Intent();
                intent.setClass(this, DialerActivity.class);
                intent.putExtra("userName", nameForCall);
                intent.putExtra("userImg", profileForCall);
                intent.putExtra("userId", userIdForCall);
                intent.putExtra("type", "1");//1=Live Call, 2=Normal Call
                intent.putExtra("isAudio", false);
                startActivity(intent);


            } else
            {
                showAlertWallet("You don't have sufficient balance in your wallet to call." +
                        " Please purchase coins to make this call.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showMessageDialog(String message) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallListActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_message_recharge);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage, tvCancel;
        tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);
        tvCancel = bottomSheetDialog.findViewById(R.id.tvCancel);

        Button btnRecharge = bottomSheetDialog.findViewById(R.id.btnRecharge);

        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitGetCoinPlanApi();

            }
        });

        tvMessage.setText(message);

        tvCancel.setOnClickListener(v -> { bottomSheetDialog.dismiss(); });

    }

    private void hitGetCoinPlanApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseCoinPlanJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveCallListActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseCoinPlanJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                    hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                    hashMap.put("coin_amount", jsonObject.getString("coin_amount"));
                    hashMap.put("playstore_product_id", jsonObject.getString("playstore_product_id"));

                    arrayList.add(hashMap);
                }

                showCoinBottomDialog(arrayList);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showCoinBottomDialog(ArrayList<HashMap<String, String>> arrayList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallListActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_coins);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet)
                .setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        RecyclerView rvCoins = bottomSheetDialog.findViewById(R.id.rvCoins);
        rvCoins.setLayoutManager(new GridLayoutManager(this, 3));

        rvCoins.setAdapter(new AdapterCoins(arrayList, bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    private class AdapterCoins extends RecyclerView.Adapter<AdapterCoins.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterCoins(ArrayList<HashMap<String, String>> arrayList, BottomSheetDialog dialog) {

            data = arrayList;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public AdapterCoins.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coins, viewGroup, false);
            return new AdapterCoins.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterCoins.MyViewHolder holder, final int position) {

            //holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            // holder.ivImage.setImageDrawable(R.drawable.ic_coin);
            holder.tvCoins.setText(data.get(position).get("coin_amount"));
            holder.tvAmount.setText("Rs. " + data.get(position).get("coin_plan_price"));

            holder.llMain.setTag(R.string.amount, data.get(position).get("coin_plan_price"));
            holder.llMain.setTag(R.string.coins, data.get(position).get("coin_amount"));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bottomSheetDialog.dismiss();
                    coinPanId = data.get(position).get("coin_plan_id");

                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModelCoin.coins = coins;
                    viewModelCoin.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

/*
                    bp.consumePurchase(data.get(position).get("playstore_product_id"));
                    bp.purchase(LiveActivityNew.this,data.get(position).get("playstore_product_id"));
*/
                    // bp.consumePurchase(data.get(position).get("playstore_product_id"));


                    List<String> skuList = new ArrayList<>();

                    skuList.add(data.get(position).get("playstore_product_id")); // product id is which you added on play store console

                    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                    params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);


                    billingClient.querySkuDetailsAsync(params.build(),
                            new SkuDetailsResponseListener() {
                                @Override
                                public void onSkuDetailsResponse(BillingResult billingResult,
                                                                 List<SkuDetails> skuDetailsList) {
                                    // Process the result.
                                    Log.v("dkjksdq1", "sqas");
                                    for (SkuDetails skuDetails : skuDetailsList) {
                                        Log.v("skuDetailsAsyncSize", "kjxhwxx");
                                        String sku = skuDetails.getSku();
                                        String price = skuDetails.getPrice();
                                        Log.v("dkjksdq2", "sqas");
                                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                                .setSkuDetails(skuDetails)
                                                .build();

                                        int responseCode = billingClient.launchBillingFlow(LiveCallListActivity.this,
                                                billingFlowParams).getResponseCode();

                                        Log.v("dkjksdq3", String.valueOf(responseCode));
                                    }
                                }

                            });

                    //initializePayment((String) holder.llMain.getTag(R.string.amount));


                }
            });

        }


        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            TextView tvCoins, tvAmount;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);

                tvCoins = itemView.findViewById(R.id.tvCoins);
                tvAmount = itemView.findViewById(R.id.tvAmount);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }

    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {

        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {
            runOnUiThread(() -> {

               /* if (mChannelName.equalsIgnoreCase(member.getUserId())) {
                    openDialog("Host has ended the party");
                } //else
                //showToast(member.getUserId() + " has left the party");
*/
            });
        }
    }

    public void openDialog(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LiveCallListActivity.this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("OK",
                (dialog, arg1) -> {
                    dialog.dismiss();
                    finish();
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);

    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.v("wdsdwdw3", String.valueOf(reason));
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
       /* SurfaceView surface = prepareRtcVideo(uid, false);
        //videoGrid.addUserVideoSurface(uid, surface, false);
        Log.v("wdwdwdw2", String.valueOf(arrayListVideoGrid.size()));
        if (arrayListVideoGrid.size()>0)
        arrayListVideoGrid.get(0).addUserVideoSurface(uid, surface, false);*/

        SurfaceView surface = RtcEngine.CreateRendererView(getApplicationContext());

        rtcEngine().setupRemoteVideo(
                new VideoCanvas(
                        surface,
                        VideoCanvas.RENDER_MODE_HIDDEN,
                        uid,
                        com.fairtok.openlive.Constants.VIDEO_MIRROR_MODES[config().getMirrorRemoteIndex()]
                )
        );

/*

        if (videoGrid!=null )
            videoGrid.addUserVideoSurface(uid, surface, false);
*/


    }

    private void removeRemoteUser(int uid) {

        rtcEngine().setupRemoteVideo(new VideoCanvas(null, VideoCanvas.RENDER_MODE_HIDDEN, uid));
        //removeRtcVideo(uid, false);

        //videoGrid.removeUserVideo(uid, false);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {

        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {


        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }
    }

    @Override
    protected void onStop() {
        super.onStop();


    }

    @Override
    protected void onResume() {
        super.onResume();

        hitGetChatListApi();
    }

    private void showPurchaseResultToast(Boolean status) {


        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModelCoin.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
    }

}
