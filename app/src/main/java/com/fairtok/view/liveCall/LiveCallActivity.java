package com.fairtok.view.liveCall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.bumptech.glide.Glide;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.adapter.MyPerformanceArrayAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.bean.ChatListPOJO;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.customview.BottomSheetListView;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.openlive.activities.RtcBaseActivity;
import com.fairtok.openlive.bottomsheet.GiftAnimWindow;
import com.fairtok.openlive.chatting.adapter.MessageAdapter;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.chatting.model.MessageListBean;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.stats.LocalStatsData;
import com.fairtok.openlive.stats.RemoteStatsData;
import com.fairtok.openlive.stats.StatsData;
import com.fairtok.openlive.ui.VideoGridContainer;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.TrakConstant;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannel;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmMessageType;
import io.agora.rtm.RtmStatusCode;
import io.reactivex.disposables.CompositeDisposable;

public class LiveCallActivity extends RtcBaseActivity implements View.OnClickListener, CompleteTaskListner,
        BillingProcessor.IBillingHandler {

    ImageView  /*ivMuteAudio, ivBeautification, ivSwitchCamera, ivGift, ivBigImage,ivMuteVideo,*/ ivChat, ivProfile, ivChatHost,
            ivAdd, ivCoin, ivProfile1, ivProfile2, ivProfile3, ivInstagram;

    EditText etMessage;


    TextView tvViewers, tvName, tvGoldCoin, tvTotalDiamonds, tvSilverCoin;

    LinearLayout llMessage, llBottomAudience, llHostBottom, llTopViewers;

    RelativeLayout rlTop;

    boolean isBroadcaster = false, showInvite = false, mIsPeerToPeerMode = false, isActivityStop = false;

    SessionManager sessionManager;

    private String mUserId = "", mPeerId = "", mChannelName = "", profilePic = "", liveId = "", name = "",
            join_id = "", instaProfileUrl = "", coinPanId = "";

    RecyclerView rvMessageList, rvAddCoin;

    ArrayList<HashMap<String, String>> arrayListAddCoin = new ArrayList<>();

    private VideoGridContainer videoGrid;

    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;

    ArrayList<ChatListPOJO> arrayList;
    private final List<MessageBean> mMessageBeanList = new ArrayList<>();
    List<RtmChannelMember> memberList = new ArrayList<>();

    private int mChannelMemberCount = 1, currentWalletBalance = 0;

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;

    private MessageAdapter mMessageAdapter;

    PrefHandler prefHandler;
    String FPaymentId, FTransactionId, FPaymentMode, FTransactionStatus, FAmount, FUserName, FEmail, FPhone, FMessage;
    JSONObject TrakNPayResponse;
    BillingProcessor bp;

    String coins = "0";

    private final CompositeDisposable disposable = new CompositeDisposable();
    public ObservableBoolean isLoading = new ObservableBoolean(false);

    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public MutableLiveData<RestResponse> purchase = new MutableLiveData<>();
    CoinPurchaseViewModel viewModel;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_call);

        prefHandler = new PrefHandler(this);
        init();
        
    }

    private void init() {

        sessionManager = new SessionManager(this);
        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
        bp = new BillingProcessor(this, String.valueOf(R.string.play_lic_key), this);
        bp.initialize();

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);
        viewModel.purchase.observe(this, new Observer<RestResponse>() {
            @Override
            public void onChanged(RestResponse purchase) {
                LiveCallActivity.this.showPurchaseResultToast(purchase.getStatus());
            }
        });

        ivChat = findViewById(R.id.ivChat);
        ivProfile = findViewById(R.id.ivProfile);
        ivAdd = findViewById(R.id.ivAdd);
        ivChatHost = findViewById(R.id.ivChatHost);
        ivCoin = findViewById(R.id.ivCoin);
        ivProfile1 = findViewById(R.id.ivProfile1);
        ivProfile2 = findViewById(R.id.ivProfile2);
        ivProfile3 = findViewById(R.id.ivProfile3);
        ivInstagram = findViewById(R.id.ivInstagram);
        rlTop = findViewById(R.id.rlTop);

        llTopViewers = findViewById(R.id.llTopViewers);

        Glide.with(this).load(Const.ITEM_BASE_URL + Utils.hostProfilePic).into(ivProfile);

        profilePic = sessionManager.getUser().getData().getUserProfile();
        name = sessionManager.getUser().getData().getFullName();

        llMessage = findViewById(R.id.llMessage);
        llBottomAudience = findViewById(R.id.llBottomAudience);
        llHostBottom = findViewById(R.id.llHostBottom);

        tvSilverCoin = findViewById(R.id.tvSilverCoin);
        tvTotalDiamonds = findViewById(R.id.tvTotalDiamonds);
        tvGoldCoin = findViewById(R.id.tvGoldCoin);
        tvViewers = findViewById(R.id.tvViewers);
        tvName = findViewById(R.id.tvName);
        tvName.setText(Utils.hostFullName);
        tvName.setSelected(true);

        etMessage = findViewById(R.id.etMessage);

        rvMessageList = findViewById(R.id.rvMessageList);
        rvMessageList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvMessageList.setAdapter(mMessageAdapter);

        rvAddCoin = findViewById(R.id.rvAddCoin);
        rvAddCoin.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvAddCoin.setAdapter(new AdapterAddCoin(getResources().getIntArray(R.array.add_coin_values),
                getResources().getIntArray(R.array.add_coin_amount_values)));

        videoGrid = findViewById(R.id.videoGrid);
        videoGrid.setStatsManager(statsManager());

        liveId = getIntent().getStringExtra("liveId");

        if (getIntent().getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME).matches("@" + sessionManager.getUser().getData().getUserName())) {

            isBroadcaster = true;
            showInvite = true;

            llBottomAudience.setVisibility(View.GONE);
            llHostBottom.setVisibility(View.VISIBLE);
            rvAddCoin.setVisibility(View.GONE);

            becomesHost(false, false);

        }
        //Audience
        else {

            isBroadcaster = false;
            showInvite = false;

            llBottomAudience.setVisibility(View.VISIBLE);
            llHostBottom.setVisibility(View.GONE);
            rvAddCoin.setVisibility(View.VISIBLE);
            config().setChannelName(getIntent().getStringExtra("targetName"));

            becomeAudience();
        }

        hitGetViewersApi("1");
        hitGetHostProfileDataApi();
        hitGetLiveDetailApi();
        initChat();
        mVideoDimension = com.fairtok.openlive.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];

       /* final View activityRootView = findViewById(R.id.rootView);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

              *//*  Rect r = new Rect();

                activityRootView.getWindowVisibleDisplayFrame(r);

                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);

                Log.d("heightDiff", String.valueOf(heightDiff));

                if (heightDiff > 250) {

                    //enter your code here
                    Log.d("is Opened", "is Opened");

                    llHostBottom.setVisibility(View.GONE);
                    llBottomAudience.setVisibility(View.GONE);

                    llMessage.setVisibility(View.VISIBLE);
                    etMessage.requestFocus();
                    Log.d("check Opened", String.valueOf(llMessage.getVisibility()));

                }
                else
                    {

                    //enter code for hid
                    Log.d("is Closed", "is Closed");

                    llMessage.setVisibility(View.GONE);
                    Log.d("check Opened", String.valueOf(llMessage.getVisibility()));

                    if (isBroadcaster)
                    {
                        llHostBottom.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        llBottomAudience.setVisibility(View.VISIBLE);
                    }

                }
*//*            }
        });*/
    }

    private void hitGetLiveDetailApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.live_details;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            parseLiveDetail(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseLiveDetail(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                tvGoldCoin.setText(response.getJSONObject("data").getString("CoinsReceived"));
                tvSilverCoin.setText(response.getJSONObject("data").getString("CoinsReceivedSilver"));

                if (isBroadcaster) {

                    int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                    int updatedCoins = currentCoins + AppUtils.ifEmptyReturn0Int(response.getJSONObject("data").getString("CoinsReceived"));
                    User user = sessionManager.getUser();
                    user.getData().setPaidCoins("" + updatedCoins);
                    sessionManager.saveUser(user);
                }
                else {
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void hitGetHostProfileDataApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //   //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.user_details;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("user_id", isBroadcaster ? Global.USER_ID : Utils.hostUserId)
                    .addUrlEncodeFormBodyParameter("my_user_id", Global.USER_ID)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //         //AppUtils.hideDialog();
                            parseGetHostProfile(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //          //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGetHostProfile(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                instaProfileUrl = response.getJSONObject("data").getString("insta_url");

                if (isBroadcaster) {


                } else {

                    if (response.getJSONObject("data").getString("is_following").equals("0")) {

                        ivAdd.setImageResource(R.drawable.ic_add);
                    } else {
                        ivAdd.setImageResource(R.drawable.ic_star);
                    }



                    tvName.setText(response.getJSONObject("data").getString("full_name"));
                    Glide.with(this).load(Const.ITEM_BASE_URL +
                            response.getJSONObject("data").getString("user_profile")).into(ivProfile);
                    tvTotalDiamonds.setText(response.getJSONObject("data").getString("diamondCoins"));
                }

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivChatHost:
            case R.id.ivChat:

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

                etMessage.requestFocus();
                llHostBottom.setVisibility(View.GONE);
                llBottomAudience.setVisibility(View.GONE);

                llMessage.setVisibility(View.VISIBLE);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                //llMessage.setVisibility(llMessage.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);

                break;

            case R.id.ivSendMessage:

                sendMessage();

                break;

            case R.id.ivGift:

                showGiftBottomDialog();

                break;

            case R.id.llMessage:

                break;

            case R.id.ivClose:

                onBackPressed();

                break;

/*
            case R.id.rootView:

                AppUtils.hideSoftKeyboard(this);

                break;*/

            case R.id.ivShare:

                shareLiveSession();

                break;

            case R.id.llTopViewers:

                hitGetViewersApi("2");

                break;

            case R.id.ivAdd:

                hitFollowUnfollowApi(Utils.hostUserId, ivAdd);

                break;

            case R.id.ivInstagram:

                if (isBroadcaster) {

                    showEnterInstagramProfileDialog();

                } else {
                    openInstaProfile();
                }

                break;

        }

    }

    private void shareLiveSession() {

        String uri = Uri.parse(Const.BASE_URL_SHARE)
                .buildUpon()
                .appendPath("liveSession")
                .appendQueryParameter("liveId", liveId)
                .appendQueryParameter("hostId", Global.USER_ID)
                .appendQueryParameter("targetName", getIntent().getStringExtra("targetName").trim())
                .build().toString();

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        String shareMessage = "Live session is going on. Please join before it end"+"\n";
        shareMessage = shareMessage +" " +uri;
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
        startActivity(Intent.createChooser(shareIntent, "choose one"));

    }

    private void showEnterInstagramProfileDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallActivity.this, R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_insta_url);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        EditText etInstaProfile = bottomSheetDialog.findViewById(R.id.etInstaProfile);
        if (!instaProfileUrl.equals("0")) etInstaProfile.setText(instaProfileUrl);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();

            if (etInstaProfile.getText().toString().isEmpty())
                AppUtils.showToastSort(LiveCallActivity.this, getString(R.string.pleaseEnterInstaProfile));
            else
                hitUpdateInstagramApi(etInstaProfile.getText().toString().trim());

        });

    }

    private void hitUpdateInstagramApi(String instaProfile) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.update_instagram;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("insta_url", instaProfile)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            try {
                                if (response.getBoolean("status")) instaProfileUrl = instaProfile;

                                Log.v("postApi-resp", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveCallActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void openInstaProfile() {

        if (instaProfileUrl.equals("0") || instaProfileUrl.isEmpty()) {

            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallActivity.this,
                    R.style.CustomBottomSheetDialogTheme);
            bottomSheetDialog.setContentView(R.layout.dialog_message);
            bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
            bottomSheetDialog.show();

            TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

            tvMessage.setText(getString(R.string.intstagramProfileNotAvail));

            bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> bottomSheetDialog.dismiss());

        } else {
            Uri uri = Uri.parse("http://instagram.com/_u/" + instaProfileUrl);
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

            likeIng.setPackage("com.instagram.android");

            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/" + instaProfileUrl)));
            }

        }
    }

    private void hitGetViewersApi(String type) {

        if (AppUtils.isNetworkAvailable(this)) {

            //if (type.equals("2"))
            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.live_audience;
            Log.v("postApi-url", url);
            Log.v("postApi-data", Global.ACCESS_TOKEN);
            Log.v("postApi-data", liveId);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseGetViewersJson(response, type);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(LiveCallActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseGetViewersJson(JSONObject response, String type) {

        try {
            if (response.getBoolean("status")) {

                if (type.equals("1")) {

                    try {
                        JSONArray jsonArray = response.getJSONArray("data");

                        ivProfile1.setImageResource(R.drawable.fake_user_icon);
                        ivProfile2.setImageResource(R.drawable.fake_user_icon);
                        ivProfile3.setImageResource(R.drawable.fake_user_icon);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            if (i == 0) {

                                Glide.with(this).load(Const.ITEM_BASE_URL + jsonObject.getString("photo"))
                                        .into(ivProfile1);
                            } else if (i == 1) {

                                Glide.with(this).load(Const.ITEM_BASE_URL + jsonObject.getString("photo"))
                                        .into(ivProfile2);

                            } else if (i == 2) {

                                Glide.with(this).load(Const.ITEM_BASE_URL + jsonObject.getString("photo"))
                                        .into(ivProfile3);
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else
                    showViewersDialog(response);

            } /*else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));*/
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showViewersDialog(JSONObject response) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_viewers);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        try {
            JSONArray jsonArray = response.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("live_id", jsonObject.getString("live_id"));
                hashMap.put("user_id", jsonObject.getString("user_id"));
                hashMap.put("user_name", jsonObject.getString("user_name"));
                hashMap.put("fullname", jsonObject.getString("fullname"));
                hashMap.put("photo", jsonObject.getString("photo"));
                hashMap.put("CoinsSent", jsonObject.getString("CoinsSent"));
                hashMap.put("ifFollowing", jsonObject.getString("ifFollowing"));

                arrayList.add(hashMap);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RecyclerView rvViewers = bottomSheetDialog.findViewById(R.id.rvViewers);
        rvViewers.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1));
        rvViewers.setAdapter(new AdapterViewers(arrayList));


    }

    private void hitFollowUnfollowApi(String toUser, ImageView ivFollow) {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.followUnfollow;
            Log.v("postApi-url", url);
            Log.v("postApi-data", liveId);
            Log.v("postApi-data", toUser);
            Log.v("postApi-data", Global.ACCESS_TOKEN);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("to_user_id", toUser)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseFollowUnfollow(response, ivFollow);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseFollowUnfollow(JSONObject response, ImageView ivFollow) {

        try {
            if (response.getBoolean("status")) {

                if (response.getJSONObject("data").getString("type").equals("1"))
                    ivFollow.setImageResource(R.drawable.ic_star);
                else
                    ivFollow.setImageResource(R.drawable.ic_add);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void purchaseOfferCoin() {

        coins = "500";

        viewModel.coins = coins;
        viewModel.coinsAmount = "350";

        initializePayment(viewModel.coinsAmount);
    }


    private void showGiftBottomDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_gifts);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvCoins = bottomSheetDialog.findViewById(R.id.tvCoins);
        tvCoins.setText(sessionManager.getUser().getData().getPaidCoins());

        RecyclerView rvGift = bottomSheetDialog.findViewById(R.id.rvGift);
        rvGift.setLayoutManager(new GridLayoutManager(this, 4));

        bottomSheetDialog.findViewById(R.id.llCoins).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            getCoinPlan();
        });

        rvGift.setAdapter(new AdapterGift(getResources().getIntArray(R.array.gift_values), bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    private void getCoinPlan() {

        hitGetCoinPlanApi();

    }

    private void hitGetCoinPlanApi() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseCoinPlanJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseCoinPlanJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                    hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                    hashMap.put("coin_amount", jsonObject.getString("coin_amount"));

                    arrayList.add(hashMap);
                }

                showCoinBottomDialog(arrayList);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showCoinBottomDialog(ArrayList<HashMap<String, String>> arrayList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_coins);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet)
                .setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        RecyclerView rvCoins = bottomSheetDialog.findViewById(R.id.rvCoins);
        rvCoins.setLayoutManager(new GridLayoutManager(this, 3));

        rvCoins.setAdapter(new AdapterCoins(arrayList, bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    public void initializePayment(String PayAmount) {

        Log.d("PayAmount -  ", PayAmount);

        String customerID = sessionManager.getUser().getData().getUserId();
        String orderID = "OrderID" + System.currentTimeMillis() + "-" + customerID;

        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        TrakConstant.PG_ORDER_ID = Integer.toString(n);
        PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey(TrakConstant.PG_API_KEY);
        pgPaymentParams.setAmount(PayAmount);
        pgPaymentParams.setEmail(sessionManager.getUser().getData().getUserEmail());
        pgPaymentParams.setName(sessionManager.getUser().getData().getFullName());
        pgPaymentParams.setPhone(sessionManager.getUser().getData().getUserMobileNo());
        pgPaymentParams.setOrderId(orderID);
        pgPaymentParams.setCurrency(TrakConstant.PG_CURRENCY);
        pgPaymentParams.setDescription(TrakConstant.PG_DESCRIPTION);
        pgPaymentParams.setCity("NA");
        pgPaymentParams.setState(prefHandler.getLanguage());
        pgPaymentParams.setAddressLine1(TrakConstant.PG_ADD_1);
        pgPaymentParams.setAddressLine2(TrakConstant.PG_ADD_2);
        pgPaymentParams.setZipCode("560094");
        pgPaymentParams.setCountry(TrakConstant.PG_COUNTRY);
        pgPaymentParams.setReturnUrl(TrakConstant.PG_RETURN_URL);
        pgPaymentParams.setMode(TrakConstant.PG_MODE);
        pgPaymentParams.setUdf1(TrakConstant.PG_UDF1);
        pgPaymentParams.setUdf2(TrakConstant.PG_UDF2);
        pgPaymentParams.setUdf3(TrakConstant.PG_UDF3);
        pgPaymentParams.setUdf4(TrakConstant.PG_UDF4);
        pgPaymentParams.setUdf5(TrakConstant.PG_UDF5);

        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, this);
        pgPaymentInitialzer.initiatePaymentProcess();
    }

    private void sendMessage() {

        String msg = etMessage.getText().toString();

        if (!msg.equals("")) {
            RtmMessage message = mRtmClient.createMessage();

            String[] splited = name.split(" ");
            message.setText(splited[0] + " - " + msg);

            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
            message.setRawMessage(profilePic_bytes);

            //TODO
            //MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
            MessageBean messageBean = new MessageBean(name, message, true, profilePic);
            mMessageBeanList.add(messageBean);
            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            sendChannelMessage(message);

        }

        etMessage.setText("");

    }

    private void becomesHost(boolean audioMuted, boolean videoMuted) {

        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    private void becomeAudience() {
        isBroadcaster = false;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        rtcEngine().muteLocalAudioStream(true);
        rtcEngine().muteLocalVideoStream(true);
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        SurfaceView surface = prepareRtcVideo(0, true);
        videoGrid.addUserVideoSurface(0, surface, true);
        /*ivMuteAudio.setActivated(true);*/
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        removeRtcVideo(0, true);
        videoGrid.removeUserVideo(0, true);
        /*ivMuteAudio.setActivated(false);*/
    }

    private void initChat() {

        mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();
        mClientListener = new MyRtmClientListener();
        mChatManager.registerListener(mClientListener);

        Intent intent = getIntent();
        mIsPeerToPeerMode = intent.getBooleanExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        mUserId = intent.getStringExtra(MessageUtil.INTENT_EXTRA_USER_ID);
        String targetName = intent.getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME);

        if (mIsPeerToPeerMode) {
            mPeerId = targetName;

            // load history chat records
            MessageListBean messageListBean = MessageUtil.getExistMessageListBean(mPeerId);
            if (messageListBean != null) {
                mMessageBeanList.addAll(messageListBean.getMessageBeanList());
            }

            // load offline messages since last chat with this peer.
            // Then clear cached offline messages from message pool
            // since they are already consumed.
            MessageListBean offlineMessageBean = new MessageListBean(mPeerId, (List<MessageBean>) mChatManager);
            mMessageBeanList.addAll(offlineMessageBean.getMessageBeanList());
            mChatManager.removeAllOfflineMessages(mPeerId);
        } else {
            mChannelName = targetName;
            mChannelMemberCount = 1;
            createAndJoinChannel();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mMessageAdapter = new MessageAdapter(this, mMessageBeanList, message -> {
            if (message.getMessage().getMessageType() == RtmMessageType.IMAGE) {
                if (!TextUtils.isEmpty(message.getCacheFile())) {
                   /* Glide.with(this).load(message.getCacheFile()).into(ivBigImage);
                    ivBigImage.setVisibility(View.VISIBLE);*/
                } else {
                    ImageUtil.cacheImage(this, mRtmClient, (RtmImageMessage) message.getMessage(), new ResultCallback<String>() {
                        @Override
                        public void onSuccess(String file) {
                            message.setCacheFile(file);
                            runOnUiThread(() -> {
                                /*Glide.with(this).load(file).into(ivBigImage);
                                ivBigImage.setVisibility(View.VISIBLE);*/
                            });
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
        rvMessageList.setAdapter(mMessageAdapter);

    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment

        // showToast("New User Joined");
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        videoGrid.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        videoGrid.removeUserVideo(uid, false);
    }

    @Override
    protected void onGlobalLayoutCompleted() {
/*
        RelativeLayout topLayout = findViewById(R.id.rlTop);
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) topLayout.getLayoutParams();
        params.height = mStatusBarHeight + topLayout.getMeasuredHeight();
        topLayout.setLayoutParams(params);
        topLayout.setPadding(0, mStatusBarHeight, 0, 0);
*/
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        Log.v("lihh", "finish");
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        onBackPressed();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(),
                com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS);
    }

    public void onMoreClicked(View view) {
    }

    public void onPushStreamClicked(View view) {
    }

    public void onMuteAudioClicked(View view) {
        /*  if (!ivMuteVideo.isActivated()) return;*/

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
            //startVideo();
        }
        view.setActivated(!view.isActivated());
        rtcEngine().muteLocalVideoStream(view.isActivated());
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response = " + result);

        if (response_code == 1111) {
            try {
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {
                    Utils.party_id = obj.getString("id");
                } else {
                    Utils.party_id = "0";
                }

                Log.v("paras", "paras Utils.party_id in response = " + Utils.party_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response_code == 222) {

            if (!isActivityStop)
                finish();

        } else if (response_code == 10) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {

                    arrayList = new ArrayList<>();
                    JSONObject data_obj = obj.getJSONObject("post_data");
                    JSONArray jarr = data_obj.getJSONArray("data_arr");

                    for (int i = 0; i < jarr.length(); i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatListPOJO bean = new ChatListPOJO();
                        bean.setOpp_userid(jobj.getString("user_id"));
                        bean.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        bean.setReceiver_photo(jobj.getString("receiver_photo"));
                        bean.setLast_msg(jobj.getString("CoinsSent"));

                        arrayList.add(bean);
                    }
                    Log.v("memberList2", result);
                    mChannelMemberCount = arrayList.size();

                    MyPerformanceArrayAdapter itemsAdapter = new MyPerformanceArrayAdapter(this, arrayList, showInvite);
                    BottomSheetDialog dialog = new BottomSheetDialog(this);
                    dialog.setContentView(R.layout.bottom_sheet_view);
                    BottomSheetListView listView = dialog.findViewById(R.id.listViewBtmSheet);
                    listView.setAdapter(itemsAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
                        }
                    });
                    dialog.show();

                } else {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * API CALL: create and join channel
     */
    private void createAndJoinChannel() {
        // step 1: create a channel instance
        mRtmChannel = mRtmClient.createChannel(mChannelName, new MyChannelListener());
        if (mRtmChannel == null) {
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        Log.e("channel", mRtmChannel + "");

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("TAG", "join channel success");
                getChannelMemberList(false);
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "join channel failed5");
                Log.e("TAG", errorInfo.getErrorDescription());
                Log.e("TAG", String.valueOf(errorInfo.getErrorCode()));
                Log.e("TAG", errorInfo.toString());
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });
    }

    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        bp.consumePurchase(productId);
        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);
        viewModel.purchaseCoin(details.orderId, "Google InApp Purchase", viewModel.coinsAmount,
                prefHandler.getName(), prefHandler.getuId(),
                coinPanId, FTransactionStatus);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {

    }

    @Override
    public void onBillingInitialized() {

    }

    /**
     * API CALLBACK: rtm event listener
     */
    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {
            runOnUiThread(() -> {

                Log.v("paras", "paras on peer's onMessageReceived called");

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                int backg = getMessageColor(peerId);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved1", msg);
                    message.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                    hitGetLiveDetailApi();
                } else if (msg.contains("co-host-invitation")) {
                    doInvite();
                }

                if (peerId.equals(mPeerId)) {

                    MessageBean messageBean = new MessageBean(peerId, message, false, profilePic);
                    messageBean.setBackground(backg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, message);
                }
            });
        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {

            String profilePic = Utils.DEFAULT_PROFILE_URL;

            profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

            int backg = getMessageColor(peerId);
            String msg = rtmImageMessage.getText();
            if (msg.contains("gift$$$$$")) {
                rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                String[] parts = msg.split("-");
                backg = Integer.parseInt(parts[1]);
                animateGiftGif(Integer.parseInt(parts[1]));
                hitGetLiveDetailApi();
            } else if (msg.contains("co-host-invitation")) {
                doInvite();
            }

            String finalProfilePic = profilePic;
            int finalBackg = backg;
            runOnUiThread(() -> {
                if (peerId.equals(mPeerId)) {
                    MessageBean messageBean = new MessageBean(peerId, rtmImageMessage, false, finalProfilePic);
                    messageBean.setBackground(finalBackg);
                    mMessageBeanList.add(messageBean);
                    mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                    rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
                } else {
                    MessageUtil.addMessageBean(peerId, rtmImageMessage);
                }
            });
        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }
    }

    /**
     * API CALLBACK: rtm channel event listener
     */
    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(message.getRawMessage(), StandardCharsets.UTF_8);

                String account = fromMember.getUserId();

                int backg = getMessageColor(account);
                String msg = message.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved2", msg);

                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);

                    animateGiftGif(Integer.parseInt(parts[1]));

                    message.setText(parts[2] + " " + getResources().getString(R.string.live_message_gift_send));
                    hitGetLiveDetailApi();
                }


                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + message);
                MessageBean messageBean = new MessageBean(account, message, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {
            runOnUiThread(() -> {

                String profilePic = Utils.DEFAULT_PROFILE_URL;

                profilePic = new String(rtmImageMessage.getRawMessage(), StandardCharsets.UTF_8);

                String account = rtmChannelMember.getUserId();

                int backg = getMessageColor(account);
                String msg = rtmImageMessage.getText();
                if (msg.contains("gift$$$$$")) {
                    Log.v("msgRecieved3", msg);
                    rtmImageMessage.setText(getResources().getString(R.string.live_message_gift_send));
                    String[] parts = msg.split("-");
                    backg = Integer.parseInt(parts[1]);
                    animateGiftGif(Integer.parseInt(parts[1]));
                }

                Log.i("TAG", "onMessageReceived account = " + account + " msg = " + rtmImageMessage);
                MessageBean messageBean = new MessageBean(account, rtmImageMessage, false, profilePic);
                messageBean.setBackground(backg);
                mMessageBeanList.add(messageBean);
                mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
            });
        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {
            runOnUiThread(() -> {
                Log.v("ljhljkhl2", String.valueOf(member.getUserId()));
                hitGetViewersApi("1");
                mChannelMemberCount++;
                refreshChannelTitle();

                //showToast(member.getUserId() + " has joined the party");
            });
        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {
            runOnUiThread(() -> {
                mChannelMemberCount--;
                refreshChannelTitle();
                Log.v("ljhljkhl3", String.valueOf(member.getUserId()));
                hitGetViewersApi("1");
                if (mChannelName.equalsIgnoreCase(member.getUserId())) {
                    openDialog("Host has ended the party");
                } //else
                //showToast(member.getUserId() + " has left the party");

            });
        }
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(this, text, Toast.LENGTH_SHORT).show());
    }

    private int getMessageColor(String account) {
        for (int i = 0; i < mMessageBeanList.size(); i++) {
            if (account.equals(mMessageBeanList.get(i).getAccount())) {
                return mMessageBeanList.get(i).getBackground();
            }
        }
        return MessageUtil.COLOR_ARRAY[MessageUtil.RANDOM.nextInt(MessageUtil.COLOR_ARRAY.length)];
    }

    public void animateGiftGif(int giftId) {

        GiftAnimWindow window = new GiftAnimWindow(this, R.style.gift_anim_window);
        window.setAnimResource(GiftUtil.getGiftAnimRes(giftId));
        window.show();
    }

    private void doInvite() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(config().getChannelName() + " has invited you to co-host the party.");
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                becomesCoHost(false, false);
            }
        });
        builder.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                rejectCoHost();
            }
        });
        builder.show();
    }

    private void becomesCoHost(boolean audioMuted, boolean videoMuted) {

//        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    public void rejectCoHost() {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-rejected");

        MessageBean messageBean = new MessageBean(mChannelName.substring(1), message, true, "default.png");
        mMessageBeanList.add(messageBean);
        mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
        rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);
        mPeerId = mChannelName.substring(1);
        sendPeerMessage(message);
    }

    /**
     * API CALL: get channel member list
     */
    private void getChannelMemberList(boolean showMemberList) {
        memberList = new ArrayList<>();
        mRtmChannel.getMembers(new ResultCallback<List<RtmChannelMember>>() {
            @Override
            public void onSuccess(final List<RtmChannelMember> responseInfo) {
                runOnUiThread(() -> {

                    Log.v("ljhljkhl1", String.valueOf(responseInfo.size()));
                   /* Log.v("memberList1", String.valueOf(responseInfo.get(0).getChannelId()));
                    Log.v("memberList1", String.valueOf(responseInfo.get(0).getUserId()));*/
                    memberList = responseInfo;

                    mChannelMemberCount = responseInfo.size();
                    refreshChannelTitle();
                });
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "failed to get channel members, err: " + errorInfo.getErrorCode());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void refreshChannelTitle() {
        tvViewers.setText("" + (mChannelMemberCount-1));
    }

    public void openDialog(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                        finish();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * API CALL: send message to peer
     */
    private void sendPeerMessage(final RtmMessage message) {
        mRtmClient.sendMessageToPeer(mPeerId, message, mChatManager.getSendMessageOptions(), new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // do nothing
            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.PeerMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_PEER_UNREACHABLE:
                            showToast("User is offline");
                            break;
                        case RtmStatusCode.PeerMessageError.PEER_MESSAGE_ERR_CACHED_BY_SERVER:
                            showToast("cached msg");
                            break;
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityStop = false;
        if (isBroadcaster) {
            //makeHostLiveOnServer("1");
            //makingHost online on LiveUSerActivity
        } else {
            //makeHostLiveOnServer("0");
            makeHostLiveOnServerNew();

        }
    }

    private void makeHostLiveOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.join_live;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseJoinLiveJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseJoinLiveJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                join_id = response.getJSONObject("data").getString("join_id");
            } else {
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void makeHostLiveOnServer(String isHost) {
        try {
            PrefHandler pref = new PrefHandler(this);
            SessionManager sessionManager = new SessionManager(this);

            Log.v("paras", "paras making the host live user id = " + pref.getuId());

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeonline"));
            params.add(new Pair<>("userid", pref.getuId()));
            params.add(new Pair<>("isGaming", "0"));
            params.add(new Pair<>("username", sessionManager.getUser().getData().getUserName()));

            params.add(new Pair<>("fullname", sessionManager.getUser().getData().getFullName()));
            params.add(new Pair<>("photo", sessionManager.getUser().getData().getUserProfile()));
            params.add(new Pair<>("isHost", isHost));
            params.add(new Pair<>("party_id", Utils.party_id));

            Log.v("kgdkqwjgsd", String.valueOf(params));
//            party_id

            new AsyncHttpsRequest("", this, params, this, 1111, false).execute(Utils.MakeHostOnline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.v("lihh", "Destroy");

        if (isBroadcaster) {
            leaveAndReleaseChannel();
            mChatManager.unregisterListener(mClientListener);

        }
    }

    private void makeHostOfflineOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.STOP_LIVE;
            Log.v("postApi-url", url);
            Log.v("postApi-url", liveId);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                if (response.getBoolean("status")) {

                                    if (!isActivityStop) {
                                        finish();
                                    }
                                }
                                Log.v("postApi-resp", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void showLiveEndDialog() {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getApplicationContext(),
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_live_coin_earned);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvViewers, tvGoldCoin;

        tvViewers = bottomSheetDialog.findViewById(R.id.tvViewers);
        tvGoldCoin = bottomSheetDialog.findViewById(R.id.tvGoldCoin);

        tvViewers.setText(this.tvViewers.getText().toString());
        tvGoldCoin.setText(this.tvGoldCoin.getText().toString());

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

        bottomSheetDialog.findViewById(R.id.btnEnd).setOnClickListener(view -> makeHostOfflineOnServerNew());

    }

    /**
     * API CALL: leave and release channel
     */
    private void leaveAndReleaseChannel() {
        if (mRtmChannel != null) {
            mRtmChannel.leave(null);
            mRtmChannel.release();
            mRtmChannel = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("lihh", "stop");
        isActivityStop = true;
        if (isBroadcaster) {
            // makeHostOfflineOnServer();
            makeHostOfflineOnServerNew();
        } else {
            //makeAudienceOfflineOnServer();
            makeAudienceOfflineOnServerNew();
        }
    }

    private void makeAudienceOfflineOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.exit_live;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("join_id", join_id)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();

                            try {
                                if (response.getBoolean("status")) {
                                    if (!isActivityStop)
                                        finish();
                                }
                                //parseJoinLiveJson(response);
                                Log.v("postApi-resp", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    public void makeHostOfflineOnServer() {
        try {
            PrefHandler pref = new PrefHandler(this);

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", Utils.party_id));

            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeHostOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeAudienceOfflineOnServer() {
        try {
            PrefHandler pref = new PrefHandler(this);

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", pref.getuId())); //
            params.add(new Pair<>("party_id", Utils.party_id));
            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeAudienceOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                final String file = resultUri.getPath();
                ImageUtil.uploadImage(this, mRtmClient, file, new ResultCallback<RtmImageMessage>() {
                    @Override
                    public void onSuccess(final RtmImageMessage rtmImageMessage) {
                        runOnUiThread(() -> {

                            byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                            rtmImageMessage.setRawMessage(profilePic_bytes);

                            MessageBean messageBean = new MessageBean(mUserId, rtmImageMessage, true, profilePic);
                            messageBean.setCacheFile(file);
                            mMessageBeanList.add(messageBean);
                            mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                            rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);

                            sendChannelMessage(rtmImageMessage);

                        });
                    }

                    @Override
                    public void onFailure(ErrorInfo errorInfo) {

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);
                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                        showToast("Transaction Failed");
                        //transactionIdView.setText("Transaction ID: NIL");
                        //transactionStatusView.setText("Transaction Status: Transaction Error!");
                    } else {
                        JSONObject response = new JSONObject(paymentResponse);
                        TrakNPayResponse = response;

                        FPaymentId = response.getString("transaction_id");
                        FTransactionId = response.getString("transaction_id");
                        FPaymentMode = response.getString("payment_mode");
                        FTransactionStatus = response.getString("response_code");
                        FAmount = response.getString("amount");
                        FMessage = response.getString("response_message");

                        Log.e("TrakNPay", "onActivityResult: " + response);

                        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);
                      if (response.getString("response_code").equals("0"))
                      {
                          viewModel.purchaseCoin(FTransactionId, FPaymentMode, FAmount, prefHandler.getName(),
                                  FMessage, coinPanId, FTransactionStatus);
                      }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                showToast("Transaction Canceled");
            }
        }
    }

    public void purchasedCoin(String FTransactionId, String FPaymentMode, String FAmount, String user_name, String status) {

        /*disposable.add(Global.initRetrofit().purchaseCoin(Global.ACCESS_TOKEN,
                coins, FTransactionId, FPaymentMode, FAmount, user_name, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((purchase, throwable) -> {

                    if (purchase != null) {
                        this.purchase.setValue(purchase);

                    }

                }));*/
    }

    /**
     * API CALL: send message to a channel
     */
    private void sendChannelMessage(RtmMessage message) {
        mRtmChannel.sendMessage(message, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.ChannelMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                    }
                });
            }
        });
    }


    private void doLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Are you sure you want to exit from the party?.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                isActivityStop = false;

                if (isBroadcaster) {
                    //makeHostOfflineOnServer();
                    //makeHostOfflineOnServerNew();
                    showLiveEndDialog();

                } else {
                    //makeAudienceOfflineOnServer();
                    makeAudienceOfflineOnServerNew();
                }
            }
        });
        builder.setNegativeButton("No, Stay here", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private class AdapterGift extends RecyclerView.Adapter<AdapterGift.MyViewHolder> {

        int[] data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterGift(int[] stringArray, BottomSheetDialog dialog) {

            data = stringArray;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public AdapterGift.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_gifts, viewGroup, false);
            return new AdapterGift.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterGift.MyViewHolder holder, final int position) {

            if (position == 0) holder.ivCoin.setImageResource(R.drawable.coin_silver);

            holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            holder.tvCoins.setText(String.valueOf(data[position]));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int coinsToSend = data[position];

                    currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

                    Log.v("paras", "paras sending gift gift index = " + position);

                    if (position==0){

                        bottomSheetDialog.dismiss();

                        //type=2 GoldCoin, 1=silver coin
                        sendBubble(Utils.hostUserId, "" + coinsToSend, "1", position);

                        //  updateGiftCoinsAccount("" + coinsToSend);

                    }
                    else {
                        if (currentWalletBalance >= coinsToSend) {
                            bottomSheetDialog.dismiss();

                            //type=2 GoldCoin, 1=silver coin
                            sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position);

                            //  updateGiftCoinsAccount("" + coinsToSend);
                        } else {
                            bottomSheetDialog.dismiss();
                            doAlert();
                        }
                    }
                }
            });

        }

        @Override
        public int getItemCount() {

            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage, ivCoin;

            TextView tvCoins;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                ivCoin = itemView.findViewById(R.id.ivCoin);

                tvCoins = itemView.findViewById(R.id.tvCoins);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }


    private class AdapterCoins extends RecyclerView.Adapter<AdapterCoins.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterCoins(ArrayList<HashMap<String, String>> arrayList, BottomSheetDialog dialog) {

            data = arrayList;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public AdapterCoins.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coins, viewGroup, false);
            return new AdapterCoins.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterCoins.MyViewHolder holder, final int position) {

            //holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            // holder.ivImage.setImageDrawable(R.drawable.ic_coin);
            holder.tvCoins.setText(data.get(position).get("coin_amount"));
            holder.tvAmount.setText("Rs. " + data.get(position).get("coin_plan_price"));

            holder.llMain.setTag(R.string.amount, data.get(position).get("coin_plan_price"));
            holder.llMain.setTag(R.string.coins, data.get(position).get("coin_amount"));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bottomSheetDialog.dismiss();
                    coinPanId = data.get(position).get("coin_plan_id");

                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModel.coins = coins;
                    viewModel.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

                    initializePayment((String) holder.llMain.getTag(R.string.amount));

                }
            });
        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            TextView tvCoins, tvAmount;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);

                tvCoins = itemView.findViewById(R.id.tvCoins);
                tvAmount = itemView.findViewById(R.id.tvAmount);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }


    private class AdapterAddCoin extends RecyclerView.Adapter<AdapterAddCoin.MyViewHolder> {

        int[] data;
        int[] amountData;

        public AdapterAddCoin(int[] intArray, int[] array) {

            data = intArray;
            amountData = array;
        }

        @NonNull
        @Override
        public AdapterAddCoin.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_add_coin, viewGroup, false);
            return new AdapterAddCoin.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterAddCoin.MyViewHolder holder, final int position) {

            if (position == 0) {

                holder.tvCoins1.setText(String.valueOf(data[position]));
                holder.tvCoinsPrice.setText("Rs. " + amountData[position]);
                holder.ll1.setVisibility(View.VISIBLE);
                holder.ll2.setVisibility(View.GONE);

            } else {
                holder.ivImage.setImageResource(GiftUtil.getAddCoinAnimRes(position - 1));
                holder.tvCoins2.setText(String.valueOf(amountData[position]));
                holder.ll1.setVisibility(View.GONE);
                holder.ll2.setVisibility(View.VISIBLE);

            }

            holder.llMain.setTag(R.string.amount, String.valueOf(amountData[position]));
            holder.llMain.setTag(R.string.coins, String.valueOf(data[position]));

            holder.llMain.setOnClickListener(view -> {


                if (position == 0) {
                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModel.coins = coins;
                    viewModel.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

                    initializePayment((String) holder.llMain.getTag(R.string.amount));
                } else {

                    currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());

                    Log.v("paras", "paras sending gift gift index = " + position);

                    int coinsToSend = data[position];

                    if (currentWalletBalance >= coinsToSend) {


                        //type=2 GoldCoin, 1=silver coin
                        sendBubble(Utils.hostUserId, "" + coinsToSend, "2", position - 1);

                        //  updateGiftCoinsAccount("" + coinsToSend);
                    } else {
                        doAlert();
                    }

                }


            });
        }

        @Override
        public int getItemCount() {
            return data.length;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            LinearLayout llMain, ll1, ll2;

            TextView tvCoins1, tvCoins2, tvCoinsPrice;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);
                tvCoins1 = itemView.findViewById(R.id.tvCoins1);
                tvCoins2 = itemView.findViewById(R.id.tvCoins2);
                tvCoinsPrice = itemView.findViewById(R.id.tvCoinsPrice);

                llMain = itemView.findViewById(R.id.llMain);

                ll1 = itemView.findViewById(R.id.ll1);
                ll2 = itemView.findViewById(R.id.ll2);

            }
        }
    }

    private class AdapterViewers extends RecyclerView.Adapter<AdapterViewers.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        public AdapterViewers(ArrayList<HashMap<String, String>> arrayList) {

            data = arrayList;
        }

        @NonNull
        @Override
        public AdapterViewers.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_viewers, viewGroup, false);
            return new AdapterViewers.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterViewers.MyViewHolder holder, final int position) {

            switch (position) {

                case 0:
                    holder.tvPosition.setText("1");
                    break;
                case 1:
                    holder.tvPosition.setText("2");
                    break;
                case 2:
                    holder.tvPosition.setText("3");
                    break;

                default:
                    holder.tvPosition.setVisibility(View.GONE);
                    break;

            }

            holder.tvName.setText(data.get(position).get("fullname"));

            if (data.get(position).get("CoinsSent").equals("0"))
                holder.llCoin.setVisibility(View.GONE);
            else {
                holder.llCoin.setVisibility(View.VISIBLE);
                holder.tvCoinSent.setText(data.get(position).get("CoinsSent"));
            }

            if (sessionManager.getUser().getData().getUserId().equals(data.get(position).get("user_id")))
                holder.ivFollow.setVisibility(View.GONE);

            Glide.with(getApplicationContext()).load(Const.ITEM_BASE_URL + data.get(position).get("photo")).into(holder.ivProfile);

            if (data.get(position).get("ifFollowing").equals("0")) {
                holder.ivFollow.setVisibility(View.VISIBLE);
            } else {
                holder.ivFollow.setVisibility(View.GONE);
            }

            holder.ivFollow.setOnClickListener(v -> hitFollowUnfollowApi(data.get(position).get("user_id"),
                    holder.ivFollow
            ));

        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvPosition, tvName, tvCoinSent;

            ImageView ivProfile, ivFollow;

            LinearLayout llCoin;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                tvPosition = itemView.findViewById(R.id.tvPosition);
                tvName = itemView.findViewById(R.id.tvName);
                tvCoinSent = itemView.findViewById(R.id.tvCoinSent);

                ivProfile = itemView.findViewById(R.id.ivProfile);
                ivFollow = itemView.findViewById(R.id.ivFollow);

                llCoin = itemView.findViewById(R.id.llCoin);

            }
        }
    }


    public void sendBubble(String toUserId, String coin, String type, int position) {

        Log.v("ljhhhlk", coin);
        Log.v("ljhhhlk", liveId);
        Log.v("ljhhhlk", Global.ACCESS_TOKEN);

        if (AppUtils.isNetworkAvailable(this)) {

            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.liveSendCoin;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("live_id", liveId)
                    .addUrlEncodeFormBodyParameter("join_id", join_id)
                    .addUrlEncodeFormBodyParameter("coins", coin)
                    .addUrlEncodeFormBodyParameter("coin_type", type)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Log.v("postApi-resp", String.valueOf(response));
                            try {
                                if (response.getBoolean("status")) {

                                    try {

                                        animateGiftGif(position);

                                        String[] splited = name.split(" ");

                                        String msg = "gift$$$$$-" + position + "-" + splited[0];
                                        RtmMessage message = mRtmClient.createMessage();
                                        message.setText(msg);
                                        byte[] profilePic_bytes = profilePic.getBytes(StandardCharsets.UTF_8);
                                        message.setRawMessage(profilePic_bytes);
                                        sendChannelMessage(message);

                                        message.setText(splited[0] + " " + getString(R.string.live_message_gift_send));

                                        MessageBean messageBean = new MessageBean(mUserId, message, true, profilePic);
                                        messageBean.setBackground(position);
                                        mMessageBeanList.add(messageBean);
                                        mMessageAdapter.notifyItemRangeChanged(mMessageBeanList.size(), 1);
                                        rvMessageList.scrollToPosition(mMessageBeanList.size() - 1);

                                        int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                                        int updatedCoins = currentCoins + -Integer.parseInt(coin);
                                        User user = sessionManager.getUser();
                                        user.getData().setPaidCoins("" + updatedCoins);
                                        sessionManager.saveUser(user);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    hitGetLiveDetailApi();
                                } else {
                                    final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LiveCallActivity.this,
                                            R.style.CustomBottomSheetDialogTheme);
                                    bottomSheetDialog.setContentView(R.layout.dialog_message);
                                    bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
                                    bottomSheetDialog.show();

                                    TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

                                    tvMessage.setText(response.getString("message"));

                                    bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> bottomSheetDialog.dismiss());
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();


/*
        disposable.add(Global.initRetrofit().sendCoinNew(Global.ACCESS_TOKEN, liveId, join_id, coin, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> onLoadMoreComplete.setValue(true))
                .subscribe((coinSend, throwable) -> {
                    if (coinSend != null && coinSend.getStatus() != null) {

                        try {

                            int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                            int updatedCoins = currentCoins + -Integer.parseInt(coin);
                            User user = sessionManager.getUser();
                            user.getData().setPaidCoins("" + updatedCoins);
                            sessionManager.saveUser(user);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    hitGteLiveDtailApi();

                }));
*/
    }

    public void updateGiftCoinsAccount(String coinSent) {
        PrefHandler pref = new PrefHandler(this);

        List<Pair<String, String>> params = new ArrayList<>();

        params.add(new Pair<>("coinSent", coinSent));
        params.add(new Pair<>("senderId", pref.getuId()));
        params.add(new Pair<>("partyId", Utils.party_id));

        new AsyncHttpsRequest("", this, params, this, 1, false).
                execute(Utils.UPDATE_COINS);
    }

    private void doAlert() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("You don't have enough gold coins to send this gift.");
        builder.setPositiveButton("Add Coins", (dialog, which) -> {
            dialog.dismiss();


            getCoinPlan();
           /* CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
            fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());*/
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {

        if (llMessage.getVisibility() == View.VISIBLE) {
            AppUtils.hideSoftKeyboard(this);
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            llMessage.setVisibility(View.GONE);
            etMessage.clearFocus();
            if (isBroadcaster) {
                llHostBottom.setVisibility(View.VISIBLE);
            } else {
                llBottomAudience.setVisibility(View.VISIBLE);
            }


        } else {
            if (isBroadcaster)
                showLiveEndDialog();
            else
                doLogout();
        }
    }

    private void showPurchaseResultToast(Boolean status) {


        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModel.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                //((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                onBackPressed();
            }
        }
        return super.dispatchTouchEvent(ev);
    }


}