package com.fairtok.view.liveCall;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.openduo.activities.CallActivity;
import com.fairtok.openduo.agora.Config;
import com.fairtok.openduo.agora.EngineEventListener;
import com.fairtok.openduo.agora.IEventListener;
import com.fairtok.openlive.activities.BaseActivity;
import com.fairtok.openlive.activities.RtcBaseActivity;
import com.fairtok.openlive.bottomsheet.GiftAnimWindow;
import com.fairtok.openlive.chatting.adapter.MessageAdapter;
import com.fairtok.openlive.chatting.rtmtutorial.ChatManager;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.openlive.chatting.utils.MessageUtil;
import com.fairtok.openlive.stats.LocalStatsData;
import com.fairtok.openlive.stats.RemoteStatsData;
import com.fairtok.openlive.stats.StatsData;
import com.fairtok.openlive.ui.VideoGridContainer;
import com.fairtok.openlive.utils.GiftUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoEncoderConfiguration;
import io.agora.rtm.ErrorInfo;
import io.agora.rtm.LocalInvitation;
import io.agora.rtm.RemoteInvitation;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmCallEventListener;
import io.agora.rtm.RtmCallManager;
import io.agora.rtm.RtmChannel;
import io.agora.rtm.RtmChannelAttribute;
import io.agora.rtm.RtmChannelListener;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmClient;
import io.agora.rtm.RtmClientListener;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.agora.rtm.RtmMediaOperationProgress;
import io.agora.rtm.RtmMessage;
import io.agora.rtm.RtmStatusCode;
import io.reactivex.disposables.CompositeDisposable;

public class LiveCallActivityNew extends RtcBaseActivity implements View.OnClickListener, CompleteTaskListner,
        RtmClientListener, RtmCallEventListener , IEventListener {

    SessionManager sessionManager;

    private VideoGridContainer videoGrid;

    private VideoEncoderConfiguration.VideoDimensions mVideoDimension;

    String liveId = "";


    private final int mChannelMemberCount = 1;
    private final int currentWalletBalance = 0;

    private ChatManager mChatManager;
    private RtmClient mRtmClient;
    private RtmClientListener mClientListener;
    private RtmChannel mRtmChannel;
    private EngineEventListener mEventListener;

    PrefHandler prefHandler;

    boolean isBroadcaster = true, isActivityStop = false;

    String mChannelName = "";

    private final CompositeDisposable disposable = new CompositeDisposable();
    public ObservableBoolean isLoading = new ObservableBoolean(false);

    RtmCallManager mRtmCallManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        config().setChannelName(getIntent().getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_call_new);
        prefHandler = new PrefHandler(this);
        init();
    }

    private void init() {

        setUpForCall();

        sessionManager = new SessionManager(this);

        videoGrid = findViewById(R.id.videoGrid);
        videoGrid.setStatsManager(statsManager());

        liveId = getIntent().getStringExtra("liveId");

        if (getIntent().getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME).matches("@" + sessionManager.getUser().getData().getUserName())) {

            isBroadcaster = true;

            becomesHost(false, false);

        }
        //Audience
        else {

            isBroadcaster = false;

          //  config().setChannelName(getIntent().getStringExtra("targetName"));

            becomeAudience();
        }

        initChat();
        mVideoDimension = com.fairtok.openlive.Constants.VIDEO_DIMENSIONS[config().getVideoDimenIndex()];

    }

    private void setUpForCall() {

        mEventListener = new EngineEventListener();
        try {
            RtmClient mRtmClientLocal = RtmClient.createInstance(getApplicationContext(), getString(R.string.private_app_id), mEventListener);

            mRtmCallManager = mRtmClientLocal.getRtmCallManager();
            mRtmCallManager.setEventListener(mEventListener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ivClose:

                onBackPressed();

                break;
        }
    }

    private void becomesHost(boolean audioMuted, boolean videoMuted) {

        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    private void becomeAudience() {
       /* isBroadcaster = false;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        rtcEngine().muteLocalAudioStream(true);
        rtcEngine().muteLocalVideoStream(true);*/
    }

    private void startBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        SurfaceView surface = prepareRtcVideo(0, true);
        videoGrid.addUserVideoSurface(0, surface, true);
        /*ivMuteAudio.setActivated(true);*/
    }

    private void stopBroadcast() {
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_AUDIENCE);
        removeRtcVideo(0, true);
        videoGrid.removeUserVideo(0, true);
        /*ivMuteAudio.setActivated(false);*/
    }

    private void initChat() {

        mChatManager = MyApplication.the().getChatManager();
        mRtmClient = mChatManager.getRtmClient();
        mClientListener = new MyRtmClientListener();
        mChatManager.registerListener(mClientListener);

        /*Intent intent = getIntent();
        mIsPeerToPeerMode = intent.getBooleanExtra(MessageUtil.INTENT_EXTRA_IS_PEER_MODE, false);
        mUserId = intent.getStringExtra(MessageUtil.INTENT_EXTRA_USER_ID);
        String targetName = intent.getStringExtra(MessageUtil.INTENT_EXTRA_TARGET_NAME);

        if (mIsPeerToPeerMode) {
            mPeerId = targetName;

            // load history chat records
            MessageListBean messageListBean = MessageUtil.getExistMessageListBean(mPeerId);
            if (messageListBean != null) {
                mMessageBeanList.addAll(messageListBean.getMessageBeanList());
            }

            // load offline messages since last chat with this peer.
            // Then clear cached offline messages from message pool
            // since they are already consumed.
            MessageListBean offlineMessageBean = new MessageListBean(mPeerId, (List<MessageBean>) mChatManager);
            mMessageBeanList.addAll(offlineMessageBean.getMessageBeanList());
            mChatManager.removeAllOfflineMessages(mPeerId);
        } else {
            mChannelName = targetName;
            mChannelMemberCount = 1;
            createAndJoinChannel();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        mMessageAdapter = new MessageAdapter(this, mMessageBeanList, message -> {
            if (message.getMessage().getMessageType() == RtmMessageType.IMAGE) {
                if (!TextUtils.isEmpty(message.getCacheFile())) {
                   *//* Glide.with(this).load(message.getCacheFile()).into(ivBigImage);
                    ivBigImage.setVisibility(View.VISIBLE);*//*
                } else {
                    ImageUtil.cacheImage(this, mRtmClient, (RtmImageMessage) message.getMessage(), new ResultCallback<String>() {
                        @Override
                        public void onSuccess(String file) {
                            message.setCacheFile(file);
                            runOnUiThread(() -> {
                                *//*Glide.with(LiveActivityNew.this).load(file).into(ivBigImage);
                                ivBigImage.setVisibility(View.VISIBLE);*//*
                            });
                        }

                        @Override
                        public void onFailure(ErrorInfo errorInfo) {

                        }
                    });
                }
            }
        });
        rvMessageList.setAdapter(mMessageAdapter);*/

    }

    @Override
    public void onJoinChannelSuccess(String channel, int uid, int elapsed) {
        // Do nothing at the moment
    }

    @Override
    public void onUserJoined(int uid, int elapsed) {
        // Do nothing at the moment

        // showToast("New User Joined");
    }

    @Override
    public void onUserOffline(final int uid, int reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                removeRemoteUser(uid);
            }
        });
    }

    @Override
    public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.v("jgg", ";lkkhn");
                //renderRemoteUser(uid);
            }
        });
    }

    private void renderRemoteUser(int uid) {
        SurfaceView surface = prepareRtcVideo(uid, false);
        videoGrid.addUserVideoSurface(uid, surface, false);
    }

    private void removeRemoteUser(int uid) {
        removeRtcVideo(uid, false);
        videoGrid.removeUserVideo(uid, false);
    }

    @Override
    protected void onGlobalLayoutCompleted() {
/*
        RelativeLayout topLayout = findViewById(R.id.rlTop);
        RelativeLayout.LayoutParams params =
                (RelativeLayout.LayoutParams) topLayout.getLayoutParams();
        params.height = mStatusBarHeight + topLayout.getMeasuredHeight();
        topLayout.setLayoutParams(params);
        topLayout.setPadding(0, mStatusBarHeight, 0, 0);
*/
    }

    @Override
    public void onLocalVideoStats(IRtcEngineEventHandler.LocalVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setWidth(mVideoDimension.width);
        data.setHeight(mVideoDimension.height);
        data.setFramerate(stats.sentFrameRate);
    }

    @Override
    public void onRtcStats(IRtcEngineEventHandler.RtcStats stats) {
        if (!statsManager().isEnabled()) return;

        LocalStatsData data = (LocalStatsData) statsManager().getStatsData(0);
        if (data == null) return;

        data.setLastMileDelay(stats.lastmileDelay);
        data.setVideoSendBitrate(stats.txVideoKBitRate);
        data.setVideoRecvBitrate(stats.rxVideoKBitRate);
        data.setAudioSendBitrate(stats.txAudioKBitRate);
        data.setAudioRecvBitrate(stats.rxAudioKBitRate);
        data.setCpuApp(stats.cpuAppUsage);
        data.setCpuTotal(stats.cpuAppUsage);
        data.setSendLoss(stats.txPacketLossRate);
        data.setRecvLoss(stats.rxPacketLossRate);
    }

    @Override
    public void onNetworkQuality(int uid, int txQuality, int rxQuality) {
        if (!statsManager().isEnabled()) return;

        StatsData data = statsManager().getStatsData(uid);
        if (data == null) return;

        data.setSendQuality(statsManager().qualityToString(txQuality));
        data.setRecvQuality(statsManager().qualityToString(rxQuality));
    }

    @Override
    public void onRemoteVideoStats(IRtcEngineEventHandler.RemoteVideoStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setWidth(stats.width);
        data.setHeight(stats.height);
        data.setFramerate(stats.rendererOutputFrameRate);
        data.setVideoDelay(stats.delay);
    }

    @Override
    public void onRemoteAudioStats(IRtcEngineEventHandler.RemoteAudioStats stats) {
        if (!statsManager().isEnabled()) return;

        RemoteStatsData data = (RemoteStatsData) statsManager().getStatsData(stats.uid);
        if (data == null) return;

        data.setAudioNetDelay(stats.networkTransportDelay);
        data.setAudioNetJitter(stats.jitterBufferDelay);
        data.setAudioLoss(stats.audioLossRate);
        data.setAudioQuality(statsManager().qualityToString(stats.quality));
    }

    @Override
    public void finish() {
        super.finish();
        Log.v("lihh", "finish");
        statsManager().clearAllData();
    }

    public void onLeaveClicked(View view) {
        onBackPressed();
    }

    public void onSwitchCameraClicked(View view) {
        rtcEngine().switchCamera();
    }

    public void onBeautyClicked(View view) {
        view.setActivated(!view.isActivated());
        rtcEngine().setBeautyEffectOptions(view.isActivated(),
                com.fairtok.openlive.Constants.DEFAULT_BEAUTY_OPTIONS);
    }

    public void onMoreClicked(View view) {
    }

    public void onPushStreamClicked(View view) {
    }

    public void onMuteAudioClicked(View view) {
        /*  if (!ivMuteVideo.isActivated()) return;*/

        rtcEngine().muteLocalAudioStream(view.isActivated());
        view.setActivated(!view.isActivated());
    }

    public void onMuteVideoClicked(View view) {
        if (view.isActivated()) {
            stopBroadcast();
        } else {
            startBroadcast();
            //startVideo();
        }
        view.setActivated(!view.isActivated());
        rtcEngine().muteLocalVideoStream(view.isActivated());
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response = " + result);

        if (response_code == 1111) {
            try {
                JSONObject obj = new JSONObject(result);
                if (obj.getInt("success") == 1) {
                    Utils.party_id = obj.getString("id");
                } else {
                    Utils.party_id = "0";
                }

                Log.v("paras", "paras Utils.party_id in response = " + Utils.party_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (response_code == 222) {

            if (!isActivityStop)
                finish();

        } else if (response_code == 10) {
/*
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {

                    arrayList = new ArrayList<>();
                    JSONObject data_obj = obj.getJSONObject("post_data");
                    JSONArray jarr = data_obj.getJSONArray("data_arr");

                    for (int i = 0; i < jarr.length(); i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatListPOJO bean = new ChatListPOJO();
                        bean.setOpp_userid(jobj.getString("user_id"));
                        bean.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        bean.setReceiver_photo(jobj.getString("receiver_photo"));
                        bean.setLast_msg(jobj.getString("CoinsSent"));

                        arrayList.add(bean);
                    }
                    Log.v("memberList2", result);
                    mChannelMemberCount = arrayList.size();

                    MyPerformanceArrayAdapter itemsAdapter = new MyPerformanceArrayAdapter(LiveCallActivityNew.this, arrayList, showInvite);
                    BottomSheetDialog dialog = new BottomSheetDialog(LiveCallActivityNew.this);
                    dialog.setContentView(R.layout.bottom_sheet_view);
                    BottomSheetListView listView = dialog.findViewById(R.id.listViewBtmSheet);
                    listView.setAdapter(itemsAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
                        }
                    });
                    dialog.show();

                } else {
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
*/
        }
    }

    /**
     * API CALL: create and join channel
     */
    private void createAndJoinChannel() {
        // step 1: create a channel instance
        mRtmChannel = mRtmClient.createChannel(mChannelName, new MyChannelListener());
        if (mRtmChannel == null) {
            showToast(getString(R.string.join_channel_failed));
            finish();
            return;
        }

        Log.e("channel", mRtmChannel + "");

        // step 2: join the channel
        mRtmChannel.join(new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void responseInfo) {
                Log.i("TAG", "join channel success");

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                Log.e("TAG", "join channel failed1");
                Log.e("TAG", errorInfo.getErrorDescription());
                Log.e("TAG", String.valueOf(errorInfo.getErrorCode()));
                Log.e("TAG", errorInfo.toString());
                runOnUiThread(() -> {
                    showToast(getString(R.string.join_channel_failed));
                    finish();
                });
            }
        });
    }

    @Override
    public void onLocalInvitationReceivedByPeer(LocalInvitation localInvitation) {

        Log.v("lklnhk", "1");
    }

    @Override
    public void onLocalInvitationAccepted(LocalInvitation localInvitation, String s) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onLocalInvitationRefused(LocalInvitation localInvitation, String s) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onLocalInvitationCanceled(LocalInvitation localInvitation) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onLocalInvitationFailure(LocalInvitation localInvitation, int i) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onRemoteInvitationReceived(RemoteInvitation remoteInvitation) {

        Log.v("lkkhlhk", "1");
        global().setRemoteInvitation(remoteInvitation);

        String[] parts = remoteInvitation.getContent().split("-");
        String callerName = parts[0];
        String callerImg = parts[1];

        String calleeName = parts[2];
        String calleeImg = parts[3];
        String type = "1";

        gotoCallingActivity(remoteInvitation.getChannelId(), remoteInvitation.getCallerId(),
                com.fairtok.openduo.Constants.ROLE_CALLEE, callerName, callerImg, calleeName, calleeImg, type);
    }

    @Override
    public void onRemoteInvitationAccepted(RemoteInvitation remoteInvitation) {
        Log.v("lkkhlhk", "@");
    }

    @Override
    public void onRemoteInvitationRefused(RemoteInvitation remoteInvitation) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onRemoteInvitationCanceled(RemoteInvitation remoteInvitation) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onRemoteInvitationFailure(RemoteInvitation remoteInvitation, int i) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onConnectionStateChanged(int i, int i1) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onMessageReceived(RtmMessage rtmMessage, String s) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onImageMessageReceivedFromPeer(RtmImageMessage rtmImageMessage, String s) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onTokenExpired() {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onPeersOnlineStatusChanged(Map<String, Integer> map) {
        Log.v("lklnhk", "1");
    }

    @Override
    public void onLocalInvitationReceived(LocalInvitation localInvitation) {

    }

    protected void gotoCallingActivity(String channel, String peer, int role, String callerName, String callerImg, String calleeName, String calleeImg, String type) {


        Intent intent = new Intent(this, CallActivity.class);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLING_CHANNEL, channel);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLING_PEER, peer);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLING_ROLE, role);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLEE_NAME, calleeName);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLEE_IMG, calleeImg);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLER_NAME, callerName);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALLER_IMG, callerImg);
        intent.putExtra(com.fairtok.openduo.Constants.KEY_CALL_TYPE, type);
        startActivity(intent);
        finish();

    }

    /**
     * API CALLBACK: rtm event listener
     */
    class MyRtmClientListener implements RtmClientListener {

        @Override
        public void onConnectionStateChanged(final int state, int reason) {
            runOnUiThread(() -> {
                switch (state) {
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_RECONNECTING:
                        showToast(getString(R.string.reconnecting));
                        break;
                    case RtmStatusCode.ConnectionState.CONNECTION_STATE_ABORTED:
                        Log.v("dwdq","1");
                        showToast(getString(R.string.account_offline));
                        setResult(MessageUtil.ACTIVITY_RESULT_CONN_ABORTED);
                        finish();
                        break;
                }
            });
        }

        @Override
        public void onMessageReceived(final RtmMessage message, final String peerId) {
            runOnUiThread(() -> {


            });
        }

        @Override
        public void onImageMessageReceivedFromPeer(final RtmImageMessage rtmImageMessage, final String peerId) {


        }

        @Override
        public void onFileMessageReceivedFromPeer(RtmFileMessage rtmFileMessage, String s) {

        }

        @Override
        public void onMediaUploadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onMediaDownloadingProgress(RtmMediaOperationProgress rtmMediaOperationProgress, long l) {

        }

        @Override
        public void onTokenExpired() {

        }

        @Override
        public void onPeersOnlineStatusChanged(Map<String, Integer> map) {

        }
    }

    /**
     * API CALLBACK: rtm channel event listener
     */
    class MyChannelListener implements RtmChannelListener {
        @Override
        public void onMemberCountUpdated(int i) {

        }

        @Override
        public void onAttributesUpdated(List<RtmChannelAttribute> list) {

        }

        @Override
        public void onMessageReceived(final RtmMessage message, final RtmChannelMember fromMember) {
            runOnUiThread(() -> {


            });
        }

        @Override
        public void onImageMessageReceived(final RtmImageMessage rtmImageMessage, final RtmChannelMember rtmChannelMember) {
            runOnUiThread(() -> {

            });
        }

        @Override
        public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

        }

        @Override
        public void onMemberJoined(RtmChannelMember member) {
            runOnUiThread(() -> {


                //showToast(member.getUserId() + " has joined the party");
            });
        }

        @Override
        public void onMemberLeft(RtmChannelMember member) {
            runOnUiThread(() -> {

            });
        }
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(LiveCallActivityNew.this, text, Toast.LENGTH_SHORT).show());
    }


    public void animateGiftGif(int giftId) {

        GiftAnimWindow window = new GiftAnimWindow(LiveCallActivityNew.this, R.style.gift_anim_window);
        window.setAnimResource(GiftUtil.getGiftAnimRes(giftId));
        window.show();
    }

    private void doInvite() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveCallActivityNew.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(config().getChannelName() + " has invited you to co-host the party.");
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                becomesCoHost(false, false);
            }
        });
        builder.setNegativeButton("Reject", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                rejectCoHost();
            }
        });
        builder.show();
    }

    private void becomesCoHost(boolean audioMuted, boolean videoMuted) {

//        isBroadcaster = true;
        rtcEngine().setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        rtcEngine().muteLocalAudioStream(audioMuted);
        rtcEngine().muteLocalVideoStream(videoMuted);
        startBroadcast();
    }

    public void rejectCoHost() {
        RtmMessage message = mRtmClient.createMessage();
        message.setText("co-host-rejected");
    }

    /**
     * API CALL: send message to peer
     */

    @Override
    protected void onResume() {
        super.onResume();
        isActivityStop = false;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.v("lihh", "Destroy");


        removeChannelAndEvent();

    }

    private void removeChannelAndEvent() {

        if (isBroadcaster) {
            leaveAndReleaseChannel();
            mChatManager.unregisterListener(mClientListener);

        }

        removeEventListener(this);
    }

    private void makeHostOfflineOnServerNew() {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = Const.BASE_URL + Const.STOP_LIVE_CALL;
            Log.v("postApi-url", url);
            Log.v("postApi-url", liveId);
            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("liveId", liveId)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("status")) {

                                    if (!isActivityStop) {
                                        finish();
                                    }
                                }
                                Log.v("postApi-resp", String.valueOf(response));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();
    }

    /**
     * API CALL: leave and release channel
     */
    private void leaveAndReleaseChannel() {
        if (mRtmChannel != null) {
            mRtmChannel.leave(null);
            mRtmChannel.release();
            mRtmChannel = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.v("lihh", "stop");
        isActivityStop = true;
        if (isBroadcaster) {
            // makeHostOfflineOnServer();
            makeHostOfflineOnServerNew();
        } else {
            //makeAudienceOfflineOnServer();

        }
    }


    public void makeHostOfflineOnServer() {
        try {
            PrefHandler pref = new PrefHandler(this);

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "makemeoffline"));
            params.add(new Pair<>("userid", Utils.party_id));

            new AsyncHttpsRequest("", this, params, this, 222, false).execute(Utils.MakeHostOffline_URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();

                final String file = resultUri.getPath();
                ImageUtil.uploadImage(this, mRtmClient, file, new ResultCallback<RtmImageMessage>() {
                    @Override
                    public void onSuccess(final RtmImageMessage rtmImageMessage) {
                        runOnUiThread(() -> {

                            sendChannelMessage(rtmImageMessage);

                        });
                    }

                    @Override
                    public void onFailure(ErrorInfo errorInfo) {

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                result.getError().printStackTrace();
            }
        } else if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                showToast("Transaction Canceled");
            }
        }
    }


    /**
     * API CALL: send message to a channel
     */
    private void sendChannelMessage(RtmMessage message) {
        mRtmChannel.sendMessage(message, new ResultCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }

            @Override
            public void onFailure(ErrorInfo errorInfo) {
                // refer to RtmStatusCode.ChannelMessageState for the message state
                final int errorCode = errorInfo.getErrorCode();
                runOnUiThread(() -> {
                    switch (errorCode) {
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_TIMEOUT:
                        case RtmStatusCode.ChannelMessageError.CHANNEL_MESSAGE_ERR_FAILURE:
                            showToast(getString(R.string.send_msg_failed));
                            break;
                    }
                });
            }
        });
    }


    private void doLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveCallActivityNew.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Are you sure you want to exit from the party?.");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                isActivityStop = false;

                if (isBroadcaster) {
                    //makeHostOfflineOnServer();
                    makeHostOfflineOnServerNew();
                    finish();

                } else {
                    //makeAudienceOfflineOnServer();
                }
            }
        });
        builder.setNegativeButton("No, Stay here", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public void onBackPressed() {

        doLogout();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int[] scrcoords = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()) {
                //((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                onBackPressed();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerEventListener(this);
    }


    private void registerEventListener(IEventListener listener) {
        application().registerEventListener(listener);
    }

    private void removeEventListener(IEventListener listener) {
        application().removeEventListener(listener);
    }

    public MyApplication application() {
        return (MyApplication) getApplication();
    }

    protected RtcEngine rtcEngine() {
        return application().rtcEngine();
    }

    protected RtmClient rtmClient() {
        return application().rtmClient();
    }

    protected RtmCallManager rtmCallManager() {
        return application().rtmCallManager();
    }


    protected com.fairtok.openduo.agora.Global global() {
        return application().global();
    }


}