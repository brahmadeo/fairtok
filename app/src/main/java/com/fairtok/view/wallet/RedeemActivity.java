package com.fairtok.view.wallet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.ActivityRedeemBinding;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Global;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.web.WebViewActivity;
import com.fairtok.viewmodel.RedeemViewModel;
import com.fairtok.viewmodel.WalletViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class RedeemActivity extends BaseActivity {

    ActivityRedeemBinding binding;
    RedeemViewModel viewModel;
    PrefHandler prefHandler;

    WalletViewModel viewWalletModel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_redeem);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new RedeemViewModel()).createFor()).get(RedeemViewModel.class);
        viewWalletModel = ViewModelProviders.of(this, new ViewModelFactory(new WalletViewModel()).createFor()).get(WalletViewModel.class);

        prefHandler = new PrefHandler(this);

        initView();
        initListeners();
        initObserve();
        binding.setViewmodel(viewModel);
    }

    private void initView() {
       /* if (getIntent().getStringExtra("coins") != null) {
            viewModel.coindCount = getIntent().getStringExtra("coins");
            viewModel.coinRate = getIntent().getStringExtra("coinrate");
            viewModel.silverOrGold = getIntent().getStringExtra("silverOrGold");
            double dimonds = Double.parseDouble(viewModel.coindCount) * Double.parseDouble(viewModel.coinRate);
            viewModel.dimonds = ""+dimonds;
            binding.tvCount.setText(Global.prettyCount(dimonds));

            viewModel.accountId = prefHandler.getName()+" | "+prefHandler.getEmail() +" | "+prefHandler.getMobile();
            binding.edAccount.setText(viewModel.accountId);
        }*/

       /* viewModel.coindCount = getIntent().getStringExtra("coins");
        viewModel.coinRate = getIntent().getStringExtra("coinrate");
        viewModel.silverOrGold = getIntent().getStringExtra("silverOrGold");*/

        viewWalletModel.fetchMyWallet();

        //viewModel.accountId = prefHandler.getName()+" | "+prefHandler.getEmail() +" | "+prefHandler.getMobile();
        viewModel.accountId = prefHandler.getName()+" | "+prefHandler.getEmail() ;
        binding.edAccount.setText(viewModel.accountId);
    }


    private void initListeners() {
        binding.tvTerm.setOnClickListener(v -> startActivity(new Intent(this, WebViewActivity.class)));
        String[] paymentTypes = getResources().getStringArray(R.array.payment);
        binding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_text_light));
                // ((TextView) parent.getChildAt(0)).setTextSize(5);
                viewModel.requestType = paymentTypes[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        binding.btnRedeem.setOnClickListener(v -> {
            if (viewModel.accountId != null && !TextUtils.isEmpty(viewModel.accountId)) {
                viewModel.callApiToRedeem(RedeemActivity.this);
            } else {
                Toast.makeText(this, "Please enter your account ID", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initObserve() {
        viewModel.redeem.observe(this, redeem -> {
            if (redeem.getStatus()) {
                Toast.makeText(this, "Request Submitted Successfully", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, MainActivity.class));
                finishAffinity();
            }
        });

        viewWalletModel.myWallet.observe(this, wallet -> {
            binding.tvCount.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getDiamondCoins())));

            double dimonds = AppUtils.returnStringToDouble(wallet.getData().getDiamondCoins());
            viewModel.dimonds = ""+dimonds;
            binding.tvCount.setText(Global.prettyCount(dimonds));
        });
    }


    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}