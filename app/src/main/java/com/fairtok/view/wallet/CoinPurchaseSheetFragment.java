package com.fairtok.view.wallet;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.databinding.FragmentPurchaseCoinSheetBinding;
import com.fairtok.model.user.User;
import com.fairtok.utils.SessionManager;
import com.fairtok.utils.TrakConstant;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;


public class CoinPurchaseSheetFragment extends BottomSheetDialogFragment implements BillingProcessor.IBillingHandler {

    FragmentPurchaseCoinSheetBinding binding;
    CoinPurchaseViewModel viewModel;
    BillingProcessor bp;

    private String orderID = "";
    private String customerID = "";
    private String PayAmount = "0.0";
    SessionManager sessionManager;
    PrefHandler prefHandler;

    String FPaymentId,FTransactionId,FPaymentMode,FTransactionStatus,FAmount,FUserName, FEmail,FPhone,FMessage;
    JSONObject TrakNPayResponse;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        bottomSheetDialog.setOnShowListener(dialog1 -> {
            BottomSheetDialog dialog = (BottomSheetDialog) dialog1;
            dialog.setCanceledOnTouchOutside(true);
        });

        return bottomSheetDialog;

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchase_coin_sheet, container, false);
        sessionManager = new SessionManager(getActivity());
        prefHandler = new PrefHandler(getActivity());
        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);

        initView();
        initListeners();
        initObserve();
        binding.setViewmodel(viewModel);
    }

    private void initView() {
        viewModel.fetchCoinPlans();
        bp = new BillingProcessor(getActivity(), getActivity().getResources().getString(R.string.play_lic_key), this);
        bp.initialize();
    }


    private void initListeners() {
        viewModel.adapter.onRecyclerViewItemClick = (data, position) -> {
            viewModel.coins = data.getCoinAmount();
            viewModel.coinsAmount=data.getCoinPlanPrice();

            Log.v("lkhlkh", viewModel.coins);
            Log.v("lkhlkh", viewModel.coinsAmount);

            Log.v("paras","paras product id = "+data.getPlaystoreProductId());
//            bp.purchase(getActivity(), data.getPlaystoreProductId());

            initializePayment(viewModel.coinsAmount);
        };
    }

    private void initObserve() {
        viewModel.purchase.observe(getViewLifecycleOwner(), purchase -> showPurchaseResultToast(purchase.getStatus()));
    }

    private void showPurchaseResultToast(Boolean status) {

        dismiss();
        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModel.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);

            }
            catch (NumberFormatException e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(getContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());

    }


    @Override
    public void onProductPurchased(String productId, TransactionDetails details) {
        bp.consumePurchase(productId);
        viewModel.purchaseCoin(details.orderId,"Google InApp Purchase",viewModel.coinsAmount,prefHandler.getName(),prefHandler.getuId(),
                "0", FTransactionStatus);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, Throwable error) {
        Toast.makeText(getActivity(), "Something went wrong..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBillingInitialized() {
        Log.d("BILLING", "onBillingInitialized: " + "Initialized successfully");
    }

    @Override
    public void onDestroy() {
        if (bp != null) {
            bp.release();
        }
        super.onDestroy();
    }

    public void initializePayment(String PayAmount){

        customerID = sessionManager.getUser().getData().getUserId();
        orderID = "OrderID" + System.currentTimeMillis()+"-"+customerID;

        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        TrakConstant.PG_ORDER_ID=Integer.toString(n);
        PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey(TrakConstant.PG_API_KEY);
        pgPaymentParams.setAmount(PayAmount);
        pgPaymentParams.setEmail(sessionManager.getUser().getData().getUserEmail());
        pgPaymentParams.setName(sessionManager.getUser().getData().getFullName());
        pgPaymentParams.setPhone(sessionManager.getUser().getData().getUserMobileNo());
        pgPaymentParams.setOrderId(orderID);
        pgPaymentParams.setCurrency(TrakConstant.PG_CURRENCY);
        pgPaymentParams.setDescription(TrakConstant.PG_DESCRIPTION);
        pgPaymentParams.setCity("NA");
        pgPaymentParams.setState(prefHandler.getLanguage());
        pgPaymentParams.setAddressLine1(TrakConstant.PG_ADD_1);
        pgPaymentParams.setAddressLine2(TrakConstant.PG_ADD_2);
        pgPaymentParams.setZipCode("560094");
        pgPaymentParams.setCountry(TrakConstant.PG_COUNTRY);
        pgPaymentParams.setReturnUrl(TrakConstant.PG_RETURN_URL);
        pgPaymentParams.setMode(TrakConstant.PG_MODE);
        pgPaymentParams.setUdf1(TrakConstant.PG_UDF1);
        pgPaymentParams.setUdf2(TrakConstant.PG_UDF2);
        pgPaymentParams.setUdf3(TrakConstant.PG_UDF3);
        pgPaymentParams.setUdf4(TrakConstant.PG_UDF4);
        pgPaymentParams.setUdf5(TrakConstant.PG_UDF5);

        PaymentGatewayPaymentInitializer pgPaymentInitialzer =
                new PaymentGatewayPaymentInitializer(pgPaymentParams,getActivity());
        pgPaymentInitialzer.initiatePaymentProcess();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);
                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                        ShowToast(getActivity(), "Transaction Failed");
                        //transactionIdView.setText("Transaction ID: NIL");
                        //transactionStatusView.setText("Transaction Status: Transaction Error!");
                    } else {
                        JSONObject response = new JSONObject(paymentResponse);
                        TrakNPayResponse = response;

                        FPaymentId = response.getString("transaction_id");
                        FTransactionId = response.getString("transaction_id");
                        FPaymentMode = response.getString("payment_mode");
                        FTransactionStatus = response.getString("response_code");
                        FAmount = response.getString("amount");
                        FMessage = response.getString("response_message");

                        Log.e("TrakNPay", "onActivityResult: " + response);

                        if (response.getString("response_code").equals("0"))
                        {
                            viewModel.purchaseCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),
                                    FMessage, "0", FTransactionStatus);
                        }


                        //transactionIdView.setText("Transaction ID: "+response.getString("transaction_id"));
                        // transactionStatusView.setText("Transaction Status: "+response.getString("response_message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                ShowToast(getActivity(), "Transaction Canceled");
            }
        }
    }

    public void ShowToast(Context context, String msg)
    {
        Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);

    }
}