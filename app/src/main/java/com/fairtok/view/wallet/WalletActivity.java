package com.fairtok.view.wallet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Observable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.bean.ChatMessage;
import com.fairtok.databinding.ActivityWalletBinding;
import com.fairtok.databinding.CustomToastBinding;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.openlive.chatting.model.MessageBean;
import com.fairtok.openlive.chatting.utils.ImageUtil;
import com.fairtok.utils.AppUtils;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.TrakConstant;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.viewmodel.CoinPurchaseViewModel;
import com.fairtok.viewmodel.WalletViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.googlecode.mp4parser.authoring.Edit;
import com.test.pg.secure.pgsdkv4.PGConstants;
import com.test.pg.secure.pgsdkv4.PaymentGatewayPaymentInitializer;
import com.test.pg.secure.pgsdkv4.PaymentParams;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import io.agora.rtm.ErrorInfo;
import io.agora.rtm.ResultCallback;
import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class WalletActivity extends BaseActivity {

    ActivityWalletBinding binding;
    WalletViewModel viewModel;
    CoinPurchaseViewModel viewModelCoinPurchase;

    TextView tvDiamonds;

    PrefHandler prefHandler;

    JSONObject TrakNPayResponse;

    String FPaymentId, FTransactionId, FPaymentMode, FTransactionStatus, FAmount, FUserName, FEmail, FPhone, FMessage, coinPanId,
            coins="0";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefHandler = new PrefHandler(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wallet);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new WalletViewModel()).createFor()).get(WalletViewModel.class);

        viewModelCoinPurchase = ViewModelProviders.of(this, new ViewModelFactory(new CoinPurchaseViewModel()).createFor()).get(CoinPurchaseViewModel.class);
        viewModelCoinPurchase.purchase.observe(this, purchase -> showPurchaseResultToast(purchase.getStatus()));

    }

    @Override
    protected void onResume() {
        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("com.fairtok.wallet"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        initView();
        initListeners();
        initObserve();
        super.onResume();
    }

    @Override
    public void onStop() {
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equalsIgnoreCase("com.fairtok.wallet")) {
                    initView();
                    initListeners();
                    initObserve();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    private void initView() {

        viewModel.fetchMyWallet();
        viewModel.fetchCoinRate();
        viewModel.fetchRewardingActions();

        tvDiamonds = findViewById(R.id.tvDiamonds);
    }

    private void initListeners() {

        binding.imgBack.setOnClickListener(v -> onBackPressed());

        //binding.setOnMoreClick(v -> showBuyBottomSheet());
        binding.setOnMoreClick(v -> hitGetCoinPlanApi());

        binding.setOnRedeemClick(v -> {
            if (viewModel.myWallet.getValue() != null) {
                if (Integer.parseInt(viewModel.myWallet.getValue().getData().getMyWallet())
                        > Integer.parseInt(viewModel.coinRate.getValue().getData().getMinSilver())) {

                    showRedeemDialog("1");
                /*Intent intent = new Intent(this, RedeemActivity.class);
                    intent.putExtra("silverOrGold","Silver");
                    intent.putExtra("coins", viewModel.myWallet.getValue().getData().getMyWallet());
                    intent.putExtra("coinrate", viewModel.coinRate.getValue() != null ? viewModel.coinRate.getValue().getData().getUsdRate() : "0");
                    startActivity(intent);*/
                } else {
                    Toast.makeText(this, "Minimum " + viewModel.coinRate.getValue().getData().getMinSilver() + " Silver required to convert & redeem", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.setOnRedeemGoldClick(v -> {
            if (viewModel.myWallet.getValue() != null) {

                Log.v("ljjhqjksh", String.valueOf(Integer.parseInt(viewModel.myWallet.getValue().getData().getPaidCoins())));

                if (Integer.parseInt(viewModel.myWallet.getValue().getData().getPaidCoins()) >
                        Integer.parseInt(viewModel.coinRate.getValue().getData().getMinGold())) {
                    showRedeemDialog("2");
                } else {
                    Toast.makeText(this, "Minimum " + viewModel.coinRate.getValue().getData().getMinGold() + " Gold required to convert & redeem", Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.rlDiamond.setOnClickListener(view -> {
            Intent intent = new Intent(WalletActivity.this, RedeemActivity.class);
            startActivity(intent);
        });

    }

    private void showRedeemDialog(String type) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(WalletActivity.this, R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_redeem);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);
        tvMessage.setText(type.equals("1") ? getString(R.string.silverCoin) : getString(R.string.goldCoin));

        EditText etCoin = bottomSheetDialog.findViewById(R.id.etCoin);

        bottomSheetDialog.findViewById(R.id.btnRedeem).setOnClickListener(view -> {

            if (etCoin.getText().toString().trim().isEmpty()) {


                AppUtils.showToastSort(WalletActivity.this, getString(R.string.pleaseEnterCoin));

            } else if (type.equals("1")) {

                if (viewModel.myWallet.getValue() != null) {
                    if (Integer.parseInt(viewModel.myWallet.getValue().getData().getMyWallet())
                            > Integer.parseInt(viewModel.coinRate.getValue().getData().getMinSilver())) {
                        bottomSheetDialog.dismiss();
                        hitRedeemApi(type, etCoin.getText().toString().trim());
                    }
                    else {
                        showMessageDialog(getString(R.string.notSufficentCoin));
                    }
                }

            } else if (type.equals("2")) {

                if (Integer.parseInt(viewModel.myWallet.getValue().getData().getPaidCoins())  >
                        Integer.parseInt(viewModel.coinRate.getValue().getData().getMinGold())) {
                    bottomSheetDialog.dismiss();
                    hitRedeemApi(type, etCoin.getText().toString().trim());
                }

                else {
                    showMessageDialog(getString(R.string.notSufficentCoin));
                }
            }

        });
    }

    private void hitRedeemApi(String type, String coin) {

        if (AppUtils.isNetworkAvailable(this)) {

            String url = type.equals("1") ? Const.BASE_URL + Const.convert_silver_coins : Const.BASE_URL + Const.convert_gold_coins;
            Log.v("postApi-url", url);

            AndroidNetworking.post(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .addUrlEncodeFormBodyParameter("coins", coin)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            parseRedeem(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {

                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(WalletActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseRedeem(JSONObject response) {

        try {
            showMessageDialog(response.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showMessageDialog(String message) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(WalletActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_message);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            viewModel.fetchMyWallet();
        });
    }

    @SuppressLint("SetTextI18n")
    private void initObserve() {
        viewModel.myWallet.observe(this, wallet -> {

            User user = sessionManager.getUser();
            user.getData().setPaidCoins(wallet.getData().getPaidCoins());
            sessionManager.saveUser(user);

            Log.i("paidcoinsss ", Integer.parseInt(wallet.getData().getPaidCoins())+"");
            binding.tvCount2.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getPaidCoins())));
            binding.tvCount.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getMyWallet())));
            //AddSilverCoin(wallet.getData().getTotalSilver(), wallet.getData().getMyWallet());
            Log.i("paidcoinsss1 ", wallet.getData().getMyWallet());
            binding.tvDailyCheckin.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getCheckIn())));
            binding.tvFromYourFans.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getFromFans())));
            binding.tvPurchased.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getPaidCoins())));
            binding.tvTimeSpent.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getSpenInApp())));
            binding.tvVideoUpload.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getUploadVideo())));
            binding.tvTotalEarning.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getTotalReceived())));
            binding.tvTotalSpending.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getTotalSend())));
            binding.tvDiamonds.setText(Global.prettyCount(Integer.parseInt(wallet.getData().getDiamondCoins())));
        });
        //viewModel.coinRate.observe(this, coinRate -> binding.tvCoinRate.setText("1 Coin = ".concat(coinRate.getData().getUsdRate() + " INR")));

        viewModel.coinRate.observe(this, coinRate -> binding.tvCoinRate.setText(Html.fromHtml(coinRate.getData().getAllRate())));

        viewModel.rewardingActions.observe(this, rewardingActions -> {
            binding.tvEvery10Reward.setText(viewModel.rewardingActionsList.get(0).getCoin());
            binding.tvDailyReward.setText(viewModel.rewardingActionsList.get(1).getCoin());
            binding.tvUploadReward.setText(viewModel.rewardingActionsList.get(2).getCoin());
        });
    }
    private void AddSilverCoin(String totSilver, String refer){
        int total = Integer.parseInt(totSilver) + Integer.parseInt(refer);
        binding.tvCount.setText(Global.prettyCount(total));
        Log.i("silverCoin  ", totSilver+"");
    }


    private void showBuyBottomSheet() {
        CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
        fragment.show(getSupportFragmentManager(), fragment.getClass().getSimpleName());
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    private void hitGetCoinPlanApi() {

        if (AppUtils.isNetworkAvailable(this)) {
            //AppUtils.showRequestDialog(this);

            String url = Const.BASE_URL + Const.coin_plans;
            Log.v("postApi-url", url);

            AndroidNetworking.get(url)
                    .addHeaders("Authorization", Global.ACCESS_TOKEN)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            //AppUtils.hideDialog();
                            parseCoinPlanJson(response);
                            Log.v("postApi-resp", String.valueOf(response));
                        }

                        @Override
                        public void onError(ANError anError) {
                            //AppUtils.hideDialog();
                            Log.v("postApi-resp", String.valueOf(anError.getErrorDetail()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorBody()));
                            Log.v("postApi-resp", String.valueOf(anError.getErrorCode()));
                            Toast.makeText(WalletActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    });

        } else
            Toast.makeText(this, "No internet available", Toast.LENGTH_SHORT).show();

    }

    private void parseCoinPlanJson(JSONObject response) {

        try {
            if (response.getBoolean("status")) {

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

                JSONArray jsonArray = response.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("coin_plan_id", jsonObject.getString("coin_plan_id"));
                    hashMap.put("coin_plan_price", jsonObject.getString("coin_plan_price"));
                    hashMap.put("coin_amount", jsonObject.getString("coin_amount"));

                    arrayList.add(hashMap);
                }

                showCoinBottomDialog(arrayList);

            } else
                AppUtils.showToastSort(getApplicationContext(), response.getString("message"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showCoinBottomDialog(ArrayList<HashMap<String, String>> arrayList) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(WalletActivity.this,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_coins);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet)
                .setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        RecyclerView rvCoins = bottomSheetDialog.findViewById(R.id.rvCoins);
        rvCoins.setLayoutManager(new GridLayoutManager(this, 3));

        rvCoins.setAdapter(new AdapterCoins(arrayList, bottomSheetDialog));

        bottomSheetDialog.findViewById(R.id.ivClose).setOnClickListener(view -> bottomSheetDialog.dismiss());

    }

    private class AdapterCoins extends RecyclerView.Adapter<AdapterCoins.MyViewHolder> {

        ArrayList<HashMap<String, String>> data;

        BottomSheetDialog bottomSheetDialog;

        public AdapterCoins(ArrayList<HashMap<String, String>> arrayList, BottomSheetDialog dialog) {

            data = arrayList;
            bottomSheetDialog = dialog;
        }

        @NonNull
        @Override
        public AdapterCoins.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inflate_coins, viewGroup, false);
            return new AdapterCoins.MyViewHolder(view);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull AdapterCoins.MyViewHolder holder, final int position) {

            //holder.ivImage.setImageResource(GiftUtil.getGiftAnimRes(position));
            // holder.ivImage.setImageDrawable(R.drawable.ic_coin);
            holder.tvCoins.setText(data.get(position).get("coin_amount"));
            holder.tvAmount.setText("Rs. " + data.get(position).get("coin_plan_price"));

            holder.llMain.setTag(R.string.amount, data.get(position).get("coin_plan_price"));
            holder.llMain.setTag(R.string.coins, data.get(position).get("coin_amount"));

            holder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    bottomSheetDialog.dismiss();
                    coinPanId = data.get(position).get("coin_plan_id");

                    coins = (String) holder.llMain.getTag(R.string.coins);

                    viewModelCoinPurchase.coins = coins;
                    viewModelCoinPurchase.coinsAmount = (String) holder.llMain.getTag(R.string.amount);

                    initializePayment((String) holder.llMain.getTag(R.string.amount));

                }
            });
        }

        @Override
        public int getItemCount() {

            return data.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            ImageView ivImage;

            TextView tvCoins, tvAmount;

            LinearLayout llMain;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                ivImage = itemView.findViewById(R.id.ivImage);

                tvCoins = itemView.findViewById(R.id.tvCoins);
                tvAmount = itemView.findViewById(R.id.tvAmount);

                llMain = itemView.findViewById(R.id.llMain);

            }
        }
    }

    public void initializePayment(String PayAmount) {

        Log.d("PayAmount -  ", PayAmount);

        String customerID = sessionManager.getUser().getData().getUserId();
        String orderID = "OrderID" + System.currentTimeMillis() + "-" + customerID;

        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        TrakConstant.PG_ORDER_ID = Integer.toString(n);
        PaymentParams pgPaymentParams = new PaymentParams();
        pgPaymentParams.setAPiKey(TrakConstant.PG_API_KEY);
        pgPaymentParams.setAmount(PayAmount);
        pgPaymentParams.setEmail(sessionManager.getUser().getData().getUserEmail());
        pgPaymentParams.setName(sessionManager.getUser().getData().getFullName());
        pgPaymentParams.setPhone(sessionManager.getUser().getData().getUserMobileNo());
        pgPaymentParams.setOrderId(orderID);
        pgPaymentParams.setCurrency(TrakConstant.PG_CURRENCY);
        pgPaymentParams.setDescription(TrakConstant.PG_DESCRIPTION);
        pgPaymentParams.setCity("NA");
        pgPaymentParams.setState(prefHandler.getLanguage());
        pgPaymentParams.setAddressLine1(TrakConstant.PG_ADD_1);
        pgPaymentParams.setAddressLine2(TrakConstant.PG_ADD_2);
        pgPaymentParams.setZipCode("560094");
        pgPaymentParams.setCountry(TrakConstant.PG_COUNTRY);
        pgPaymentParams.setReturnUrl(TrakConstant.PG_RETURN_URL);
        pgPaymentParams.setMode(TrakConstant.PG_MODE);
        pgPaymentParams.setUdf1(TrakConstant.PG_UDF1);
        pgPaymentParams.setUdf2(TrakConstant.PG_UDF2);
        pgPaymentParams.setUdf3(TrakConstant.PG_UDF3);
        pgPaymentParams.setUdf4(TrakConstant.PG_UDF4);
        pgPaymentParams.setUdf5(TrakConstant.PG_UDF5);

        PaymentGatewayPaymentInitializer pgPaymentInitialzer = new PaymentGatewayPaymentInitializer(pgPaymentParams, this);
        pgPaymentInitialzer.initiatePaymentProcess();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == PGConstants.REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String paymentResponse = data.getStringExtra(PGConstants.PAYMENT_RESPONSE);
                    System.out.println("paymentResponse: " + paymentResponse);
                    if (paymentResponse.equals("null")) {
                        System.out.println("Transaction Error!");
                        showToast("Transaction Failed");
                        //transactionIdView.setText("Transaction ID: NIL");
                        //transactionStatusView.setText("Transaction Status: Transaction Error!");
                    } else {
                        JSONObject response = new JSONObject(paymentResponse);
                        TrakNPayResponse = response;

                        FPaymentId = response.getString("transaction_id");
                        FTransactionId = response.getString("transaction_id");
                        FPaymentMode = response.getString("payment_mode");
                        FTransactionStatus = response.getString("response_code");
                        FAmount = response.getString("amount");
                        FMessage = response.getString("response_message");

                        Log.e("TrakNPay", "onActivityResult: " + response);

                        //purchasedCoin(FTransactionId,FPaymentMode,FAmount,prefHandler.getName(),FTransactionStatus);
                        if (response.getString("response_code").equals("0")){
                            viewModelCoinPurchase.purchaseCoin(FTransactionId, FPaymentMode, FAmount, prefHandler.getName(),
                                    FMessage, coinPanId, FTransactionStatus);

                        }
                        else
                            showToast("Transaction Canceled");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                showToast("Transaction Canceled");
            }
        }
    }

    private void showToast(final String text) {
        runOnUiThread(() -> Toast.makeText(WalletActivity.this, text, Toast.LENGTH_SHORT).show());
    }

    private void showPurchaseResultToast(Boolean status) {


        Intent intent = new Intent();
        intent.setAction("com.fairtok.wallet");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        CustomToastBinding binding;
        binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.custom_toast, null, false);
        binding.setStatus(status);
        if (status) {

            try {
                int currentCoins = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                int updatedCoins = currentCoins + Integer.parseInt(viewModelCoinPurchase.coins);
                User user = sessionManager.getUser();
                user.getData().setPaidCoins("" + updatedCoins);
                sessionManager.saveUser(user);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            binding.tvToastMessage.setText("Coins Added To Your Wallet\nSuccessfully..");
        } else {
            String string = "Something Went Wrong !";
            binding.tvToastMessage.setText(string);
        }
        Toast toast = new Toast(this);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(binding.getRoot());
        toast.show();

        Log.v("lkhkh", sessionManager.getUser().getData().getPaidCoins());
    }

}