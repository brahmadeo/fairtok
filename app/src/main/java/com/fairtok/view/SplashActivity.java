package com.fairtok.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivitySplashNewBinding;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.messaging.FirebaseMessaging;
/*import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;*/

import org.json.JSONObject;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class SplashActivity extends BaseActivity implements CompleteTaskListner {

    private ActivitySplashNewBinding binding;
    private SessionManager sessionManager;
    PrefHandler prefHandler;
    private String TAG = "splashactivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_new);
        sessionManager = new SessionManager(this);
        MyApplication.sessionManager = sessionManager;

        prefHandler = new PrefHandler(this);
        FirebaseApp.initializeApp(this);
        //firebaseRefferalSetup();

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                    Log.w("notification", "getInstanceId failed", task.getException());
                    return;
                }

                // Get new Instance ID token
                if (task.getResult() != null) {
                    String token = task.getResult();
                    Global.FIREBASE_DEVICE_TOKEN = token;
                    Log.i("notification111 ", token);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Error ", "getInstanceId failed"+ e.getMessage());
            }
        });

        new Handler().postDelayed(() -> {
            //finish();
            if (prefHandler.isLoggedIn()) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finishAffinity();
                /*if (prefHandler.getIsLanguageSet()){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finishAffinity();
                }else {
                    startActivity(new Intent(SplashActivity.this, LanguageSelection.class));
                    finishAffinity();
                }*/
            } else {
                //firebaseRefferalSetup();
                if (prefHandler.isFirstTimeLaunch()) {
                    //setup();
                    firebaseRefferalSetup();
                }
                else
                    //gotoLanguageActivity();
                    prefHandler.setIsFirstTimeLaunch(false);
                    startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
            }

        }, 3000);

    }

    private void gotoLanguageActivity() {

        startActivity(new Intent(SplashActivity.this, LanguageSelection.class));
        finish();
    }

    private void firebaseRefferalSetup(){
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            //Log.i(TAG, "test "+deepLink.getEncodedPath().substring(1, deepLink.getEncodedPath().length())+"");
                            String link = deepLink.getEncodedPath().substring(1, deepLink.getEncodedPath().length());
                            String[] strArr = link.split("=");
                            prefHandler.setReferralUrl(strArr[1]);
                            Toast.makeText(SplashActivity.this, strArr[1]+"", Toast.LENGTH_SHORT).show();
                        }
                        prefHandler.setIsFirstTimeLaunch(false);
                        startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
                    }
                });
    }

    private void setup() {

        InstallReferrerClient referrerClient;

        referrerClient = InstallReferrerClient.newBuilder(this).build();
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Connection established.
                        ReferrerDetails response = null;
                        try {
                            response = referrerClient.getInstallReferrer();
                            Log.v("dwdwd", String.valueOf(response.getGooglePlayInstantParam()));
                            Log.v("dwdwd", String.valueOf(response.getInstallReferrer()));
                            String referrerUrl = response.getInstallReferrer();
                            //long referrerClickTime = response.getReferrerClickTimestampSeconds();
                            //long appInstallTime = response.getInstallBeginTimestampSeconds();
                            //boolean instantExperienceLaunched = response.getGooglePlayInstantParam();
                            String[] strArr = referrerUrl.split("&")[1].split("=");

                            prefHandler.setIsFirstTimeLaunch(false);
                            prefHandler.setReferralUrl(referrerUrl);

                            //gotoLanguageActivity();
                            startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));

                        } catch (RemoteException e) {
                            e.printStackTrace();
                            //gotoLanguageActivity();
                            startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
                        }
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        prefHandler.setIsFirstTimeLaunch(false);
                        // API not available on the current Play Store app.
                        //gotoLanguageActivity();
                        startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
                        prefHandler.setIsFirstTimeLaunch(false);
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        prefHandler.setIsFirstTimeLaunch(false);
                        // Connection couldn't be established.
                        //gotoLanguageActivity();
                        startActivity(new Intent(SplashActivity.this, GoogleLoginActivity.class));
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                referrerClient.startConnection(this);
            }
        });

    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);
        if (response_code == 0) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {
                    openOtpScreen(SplashActivity.this);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (response_code == 2) {
            try {
                prefHandler.setIsMobileVerified(true);
                dismissBottom();

                initLoginAfterMobile(SplashActivity.this, () -> {

                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (response_code == 3) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                String msg = obj.getString("success_msg");

                if (success == 1) {
                    prefHandler.setIsMobileVerified(true);
                    dismissBottom();

                    initLoginAfterMobile(SplashActivity.this, () -> {

                        startActivity(new Intent(SplashActivity.this, LanguageSelection.class));
                        finish();

                    });
                } else
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void onClose() {
        showAlert("Please complete onetime login process to continue enjoying our app.");
    }

    public void showAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SplashActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                initLogin(SplashActivity.this, () -> {
                });
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
