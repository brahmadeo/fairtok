package com.fairtok.view.base;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ItemLoginBinding;
import com.fairtok.databinding.ItemPhoneBinding;
import com.fairtok.openduo.activities.BaseCallActivity;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.NetWorkChangeReceiver;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.GoogleLoginActivity;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.web.WebViewActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.fairtok.utils.Global.RC_SIGN_IN;


/**
 * Created by DeveloperAndroid on 06/05/2019.
 */
@SuppressLint("NewApi")
public abstract class BaseActivity extends BaseCallActivity {

    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;
    //firebase auth object
    private FirebaseAuth mAuth;

    private String token;

    //The edittext to input the code
    private EditText editTextCode;


    CompleteTaskListner listener;
    boolean isIndian=false;

    Context context;
    public SessionManager sessionManager = null;
    private NetWorkChangeReceiver netWorkChangeReceiver = null;
    private BottomSheetDialog dialog;
    private CompositeDisposable disposable = new CompositeDisposable();
    private RelativeLayout btnGoogleLogin;
    private GoogleSignInClient mGoogleSignInClient;

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(), (object, response) -> {
                        try {
                            HashMap<String, Object> hashMap = new HashMap<>();
                            hashMap.put("device_token", Global.FIREBASE_DEVICE_TOKEN);
                            hashMap.put("user_email", object.getString("email"));
                            hashMap.put("full_name", object.getString("name"));
                            hashMap.put("login_type", Const.FACEBOOK_LOGIN);
                            hashMap.put("user_name", object.getString("id"));
                            hashMap.put("identity", object.getString("id"));
                            hashMap.put("country", Global.COUNTRY);
                            hashMap.put("state", Global.STATE);
                            registerUser(hashMap,context,BaseActivity.this);
                        } catch (JSONException | NullPointerException e) {
                            e.printStackTrace();
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        sessionManager = new SessionManager(this);

        //initializing objects
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        /*FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (task.isSuccessful()){
                    token = task.getResult();
                }

            }
        });*/
    }

    protected void startReceiver() {
        netWorkChangeReceiver = new NetWorkChangeReceiver(this::showHideInternet);
        registerNetworkBroadcastForNougat();
    }

    private void showHideInternet(Boolean isOnline) {
        final TextView tvInternetStatus = findViewById(R.id.tv_internet_status);

        if (isOnline) {
            if (tvInternetStatus != null && tvInternetStatus.getVisibility() == View.VISIBLE && tvInternetStatus.getText().toString().equalsIgnoreCase(getString(R.string.no_internet_connection))) {
                tvInternetStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.kellygreen));
                tvInternetStatus.setText(R.string.back_online);
                new Handler().postDelayed(() -> slideToBottom(tvInternetStatus), 200);
            }
        } else {
            if (tvInternetStatus != null) {
                tvInternetStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.india_red));
                tvInternetStatus.setText(R.string.no_internet_connection);
                if (tvInternetStatus.getVisibility() == View.GONE) {
                    slideToTop(tvInternetStatus);
                }
            }
        }
    }

    private void slideToTop(View view) {
        TranslateAnimation animation = new TranslateAnimation(0f, 0f, view.getHeight(), 0f);
        animation.setDuration(300);
        view.startAnimation(animation);
        view.setVisibility(View.VISIBLE);
    }

    private void slideToBottom(final View view) {
        TranslateAnimation animation = new TranslateAnimation(0f, 0f, 0f, view.getHeight());
        animation.setDuration(300);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    private void registerNetworkBroadcastForNougat() {
        registerReceiver(netWorkChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(netWorkChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    /**
     * Hide keyboard when user click anywhere on screen
     *
     * @param event contains int value for motion event actions
     * @return boolean value of touch event.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        closeKeyboard();
        return true;
    }

    public void closeKeyboard() {
        try {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (im != null) {
                if (getCurrentFocus() != null) {
                    im.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }
        } catch (NullPointerException ignored) {
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void openActivity(Activity activity) {
        startActivity(new Intent(this, activity.getClass()));
    }

    public void initFaceBook() {
        CallbackManager callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, mFacebookCallback);
    }

    public void  initLogin(Context context, OnLoginSheetClose close)
    {
        this.context=context;
        listener = (CompleteTaskListner) context;
        isIndian=false;

        PrefHandler prefHandler = new PrefHandler(context);
        if(prefHandler.getIsMobileVerified())
        {
            initLoginAfterMobile(context, close);
        }
        else {
            initPhoneVerification(context, close);
        }

//        initLoginAfterMobile(context, close);

    }

    public void initPhoneVerification(Context context, OnLoginSheetClose close)
    {

        this.context=context;

        dialog = new BottomSheetDialog(context);

        //close = (OnLoginSheetClose) context;

        ItemPhoneBinding phoneBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_phone, null, false);
        dialog.setContentView(phoneBinding.getRoot());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setDismissWithAnimation(true);

        FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
        btnGoogleLogin = dialog.findViewById(R.id.btnGoogleLogin);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        CountryCodePicker ccp= (CountryCodePicker) dialog.findViewById(R.id.ccp);

        btnGoogleLogin.setOnClickListener(view -> {
            btnGoogleLogin.setClickable(false);

            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            ((MainActivity) context).startActivityForResult(signInIntent, RC_SIGN_IN);
            //googleLogin();
        });
        /*phoneBinding.setOnGoogleClick(v -> {
            googleLogin();
        });*/

         phoneBinding.setOnSendClick(v -> {

             String mobile=phoneBinding.mobileNumber.getText().toString().trim();

             if (phoneBinding.mobileNumber.getText().toString().isEmpty())
             {
                 Toast.makeText(phoneBinding.getRoot().getContext(),"Please enter the mobile Number.",Toast.LENGTH_SHORT).show();
             }
             else
             {
                 PrefHandler prefHandler = new PrefHandler(context);
                 prefHandler.setMobile(mobile);
                 dialog.dismiss();

//                 if(ccp.getSelectedCountryCode().equalsIgnoreCase("91")) {
//                     isIndian=true;
//                     sendSms(mobile, context, (MainActivity) context);
//                 }
//                 else
                     sendVerificationCode("+"+ccp.getSelectedCountryCode(), mobile,context);

                 Log.v("Paras","Paras : "+ccp.getSelectedCountryCode());

             }
         });

        phoneBinding.setOnTermClick(v -> openActivity(new WebViewActivity()));

        phoneBinding.setOnCloseClick(v -> onCloseClick());

        OnLoginSheetClose finalClose = close;
        dialog.setOnDismissListener(view -> finalClose.onClose());

        dialog.show();

    }
    private void googleLogin(){

        //Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        //((MainActivity) context).startActivityForResult(signInIntent, RC_SIGN_IN);

        /*try {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
        catch(NullPointerException e){
            btnGoogleLogin.setClickable(true);
            e.printStackTrace();
        }
        catch (ClassCastException e)
        {
            btnGoogleLogin.setClickable(true);
            e.printStackTrace();
        }
        catch (Exception e)
        {
            btnGoogleLogin.setClickable(true);
            e.printStackTrace();
        }*/
    }
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (resultCode == RESULT_OK && requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else {
            if (btnGoogleLogin!=null){
                btnGoogleLogin.setClickable(true);
                Log.i("Baseactivity", "canceled");
            }
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            PrefHandler prefHandler = new PrefHandler(this);
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("device_token", Global.FIREBASE_DEVICE_TOKEN);
                hashMap.put("user_email", account.getEmail());
                hashMap.put("full_name", account.getDisplayName());
                hashMap.put("login_type", Const.GOOGLE_LOGIN);
                hashMap.put("user_name", Objects.requireNonNull(account.getEmail()).split("@")[0]);
                hashMap.put("identity", account.getEmail());
                // hashMap.put("mobile", prefHandler.getMobile());
                hashMap.put("language", prefHandler.getLanguage());
                hashMap.put("referal_code", prefHandler.getReferralUrl());
                registerUser(hashMap, context,BaseActivity.this);
                Log.e("Google login", "handleSignInResult: " + account.getEmail());
            }
            // Signed in successfully, show authenticated UI.

        } catch (ApiException e) {
            btnGoogleLogin.setClickable(true);
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Google login", "signInResult:failed code=" + e.getStatusCode());
        }
    }*/

    public void onCloseClick()
    {
        dismissBottom();
    }

    public void initLoginAfterMobile(Context context, OnLoginSheetClose close)
    {
        this.context=context;

        dialog = new BottomSheetDialog(context);

        ItemLoginBinding loginBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_phone, null, false);
        dialog.setContentView(loginBinding.getRoot());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setDismissWithAnimation(true);

        FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
        btnGoogleLogin = dialog.findViewById(R.id.btnGoogleLogin);

        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        }

        btnGoogleLogin.setOnClickListener(v ->
        {
            btnGoogleLogin.setClickable(false);
            try {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                ((MainActivity) context).startActivityForResult(signInIntent, RC_SIGN_IN);
            }
            catch(NullPointerException e){
                btnGoogleLogin.setClickable(true);
                e.printStackTrace();
            }
            catch (ClassCastException e)
            {
                btnGoogleLogin.setClickable(true);
                e.printStackTrace();
            }
            catch (Exception e)
            {
                btnGoogleLogin.setClickable(true);
                e.printStackTrace();
            }
        });

        loginBinding.setOnFacebookClick(v -> {
            LoginManager.getInstance().logInWithReadPermissions((MainActivity) context, Collections.singletonList("public_profile"));

            LoginManager.getInstance().logInWithReadPermissions(
                    (MainActivity) context,
                    Arrays.asList("user_photos", "email", "user_birthday", "public_profile")
            );
        });
        loginBinding.setOnTermClick(v -> openActivity(new WebViewActivity()));

        loginBinding.setOnCloseClick(v -> dismissBottom());

        dialog.setOnDismissListener(view -> close.onClose());

        dialog.show();

    }

    public void openOtpScreen(Context context)
    {

        this.context=context;

        dialog = new BottomSheetDialog(context);

        ItemPhoneBinding phoneBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_phone, null, false);
        dialog.setContentView(phoneBinding.getRoot());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setDismissWithAnimation(true);

        TextView tvLabel= dialog.findViewById(R.id.tvLabel);
        //tvLabel.setText("Please enter OTP received in your mobile");

        phoneBinding.mobileNumber.setHint("Enter OTP");

        CountryCodePicker ccp= (CountryCodePicker) dialog.findViewById(R.id.ccp);
        ccp.setVisibility(View.GONE);

        dialog.findViewById(R.id.divider).setVisibility(View.GONE);

        editTextCode=phoneBinding.mobileNumber;

        Button btnSubmit=dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setText("Submit");

        FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        }


        phoneBinding.setOnSendClick(v -> {
            String mobile=phoneBinding.mobileNumber.getText().toString().trim();
            if (phoneBinding.mobileNumber.getText().toString().trim().isEmpty())
            {
                Toast.makeText(phoneBinding.getRoot().getContext(),"Please enter the OTP.",Toast.LENGTH_SHORT).show();
            }
            else
            {
//                if(isIndian)
//                    verifyMobile(mobile,context,(MainActivity)context);
//                else
                    verifyVerificationCode(mobile);
            }
        });

        phoneBinding.setOnTermClick(v -> openActivity(new WebViewActivity()));

        phoneBinding.setOnCloseClick(v -> dismissBottom());

        //dialog.setOnDismissListener(view -> close.onClose());

        dialog.show();

    }

    public void dismissBottom() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public void registerUser(HashMap<String, Object> hashMap, Context context,AppCompatActivity activity) {
        disposable.add(Global.initRetrofit().registrationUser(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((user, throwable) -> {
                    btnGoogleLogin.setClickable(true);
                    if (user != null && user.getStatus()) {
                        sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                        sessionManager.saveUser(user);
                        Global.ACCESS_TOKEN = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getToken() : "";
                        Global.USER_ID = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserId() : "";
                        sessionManager.saveBooleanValue("notification", true);
                        dismissBottom();

                        PrefHandler prefHandler = new PrefHandler(context);
                        prefHandler.setIsLoggedIn(true);
                        prefHandler.setuId(Global.USER_ID);
                        prefHandler.setName(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserName() : "");
                        prefHandler.setEmail(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserEmail() : "");
                        prefHandler.setProfile(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserProfile() : "");

                        registerDeviceToken(context,activity);
                    }
                }));
    }

    public void registerDeviceToken(Context context,AppCompatActivity activity)
    {
        PrefHandler prefHandler = new PrefHandler(context);
        Log.d("paras", "FCM Token: " + token);

        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "register_device"));
        params.add(new Pair<>("userid",prefHandler.getuId()));
        params.add(new Pair<>("token",token));
        params.add(new Pair<>("device_type","android"));
        new AsyncHttpsRequest("", context, params, activity, 1, false).execute(Utils.DEVICE_REGISTER_URL);
    }



    public void sendSms(String mobileNo,Context context,AppCompatActivity activity) {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("mobile", mobileNo));

        new AsyncHttpsRequest("Wait...!", context, params, activity, 0, false).execute(Utils.SEND_SMS_URL);
    }

    public void verifyMobile(String verificationCode,Context context,AppCompatActivity activity) {

        PrefHandler prefHandler = new PrefHandler(context);

        List<Pair<String, String>> params = new ArrayList<>();

        params.add(new Pair<>("mobile", prefHandler.getMobile()));
        params.add(new Pair<>("code", verificationCode));


        new AsyncHttpsRequest("", context, params, activity, 3, false).execute(Utils.VERIFICATION_URL);
    }


    protected void setStatusBarTransparentFlag() {

        View decorView = getWindow().getDecorView();
        decorView.setOnApplyWindowInsetsListener((v, insets) -> {
            WindowInsets defaultInsets = v.onApplyWindowInsets(insets);
            return defaultInsets.replaceSystemWindowInsets(
                    defaultInsets.getSystemWindowInsetLeft(),
                    0,
                    defaultInsets.getSystemWindowInsetRight(),
                    defaultInsets.getSystemWindowInsetBottom());
        });
        ViewCompat.requestApplyInsets(decorView);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.transparent));
    }

    protected void removeStatusBarTransparentFlag() {
        View decorView = getWindow().getDecorView();
        decorView.setOnApplyWindowInsetsListener((v, insets) -> {
            WindowInsets defaultInsets = v.onApplyWindowInsets(insets);
            return defaultInsets.replaceSystemWindowInsets(
                    defaultInsets.getSystemWindowInsetLeft(),
                    defaultInsets.getSystemWindowInsetTop(),
                    defaultInsets.getSystemWindowInsetRight(),
                    defaultInsets.getSystemWindowInsetBottom());
        });
        ViewCompat.requestApplyInsets(decorView);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    public interface OnLoginSheetClose {
        void onClose();
    }


    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String countryCode, String mobile,Context context) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                countryCode + mobile,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks);

        openOtpScreen(context);
    }


    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };


    private void verifyVerificationCode(String code) {
        try {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        //signing the user
        signInWithPhoneAuthCredential(credential);
        }
        catch (Exception e){
            Toast toast = Toast.makeText(getApplicationContext(), "Verification Code is wrong", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(BaseActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            dismissBottom();
                            listener.completeTask("Success", 2);

                            String message = "Your Mobile Number is Verified Successfully.";
                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}