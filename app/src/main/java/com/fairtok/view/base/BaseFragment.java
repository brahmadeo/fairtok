package com.fairtok.view.base;


import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fairtok.MyApplication;
import com.fairtok.openlive.rtc.EngineConfig;
import com.fairtok.openlive.rtc.EventHandler;
import com.fairtok.openlive.stats.StatsManager;
import com.fairtok.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import io.agora.rtc.RtcEngine;


/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {


    public SessionManager sessionManager;
    private Context context;

    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = getActivity();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity();

        if (context != null) {
            sessionManager = new SessionManager(context);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }


//    brahma start

    protected MyApplication application() {
        return (MyApplication) getContext();
    }

    protected RtcEngine rtcEngine() {
        return application().rtcEngine();
    }

    protected EngineConfig config() {
        return application().engineConfig();
    }

    protected StatsManager statsManager() { return application().statsManager(); }

    protected void registerRtcEventHandler(EventHandler handler) {
        application().registerEventHandler(handler);
    }

    protected void removeRtcEventHandler(EventHandler handler) {
        application().removeEventHandler(handler);
    }

    // brahma end


}
