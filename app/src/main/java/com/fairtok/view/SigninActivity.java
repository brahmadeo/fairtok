package com.fairtok.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.WindowManager;
import android.widget.Toast;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.openlive.activities.LiveUsersActivity;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.home.MainActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.hbb20.CountryCodePicker;

import java.util.HashMap;
import java.util.regex.Pattern;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SigninActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    PrefHandler prefHandler;
    TextInputEditText mobileNumber, password;

    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;
    //firebase auth object
    private FirebaseAuth mAuth;
    boolean flag=false;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        sessionManager = new SessionManager(this);
        mobileNumber = findViewById(R.id.mobile_number);
        password = findViewById(R.id.password);

        findViewById(R.id.btnSubmit).setOnClickListener(view -> {
            String mobile = mobileNumber.getText().toString().trim();
            if (mobileNumber.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please enter the email id.", Toast.LENGTH_SHORT).show();
            }else if (!validEmail(mobile)){
                Toast.makeText(getApplicationContext(), "Please enter valid email.", Toast.LENGTH_SHORT).show();
            }
            else if (password.getText().toString().isEmpty()){
                Toast.makeText(getApplicationContext(), "Please enter the password.", Toast.LENGTH_SHORT).show();
            }else {
                // api call
                if (password.getText().toString().trim().equals("fairtok@123")){
                    Log.i("credentials ", mobile+" , "+ password.getText().toString().trim());
                    PrefHandler prefHandler = new PrefHandler(SigninActivity.this);

                    String device_token = "foCh8umAQPCU75EGlOxEmu:APA91bE04mm1v9XpIvxw7RyB677C3wAz2WZUi_uY6YvF-lAUU6gRuiVgSYKUaUpEHZHPAFeKvWOv1QMQu4MQUoc6LhFS0H0mIbKeLdLUtTAEBgFb5C96bVTM06Fie65MSYhMGkoIkwzV";
                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("device_token", device_token);
                    hashMap.put("user_email", mobile);
                    hashMap.put("full_name", "Fairtok");
                    hashMap.put("login_type", "user");
                    hashMap.put("user_name", "Fairtok");
                    hashMap.put("identity", mobile);
                    // hashMap.put("mobile", prefHandler.getMobile());
                    hashMap.put("language", prefHandler.getLanguage());
                    hashMap.put("referal_code", prefHandler.getReferralUrl());

                    Log.i("paramsss ", hashMap.toString());
                    registerUser(hashMap, SigninActivity.this,SigninActivity.this);
                }else {
                    Toast.makeText(getApplicationContext(), "Invalid User.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private boolean validEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
    public void registerUser(HashMap<String, Object> hashMap, Context context, AppCompatActivity activity) {
        Log.i("hashmapp ", hashMap.toString());
        disposable.add(Global.initRetrofit().registrationUser(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((user, throwable) -> {
                    if (user != null && user.getStatus()) {
                        sessionManager.saveBooleanValue(Const.IS_LOGIN, true);
                        sessionManager.saveUser(user);
                        Global.ACCESS_TOKEN = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getToken() : "";
                        Global.USER_ID = sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserId() : "";
                        sessionManager.saveBooleanValue("notification", true);

                        PrefHandler prefHandler = new PrefHandler(context);
                        prefHandler.setIsLoggedIn(true);
                        prefHandler.setuId(Global.USER_ID);
                        prefHandler.setName(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserName() : "");
                        prefHandler.setEmail(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserEmail() : "");
                        prefHandler.setProfile(sessionManager.getUser().getData() != null ? sessionManager.getUser().getData().getUserProfile() : "");

                        finish();
                        if (prefHandler.getIsLanguageSet()){
                            startActivity(new Intent(SigninActivity.this, MainActivity.class));
                        }else {
                            startActivity(new Intent(SigninActivity.this, LanguageSelection.class));
                        }

                    }else {
                        Toast.makeText(SigninActivity.this, "Invalid User", Toast.LENGTH_SHORT).show();
                    }
                }));
    }
}