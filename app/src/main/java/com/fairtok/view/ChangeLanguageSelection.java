package com.fairtok.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.model.LanguageBean;
import com.fairtok.view.home.MainActivity;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChangeLanguageSelection extends AppCompatActivity {

    ArrayList<LanguageBean> items;
    RecyclerView recyclerView;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    String TAG="Paras";
    RelativeLayout sendLayout,latlngLayout;
    EditText messageEdit,ed_latlng;
    PrefHandler pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        pref = new PrefHandler(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.staff_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }


        pref = new PrefHandler(this);

        TextView title = findViewById(R.id.title);
        title.setText("Please Select Language");

        items = new ArrayList<LanguageBean>();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
        sendLayout = findViewById(R.id.sendLayout);

        latlngLayout = findViewById(R.id.latlngLayout);

        sendLayout.setVisibility(View.GONE);
        latlngLayout.setVisibility(View.GONE);

        messageEdit = findViewById(R.id.messageEdit);
        ed_latlng = findViewById(R.id.latlng);

        TextView choose = findViewById(R.id.choose);
        choose.setText("Please Select Your \nPreferable Language");

        findViewById(R.id.back_arrow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Button btn = findViewById(R.id.btnSubmit);
        btn.setText(getString(R.string.submit));

        findViewById(R.id.btnSubmit).setVisibility(View.GONE);

        prepareData();

    }

    public void prepareData()
    {
        items = new ArrayList<LanguageBean>();
        items.add(new LanguageBean("en","English"));
        items.add(new LanguageBean("hi","हिन्दी"));
        items.add(new LanguageBean("gu","ગુજરાતી"));
        items.add(new LanguageBean("ta","தமிழ்"));
        items.add(new LanguageBean("kn","ಕನ್ನಡ"));
        items.add(new LanguageBean("te","తెలుగు"));
        items.add(new LanguageBean("ml","മലയാളം"));
        items.add(new LanguageBean("mr","मराठी"));
        items.add(new LanguageBean("pa","ਪੰਜਾਬੀ"));
        items.add(new LanguageBean("bn","বাংলা"));
//        items.add(new LanguageBean("","ଓଡିଆ"));

        recyclerView.setAdapter(new MaintenanceStaffAdapter(items,this));
    }



    public class MaintenanceStaffAdapter extends RecyclerView.Adapter<MaintenanceStaffAdapter.ViewHolder> {

        private List<LanguageBean> mItems;
        Context context;
        private int selected = -1;

        public MaintenanceStaffAdapter(List<LanguageBean> items, Context context) {
            mItems = items;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_lang, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            final LanguageBean item = mItems.get(i);
            viewHolder.tvName.setText(item.getName());
            viewHolder.tvType.setText("");
            viewHolder.tvAvailability.setText(item.getId());

            viewHolder.rlMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selected = i;
                    pref.setIsLanguageSet(true);
                    pref.setLanguage(item.getId());
                    viewHolder.tvName.setTextColor(context.getResources().getColor(R.color.white));
                    viewHolder.tvQtyLable.setTextColor(context.getResources().getColor(R.color.btn_color4));
                    viewHolder.tvAvailability.setTextColor(context.getResources().getColor(R.color.btn_color4));
                    viewHolder.rlMain.setBackground(context.getDrawable(R.drawable.rectangular_solid_pink));
                    finish();
                }
            });

            /*if (selected == i){
                viewHolder.tvName.setTextColor(context.getResources().getColor(R.color.btn_color4));
                viewHolder.tvQtyLable.setTextColor(context.getResources().getColor(R.color.btn_color4));
                viewHolder.tvAvailability.setTextColor(context.getResources().getColor(R.color.btn_color4));
                viewHolder.main_layout.setBackground(context.getDrawable(R.drawable.d_round_border_selected));
                 new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(context, MainActivity.class));
                        }
                    }, 500);
            }else {
                viewHolder.tvName.setTextColor(context.getResources().getColor(R.color.white));
                viewHolder.tvQtyLable.setTextColor(context.getResources().getColor(R.color.white));
                viewHolder.tvAvailability.setTextColor(context.getResources().getColor(R.color.white));
                viewHolder.main_layout.setBackground(context.getDrawable(R.drawable.d_round_border));
            }*/

        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView tvName,tvAvailability,tvType,tvQtyLable;
            RelativeLayout rlMain;
            private LinearLayout main_layout;
            CircleImageView imgUser;

            ViewHolder(View v) {
                super(v);
                main_layout = v.findViewById(R.id.main_layout);
                tvName = (TextView)v.findViewById(R.id.tvName);
                rlMain = (RelativeLayout) v.findViewById(R.id.rlMain);
                imgUser = (CircleImageView) v.findViewById(R.id.imgUser);
                imgUser.setVisibility(View.GONE);
                tvAvailability = (TextView)v.findViewById(R.id.tvAvailability);
                tvType = (TextView)v.findViewById(R.id.tvType);
                tvQtyLable = v.findViewById(R.id.tvQtyLable);
                tvQtyLable.setText("Sr. #");
            }
        }
    }

}