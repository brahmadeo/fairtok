package com.fairtok.view.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.databinding.ActivityHashtagBinding;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Global;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.video.PlayerActivity;
import com.fairtok.viewmodel.HashTagViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.gson.Gson;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class HashTagActivity extends BaseActivity {

    ActivityHashtagBinding binding;

    HashTagViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hashtag);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new HashTagViewModel()).createFor()).get(HashTagViewModel.class);

        initView();
        initObserve();
        initListeners();

        binding.setViewmodel(viewModel);

    }

    private void initListeners() {
        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onLoadMore());
        binding.imgBack.setOnClickListener(v -> onBackPressed());
    }

    private void initView() {
        if (getIntent().getStringExtra("hashtag") != null) {
            viewModel.hashtag = getIntent().getStringExtra("hashtag");
            //binding.tvVideoCount.setText(Global.prettyCount(viewModel.explore.getHashTagVideosCountl()) + " Videos");
            if(getIntent().hasExtra("position")) {
                viewModel.hasPosition = true;
                viewModel.position = getIntent().getIntExtra("position", 0);
            }
            else
            {
                viewModel.hasPosition=false;
            }
        }
        binding.refreshlout.setEnableRefresh(false);
        viewModel.adapter.isHashTag = true;
        viewModel.adapter.word = viewModel.hashtag;
        viewModel.fetchHashTagVideos(false);


    }

    private void initObserve() {
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
        viewModel.video.observe(this, video -> performAction(video));

    }

    private void performAction(Video video)
    {
        binding.tvVideoCount.setText(Global.prettyCount(video.getPost_count()).concat(" Videos"));

        if(video.getPost_count()>0)
        {
            if(viewModel.hasPosition)
            {
                Intent intent = new Intent(binding.getRoot().getContext(), PlayerActivity.class);
                intent.putExtra("video_list", new Gson().toJson(video.getData()));
                intent.putExtra("position", viewModel.position);
                intent.putExtra("type", 2);
                intent.putExtra("hash_tag", viewModel.hashtag);
                binding.getRoot().getContext().startActivity(intent);
            }
        }
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}