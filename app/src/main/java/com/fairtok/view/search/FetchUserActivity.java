package com.fairtok.view.search;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.fairtok.R;
import com.fairtok.databinding.ActivityFetchUserBinding;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.fairtok.view.profile.ProfileFragment;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class FetchUserActivity extends BaseActivity {

    ActivityFetchUserBinding binding;
    private boolean isBranch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_fetch_user);

        String userid = getIntent().getStringExtra("userid");
        isBranch = getIntent().getBooleanExtra("isBranch", false);

        ProfileFragment fragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString("userid", userid);
        fragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lout_main, fragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (isBranch) {
            startActivity(new Intent(this, MainActivity.class));
            finishAffinity();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}