package com.fairtok.view.search;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.LoadAds;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.DialogSearchBinding;
import com.fairtok.databinding.FragmentSearchBinding;
import com.fairtok.databinding.ItemSearchUserBinding;
import com.fairtok.databinding.ItemSearchVideosBinding;
import com.fairtok.model.user.SearchUser;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Global;
import com.fairtok.view.video.PlayerActivity;
import com.fairtok.viewmodel.MainViewModel;
import com.fairtok.viewmodel.SearchActivityViewModel;
import com.fairtok.viewmodel.SearchFragmentViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    private static final int MY_PERMISSIONS_REQUEST = 101;
    SearchFragmentViewModel viewModel;
    FragmentSearchBinding binding;
    private MainViewModel parentViewModel;

    private static final int PERMISSION_REQ_CODE = 1 << 2;
    private final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
    };
    private AdView mAdView;

    public SearchFragment() {

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for getContext() fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);

        LinearLayout llAdview = binding.getRoot().findViewById(R.id.llAdview);
        LoadAds.loadAdmob(llAdview.getContext(),llAdview,getString(R.string.banner_ad_id));

        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void initView() {
        binding.refreshlout.setEnableRefresh(false);
    }


    private void initObserve() {
        parentViewModel.selectedPosition.observe(this, position -> {
            if (position != null && position == 1) {
                viewModel.exploreStart = 0;
                viewModel.fetchExploreItems(false);

            }
        });
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());
    }

    private void initListeners() {

        binding.imgSearch.setOnClickListener(v -> {
//            Intent intent = new Intent(getActivity(), SearchActivity.class);
//            intent.putExtra("search", binding.etSearch.getText().toString());
//            startActivity(intent);

            openSearchSheet(v.getContext());
        });


        binding.imgQr.setOnClickListener(v -> initPermission());

        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> viewModel.onExploreLoadMore());

    }

    /*private void initPermission() {
        if (getActivity() != null && getActivity().getPackageManager() != null) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST);
            } else {
                startActivity(new Intent(getContext(), QRScanActivity.class));
            }
        }
    }*/

    private void initPermission() {
        boolean granted = true;
        for (String per : PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }

        if (granted) {
            startActivity(new Intent(getContext(), QRScanActivity.class));
        } else {
            requestPermissions();
        }
    }
    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getContext(), permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_REQ_CODE);
    }

   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && getActivity() != null && getActivity().getPackageManager() != null) {
                startActivity(new Intent(getContext(), QRScanActivity.class));
            }
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            parentViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new SearchFragmentViewModel()).createFor()).get(SearchFragmentViewModel.class);
        initView();
        initObserve();
        initListeners();
        binding.setViewmodel(viewModel);

    }

    public void openSearchSheet(Context context)
    {

        final String[] paths = {"Users", "Videos"};

        SearchActivityViewModel searchActivityViewModelviewModel = ViewModelProviders.of(this).get(SearchActivityViewModel.class);

        BottomSheetDialog dialog = new BottomSheetDialog(context);

        DialogSearchBinding loginBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_search, null, false);
        dialog.setContentView(loginBinding.getRoot());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setDismissWithAnimation(true);
        loginBinding.setViewmodel(searchActivityViewModelviewModel);

        View bottomSheetView = dialog.getWindow().getDecorView().findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior.from(bottomSheetView).setHideable(false);


        FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
        if (bottomSheet != null) {
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        }


        LinearLayoutManager llm = new LinearLayoutManager(context);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        loginBinding.recyclerview.setLayoutManager(llm);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,paths);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loginBinding.spinner.setAdapter(adapter);
        loginBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                searchActivityViewModelviewModel.searchtype.set(i);

                if(i==0)
                {
                    loginBinding.recyclerview.setLayoutManager(llm);
                }
                else
                {
                    loginBinding.recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        loginBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null) {
                    BottomSheetBehavior.from(bottomSheetView).setHideable(true);
                    dialog.dismiss();
                }
            }
        });

        Glide.with(context).asGif().load(R.drawable.search_progress).into(loginBinding.imgSearching);

        loginBinding.imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                searchActivityViewModelviewModel.search_text = loginBinding.etSearch.getText().toString().trim();

                Utils.hideKeyboard(view.getContext(),loginBinding.etSearch);

                if(TextUtils.isEmpty(searchActivityViewModelviewModel.search_text))
                {
                    Toast.makeText(context,"Please enter something to search",Toast.LENGTH_SHORT).show();
                }
                else {
                    searchActivityViewModelviewModel.videoStart=0;
                    searchActivityViewModelviewModel.userStart=0;

                    loginBinding.searchingLayout.setVisibility(View.VISIBLE);


                    if (loginBinding.spinner.getSelectedItemPosition() == 0) {
                        searchActivityViewModelviewModel.searchForUser(false);
                    } else {
                        searchActivityViewModelviewModel.searchForVideos(false);
                    }
                }
            }
        });

        loginBinding.refreshlout.setOnLoadMoreListener(refreshLayout -> {
            if (loginBinding.spinner.getSelectedItemPosition() == 0) {
                searchActivityViewModelviewModel.onUserLoadMore();
            } else {
                searchActivityViewModelviewModel.onVideoLoadMore();
            }
        });



        if (loginBinding.spinner.getSelectedItemPosition() == 0) {

            loginBinding.recyclerview.setAdapter(searchActivityViewModelviewModel.searchUseradapter);
            loginBinding.recyclerview.setLayoutManager(llm);
            //searchActivityViewModelviewModel.searchUseradapter.updateData(searchActivityViewModelviewModel.userList);
        }
        else
        {

            loginBinding.recyclerview.setAdapter(searchActivityViewModelviewModel.videoListAdapter);
            loginBinding.recyclerview.setLayoutManager(new GridLayoutManager(context, 2));
            //videoListAdapter.updateData(searchActivityViewModelviewModel.videoList);
        }


//        SearchUserAdapter searchUserAdapter = new SearchUserAdapter();
//        VideoListAdapter videoListAdapter = new VideoListAdapter();

        searchActivityViewModelviewModel.onLoadMoreComplete.observe(this, onLoadMore -> loginBinding.refreshlout.finishLoadMore());

        searchActivityViewModelviewModel.isloading.observe(this, isLoading -> {

                if(isLoading==false) {
                    loginBinding.searchingLayout.setVisibility(View.GONE);

                    if (searchActivityViewModelviewModel.userList.size() > 0) {
                        loginBinding.recyclerview.setVisibility(View.VISIBLE);
                        loginBinding.loutNothing.setVisibility(View.GONE);
                    } else {
                        loginBinding.recyclerview.setVisibility(View.GONE);
                        loginBinding.loutNothing.setVisibility(View.VISIBLE);
                    }
                }

        });

        dialog.show();

    }

    public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.HashTagVideoViewHolder> {
        public ArrayList<Video.Data> mList = new ArrayList<>();
        public boolean isHashTag = false;
        public String word = "";


        @NonNull
        @Override
        public HashTagVideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_videos, parent, false);
            return new HashTagVideoViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull HashTagVideoViewHolder holder, int position) {
            holder.setModel(position);

        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public void updateData(List<Video.Data> list) {
            mList = (ArrayList<Video.Data>) list;
            notifyDataSetChanged();

        }

        public void loadMore(List<Video.Data> data) {
            for (int i = 0; i < data.size(); i++) {
                mList.add(data.get(i));
                notifyItemInserted(mList.size() - 1);
            }

        }


        class HashTagVideoViewHolder extends RecyclerView.ViewHolder {
            ItemSearchVideosBinding binding;

            HashTagVideoViewHolder(@NonNull View itemView) {
                super(itemView);
                binding = DataBindingUtil.bind(itemView);
                if (binding != null) {
                    binding.executePendingBindings();
                }
            }

            public void setModel(int position) {
                binding.setModel(mList.get(position));
                binding.tvLikeCount.setText(Global.prettyCount(Integer.parseInt(mList.get(position).getPostLikesCount())));
                binding.getRoot().setOnClickListener(v -> {
                    Intent intent = new Intent(binding.getRoot().getContext(), PlayerActivity.class);
                    intent.putExtra("video_list", new Gson().toJson(mList));
                    intent.putExtra("position", position);
                    if (isHashTag) {
                        intent.putExtra("type", 2);
                        intent.putExtra("hash_tag", word);
                    } else {
                        intent.putExtra("type", 3);
                        intent.putExtra("keyword", word);
                    }
                    binding.getRoot().getContext().startActivity(intent);
                });
            }

        }
    }

    public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.SearchUserViewHolder> {
        public ArrayList<SearchUser.User> mList = new ArrayList<>();


        @NonNull
        @Override
        public SearchUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_user, parent, false);
            return new SearchUserViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SearchUserViewHolder holder, int position) {
            holder.setModel(mList.get(position));

        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public void updateData(List<SearchUser.User> list) {
            mList = (ArrayList<SearchUser.User>) list;
            notifyDataSetChanged();

        }

        public void loadMore(List<SearchUser.User> data) {
            for (int i = 0; i < data.size(); i++) {
                mList.add(data.get(i));
                notifyItemInserted(mList.size() - 1);
            }

        }

        class SearchUserViewHolder extends RecyclerView.ViewHolder {
            ItemSearchUserBinding binding;

            SearchUserViewHolder(@NonNull View itemView) {
                super(itemView);
                binding = DataBindingUtil.bind(itemView);
            }

            public void setModel(SearchUser.User user) {
                binding.setUser(user);
                binding.tvDetails.setText(Global.prettyCount(user.getUserFollowerCount()).concat(" Fans  " + Global.prettyCount(user.getUserPostCount()) + " Videos"));
                binding.getRoot().setOnClickListener(v -> {

                    Intent intent = new Intent(binding.getRoot().getContext(), FetchUserActivity.class);
                    intent.putExtra("userid", user.getUserId());
                    binding.getRoot().getContext().startActivity(intent);

                });
            }

        }
    }


}

