package com.fairtok.view.slider;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;

import com.fairtok.R;
import com.fairtok.databinding.ActivitySliderBinding;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class SliderActivity extends BaseActivity {

    ActivitySliderBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_slider);
        initListener();
    }

    private void initListener() {
        binding.tvSkip.setOnClickListener(v -> {
            openActivity(new MainActivity());
            finish();
        });
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}
