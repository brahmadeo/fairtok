package com.fairtok.view.web;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.fairtok.R;
import com.fairtok.databinding.ActivityWebViewBinding;
import com.fairtok.view.base.BaseActivity;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

public class WebViewActivity extends BaseActivity {

    ActivityWebViewBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);

        binding.webview.loadUrl("https://fairtok.com/privacy.html");

        binding.imgBack.setOnClickListener(v -> onBackPressed());

    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}