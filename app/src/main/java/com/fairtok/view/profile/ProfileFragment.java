package com.fairtok.view.profile;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.ChatActivity;
import com.fairtok.chat.ChatListActivity;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.ProfileImageActivity;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.FragmentProfileBinding;
import com.fairtok.model.user.User;
import com.fairtok.openduo.activities.DialerActivity;
import com.fairtok.openduo.agora.Config;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;
import com.fairtok.view.base.BaseFragment;
import com.fairtok.view.familyroom.JoinFamilyActivity;
import com.fairtok.view.home.ReportSheetFragment;
import com.fairtok.view.wallet.CoinPurchaseSheetFragment;
import com.fairtok.viewmodel.MainViewModel;
import com.fairtok.viewmodel.ProfileViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;

import static android.app.Activity.RESULT_OK;
import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements CompleteTaskListner {

    public ProfileViewModel viewModel;
    private final int UPDATE_DATA = 100;
    private MainViewModel parentViewModel;
    public FragmentProfileBinding binding;
    String userId;
    boolean isMutualFollower = false, isAudio = false;
    String msg = "You have to follow this creator in order to call";
    Config config;

    public static ImageView img_user;

    // Permission request when we want to go to next activity
    // when all necessary permissions are granted.
    private static final int PERMISSION_REQ_FORWARD = 1 << 4;

    // Permission request when we want to stay in
    // current activity even if all permissions are granted.
    private static final int PERMISSION_REQ_STAY = 1 << 3;

    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//            Manifest.permission.CALL_PHONE,
    };

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        config = new Config(getActivity());
        String idFormat = getResources().getString(R.string.identifier_format);
        String identifier = String.format(idFormat, config.getUserId());
        Log.v("paras", "Paras your video call id = " + identifier);
        sessionManager = new SessionManager(getActivity());

        img_user = binding.imgUser;

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void openImage() {
        String image = Const.BASE_URL_IMAGE.concat(viewModel.user.getValue().getData().getUserProfile());
        Log.v("paras", "Paras image here " + image);
        Intent i = new Intent(getActivity(), ProfileImageActivity.class);
        i.putExtra("profile", image);
        i.putExtra("username", viewModel.user.getValue().getData().getFullName());
        startActivity(i);
    }

    private void initView() {
        viewModel.isPrivateVideos.set(false);
        viewModel.isLikedVideos.set(false);
        if (getArguments() != null) {
            userId = getArguments().getString("userid");

            Log.v("Paras", "Paras userId = " + userId);

            if (userId != null && !userId.equals(Global.USER_ID)) {
                viewModel.isMyAccount.set(-1);
            }
            //checkMutualFollowers(userId,getActivity());
            viewModel.fetchUserById(getArguments().getString("userid"));
            Log.v("jhjh", "1");
            viewModel.userId = getArguments().getString("userid");
            viewModel.isBackBtn.set(true);

            binding.imgUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openImage();
                }
            });

        }
        if (userId != null && userId.equals(Global.USER_ID)) {
            ProfileVideoPagerAdapter adapter = new ProfileVideoPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            binding.viewPager.setAdapter(adapter);
        } else {
            OthersProfileVideoPagerAdapter adapter = new OthersProfileVideoPagerAdapter(getChildFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            binding.viewPager.setAdapter(adapter);
        }
    }

    public class ProfileVideoPagerAdapter extends FragmentPagerAdapter {

        public ProfileVideoPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.colorTheme));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvPrivateVideos.setTextColor(getResources().getColor(R.color.white));
                    return ProfileVideosFragment.getNewInstance(position);

                case 1:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.colorTheme));
//                    binding.tvPrivateVideos.setTextColor(getResources().getColor(R.color.white));
                    return LikedVideosFragment.getNewInstance(position);


                case 2:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvPrivateVideos.setTextColor(getResources().getColor(R.color.colorTheme));
                    return PrivateVideosFragment.getNewInstance(position);


                default:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.colorTheme));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvPrivateVideos.setTextColor(getResources().getColor(R.color.white));
                    return ProfileVideosFragment.getNewInstance(position);


            }

        }

        @Override
        public int getCount() {
            int count = 3;
            return count;
        }
    }

    public class OthersProfileVideoPagerAdapter extends FragmentPagerAdapter {

        public OthersProfileVideoPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ProfileVideosFragment.getNewInstance(position);

                case 1:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.white));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.colorTheme));
                    return LikedVideosFragment.getNewInstance(position);

                default:
//                    binding.tvVideos.setTextColor(getResources().getColor(R.color.colorTheme));
//                    binding.tvLikedVideos.setTextColor(getResources().getColor(R.color.white));
                    return ProfileVideosFragment.getNewInstance(position);


            }
        }

        @Override
        public int getCount() {
            int count = 1;
            return count;
        }
    }

    private void initObserver() {
        parentViewModel.selectedPosition.observe(this, position -> {
            if (position != null && position == 3) {
                viewModel.selectPosition.setValue(position);


                if (sessionManager.getUser() != null) {
                    viewModel.user.setValue(sessionManager.getUser());
                    viewModel.fetchUserById(sessionManager.getUser().getData().getUserId());
                    viewModel.userId = sessionManager.getUser().getData().getUserId();
                    viewModel.isBackBtn.set(false);
                }
            }
        });
        viewModel.onItemClick.observe(getViewLifecycleOwner(), type -> {
            if (type != null) {
                switch (type) {
                    // On option menu click
                    case 0:
                        if (viewModel.isMyAccount.get() == 1 || viewModel.isMyAccount.get() == 2) {
                            // other user profile
                            showPopMenu();
                        } else {
                            // my profile
                            startActivity(new Intent(getActivity(), SettingsActivity.class));
                        }
                        break;
                    // On Follow, UnFollow, edit btn click
                    case 1:
                        handleButtonClick();
                        break;
                    // Back btn
                    case 2:
                        if (getActivity() != null) {
                            getActivity().onBackPressed();
                        }
                        break;
                    // On My videos tab click
                    case 3:
                        viewModel.isLikedVideos.set(false);
                        viewModel.isPrivateVideos.set(false);
                        binding.viewPager.setCurrentItem(0);
                        break;
                    // On liked videos click
                    case 4:
//                        if (sessionManager.getUser() != null && sessionManager.getUser().getData().getUserId().equals(userId))
//                        {
                        MyApplication.sessionManager = sessionManager;
                        viewModel.isLikedVideos.set(true);
                        viewModel.isPrivateVideos.set(false);
                        binding.viewPager.setCurrentItem(1);
//                        }
//                        else
//                        {
////                            viewModel.isLikedVideos.set(false);
//                            binding.viewPager.setCurrentItem(1);
//                        }

                        break;

                    case 11:
                        MyApplication.sessionManager = sessionManager;

                        viewModel.isLikedVideos.set(false);
                        viewModel.isPrivateVideos.set(true);
                        binding.viewPager.setCurrentItem(2);
                        break;

                    case 12:
                        MyApplication.sessionManager = sessionManager;
                        viewModel.isLikedVideos.set(false);
                        viewModel.isPrivateVideos.set(true);
                        binding.viewPager.setCurrentItem(3);
                        break;
                    case 13:
                        startActivity(new Intent(getActivity(), JoinFamilyActivity.class));
                        break;
                    // On Followers Click
                    case 5:
                        handleFollowerClick(0);
                        break;
                    // On Following Click
                    case 6:
                        handleFollowerClick(1);
                        break;

                    case 7:
                        handleChatClick();
                        break;

                    case 8:
                        isAudio = false;
                        handleVideoCallClick();

                        break;

                    case 9:
                        isAudio = true;
                        handleVideoCallClick();
                        break;


                }
                viewModel.onItemClick.setValue(null);
            }
        });
        viewModel.intent.observe(getViewLifecycleOwner(), intent -> {
            if (intent != null) {
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        viewModel.user.observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                binding.setViewModel(viewModel);
                if (Global.USER_ID.equals(user.getData().getUserId())) {
                    sessionManager.saveUser(user);
                }
            }
        });

        viewModel.followApi.observe(getViewLifecycleOwner(), checkUsername -> {
            if (viewModel.user.getValue() != null) {
                if (viewModel.isMyAccount.get() == 1) {
                    viewModel.user.getValue().getData().setFollowersCount(viewModel.user.getValue().getData().getFollowersCount() + 1);
                } else {
                    viewModel.user.getValue().getData().setFollowersCount(viewModel.user.getValue().getData().getFollowersCount() - 1);
                }
                binding.tvFansCount.setText(Global.prettyCount(viewModel.user.getValue().getData().getFollowersCount()));
            }
        });

    }

    private void handleVideoCallClick() {
        PrefHandler prefHandler = new PrefHandler(getActivity());

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            if (viewModel.isMyAccount.get() == 2 || viewModel.isMyAccount.get() == 1) {
//                if(viewModel.user.getValue().getData().getIsFollowing()==1)
//                {
                Utils.CURRENT_RECEIVER_ID = viewModel.user.getValue().getData().getUserId();
                Utils.CURRENT_RECEIVER_NAME = viewModel.user.getValue().getData().getFullName();

                Log.v("kjlkjh1", Utils.CURRENT_RECEIVER_ID);
                Log.v("kjlkjh2", Utils.CURRENT_RECEIVER_NAME);

                int currentWalletBalance = Integer.parseInt(sessionManager.getUser().getData().getPaidCoins());
                Log.v("kjhkjh", String.valueOf(currentWalletBalance));
                if (isAudio && currentWalletBalance >= prefHandler.getAudioCharges())
                    checkPermissions();
                else if (!isAudio && currentWalletBalance >= prefHandler.getVideoCharges())
                    checkPermissions();
                else
                    showAlertWallet("You don't have sufficient balance in your wallet to call." +
                            " Please purchase coins to make this call.");
//                }
//                else
//                {
//                    showAlert(msg);
//                }

            } else {

            }
        } else {
            Toast.makeText(getContext(), "You have to login first", Toast.LENGTH_SHORT).show();
        }

    }

    private void handleVisibility() {
        Log.v("paras", "paras logedIn=" + sessionManager.getBooleanValue(Const.IS_LOGIN));
        Log.v("paras", "paras isMyAccount=" + viewModel.isMyAccount.get());

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {

            if (userId != null && userId.equals(Global.USER_ID)) {
                binding.btnAudioCall.setVisibility(View.GONE);
                binding.btnCall.setVisibility(View.GONE);

            } else {

                binding.btnAudioCall.setVisibility(View.VISIBLE);
                //binding.btnCall.setVisibility(View.VISIBLE);

            }
        } else {
            binding.btnAudioCall.setVisibility(View.GONE);
            binding.btnCall.setVisibility(View.GONE);
        }

    }

    private void checkPermissions() {
        if (!permissionArrayGranted(null)) {
            requestPermissions(PERMISSION_REQ_FORWARD);
        } else {
            gotoDialerActivity();
        }
    }

    private boolean permissionArrayGranted(@Nullable String[] permissions) {
        String[] permissionArray = permissions;
        if (permissionArray == null) {
            permissionArray = PERMISSIONS;
        }

        boolean granted = true;
        for (String per : permissionArray) {
            if (!permissionGranted(per)) {
                granted = false;
                break;
            }
        }
        return granted;
    }

    private boolean permissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(
                getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions(int request) {
        requestPermissions(PERMISSIONS, request);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_FORWARD || requestCode == PERMISSION_REQ_STAY) {
            boolean granted = permissionArrayGranted(permissions);
            if (granted && requestCode == PERMISSION_REQ_FORWARD) {
                gotoDialerActivity();
            } else if (!granted) {
                toastNeedPermissions();
            }
        }
    }

    public void onStartCall(View view) {
        requestPermissions(PERMISSION_REQ_FORWARD);
    }

    private void toastNeedPermissions() {
        Toast.makeText(getActivity(), R.string.need_necessary_permissions, Toast.LENGTH_LONG).show();
    }

    public void gotoDialerActivity() {
        PrefHandler prefHandler = new PrefHandler(getActivity());
        showOkCancelAlert("Video call will charge you " + prefHandler.getVideoCharges() + " coins, Audio call will charge " + prefHandler.getAudioCharges() + " coins from your wallet.");
    }

    public void openDialerActivity() {

        Log.v("lqjhnsljq1", viewModel.user.getValue().getData().getFullName());
        Log.v("lqjhnsljq2", viewModel.user.getValue().getData().getUserProfile());
        Log.v("lqjhnsljq3", viewModel.user.getValue().getData().getUserId());

        Intent intent = new Intent();
        intent.setClass(getActivity(), DialerActivity.class);
        intent.putExtra("userName", viewModel.user.getValue().getData().getFullName());
        intent.putExtra("userImg", viewModel.user.getValue().getData().getUserProfile());
        intent.putExtra("userId", viewModel.user.getValue().getData().getUserId());
        intent.putExtra("type", "1");//1=Live Call, 2=Normal Call
        intent.putExtra("isAudio", isAudio);
        startActivity(intent);
    }

    private int mCallNumber;

    public void appendNumber(String digit) {
        if (TextUtils.isDigitsOnly(digit)) {
            mCallNumber = mCallNumber * 10 + digitToInt(digit);
        }

    }

    public int digitToInt(String digit) {
        return Integer.valueOf(digit);
    }

    private void showPopMenu() {
        if (getActivity() != null) {
            PopupMenu popupMenu = new PopupMenu(getActivity(), binding.imgOption);
            popupMenu.getMenuInflater().inflate(R.menu.profile_menu, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.share:
                        shareProfile();
                        return true;
                    case R.id.report:
                        ReportSheetFragment fragment = new ReportSheetFragment();
                        Bundle args = new Bundle();
                        args.putString("userid", viewModel.userId);
                        args.putInt("reporttype", 0);
                        fragment.setArguments(args);
                        fragment.show(getChildFragmentManager(), fragment.getClass().getSimpleName());
                        return true;

                }
                return false;
            });
            popupMenu.show();
        }
    }

    private void shareProfile() {
        if (getActivity() != null && viewModel.user.getValue() != null) {
            String json = new Gson().toJson(viewModel.user.getValue());
            String title = viewModel.user.getValue().getData().getFullName();

            Log.i("ShareJson", "Json Object: " + json);
            BranchUniversalObject buo = new BranchUniversalObject()
                    .setCanonicalIdentifier("content/12345")
                    .setTitle(title)
                    .setContentImageUrl(Const.ITEM_BASE_URL + viewModel.user.getValue().getData().getUserProfile())
                    .setContentDescription("Hey There, Check This " + getActivity().getResources().getString(R.string.app_name) + " Profile")
                    .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                    .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                    .setContentMetadata(new ContentMetadata().addCustomMetadata("data", json));

            LinkProperties lp = new LinkProperties()
                    .setFeature("sharing")
                    .setCampaign("Content launch")
                    .setStage("User")
                    .addControlParameter("custom", "data")
                    .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

            buo.generateShortUrl(getActivity(), lp, (url, error) -> {

                Log.d("PROFILEURL", "shareProfile: " + url);

                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                String shareBody = url + "\nThis Profile Is Amazing On " + getActivity().getResources().getString(R.string.app_name) + " App";
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Profile Share");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Profile"));
            });
        }
    }

    private void handleFollowerClick(int itemType) {

        Intent intent = new Intent(getActivity(), FollowerFollowingActivity.class);
        intent.putExtra("itemtype", itemType);
        intent.putExtra("user", new Gson().toJson(viewModel.user.getValue()));
        startActivity(intent);

    }

    private void handleButtonClick() {

        Log.v("paras", "paras btn text = " + binding.btnFollow.getText().toString());
        String btnText = binding.btnFollow.getText().toString();
        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            viewModel.followUnfollow();

        } else {
            Toast.makeText(getContext(), "You have to login first", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleChatClick() {

        if (sessionManager.getBooleanValue(Const.IS_LOGIN)) {
            if (viewModel.isMyAccount.get() == 2 || viewModel.isMyAccount.get() == 1) {

//                if(isMutualFollower && viewModel.user.getValue().getData().getIsFollowing()==1)
//                {
                Utils.CURRENT_RECEIVER_ID = userId;
                Utils.CURRENT_RECEIVER_NAME = viewModel.user.getValue().getData().getFullName();
                Utils.CURRENT_RECEIVER_IMAGE = viewModel.user.getValue().getData().getUserProfile();
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                startActivity(intent);
//                }
//                else
//                {
//                    showAlert(msg);
//                }

            } else {

                startActivity(new Intent(getActivity(), ChatListActivity.class));
            }
        } else {
            Toast.makeText(getContext(), "You have to login first", Toast.LENGTH_SHORT).show();
        }
    }


    private void initListener() {
        binding.btnFollow.setOnClickListener(v -> handleButtonClick());

        binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewModel.isLikedVideos.set(position == 1);
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == UPDATE_DATA) {
                if (data != null) {
                    viewModel.user.setValue(new Gson().fromJson(data.getStringExtra("user"), User.class));
                    binding.setViewModel(viewModel);

                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null) {
            parentViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new ProfileViewModel()).createFor()).get(ProfileViewModel.class);

        initView();
        initObserver();
        initListener();
        handleVisibility();
        if (userId != null && userId.equals(Global.USER_ID)) {
            binding.btnFollow.setVisibility(View.GONE);
        } else {
            binding.btnFollow.setVisibility(View.VISIBLE);
        }
        Log.i("timeeee111 ", viewModel.getTotalTimeSpent());

        binding.setViewModel(viewModel);

    }

    public void checkMutualFollowers(String followId, Context context) {
        PrefHandler prefHandler = new PrefHandler(getActivity());
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("followerId", followId));
        params.add(new Pair<>("userId", prefHandler.getuId()));

        new AsyncHttpsRequest("", context, params, this, 0, false).execute(Utils.CHECKMUTUALFOLLOW);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if (response_code == 0) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                msg = obj.getString("success_msg");

                isMutualFollower = success == 1;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public void showAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showAlertWallet(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                CoinPurchaseSheetFragment fragment = new CoinPurchaseSheetFragment();
                fragment.show(getActivity().getSupportFragmentManager(), fragment.getClass().getSimpleName());
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showOkCancelAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                openDialerActivity();
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
