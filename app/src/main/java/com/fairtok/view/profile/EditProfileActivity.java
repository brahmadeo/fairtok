package com.fairtok.view.profile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.databinding.ActivityEditProfileBinding;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.view.ChangeLanguageSelection;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.media.BottomSheetImagePicker;
import com.fairtok.viewmodel.EditProfileViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;

import static com.fairtok.utils.BindingAdapters.loadMediaImage;
import static com.fairtok.utils.BindingAdapters.loadMediaImage1;

public class EditProfileActivity extends BaseActivity implements CompleteTaskListner {

    ActivityEditProfileBinding binding;
    private EditProfileViewModel viewModel;
    PrefHandler pref;
    CustomDialogBuilder customDialogBuilder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        startReceiver();
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new EditProfileViewModel()).createFor()).get(EditProfileViewModel.class);
        initView();
        initObserve();
        initListener();
        binding.setViewmodel(viewModel);
        pref = new PrefHandler(this);

        customDialogBuilder = new CustomDialogBuilder(this);

        binding.btnEdit.setOnClickListener(view -> {

            viewModel.updateUser(view.getContext(), binding.rbMale.isChecked()?"1":"2",customDialogBuilder);
        });

        binding.tvLang.setText(pref.getLanguage());

        if (sessionManager.getUser().getData().getGender()!=null){
            if (sessionManager.getUser().getData().getGender().equals("1"))
                binding.rbMale.setChecked(true);
            else
                binding.rbFemale.setChecked(true);
        }

        binding.btnChangeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditProfileActivity.this, ChangeLanguageSelection.class));
            }
        });

        binding.btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removePic(view.getContext());
            }
        });

        binding.edUserName.setFilters(new InputFilter[] { filter });
    }

    /* To restrict Space Bar in Keyboard */
    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    Toast.makeText(getApplicationContext(),"Space is not allowed in UserName",Toast.LENGTH_SHORT).show();
                    return "";
                }
            }
            return null;
        }

    };


    public void removePic(Context context) {
        PrefHandler prefHandler = new PrefHandler(context);
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag","removePic"));
        params.add(new Pair<>("userid", prefHandler.getuId()));

        new AsyncHttpsRequest("Wait...!", context, params, this, 0, false).execute(Utils.REMOVE_PIC);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response list = " + result);

        if(response_code == 0)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                String msg = obj.getString("success_msg");
                showAlertMsg(msg);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }

    private void initView() {
        viewModel.user = sessionManager.getUser();
        Log.i("profileUrlll ", Const.BASE_URL_IMAGE+sessionManager.getUser().getData().getUserProfile());
        Glide.with(this).load(Const.BASE_URL_IMAGE+sessionManager.getUser().getData().getUserProfile()).into(binding.profileImg);
        viewModel.updateData();
        if (viewModel.user != null) {
            viewModel.cur_userName = sessionManager.getUser().getData().getUserName();
            if (viewModel.user.getData().getBio() != null && !viewModel.user.getData().getBio().isEmpty()) {
                viewModel.length.set(viewModel.user.getData().getBio().length());
            }
        }
    }

    private void initObserve() {
        viewModel.toast.observe(this, s -> {
            if (s != null && !s.isEmpty()) {
                Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
            }
        });
        viewModel.updateProfile.observe(this, isUpdate -> {
            if (isUpdate != null && isUpdate) {
                customDialogBuilder.hideLoadingDialog();
                showAlert("Your Profile has been successfully updated.");

            }
        });
    }

    public void showAlert(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(EditProfileActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                Intent intent = new Intent();
                intent.putExtra("user", new Gson().toJson(viewModel.user));
                sessionManager.saveUser(viewModel.user);
                setResult(RESULT_OK, intent);
                onBackPressed();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void showAlertMsg(String msg) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(EditProfileActivity.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(msg);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                dialog.dismiss();
                viewModel.user.getData().setUserProfile("user_default_img.png");
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void initListener() {
        binding.setOnChangeClick(view ->
                showPhotoSelectSheet()
        );

        binding.imgBack.setOnClickListener(v -> onBackPressed());
    }


    private List<String> urlList = new ArrayList<>();
    private void showPhotoSelectSheet() {
        //if (urlList.size()<=2){
            BottomSheetImagePicker bottomSheetImagePicker = new BottomSheetImagePicker();
            bottomSheetImagePicker.setOnDismiss(uri -> {
                if (!uri.isEmpty()) {
                    loadMediaImage(binding.profileImg, uri, true);
                    //urlList.add(uri);
                    //Log.i("imageUriiiii ", urlList.toString());
                    viewModel.imageUri = uri;
                }
            });
            bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());
//        }else {
//            Log.i("imageUriiiii ", "already image taken");
//        }

    }

    @Override
    public void onDestroy() {
        unregisterNetworkChanges();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.tvLang.setText(pref.getLanguage());
        viewModel.language=pref.getLanguage();
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}