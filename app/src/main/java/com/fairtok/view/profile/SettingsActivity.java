package com.fairtok.view.profile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.login.LoginManager;
import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.ActivitySettingsBinding;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.view.SplashActivity;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.wallet.WalletActivity;
import com.fairtok.viewmodel.SettingsActivityViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
/*import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;*/
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.util.ContentMetadata;
import io.branch.referral.util.LinkProperties;

public class SettingsActivity extends BaseActivity {

    ActivitySettingsBinding binding;
    SettingsActivityViewModel viewModel;
    PrefHandler prefHandler;
    private String TAG = "settingsactivity";
    private Uri mInvitationUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new SettingsActivityViewModel()).createFor()).get(SettingsActivityViewModel.class);
        prefHandler = new PrefHandler(this);
        createDynamicLink();
        initListeners();
        initObserve();
        statusBar();
        binding.notiSwitch.setChecked(sessionManager.getBooleanValue("notification"));
    }
    private void statusBar(){
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPink));
    }

    private void initListeners() {
        binding.notiSwitch.setOnClickListener(v -> {
            if (binding.notiSwitch.isChecked()) {
                if (!Global.FIREBASE_DEVICE_TOKEN.isEmpty()) {
                    viewModel.updateToken(Global.FIREBASE_DEVICE_TOKEN);
                    sessionManager.saveBooleanValue("notification", true);
                }
            } else {
                viewModel.updateToken(" ");
                sessionManager.saveBooleanValue("notification", false);
            }
        });

        binding.loutEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SettingsActivity.this,EditProfileActivity.class));
            }
        });
        binding.loutPrivacy.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_policy)));
            startActivity(browserIntent);
//            Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
//            startActivity(intent);
        });

        binding.loutTermsUse.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.terms_conditions)));
            startActivity(browserIntent);
//            Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
//            startActivity(intent);
        });

        binding.loutContactUs.setOnClickListener(v -> {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_us)});
            //emailIntent.putExtra(Intent.EXTRA_SUBJECT, getPackageName());
            emailIntent.setType("text/plain");
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
            ResolveInfo best = null;
            for(final ResolveInfo info : matches)
                if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                    best = info;
            if (best != null)
                emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
            startActivity(emailIntent);
        });

        binding.loutShareProfile.setOnClickListener(v -> shareProfile());
        binding.imgBack.setOnClickListener(v -> onBackPressed());
        binding.loutWallet.setOnClickListener(v -> startActivity(new Intent(SettingsActivity.this, WalletActivity.class)));
        binding.loutVerify.setOnClickListener(v -> {
            if (sessionManager.getUser().getData().getFollowersCount() >= prefHandler.getFollowersThreshold()) {
                startActivity(new Intent(SettingsActivity.this, VerificationActivity.class));
            } else {
                Toast.makeText(this, "You can verify your profile once you have "+prefHandler.getFollowersThreshold()+" Followers...", Toast.LENGTH_SHORT).show();
            }
        });
        binding.loutMycode.setOnClickListener(v -> startActivity(new Intent(SettingsActivity.this, MyQRActivity.class)));

        binding.loutLogout.setOnClickListener(v -> new CustomDialogBuilder(this).showLogoutDialog("Log out", "Do you really want\nto log out?", "Cancel", "Log out", new CustomDialogBuilder.OnDismissListener() {
            @Override
            public void onPositiveDismiss() {

                viewModel.logOutUser();
                prefHandler.clearPreference();
            }

            @Override
            public void onNegativeDismiss() {

            }
        }));


        binding.llReferEarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //final String appLink = "https://play.google.com/store/apps/details?id=" + getPackageName()+"&ref="+Global.USER_ID;
                //final String appLink = "\nhttps://fairtok.com/"+Global.USER_ID;

                Intent sendInt = new Intent(Intent.ACTION_SEND);
                sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                sendInt.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_message) + "\n"+mInvitationUrl);
                sendInt.setType("text/plain");
                startActivity(Intent.createChooser(sendInt, "Share"));
            }
        });
    }

    private void createDynamicLink(){
        String link = "https://fairtok.com/User="+Global.USER_ID;
        String imgUrl = Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile();
        URL url;
        Uri uri = null;
        try {
            url = new URL(imgUrl);
            uri = Uri.parse( url.toURI().toString() );
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(link))
                .setDomainUriPrefix("https://fairtok.page.link")
                //.setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder().setTitle("title").setImageUrl(uri).build())
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(
                        new DynamicLink.IosParameters.Builder("com.example.ios")
                                .setAppStoreId("123456789")
                                .setMinimumVersion("1.0.1")
                                .build())
                .buildShortDynamicLink()
                .addOnSuccessListener(new OnSuccessListener<ShortDynamicLink>() {
                    @Override
                    public void onSuccess(ShortDynamicLink shortDynamicLink) {
                        mInvitationUrl = shortDynamicLink.getShortLink();
                        Log.i(TAG, mInvitationUrl.toString());
                        // ...
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, e.getMessage());
                    }
                });

    }
    private void shareProfile() {

        String json = new Gson().toJson(sessionManager.getUser());
        String title = sessionManager.getUser().getData().getFullName();

        Log.i("ShareJson", "Json Object: " + Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile());
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(title)
                .setContentImageUrl(Const.BASE_URL_IMAGE + sessionManager.getUser().getData().getUserProfile())
                .setContentDescription("Hey There, Check This " + getResources().getString(R.string.app_name) + " Profile")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("data", json));

        LinkProperties lp = new LinkProperties()
                .setFeature("sharing")
                .setCampaign("Content launch")
                .setStage("User")
                .addControlParameter("custom", "data")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        buo.generateShortUrl(this, lp, (url, error) -> {

            Log.d("PROFILEURL", "shareProfile: " + url);

            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            String shareBody = url + "\nHey, check my profile on " + getResources().getString(R.string.app_name) + " App";
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Profile Share");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(intent, "Share Profile"));
        });

    }
    private void shareEarn() {

        String json = new Gson().toJson(sessionManager.getUser());
        String title = sessionManager.getUser().getData().getFullName();

        Log.i("ShareJson", "Json Object: " + Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile());
        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("content/12345")
                .setTitle(title)
                .setContentImageUrl(Const.ITEM_BASE_URL + sessionManager.getUser().getData().getUserProfile())
                .setContentDescription("Hey There, Check This " + getResources().getString(R.string.app_name) + " Profile")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
                .setContentMetadata(new ContentMetadata().addCustomMetadata("data", json));

        LinkProperties lp = new LinkProperties()
                .setFeature("sharing")
                .setCampaign("Content launch")
                .setStage("User")
                .addControlParameter("custom", "data")
                .addControlParameter("custom_random", Long.toString(Calendar.getInstance().getTimeInMillis()));

        buo.generateShortUrl(this, lp, (url, error) -> {

            Log.d("PROFILEURL", "shareProfile: " + url);

            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            String shareBody = url + "\nHey, check my profile on " + getResources().getString(R.string.app_name) + " App";
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Profile Share");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(intent, "Share Profile"));
        });

    }

    private void initObserve() {
        viewModel.logOut.observe(this, logout -> logOutUser());
        viewModel.updateToken.observe(this, updateToken -> {
            if (binding.notiSwitch.isChecked()) {
                Toast.makeText(this, "Notifications Turned On", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Notifications Turned Off", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void logOutUser() {
        if (sessionManager.getUser().getData().getLoginType().equals("google")) {
            GoogleSignInOptions gso = new GoogleSignInOptions.
                    Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                    build();

            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(SettingsActivity.this, gso);
            googleSignInClient.signOut();

        } else {
            LoginManager.getInstance().logOut();
        }

        sessionManager.clear();
        prefHandler.clearPreference();
        Global.ACCESS_TOKEN = "";
        Global.USER_ID = "";
        startActivity(new Intent(SettingsActivity.this, SplashActivity.class));
        finishAffinity();
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}