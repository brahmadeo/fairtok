package com.fairtok.view.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.FragmentLikedVideosBinding;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.viewmodel.LikedVideosViewModel;
import com.fairtok.viewmodel.ProfileViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

import org.jetbrains.annotations.NotNull;


public class LikedVideosFragment extends Fragment {

    FragmentLikedVideosBinding binding;
    LikedVideosViewModel viewModel;
    ProfileViewModel parentViewModel;
    PrefHandler prefHandler;

    public static LikedVideosFragment getNewInstance(int vidType) {
        LikedVideosFragment fragment = new LikedVideosFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", vidType);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_liked_videos, container, false);
        prefHandler = new PrefHandler(getActivity());
        return binding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getParentFragment() != null) {
            parentViewModel = ViewModelProviders.of(getParentFragment()).get(ProfileViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new LikedVideosViewModel()).createFor()).get(LikedVideosViewModel.class);
        initView();
        initListeners();
        initObserve();
        binding.setViewModel(viewModel);
    }

    private void initView() {
        if (getArguments() != null) {
            viewModel.vidType = getArguments().getInt("type");
        }

        prefHandler = new PrefHandler(getActivity());
        viewModel.userId = parentViewModel.userId;

        viewModel.likeVidStart = 0;
        viewModel.fetchUserLikedVideos(false,getActivity());

        binding.refreshlout.setEnableRefresh(false);

    }

    private void initObserve() {
        prefHandler = new PrefHandler(getActivity());
        parentViewModel.selectPosition.observe(getViewLifecycleOwner(), position -> {
            if (position != null && position == 3) {
                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fall_in);
                binding.recyclerview.startAnimation(animation);
                if (viewModel.userId == null || viewModel.userId.isEmpty()) {
                    viewModel.userId = Global.USER_ID;
                }


                viewModel.likeVidStart = 0;
                viewModel.fetchUserLikedVideos(false,getActivity());

                binding.setViewModel(viewModel);
//                parentViewModel.selectPosition.setValue(null);
            }
        });
        viewModel.onLoadMoreComplete.observe(this, onLoadMore -> binding.refreshlout.finishLoadMore());

        viewModel.noDraftVideos.observe(getViewLifecycleOwner(), noDraftVideos -> {
            if(noDraftVideos)
            {
                binding.noData.setVisibility(View.VISIBLE);
            }
            else
            {
                binding.noData.setVisibility(View.GONE);
            }
        });

        viewModel.noPrivateVideos.observe(getViewLifecycleOwner(), noDraftVideos -> {
            if(noDraftVideos)
            {
                binding.noData.setVisibility(View.VISIBLE);
            }
            else
            {
                binding.noData.setVisibility(View.GONE);
            }
        });
    }

    private void initListeners() {
        viewModel.adapter.onRecyclerViewItemClick = (model, position, binding) -> new CustomDialogBuilder(getContext()).showSimpleDialog("Delete post !", "Do you really want to\ndelete this post?", "Cancel", "yes", new CustomDialogBuilder.OnDismissListener() {
            @Override
            public void onPositiveDismiss() {
                viewModel.deletePost(model.getPostId(), position);
            }

            @Override
            public void onNegativeDismiss() {

            }
        });

        binding.refreshlout.setOnLoadMoreListener(refreshLayout -> {
             if (viewModel.vidType == 1){
                viewModel.onUserLikedVideoLoadMore(getActivity());
            }
            else if (viewModel.vidType == 2){
                viewModel.onUserDraftVideoLoadMore(prefHandler);
            }
            else if (viewModel.vidType == 3){
                viewModel.onUserPrivateVideoLoadMore(prefHandler);
            }
        });
    }


}