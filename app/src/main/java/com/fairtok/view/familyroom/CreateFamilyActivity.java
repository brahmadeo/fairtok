package com.fairtok.view.familyroom;

import static com.fairtok.utils.BindingAdapters.loadMediaImage;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.ActivityCreateFamilyBinding;
import com.fairtok.view.media.BottomSheetImagePicker;
import com.fairtok.viewmodel.CreateFamilyViewModel;
import com.fairtok.viewmodel.JoinfamilyViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

public class CreateFamilyActivity extends AppCompatActivity implements View.OnClickListener{

    private CreateFamilyViewModel viewModel;
    private ActivityCreateFamilyBinding binding;
    private ActivityResultLauncher activityResultLauncher;
    private PrefHandler prefHandler;
    public static String family;
    public static String taillight;
    public static String familyTag;
    public static String familyLocation;
    public static String familysettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_family);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_family);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new CreateFamilyViewModel()).createFor()).get(CreateFamilyViewModel.class);

        prefHandler = new PrefHandler(this);
        initView();
        initObserver();

        binding.tvFamilyName.setText(family);
        binding.tvTaillight.setText(taillight);
        binding.familyTag.setText(familyTag);
        binding.familyLoc.setText(familyLocation);
        binding.tvSettings.setText(familysettings);


        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(), result -> {
                    Log.i("resultttttt ", result.toString());
                }
        );
    }
    private void initView(){
        binding.linFamilyName.setOnClickListener(view -> {
            startActivity(0);
        });
        binding.linFamilyTailight.setOnClickListener(view -> {
            startActivity(1);
        });
        binding.linFamilyTag.setOnClickListener(view -> {
            //startActivity(2);
            startActivity(new Intent(CreateFamilyActivity.this, TaglistActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });
        binding.linLocation.setOnClickListener(view -> {
            startActivity(3);
        });
        binding.linSettings.setOnClickListener(view -> {
            // open modal to show settings
            startActivity(4);
        });

        binding.linCover.setOnClickListener(view -> {
            showPhotoSelectSheet();
        });
        binding.btnSubmit.setOnClickListener(view -> {
            if (validate()){
                Toast.makeText(CreateFamilyActivity.this, "validated and ready to api call", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void startActivity(int position){
        activityResultLauncher.launch(new Intent(this, FillActivity.class).putExtra("position", position));
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
    private void initObserver(){

    }
    private void showPhotoSelectSheet() {
        BottomSheetImagePicker bottomSheetImagePicker = new BottomSheetImagePicker();
        bottomSheetImagePicker.setOnDismiss(uri -> {
            if (!uri.isEmpty()) {
                binding.profileImage.setVisibility(View.VISIBLE);
                binding.cameraIcon.setVisibility(View.GONE);
                loadMediaImage(binding.profileImage, uri, false);
                viewModel.imageUri = uri;
            }
        });
        bottomSheetImagePicker.show(getSupportFragmentManager(), BottomSheetImagePicker.class.getSimpleName());
    }
    private boolean validate(){
        if (binding.tvFamilyName.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter family name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (binding.tvTaillight.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter family taillight", Toast.LENGTH_SHORT).show();
            return false;
        }else if (binding.familyTag.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter family Tag", Toast.LENGTH_SHORT).show();
            return false;
        }else if (binding.familyLoc.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter family location", Toast.LENGTH_SHORT).show();
            return false;
        }if (binding.tvSettings.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter family settings", Toast.LENGTH_SHORT).show();
            return false;
        }else if (binding.edtComment.getText().toString().trim().equals("")){
            Toast.makeText(CreateFamilyActivity.this, "Enter bullets", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.linFamilyName:

            break;
            case R.id.linFamilyTailight:

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}