package com.fairtok.view.familyroom.Model;

public class FillModel {
    String title;
    String description;

    public FillModel(String title, String description){
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
