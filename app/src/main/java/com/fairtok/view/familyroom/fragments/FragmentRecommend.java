package com.fairtok.view.familyroom.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairtok.R;
import com.fairtok.adapter.RecommendFamilyAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.FragmentRecommendBinding;
import com.fairtok.viewmodel.JoinfamilyViewModel;
import com.fairtok.viewmodel.RecommendFamilyViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentRecommend#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRecommend extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    //private static final int ARG_PARAM1 = 0;
    //private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    public FragmentRecommend() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentRecommend.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRecommend newInstance(int param1, String param2) {
        FragmentRecommend fragment = new FragmentRecommend();
        Bundle args = new Bundle();
        args.putInt("type", param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt("type");
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private FragmentRecommendBinding binding;
    private RecommendFamilyViewModel viewModel;
    private JoinfamilyViewModel parentViewModel;
    private PrefHandler prefHandler;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recommend, container, false);

        initView();
        return binding.getRoot();
    }
    private void initView(){
        prefHandler = new PrefHandler(getActivity());
        viewModel.adapter = new RecommendFamilyAdapter();
        //viewModel.userVidStart = 0;
        //viewModel.fetchUserVideos(false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getParentFragment() != null) {
            parentViewModel = ViewModelProviders.of(getParentFragment()).get(JoinfamilyViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new RecommendFamilyViewModel()).createFor()).get(RecommendFamilyViewModel.class);
    }
}