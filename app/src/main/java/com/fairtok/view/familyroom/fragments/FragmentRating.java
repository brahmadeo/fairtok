package com.fairtok.view.familyroom.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.fairtok.R;
import com.fairtok.adapter.RatingFamilyAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.databinding.FragmentRatingBinding;
import com.fairtok.viewmodel.JoinfamilyViewModel;
import com.fairtok.viewmodel.RatingFamilyViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentRating#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRating extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    public FragmentRating() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentRating.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRating newInstance(int param1, String param2) {
        FragmentRating fragment = new FragmentRating();
        Bundle args = new Bundle();
        args.putInt("type", param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private PrefHandler prefHandler;
    private FragmentRatingBinding binding;
    private JoinfamilyViewModel parentViewModel;
    private RatingFamilyViewModel viewModel;
    private AlertDialog alertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rating, container, false);
        initView();
        AlertDialog();
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getParentFragment()!=null){
            parentViewModel = ViewModelProviders.of(getParentFragment()).get(JoinfamilyViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new RatingFamilyViewModel()).createFor()).get(RatingFamilyViewModel.class);

    }

    private void initView(){
        prefHandler = new PrefHandler(getActivity());
        viewModel.adapter = new RatingFamilyAdapter();
    }
    public void AlertDialog() {
        LayoutInflater m_inflater = LayoutInflater.from(getContext());

        View layout = m_inflater.inflate(R.layout.custom_dialog_ranking, null);
        AppCompatButton btnSubmit = layout.findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(view -> {
            alertDialog.dismiss();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //AlertDialog();
    }
}