package com.fairtok.view.familyroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.fairtok.R;
import com.fairtok.adapter.MomentSingPopularAdapter;
import com.fairtok.databinding.ActivityMomentSingBinding;
import com.fairtok.databinding.ActivityMomentSingBindingImpl;
import com.fairtok.view.familyroom.fragments.FragmentMoment;
import com.fairtok.view.familyroom.fragments.FragmentRating;
import com.fairtok.view.familyroom.fragments.FragmentRecommend;
import com.fairtok.view.familyroom.fragments.FragmentSinging;
import com.fairtok.viewmodel.MomentSingViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.tabs.TabLayout;

public class MomentSingActivity extends AppCompatActivity {

    private MomentSingViewModel viewModel;
    private ActivityMomentSingBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_moment_sing);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new MomentSingViewModel()).createFor()).get(MomentSingViewModel.class);

        initView();

        setViewPager();
        binding.setViewModel(viewModel);
    }
    private void initView(){
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Moment"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Let's Sing"));

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerView.setAdapter(new MomentSingPopularAdapter());
    }

    private void setViewPager(){
        //final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getSupportFragmentManager(), binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private int number_of_tabs;
        public ViewPagerAdapter(FragmentManager fm, int number_of_tabs){
            super(fm);
            this.number_of_tabs = number_of_tabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment tab = null;
            switch (position){
                case 0:
                    return FragmentMoment.newInstance("", "");
                case 1:
                    return FragmentSinging.newInstance("", "");
            }
            return null;
        }

        @Override
        public int getCount() {
            return number_of_tabs;
        }
    }
}