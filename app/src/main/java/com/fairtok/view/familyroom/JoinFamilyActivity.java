package com.fairtok.view.familyroom;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import com.fairtok.R;
import com.fairtok.databinding.ActivityJoinFamilyBinding;
import com.fairtok.view.familyroom.fragments.FragmentRating;
import com.fairtok.view.familyroom.fragments.FragmentRecommend;
import com.fairtok.viewmodel.JoinfamilyViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.material.tabs.TabLayout;

public class JoinFamilyActivity extends AppCompatActivity {
    private ActivityJoinFamilyBinding binding;
    private JoinfamilyViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_join_family);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new JoinfamilyViewModel()).createFor()).get(JoinfamilyViewModel.class);

        /*viewModel.onItemClick.observe(this, type->{
            binding.tvText.setText(type);
        });

        binding.btnClick.setOnClickListener(view -> {
            viewModel.setOnItemClick(1);
        });*/

        initView();
        initObserver();
        setViewPager();
        binding.setViewModel(viewModel);
    }
    private void initView(){
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Recomending"));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Rating"));

        binding.arraowBack.setOnClickListener(view -> {
            finish();
        });
        binding.btnCreateFamily.setOnClickListener(view -> {
            startActivity(new Intent(JoinFamilyActivity.this, CreateFamilyActivity.class));
        });
    }
    private void initObserver(){

    }
    private void setViewPager(){

        final ViewPagerAdapter adapter = new ViewPagerAdapter
                (getSupportFragmentManager(), binding.tabLayout.getTabCount());
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter{

        private int number_of_tabs;
        public ViewPagerAdapter(FragmentManager fm, int number_of_tabs){
            super(fm);
            this.number_of_tabs = number_of_tabs;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment tab = null;
            switch (position){
                case 0:
                    return FragmentRecommend.newInstance(position, "");
                case 1:
                    return FragmentRating.newInstance(position, "");
            }
            return null;
        }

        @Override
        public int getCount() {
            return number_of_tabs;
        }
    }
}