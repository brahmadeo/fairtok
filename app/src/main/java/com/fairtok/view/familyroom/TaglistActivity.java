package com.fairtok.view.familyroom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fairtok.R;
import com.fairtok.adapter.TaglistAdapter;
import com.fairtok.view.familyroom.Model.TagModel;

import java.util.ArrayList;
import java.util.List;

public class TaglistActivity extends AppCompatActivity implements TaglistAdapter.clickListener {

    private TextView tvTitle, tvDescription;
    private RecyclerView recyclerView;
    private List<TagModel> tagModelList;
    private LinearLayout back_arrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taglist);

        prepareData();
        initView();
    }
    private void prepareData(){
        tagModelList = new ArrayList<TagModel>();
        tagModelList.add(new TagModel("English"));
        tagModelList.add(new TagModel("Hindi"));
        tagModelList.add(new TagModel("Bengali"));
        tagModelList.add(new TagModel("Gujrati"));
        tagModelList.add(new TagModel("Malyalam"));
        tagModelList.add(new TagModel("Telugu"));
    }
    private void initView(){
        tvTitle = findViewById(R.id.tvTitle);
        tvDescription = findViewById(R.id.tvDescription);
        recyclerView = findViewById(R.id.recyclerview);
        back_arrow = findViewById(R.id.back_arrow);

        back_arrow.setOnClickListener(view -> {
            finish();
        });

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL,  false));
        recyclerView.setAdapter(new TaglistAdapter(this, tagModelList));

    }

    @Override
    public void itemClick(String name) {
        CreateFamilyActivity.familyTag = name;
        startActivity(new Intent(this, CreateFamilyActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, CreateFamilyActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}