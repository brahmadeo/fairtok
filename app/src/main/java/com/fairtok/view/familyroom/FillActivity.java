package com.fairtok.view.familyroom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fairtok.R;
import com.fairtok.chat.PrefHandler;
import com.fairtok.view.familyroom.Model.FillModel;
import com.googlecode.mp4parser.authoring.Edit;

import java.util.ArrayList;
import java.util.List;

public class FillActivity extends AppCompatActivity implements View.OnClickListener{
    private LinearLayout back_arrow;
    private AppCompatButton btnSubmit;
    private TextView tvTitle, tvDescription;
    private EditText edtComment;
    private List<FillModel> mList;
    private int position;
    private PrefHandler prefHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill);

        prefHandler = new PrefHandler(this);
        prepareData();
        initView();
    }
    private void initView(){
        back_arrow = findViewById(R.id.back_arrow);
        btnSubmit = findViewById(R.id.btn_submit);
        edtComment = findViewById(R.id.edtComment);
        tvTitle = findViewById(R.id.tvTitle);
        tvDescription = findViewById(R.id.tvDescription);

        btnSubmit.setOnClickListener(this);
        back_arrow.setOnClickListener(this);

        if (getIntent().getExtras()!=null){
            position = getIntent().getExtras().getInt("position");

            tvTitle.setText(mList.get(position).getTitle());
            tvDescription.setText(mList.get(position).getDescription());
        }
        
    }
    private void prepareData(){
        mList = new ArrayList<FillModel>();
        mList.add(new FillModel("Family Name", "When the family name is settled it can't be changed again within three months"));
        mList.add(new FillModel("Family Slogan", "Family taillight will be shown after family name, it can't be changed again within three months"));
        mList.add(new FillModel("Family Tag", "Add Family Tag"));
        mList.add(new FillModel("Family Location", "Add location as per your choice"));
        mList.add(new FillModel("Family Settings", "Add settings"));
        mList.add(new FillModel("Bulletin", "Do daily task and earn rewards. This family is glad to have you!"));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_submit:
                switch (position){
                    case 0:
                        CreateFamilyActivity.family = edtComment.getText().toString().trim();
                        break;
                    case 1:
                        CreateFamilyActivity.taillight = edtComment.getText().toString().trim();
                        break;
                    case 3:
                        CreateFamilyActivity.familyLocation = edtComment.getText().toString().trim();
                        break;
                    case 4:
                        CreateFamilyActivity.familysettings = edtComment.getText().toString().trim();
                        break;
                }

                startActivity(new Intent(this, CreateFamilyActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, CreateFamilyActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}