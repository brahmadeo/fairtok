package com.fairtok.view.familyroom.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fairtok.R;
import com.fairtok.adapter.SingingAdapter;
import com.fairtok.databinding.FragmentSingingBinding;
import com.fairtok.viewmodel.MomentSingViewModel;
import com.fairtok.viewmodel.SingingViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSinging#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSinging extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentSinging() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSinging.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSinging newInstance(String param1, String param2) {
        FragmentSinging fragment = new FragmentSinging();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private FragmentSingingBinding binding;
    private MomentSingViewModel parentViewModel;
    private SingingViewModel viewModel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_singing, container, false);
        initView();
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getParentFragment()!=null){
            parentViewModel = ViewModelProviders.of(getParentFragment()).get(MomentSingViewModel.class);
        }
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new SingingViewModel()).createFor()).get(SingingViewModel.class);
    }

    private void initView(){
        viewModel.adapter = new SingingAdapter();
    }
}