package com.fairtok.view.familyroom.Model;

public class TagModel {
    String name;

    public TagModel(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
