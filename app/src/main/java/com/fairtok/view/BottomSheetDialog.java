package com.fairtok.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fairtok.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
  
public class BottomSheetDialog extends BottomSheetDialogFragment { 
  
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    { 
        View v = inflater.inflate(R.layout.live_game_option, container, false);

        AppCompatTextView algo_button = v.findViewById(R.id.tvLive);
        AppCompatTextView course_button = v.findViewById(R.id.tvGame);
  
        algo_button.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View v) 
            {
                Intent intent = new Intent("fairtok-click-listener");
                intent.putExtra("isGame",false);
                //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
                LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);
            } 
        }); 
  
        course_button.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View v) 
            {
                Intent intent = new Intent("fairtok-click-listener");
                intent.putExtra("isGame",true);
                //start live stream activity
//                binding.getRoot().getContext().startActivity(intent);
                LocalBroadcastManager.getInstance(v.getContext()).sendBroadcast(intent);
            } 
        }); 
        return v; 
    } 
} 