package com.fairtok.view.preview;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.AudioAttributes;
import android.media.MediaMetadataRetriever;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.arthenica.mobileffmpeg.Config;
import com.arthenica.mobileffmpeg.ExecuteCallback;
import com.arthenica.mobileffmpeg.FFmpeg;
import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.egl.filter.GlWatermarkFilter;
import com.fairtok.MyApplication;
import com.fairtok.R;
import com.fairtok.chat.LoadAds;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.databinding.ActivityPreviewBinding;
import com.fairtok.databinding.ItemUploadSheetBinding;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.services.ServiceCallback;
import com.fairtok.services.Upload_Service;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.fairtok.view.base.BaseActivity;
import com.fairtok.view.home.MainActivity;
import com.fairtok.viewmodel.PreviewViewModel;
import com.fairtok.viewmodelfactory.ViewModelFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.agora.rtm.RtmChannelMember;
import io.agora.rtm.RtmFileMessage;
import io.agora.rtm.RtmImageMessage;
import io.reactivex.disposables.CompositeDisposable;

import static android.content.ContentValues.TAG;
import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_CANCEL;
import static com.arthenica.mobileffmpeg.Config.RETURN_CODE_SUCCESS;

public class PreviewActivity extends BaseActivity implements ServiceCallback {

    private ActivityPreviewBinding binding;
    private PreviewViewModel viewModel;
    private CustomDialogBuilder customDialogBuilder;
    private SimpleExoPlayer player;
    private CompositeDisposable disposable = new CompositeDisposable();
    private String temp = "";
    ServiceCallback serviceCallback;
    public static int  select_postion=0;
    public static boolean applyWaterMark=false;
    private RangeSeekBar rangeSeekBar;
    public Uri selectedVideoUri;
    private Runnable r;
    public static boolean isFastMotion=false;
    public static boolean isSlowMotion=false;


    File outputFadInOutFile, outputSlowMotionFile,outputFastMotionFile,outputReverseFile,currentFileToUpload,TrimmedFile;
    static File OriginalFile,outPutTrimmedFile;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_preview);
        viewModel = ViewModelProviders.of(this, new ViewModelFactory(new PreviewViewModel()).createFor()).get(PreviewViewModel.class);
        customDialogBuilder = new CustomDialogBuilder(this);

        PrefHandler prefHandler = new PrefHandler(this);
        viewModel.prefHandler = prefHandler;

        viewModel.sessionManager = sessionManager;

        initView();

        initObserve();
        showToast();
        startUploading();
        initListener();

        if(isFastMotion==true)
        {
            isFastMotion=false;
            isSlowMotion=false;

            fastMotion(OriginalFile);
        }
        else if(isSlowMotion==true)
        {
            isFastMotion=false;
            isSlowMotion=false;

            slowMotion(OriginalFile);
        }
        else {
            playVideo(OriginalFile);
        }
    }


    private void initView() {
        viewModel.videoPath = getIntent().getStringExtra("post_video");
        viewModel.videoThumbnail = getIntent().getStringExtra("post_image");
        viewModel.soundPath = getIntent().getStringExtra("post_sound");
        viewModel.soundImage = getIntent().getStringExtra("sound_image");
        viewModel.soundId = getIntent().getStringExtra("soundId");
        viewModel.soundBy = getIntent().getStringExtra("soundBy");
        viewModel.isFront = getIntent().getBooleanExtra("is_front", false);

        rangeSeekBar = (RangeSeekBar) findViewById(R.id.rangeSeekBar);

        OriginalFile = new File(viewModel.videoPath);
        selectedVideoUri = Uri.fromFile(OriginalFile);

        binding.btnOriginal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                currentFileToUpload=OriginalFile;
                playVideo(currentFileToUpload);
            }
        });

        binding.btnFast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                fastMotion(OriginalFile);
            }
        });

        binding.btnSlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                slowMotion(OriginalFile);
            }
        });

        binding.btnFade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                fadInFadOut(OriginalFile);
            }
        });

        binding.btnRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                splitVideoCommand(OriginalFile.getPath());
            }
        });

        binding.btnTrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player.stop();
                //startActivityForResult(new Intent(PreviewActivity.this,VideoTrimmerActivity.class),111);
                startActivityForResult(new Intent(PreviewActivity.this, VideoTrimmerActivity.class).putExtra("EXTRA_PATH", OriginalFile.getPath()), 111);
                //executeCutVideoCommand(rangeSeekBar.getSelectedMinValue().intValue() * 1000, rangeSeekBar.getSelectedMaxValue().intValue() * 1000);
            }
        });

    }

    private int duration;

    private void playVideo(File videoFile) {

        player = new SimpleExoPlayer.Builder(this).build();

        viewModel.videoPath = videoFile.getPath();

        PreviewActivity.applyWaterMark=false;

        if(PreviewActivity.applyWaterMark==true && Utils.isDuetVideo==false) {
            PreviewActivity.applyWaterMark=false;
            applyWaterMark(Uri.parse(videoFile.getPath()));
        }
        else {

            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(PreviewActivity.this, Util.getUserAgent(PreviewActivity.this, getResources().getString(R.string.app_name)));
            MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(videoFile.getPath()));

            player.prepare(videoSource);
            player.setRepeatMode(Player.REPEAT_MODE_ALL);
            player.setPlaybackParameters(PlaybackParameters.DEFAULT);
//            if (viewModel.isFront) {
//                binding.playerView.setScaleX(-1);
//            }

            binding.playerView.setPlayer(player);
            player.setPlayWhenReady(true);

        }

    }


    private String getTime(int seconds) {
        int hr = seconds / 3600;
        int rem = seconds % 3600;
        int mn = rem / 60;
        int sec = rem % 60;
        return String.format("%02d", hr) + ":" + String.format("%02d", mn) + ":" + String.format("%02d", sec);
    }

    private void applyWaterMark(Uri videoPath)
    {
        View view = LayoutInflater.from(this).inflate(R.layout.water_mark_layout, null);
        TextView mUsername = view.findViewById(R.id.waterrmark_username);
        mUsername.setText("@" + sessionManager.getUser().getData().getUserName());

        //second, set the width and height of inflated view
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        temp = String.valueOf(videoPath);
        String last = temp.substring(temp.lastIndexOf("/")+1,temp.lastIndexOf("."));

       // if (temp.contains("append") || temp.contains("finally"))
        {
//            temp = temp.replace("append", "append1");
            temp = temp.replace(last,last.concat("1"));
//        Bitmap myLogo = ((BitmapDrawable) getResources().getDrawable(R.drawable.logo)).getBitmap();
            Bitmap myLogo = getImageFromLayout(view);
            Bitmap bitmap_resize = Bitmap.createScaledBitmap(myLogo, 125, 125, false);
            GlWatermarkFilter filter = new GlWatermarkFilter(bitmap_resize, GlWatermarkFilter.Position.RIGHT_TOP);
            customDialogBuilder.showLoadingDialog();
            new GPUMp4Composer(String.valueOf(videoPath), temp)
                    .filter(filter)
                    .listener(new GPUMp4Composer.Listener() {
                        @Override
                        public void onProgress(double progress) {

                        }

                        @Override
                        public void onCompleted() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    customDialogBuilder.hideLoadingDialog();
                                    //Delete_file_no_watermark(viewModel.videoPath);
                                    viewModel.videoPath = temp;

                                    DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(PreviewActivity.this,
                                            Util.getUserAgent(PreviewActivity.this, getResources().getString(R.string.app_name)));
                                    MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                                            .createMediaSource(Uri.parse(viewModel.videoPath));

                                    player.prepare(videoSource);
                                    player.setRepeatMode(Player.REPEAT_MODE_ALL);
                                    player.setPlaybackParameters(PlaybackParameters.DEFAULT);
                                    if (viewModel.isFront) {
                                        //binding.playerView.setScaleX(-1);
                                    }
                                    binding.playerView.setPlayer(player);
                                    player.setPlayWhenReady(true);
                                }
                            });
                        }

                        @Override
                        public void onCanceled() {

                        }

                        @Override
                        public void onFailed(Exception exception) {
                            Log.d("TEST199", exception.getMessage().toString());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        customDialogBuilder.hideLoadingDialog();
                                        Toast.makeText(PreviewActivity.this, exception.getMessage() + " : Try Again", Toast.LENGTH_SHORT).show();

                                    } catch (Exception e) {

                                    }
                                }
                            });
                        }
                    }).start();
        }
//        else
//        {
//            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(PreviewActivity.this, Util.getUserAgent(PreviewActivity.this, getResources().getString(R.string.app_name)));
//            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(viewModel.videoPath));
//
//            player.prepare(videoSource);
//            player.setRepeatMode(Player.REPEAT_MODE_ALL);
//            player.setPlaybackParameters(PlaybackParameters.DEFAULT);
//            if (viewModel.isFront) {
//                binding.playerView.setScaleX(-1);
//            }
//
//            binding.playerView.setPlayer(player);
//            player.setPlayWhenReady(true);
//        }
    }

    private Bitmap getImageFromLayout(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void Delete_file_no_watermark(String videoPath) {
        File file = new File(videoPath);
        if (file.exists()) {
            file.delete();
        }
    }

    private void startUploading() {
        viewModel.uploadStatus.observe(this, uploadStatus -> {
            if (uploadStatus.equals(TransferState.COMPLETED)) {
                MyApplication.msg = true;
                Toast.makeText(getApplicationContext(), "Video has been uploaded successfully", Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                loadInterstitialAds(PreviewActivity.this, getString(R.string.full_screen_ad));
            }
        });
    }
    private InterstitialAd interstitial;
    public void loadInterstitialAds(Context context,String adMobId) {
        interstitial = new InterstitialAd(context);
        interstitial.setAdUnitId(adMobId);
//		interstitial.setAdUnitId("ca-app-pub-3940256099942544/1033173712");  //test id
        interstitial.setAdListener(new AdListener() {

            @Override
            public void onAdClosed() {
                // TODO Auto-generated method stub
                super.onAdClosed();
                onBackPressed();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // TODO Auto-generated method stub
                super.onAdFailedToLoad(errorCode);
                onBackPressed();
            }

            @Override
            public void onAdLeftApplication() {
                // TODO Auto-generated method stub
                super.onAdLeftApplication();
                onBackPressed();
            }

            @Override
            public void onAdLoaded() {
                //Log.i("admobResult ", interstitial.isLoaded()+"");
                if (interstitial.isLoaded()) {
                    interstitial.show();
                }
                // TODO Auto-generated method stub
                super.onAdLoaded();
            }

            @Override
            public void onAdOpened() {
                // TODO Auto-generated method stub
                super.onAdOpened();

            }

        });

        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
        interstitial.show();
    }

    private void showNotification() {
        int icon = R.mipmap.ic_launcher;
        String title = getString(R.string.app_name);
        final String CHANNEL_ID = "1001";
//        final String CHANNEL_NAME = title;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, title, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("");
            channel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});
            channel.setSound(uri, audioAttributes);
            manager.createNotificationChannel(channel);
            Notification.Builder builder = new Notification.Builder(this, CHANNEL_ID);
            builder.setSmallIcon(icon)
                    .setContentTitle(title)
                    .setContentText("Your video has been publish successfully");
            manager.notify(0, builder.build());

        } else {

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                    .setContentTitle(title)
                    .setContentText("Your video has been publish successfully")
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(uri)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSmallIcon(icon)
                    .setAutoCancel(true);

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(0, notificationBuilder.build());
            }
        }
    }

    private void showToast() {
        viewModel.isToast.observe(this, isToast -> {
            if (isToast) {
                Toast.makeText(this, "Your video has been publish Successfully.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initObserve() {
        viewModel.isLoading.observe(this, isLoading -> {
            if (isLoading) {
                //customDialogBuilder.showLoadingDialog();
                Toast.makeText(getApplicationContext(), "Video Uploading Started...", Toast.LENGTH_SHORT).show();
                Start_Service();

                Utils.isFromAdPosting = true;
               // onBackPressed();

            } else {
                Log.i("isuploaded ", Utils.isOnLine+"");
                //if(Utils.isOnLine)
                    LoadAds.loadInterstitialAds(PreviewActivity.this,getString(R.string.full_screen_ad));

//                deleteRecursive(getPath());
////                customDialogBuilder.hideLoadingDialog();
//                setResult(RESULT_OK);
//                new GlobalApi().rewardUser("3");
//                onBackPressed();
            }
        });

    }

    private void Start_Service()
    {
        serviceCallback=this;

        Upload_Service mService = new Upload_Service(serviceCallback);
        if (!Global.isMyServiceRunning(this,mService.getClass()))
        {
            Intent mServiceIntent = new Intent(this.getApplicationContext(), mService.getClass());
            mServiceIntent.setAction("startservice");
            startService(mServiceIntent);

            Intent intent = new Intent(this, Upload_Service.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        }
        else {
            Toast.makeText(this, "Please wait video already in uploading progress", Toast.LENGTH_LONG).show();
        }
    }

    // this function will stop the the ruuning service
    public void Stop_Service(){

        serviceCallback=this;

        Upload_Service mService = new Upload_Service(serviceCallback);

        if (Global.isMyServiceRunning(this,mService.getClass())) {
            Intent mServiceIntent = new Intent(this.getApplicationContext(), mService.getClass());
            mServiceIntent.setAction("stopservice");
            startService(mServiceIntent);

        }


    }

    @Override
    public void onStop() {
        super.onStop();
        Stop_Service();

    }

    private void initListener() {
        binding.setOnBackClick(v -> onBackPressed());
        binding.setOnUploadClick(v -> {

            binding.playerView.getPlayer().stop(false);

            BottomSheetDialog dialog = new BottomSheetDialog(this);
            ItemUploadSheetBinding uploadSheetBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.item_upload_sheet, null, false);
            dialog.setContentView(uploadSheetBinding.getRoot());
            dialog.setCanceledOnTouchOutside(false);
            dialog.setDismissWithAnimation(true);
            uploadSheetBinding.imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            uploadSheetBinding.btnPublish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewModel.uploadPost(view.getContext());
                    //new AddPostTask().execute();
                }
            });

            uploadSheetBinding.rdGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                    Log.d("chk", "id" + checkedId +" == "+R.id.rbDraft);
                    if(checkedId==R.id.rbPublic)
                        viewModel.publish_position.set(0);
                    else if(checkedId==R.id.rbDraft)
                        viewModel.publish_position.set(1);
                    else if(checkedId==R.id.rbPrivate)
                        viewModel.publish_position.set(2);
                }
            });


            uploadSheetBinding.ivThumb.setImageURI(Uri.parse(viewModel.videoThumbnail));
            uploadSheetBinding.setViewModel(viewModel);
            viewModel.onClickUpload.observe(this, s ->
            {
                if (!uploadSheetBinding.edtDes.getHashtags().isEmpty()) {
                    viewModel.hashTag = TextUtils.join(",", uploadSheetBinding.edtDes.getHashtags());
                    dialog.dismiss();
                }
            });
            uploadSheetBinding.edtDes.setOnHashtagClickListener((view, text) -> {

            });
            FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            if (bottomSheet != null) {
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
            dialog.show();
        });
    }

    public File getPath() {
        String state = Environment.getExternalStorageState();
        File filesDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {

            filesDir = getExternalFilesDir(null);
        } else {
            filesDir = getFilesDir();
        }
        return filesDir;
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory != null && fileOrDirectory.isDirectory() && fileOrDirectory.listFiles() != null) {
            for (File child : Objects.requireNonNull(fileOrDirectory.listFiles())) {
                deleteRecursive(child);
            }
        }
        if (fileOrDirectory != null) {
            fileOrDirectory.delete();
        }
    }

    @Override
    protected void onResume() {
        if (player != null) {
            player.setPlayWhenReady(true);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (player != null) {
            player.setPlayWhenReady(false);
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public void ShowResponce(String responce) {
        Toast.makeText(PreviewActivity.this, responce, Toast.LENGTH_LONG).show();
        //progressDialog.dismiss();


        if(responce.equalsIgnoreCase("Your Video is uploaded Successfully")) {


            startActivity(new Intent(PreviewActivity.this, MainActivity.class));
            finishAffinity();
        }
    }

    // this is importance for binding the service to the activity
    Upload_Service mService;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            Upload_Service.LocalBinder binder = (Upload_Service.LocalBinder) service;
            mService = binder.getService();

            mService.setCallbacks(PreviewActivity.this);

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {

        }
    };


    public long GetVideoDuration(File videoFile)
    {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(PreviewActivity.this, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(time));
        retriever.release();
        return seconds;
    }

    public long GetVideoDurationInMillisecond(File videoFile)
    {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(PreviewActivity.this, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long milliseconds = TimeUnit.MILLISECONDS.toMillis(Long.parseLong(time));
        retriever.release();
        return milliseconds;
    }




    public void fadInFadOut(File file)
    {
        try{outputFadInOutFile = new File(getPath(), "fadeInout".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputFastMotionFile = new File(getPath(), "fastmotion".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputSlowMotionFile = new File(getPath(), "slowmotion".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputReverseFile = new File(getPath(), "reverse".concat(".mp4"));}catch (Exception e){e.printStackTrace();}

        try{

            long duration= GetVideoDuration(file);

            customDialogBuilder.showLoadingDialog();

            String[] commandArray = {"-y", "-i", file.getPath(), "-acodec", "copy", "-vf", "fade=t=in:st=0:d=5,fade=t=out:st=" + String.valueOf(duration - 5) + ":d=5", outputFadInOutFile.getAbsolutePath()};

            long executionId = FFmpeg.executeAsync(commandArray, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        currentFileToUpload = outputFadInOutFile;
                        player.stop();
                        playVideo(currentFileToUpload);
                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                    }
                    customDialogBuilder.hideLoadingDialog();
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    public void fastMotion(File file)
    {
        try{outputFadInOutFile = new File(getPath(), "fadeInout".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputFastMotionFile = new File(getPath(), "fastmotion".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputSlowMotionFile = new File(getPath(), "slowmotion".concat(".mp4"));}catch (Exception e){e.printStackTrace();}
        try{outputReverseFile = new File(getPath(), "reverse".concat(".mp4"));}catch (Exception e){e.printStackTrace();}

        try{

            long duration= GetVideoDuration(file);

            //ffmpeg -i input.mkv -filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]" -map "[v]" -map "[a]" output.mkv
            String[] complexCommand = {"-y", "-i", file.getPath(), "-filter_complex", "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]", "-map", "[v]", "-map", "[a]", outputFastMotionFile.getAbsolutePath()};

            customDialogBuilder.showLoadingDialog();
            long executionId = FFmpeg.executeAsync(complexCommand, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        currentFileToUpload = outputFastMotionFile;
                       try { player.stop(); }catch (Exception e){e.printStackTrace();}
                        playVideo(currentFileToUpload);
                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                    }
                    customDialogBuilder.hideLoadingDialog();
                }
            });

        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    public void slowMotion(File file)
    {

        try{outputSlowMotionFile = new File(getPath(), "slowmotion".concat(".mp4"));}catch (Exception e){e.printStackTrace();}

        try{

            long duration= GetVideoDuration(file);

            String[] complexCommand = {"-y", "-i", file.getPath(), "-filter_complex", "[0:v]setpts=2.0*PTS[v];[0:a]atempo=0.5[a]", "-map", "[v]", "-map", "[a]", outputSlowMotionFile.getAbsolutePath()};

            customDialogBuilder.showLoadingDialog();

            long executionId = FFmpeg.executeAsync(complexCommand, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        currentFileToUpload = outputSlowMotionFile;
                        try { player.stop(); }catch (Exception e){e.printStackTrace();}
                        playVideo(currentFileToUpload);
                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                    }

                    customDialogBuilder.hideLoadingDialog();
                }

            });

        }
        catch (Exception e)
        {
            e.printStackTrace();

        }

    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }


    /**
     * Command for segmenting video
     */
    private void splitVideoCommand(String path) {

        String filePrefix = "split_video";
        String fileExtn = ".mp4";
        String yourRealPath = path;

        File dir = new File(getPath(), ".VideoSplit");
        if (dir.exists())
            deleteDir(dir);
        dir.mkdir();
        File dest = new File(dir, filePrefix + "%03d" + fileExtn);

        try {
            String[] complexCommand = {"-i", yourRealPath, "-movflags", "faststart","-c:v", "libx264", "-crf", "22", "-map", "0", "-segment_time", "4", "-g", "9", "-sc_threshold", "0", "-force_key_frames", "expr:gte(t,n_forced*6)", "-f", "segment", dest.getAbsolutePath()};
            customDialogBuilder.showLoadingDialog();
            long executionId = FFmpeg.executeAsync(complexCommand, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        reverseVideoCommand();


                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                    }
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Command for reversing segmented videos
     */
    private String[] lastReverseCommand;
    private void reverseVideoCommand() {

        File srcDir = new File(getPath(), ".VideoSplit");
        File[] files = srcDir.listFiles();
        String filePrefix = "reverse_video";
        String fileExtn = ".mp4";
        File destDir = new File(getPath(), ".VideoPartsReverse");
        if (destDir.exists())
            deleteDir(destDir);
        destDir.mkdir();
        for (int i = 0; i < files.length; i++) {
            File dest = new File(destDir, filePrefix + i + fileExtn);

            String command[] = {"-i", files[i].getAbsolutePath(), "-an",  "-vf", "reverse", dest.getAbsolutePath()};
            if (i == files.length - 1)
                lastReverseCommand = command;

            long executionId = FFmpeg.executeAsync(command, new ExecuteCallback() {

                @Override
                public void apply(final long executionId, final int rc) {
                    if (rc == RETURN_CODE_SUCCESS) {

                        Log.i(Config.TAG, "Async command execution completed successfully.");
                        concatVideoCommand();

                    } else if (rc == RETURN_CODE_CANCEL) {
                        Log.i(Config.TAG, "Async command execution cancelled by user.");
                    } else {

                        Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                    }
                }
            });
        }


    }

    /**
     * Command for concating reversed segmented videos
     */

    String filePath;
    private void concatVideoCommand() {

        File srcDir = new File(getPath(), ".VideoPartsReverse");
        File[] files = srcDir.listFiles();
        if (files != null && files.length > 1) {
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
        }
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder filterComplex = new StringBuilder();
        filterComplex.append("-filter_complex,");
        for (int i = 0; i < files.length; i++) {
            stringBuilder.append("-i" + "," + files[i].getAbsolutePath() + ",");
            filterComplex.append("[").append(i).append(":v").append(i).append("] [").append(i).append(":a").append(i).append("] ");

        }
        filterComplex.append("concat=n=").append(files.length).append(":v=0:a=1 [v] [a]");
        String[] inputCommand = stringBuilder.toString().split(",");
        String[] filterCommand = filterComplex.toString().split(",");

        String filePrefix = "reverse_video";
        String fileExtn = ".mp4";
        File dest = new File(getPath(), filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(getPath(), filePrefix + fileNo + fileExtn);
        }
        filePath = dest.getAbsolutePath();
        String[] destinationCommand = {"-map", "[v]", "-map", "[a]", dest.getAbsolutePath()};

        //String command[] = {"-i",inputFile1AbsolutePath,"-i",inputFile2AbsolutePath .....,"-i",inputFileNAbsolutePath,"-filter_complex","[0:0] [1:0] [2:0]...[N:0] concat=n=N:v=1:a=0",outputFileAbsolutePath};
        long executionId = FFmpeg.executeAsync(combine(inputCommand, filterCommand, destinationCommand), new ExecuteCallback() {

            @Override
            public void apply(final long executionId, final int rc) {
                if (rc == RETURN_CODE_SUCCESS) {

                    Log.i(Config.TAG, "Async command execution completed successfully.");
                    currentFileToUpload = new File(filePath);
                    player.stop();
                    playVideo(currentFileToUpload);
                } else if (rc == RETURN_CODE_CANCEL) {
                    Log.i(Config.TAG, "Async command execution cancelled by user.");
                } else {

                    Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                }

                customDialogBuilder.hideLoadingDialog();
            }
        });
    }

    public static String[] combine(String[] arg1, String[] arg2, String[] arg3) {
        String[] result = new String[arg1.length + arg2.length + arg3.length];
        System.arraycopy(arg1, 0, result, 0, arg1.length);
        System.arraycopy(arg2, 0, result, arg1.length, arg2.length);
        System.arraycopy(arg3, 0, result, arg1.length + arg2.length, arg3.length);
        return result;
    }


    private void executeCutVideoCommand(int startMs, int endMs) {


        String filePrefix = "cut_video";
        String fileExtn = ".mp4";
        String yourRealPath = OriginalFile.getPath();
        File dest = new File(getPath(), filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(getPath(), filePrefix + fileNo + fileExtn);
        }

        Log.d(TAG, "startTrim: src: " + yourRealPath);
        Log.d(TAG, "startTrim: dest: " + dest.getAbsolutePath());
        Log.d(TAG, "startTrim: startMs: " + startMs);
        Log.d(TAG, "startTrim: endMs: " + endMs);
        filePath = dest.getAbsolutePath();
        //String[] complexCommand = {"-i", yourRealPath, "-ss", "" + startMs / 1000, "-t", "" + endMs / 1000, dest.getAbsolutePath()};
        String[] complexCommand = {"-ss", "" + startMs / 1000, "-y", "-i", yourRealPath, "-t", "" + (endMs - startMs) / 1000,"-vcodec", "mpeg4", "-b:v", "2097152", "-b:a", "48000", "-ac", "2", "-ar", "22050", filePath};

        customDialogBuilder.showLoadingDialog();

        long executionId = FFmpeg.executeAsync(complexCommand, new ExecuteCallback() {

            @Override
            public void apply(final long executionId, final int rc) {
                if (rc == RETURN_CODE_SUCCESS) {

                    Log.i(Config.TAG, "Async command execution completed successfully.");
                    player.stop();
                    currentFileToUpload = new File(filePath);
                    playVideo(currentFileToUpload);
                } else if (rc == RETURN_CODE_CANCEL) {
                    Log.i(Config.TAG, "Async command execution cancelled by user.");
                } else {

                    Log.i(Config.TAG, String.format("Async command execution failed with rc=%d.", rc));

                }

                customDialogBuilder.hideLoadingDialog();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==111)
        {
            if (resultCode == RESULT_OK) {

                if (data != null) {
                    String videoPath = data.getExtras().getString("INTENT_VIDEO_FILE");
                    //Toast.makeText(PreviewActivity.this, "Video stored at " + videoPath, Toast.LENGTH_LONG).show();
                    outPutTrimmedFile = new File(videoPath);
                    currentFileToUpload = outPutTrimmedFile;
                    playVideo(outPutTrimmedFile);
                }
            }
        }
    }

    @Override
    public void onImageMessageReceived(RtmImageMessage rtmImageMessage, RtmChannelMember rtmChannelMember) {

    }

    @Override
    public void onFileMessageReceived(RtmFileMessage rtmFileMessage, RtmChannelMember rtmChannelMember) {

    }
}