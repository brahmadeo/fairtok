package com.fairtok.chat;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fairtok.R;
import com.squareup.picasso.Picasso;


public class ProfileImageActivity extends AppCompatActivity {


    ImageView imgUser;
    TextView tvUsername;
    String profileUrl,name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_imageview);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.chat_list_actionbar));
        }

        profileUrl = getIntent().getStringExtra("profile");
        name = getIntent().getStringExtra("username");

        imgUser = (ImageView) findViewById(R.id.imgUser);
        tvUsername = (TextView) findViewById(R.id.tvUsername);

        tvUsername.setText(name);

        Picasso.get().invalidate(profileUrl);

        Picasso.get().
                load(profileUrl).
                into(imgUser);

        findViewById(R.id.imgBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
