package com.fairtok.chat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.fairtok.R;
import com.fairtok.utils.Const;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;

public class Utils
{

    public static String BASE_URL="https://fairtok.com/API/Chat/";
   // public static String BASE_URL="http://3.108.183.116/API/Chat/";
    public static String Chat_LIST_URL = BASE_URL+"user_chat_list.php";
    public static String Chat_HISTORY_URL = BASE_URL+"chat_history.php";
    public static String MakeMeOffline_URL = BASE_URL+"makemeoffline.php";
    public static String MakeMeOnline_URL = BASE_URL+"makemeonline.php";
    public static String MakeHostOnline_URL = BASE_URL+"makehostonline.php";
    public static String MakeHostOffline_URL = BASE_URL+"makehostoffline.php";
    public static String MakeAudienceOffline_URL = BASE_URL+"makeAudienceoffline.php";
    public static String IS_ON_LINE_URL = BASE_URL+"islive.php";
    public static String MARK_AS_READ_URL = BASE_URL+"readchat_msg.php";
    public static String READ_CHAT_URL = BASE_URL+"chatmsg_read.php";
    public static String DELETE_CHAT_URL = BASE_URL+"chat_delete.php";
    public static String BLOCK_USER_URL = BASE_URL+"block_usr.php";
    public static String UNBLOCK_USER_URL = BASE_URL+"unblock_usr.php";
    public static String SEND_MSG = BASE_URL+"send_chatmsg.php";
    public static String DEVICE_REGISTER_URL = BASE_URL+"register_device.php";

    //TrakNPayPaymentGateway Verify Hash Link
    public static String VERIFYHASH = BASE_URL + "verifyHash";

    public static String SEND_SMS_URL = BASE_URL+"sendSms.php";
    public static String VERIFICATION_URL = BASE_URL+"verify_otp.php";
    public static String CHECKMUTUALFOLLOW = BASE_URL+"checkMutualFollower.php";
    public static String GETMUTUALFOLLOWER = BASE_URL+"getMutualFollowerList.php";
    public static String GET_LIVE_USERS = BASE_URL+"getLiveUsersList.php";
    public static String GET_MEMBER_LIST = BASE_URL+"getMemberList.php";

    public static String UPDATE_WON_AMOUNT = BASE_URL+"update_amount.php";

    public static String UPDATE_BALANCE = BASE_URL+"update_balance.php";

    public static String UPDATE_SHARE_COUNT = BASE_URL+"share_count.php";
    public static String COMMENT_LIKE_UNLIKE= BASE_URL+"comment_like.php";
    public static String REMOVE_PIC = BASE_URL+"removePhoto.php";

    public static String UPDATE_COINS = BASE_URL+"updateCoinsAccount.php";
    public static String GET_PARTY_MEMBERS = BASE_URL+"getPartyMembers.php";

    public static String hostUserId="0";
    public static String hostFullName="";
    public static int playerNo=0;
    public static boolean isCaller=false;


    public static String Publish_Draft_Private = BASE_URL+"publish_draft_private_vid.php";
    public static String SendNotification = BASE_URL+"sendNotification.php";


    public static String CURRENT_RECEIVER_ID = "";
    public static String CURRENT_RECEIVER_NAME = "";
    public static String CURRENT_RECEIVER_IMAGE = "0";
    public static String SelectedSenderId;
    public static boolean isChatScreenOpen = false;
    public static boolean isImBlocked = false;
    public static boolean isReciverBlocked = false;
    public static boolean isNeedToOpenHome = false;
    public static boolean isOnLine=false;
    public static String DEFAULT_PROFILE_URL = "user_default_img.png";
    public static String LIVE_PROFILE_URL = BASE_URL+"user_default_img.png";


    // For Live Call
    public static String LIVE_CALL_LIVE_ID = "0";
    public static String LIVE_CALL_CALL_ID = "0";
    public static String LIVE_CALL_RATE = "0";
    public static String LIVE_HOST_CALL_ID = "0";

    public static boolean isFromAdPosting = false;

    public static String hostProfilePic=DEFAULT_PROFILE_URL;
    public static String party_id="0";

    public static boolean isValidUserName=false,isDuetVideo=false;

    public static boolean isFromOwnProfile = false;

    public static String downloadedFile, VideoCreaterName="0", VideoCreaterId="0",VideoId="0",VideoCreaterUserName="0";
    public static boolean hideViewCounts=false;

    public static void clearDuetData()
    {
        isDuetVideo=false;
        VideoCreaterName="0";
        VideoCreaterId="0";
        VideoId="0";
        VideoCreaterUserName="0";
    }

    public static void showAlert(String message, View v)
    {
        try {
            Snackbar msg = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
            msg.getView().setBackgroundColor(ContextCompat.getColor(v.getContext(), R.color.colorPrimary));
            View view = msg.getView();
            TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
            tv.setTextColor(Color.WHITE);
            msg.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static File getCreatedAppDirectory(Context context)
    {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.app_name));

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("App", "failed to create directory");
                mediaStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            }
        }

        return mediaStorageDir;
    }

    public static void hideKeyboard(Context context, EditText editText)
    {
        try {
            editText.clearFocus();
            InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void hideKeyboardForActivity(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
