package com.fairtok.chat.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.chat.ChatActivity;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.chat.bean.ChatListPOJO;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MutualListAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 2;

    ArrayList<Object> list;
    Context context1;
    PrefHandler pref;

    public interface EventListener {
        void onEvent(int data, View v);
    }


    public MutualListAdapter(Context context2, ArrayList<Object> list2) {
        list = list2;
        context1 = context2;
        pref = new PrefHandler(context2);

    }

    @Override
    public int getItemViewType(int position) {
        Object recyclerViewItem = list.get(position);

        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType)
        {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                View unifiedNativeLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_unified, parent, false);
                return new UnifiedNativeAdViewHolder(unifiedNativeLayoutView);

            default:
                View view = LayoutInflater.from(context1).inflate(R.layout.chat_list_row, parent, false);
                return new ViewHolder(view);

        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case UNIFIED_NATIVE_AD_VIEW_TYPE:
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) list.get(position);
                populateNativeAdView(nativeAd, ((UnifiedNativeAdViewHolder) holder).getAdView());
                break;

            default:

                ViewHolder Vholder = (ViewHolder) holder;
                final ChatListPOJO bean = (ChatListPOJO) list.get(position);

                Vholder.tvUserName.setText(bean.getReceiver_fullname());

                Vholder.tvMsg.setText("Tap to chat with "+bean.getReceiver_fullname()+".");

                Picasso.
                        get().
                        load(bean.getReceiver_photo()).
                        into(Vholder.profileImage);

                Vholder.relCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.CURRENT_RECEIVER_ID = bean.getOpp_userid();
                        Utils.CURRENT_RECEIVER_NAME = bean.getReceiver_fullname();
                        Utils.CURRENT_RECEIVER_IMAGE = bean.getReceiver_photo();

                        Intent i1 = new Intent(context1, ChatActivity.class);
                        context1.startActivity(i1);
                    }
                });


        }


    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }



    private void populateNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView profileImage;
        TextView tvUserName;
        TextView tvTime;
        TextView tvMsg;
        TextView tvUnreadCounter;
        RelativeLayout relCard;

        public ViewHolder(View convertView) {

            super(convertView);
            profileImage = (CircleImageView) convertView.findViewById(R.id.profile_image);
            tvUserName = (TextView) convertView.findViewById(R.id.tvUsername);
            tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            tvMsg = (TextView) convertView.findViewById(R.id.tvMsg);
            tvUnreadCounter = (TextView) convertView.findViewById(R.id.tvUnreadCount);
            relCard = (RelativeLayout) convertView.findViewById(R.id.relCard);
            tvUnreadCounter.setVisibility(View.INVISIBLE);
            tvMsg.setVisibility(View.VISIBLE);
            tvTime.setVisibility(View.GONE);

        }
    }
}