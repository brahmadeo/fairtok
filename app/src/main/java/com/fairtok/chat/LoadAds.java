package com.fairtok.chat;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;

import com.fairtok.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;


public class LoadAds
{

	public static void loadAdmob(Context context, LinearLayout addLayout, String adMobId)
	{
		PrefHandler prefHandler = new PrefHandler(context);

		Log.v("Paras","Paras in LoadAds adEnabled = "+prefHandler.getAdEnabled());

		AdView adView = new AdView(context);
		adView.setAdUnitId(adMobId);
		//adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");  //Test Id
		adView.setAdSize(AdSize.SMART_BANNER);
		addLayout.addView(adView);
		AdRequest adRequest = new AdRequest.Builder()
				.build();

		adView.loadAd(adRequest);

		/*if(prefHandler.getAdEnabled().equals("2"))
		{

		}
		else {
			AdView adView = new AdView(context);
			adView.setAdUnitId(adMobId);
     		//adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");  //Test Id
			adView.setAdSize(AdSize.SMART_BANNER);
			addLayout.addView(adView);
			AdRequest adRequest = new AdRequest.Builder()
					.build();

			adView.loadAd(adRequest);
		}*/

	    
	}
	public static void loadInterstitialAds(Context context,String adMobId)
	{
		PrefHandler prefHandler = new PrefHandler(context);

		if(prefHandler.getAdEnabled().equals("0"))
		{

		}
		else {
			final InterstitialAd interstitial;
			interstitial = new InterstitialAd(context);
			interstitial.setAdUnitId(adMobId);
//		interstitial.setAdUnitId("ca-app-pub-3940256099942544/1033173712");  //test id
			interstitial.setAdListener(new AdListener() {

				@Override
				public void onAdClosed() {
					// TODO Auto-generated method stub
					super.onAdClosed();
				}

				@Override
				public void onAdFailedToLoad(int errorCode) {
					// TODO Auto-generated method stub
					super.onAdFailedToLoad(errorCode);

				}

				@Override
				public void onAdLeftApplication() {
					// TODO Auto-generated method stub
					super.onAdLeftApplication();

				}

				@Override
				public void onAdLoaded() {
					if (interstitial.isLoaded()) {
						interstitial.show();

					}
					// TODO Auto-generated method stub
					super.onAdLoaded();
				}

				@Override
				public void onAdOpened() {
					// TODO Auto-generated method stub
					super.onAdOpened();

				}

			});

			// Create ad request.
			AdRequest adRequest = new AdRequest.Builder()
					.build();

			// Begin loading your interstitial.
			interstitial.loadAd(adRequest);
			interstitial.show();
		}

	}

	// A Native Express ad is placed in every nth position in the RecyclerView.
	public static final int ITEMS_PER_AD = 5;
	// The Native Express ad height.
	private static final int NATIVE_EXPRESS_AD_HEIGHT = 150;
	// The number of native ads to load.
	public static final int NUMBER_OF_ADS = 3;
	// The AdLoader used to load ads.
	private static AdLoader adLoader;
	// List of native ads that have been successfully loaded.
	private static List<UnifiedNativeAd> mNativeAds = new ArrayList<>();


	private static void insertAdsInMenuItems(ArrayList<Object> arrayList) {
		if (mNativeAds.size() <= 0) {
			return;
		}

		int offset = (arrayList.size() / mNativeAds.size()) + 1;
		int index = ITEMS_PER_AD;
		for (UnifiedNativeAd ad : mNativeAds) {
			if(index<arrayList.size()) {
				arrayList.add(index, ad);
				index = index + ITEMS_PER_AD;
			}
		}

	}

	private void addNativeExpressAds(ArrayList<Object> arrayList,Context context) {

		// Loop through the items array and place a new Native Express ad in every ith position in
		// the items List.
		for (int i = ITEMS_PER_AD; i <= arrayList.size(); i += ITEMS_PER_AD) {
			final NativeExpressAdView adView = new NativeExpressAdView(context);
			arrayList.add(i, adView);
		}
	}

	public static void loadNativeAds(Context context, final ArrayList<Object> arrayList) {
		PrefHandler prefHandler = new PrefHandler(context);

		if(prefHandler.getAdEnabled().equals("2"))
		{

		}
		else {
			try {
				AdLoader.Builder builder = new AdLoader.Builder(context, context.getString(R.string.admobe_native_ad_id));
				adLoader = builder.forUnifiedNativeAd(
						new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
							@Override
							public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
								// A native ad loaded successfully, check if the ad loader has finished loading
								// and if so, insert the ads into the list.
								mNativeAds.add(unifiedNativeAd);
								if (!adLoader.isLoading()) {
									insertAdsInMenuItems(arrayList);
								}
							}
						}).withAdListener(
						new AdListener() {
							@Override
							public void onAdFailedToLoad(int errorCode) {
								// A native ad failed to load, check if the ad loader has finished loading
								// and if so, insert the ads into the list.
								Log.e("BroadcastActivity", "The previous native ad failed to load. Attempting to"
										+ " load another.");
								if (!adLoader.isLoading()) {
									insertAdsInMenuItems(arrayList);
								}
							}
						}).build();

				// Load the Native ads.
				adLoader.loadAds(new AdRequest.Builder().build(), NUMBER_OF_ADS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}