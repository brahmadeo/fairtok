package com.fairtok.chat.bean;

/**
 * Created by HP on 01-06-2017.
 */

public class ChatListPOJO {

    String opp_userid;
    String last_msg;
    String chatrec_id;
    String senderid;
    String receiverid;
    String msg;
    String chat_time;
    String chatuser_strid;
    String receiver_fullname;
    String receiver_photo;
    int no_of_unreadmsg;

    int iblocked;
    int iamblocked;
    String blocked_userid;

    public int getIblocked() {
        return iblocked;
    }

    public void setIblocked(int iblocked) {
        this.iblocked = iblocked;
    }

    public int getIamblocked() {
        return iamblocked;
    }

    public void setIamblocked(int iamblocked) {
        this.iamblocked = iamblocked;
    }

    public String getBlocked_userid() {
        return blocked_userid;
    }

    public void setBlocked_userid(String blocked_userid) {
        this.blocked_userid = blocked_userid;
    }

    public int getNo_of_unreadmsg() {
        return no_of_unreadmsg;
    }

    public void setNo_of_unreadmsg(int no_of_unreadmsg) {
        this.no_of_unreadmsg = no_of_unreadmsg;
    }

    public void setLast_msg(String last_msg) {
        this.last_msg = last_msg;
    }

    public String getLast_msg() {
        return last_msg;
    }

    public String getOpp_userid() {
        return opp_userid;
    }

    public void setOpp_userid(String opp_userid) {
        this.opp_userid = opp_userid;
    }

    public String getChatrec_id() {
        return chatrec_id;
    }

    public void setChatrec_id(String chatrec_id) {
        this.chatrec_id = chatrec_id;
    }

    public String getSenderid() {
        return senderid;
    }

    public void setSenderid(String senderid) {
        this.senderid = senderid;
    }

    public String getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(String receiverid) {
        this.receiverid = receiverid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

    public String getChatuser_strid() {
        return chatuser_strid;
    }

    public void setChatuser_strid(String chatuser_strid) {
        this.chatuser_strid = chatuser_strid;
    }

    public String getReceiver_fullname() {
        return receiver_fullname;
    }

    public void setReceiver_fullname(String receiver_fullname) {
        this.receiver_fullname = receiver_fullname;
    }

    public String getReceiver_photo() {
        return receiver_photo;
    }

    public void setReceiver_photo(String receiver_photo) {
        this.receiver_photo = receiver_photo;
    }
}
