package com.fairtok.chat.bean;

public class ChatMessage {
    private String id;
    private boolean isMe;
    private String message;
    private Long userId;
    private String dateTime;

    String chatrec_id;
    String senderid;
    String receiverid;
    String chatuser_strid;
    String receiver_fullname;

    int iblocked;
    int iamblocked;
    String blocked_userid;
    int isRead;

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIblocked() {
        return iblocked;
    }

    public void setIblocked(int iblocked) {
        this.iblocked = iblocked;
    }

    public int getIamblocked() {
        return iamblocked;
    }

    public void setIamblocked(int iamblocked) {
        this.iamblocked = iamblocked;
    }

    public String getBlocked_userid() {
        return blocked_userid;
    }

    public void setBlocked_userid(String blocked_userid) {
        this.blocked_userid = blocked_userid;
    }

    public String getChatrec_id() {
        return chatrec_id;
    }

    public void setChatrec_id(String chatrec_id) {
        this.chatrec_id = chatrec_id;
    }

    public String getSenderid() {
        return senderid;
    }

    public void setSenderid(String senderid) {
        this.senderid = senderid;
    }

    public String getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(String receiverid) {
        this.receiverid = receiverid;
    }

    public String getChatuser_strid() {
        return chatuser_strid;
    }

    public void setChatuser_strid(String chatuser_strid) {
        this.chatuser_strid = chatuser_strid;
    }

    public String getReceiver_fullname() {
        return receiver_fullname;
    }

    public void setReceiver_fullname(String receiver_fullname) {
        this.receiver_fullname = receiver_fullname;
    }

    public String getReceiver_photo() {
        return receiver_photo;
    }

    public void setReceiver_photo(String receiver_photo) {
        this.receiver_photo = receiver_photo;
    }

    String receiver_photo;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public boolean getIsme() {
        return isMe;
    }
    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getDate() {
        return dateTime;
    }

    public void setDate(String dateTime) {
        this.dateTime = dateTime;
    }
}