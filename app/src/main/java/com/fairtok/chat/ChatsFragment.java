package com.fairtok.chat;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fairtok.R;
import com.fairtok.chat.adapter.ChatingListAdapter;
import com.fairtok.chat.bean.ChatListPOJO;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ChatsFragment extends Fragment implements CompleteTaskListner, ChatingListAdapter.EventListener {

    View view;;
    TextView tvSeller;
    RecyclerView recyclerView;

    ArrayList<Object> arrayList;
    ChatingListAdapter adapter;
    PrefHandler prefHandler;
    View noData;
    int unReadCounter = 0;
    LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_chats, container, false);

        arrayList = new ArrayList<>();
        prefHandler = new PrefHandler(getActivity());

        noData = (View) view.findViewById(R.id.noData);

        tvSeller = (TextView) view.findViewById(R.id.tvSeller);
        recyclerView = (RecyclerView) view.findViewById(R.id.listView);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(noData.getContext());
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);


//        view.findViewById(R.id.fabSearch).setVisibility(View.VISIBLE);
//        view.findViewById(R.id.fabSearch).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getActivity(),MutualListActivity.class));
//            }
//        });

        LinearLayout llAdview = view.findViewById(R.id.llAdview);
        LoadAds.loadAdmob(llAdview.getContext(),llAdview,getString(R.string.banner_ad_id));

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getSellerData();
    }

    private void getSellerData()
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "user_chat_list"));
        params.add(new Pair<>("userid",prefHandler.getuId()));
        new AsyncHttpsRequest("Wait...!", getActivity(), params, this, 0, false).execute(Utils.Chat_LIST_URL);
    }

    private void doDelete(final String str) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_delete_chat));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                deleteAllChat(str);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void deleteAllChat(String str)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "chat_delete"));
        params.add(new Pair<>("str",str));
        new AsyncHttpsRequest("Wait...!", getActivity(), params, this, 6, false).execute(Utils.DELETE_CHAT_URL);
    }

    @Override
    public void completeTask(String result, int response_code) {
        Log.v("paras", "paras response wod list = " + result);
        if (response_code == 0) {
            try {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    unReadCounter = 0;

                    arrayList = new ArrayList<>();
                    JSONObject data_obj = obj.getJSONObject("chat_data");
                    JSONArray jarr = data_obj.getJSONArray("data_arr");

                    for(int i=0;i<jarr.length();i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatListPOJO bean = new ChatListPOJO();
                        bean.setOpp_userid(jobj.getString("opp_userid"));
                        bean.setChatrec_id(jobj.getString("chatrec_id"));
                        bean.setSenderid(jobj.getString("senderid"));
                        bean.setReceiverid(jobj.getString("receiverid"));
                        bean.setMsg(jobj.getString("msg"));
                        bean.setChat_time(jobj.getString("chat_time"));
                        bean.setChatuser_strid(jobj.getString("chatuser_strid"));
                        bean.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        bean.setReceiver_photo(jobj.getString("receiver_photo"));
                        bean.setLast_msg(jobj.getString("last_msg"));
                        bean.setNo_of_unreadmsg(jobj.getInt("no_of_unreadmsg"));

                        bean.setIblocked(jobj.getInt("iblocked"));
                        bean.setIamblocked(jobj.getInt("iamblocked"));
                        bean.setBlocked_userid(jobj.getString("blocked_userid"));

                        if(jobj.getInt("no_of_unreadmsg") > 0)
                        {
                            unReadCounter++;
                        }
                        arrayList.add(bean);
                    }

                }
                else
                {
                    Log.v("No Recent Chat","No data");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            adapter = new ChatingListAdapter(getActivity(),arrayList,ChatsFragment.this);
            recyclerView.setAdapter(adapter);

            if(arrayList.size() == 0)
            {
                noData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);

            }
            else {

                noData.setVisibility(View.INVISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            try {
                //TextView counter = BroadcastActivity.tvUnreadCount;

                if(unReadCounter>0) {
                    //counter.setVisibility(View.VISIBLE);
                    //counter.setText("" + unReadCounter);
                }
                else
                {
                    //counter.setVisibility(View.INVISIBLE);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        else if(response_code == 6)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Utils.showAlert("Conversation deleted successfully",recyclerView);
                    getSellerData();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }


    private void doBlock(final Context context,final String CURRENT_RECEIVER_ID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_block_user));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "block_usr"));
                params.add(new Pair<>("senderid",prefHandler.getuId()));
                params.add(new Pair<>("receiverid",CURRENT_RECEIVER_ID));
                Log.v("param","param = "+ params.toArray());
                new AsyncHttpsRequest("Wait...!", context, params, ChatsFragment.this, 4, false).execute(Utils.BLOCK_USER_URL);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void doUnblock(final Context context,final String CURRENT_RECEIVER_ID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_block_unuser));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "unblock_usr"));
                params.add(new Pair<>("senderid",prefHandler.getuId()));
                params.add(new Pair<>("receiverid",CURRENT_RECEIVER_ID));
                Log.v("param","param = "+ params.toArray());
                new AsyncHttpsRequest("Wait...!", context, params, ChatsFragment.this, 5, false).execute(Utils.UNBLOCK_USER_URL);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void updateMenuTitles(Menu menu) {
        MenuItem blockMenuItem = menu.findItem(R.id.block);
        if(Utils.isReciverBlocked){
            blockMenuItem.setTitle("Unblock User");
        } else {
            blockMenuItem.setTitle("Block User");
        }
    }

    public void openPopup(final View view,final Context context,final String str,final String CURRENT_RECEIVER_ID)
    {
        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater1 = popup.getMenuInflater();
        inflater1.inflate(R.menu.menu_overflow_chat, popup.getMenu());

        Menu menu = popup.getMenu();
        updateMenuTitles(menu);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Delete Chat")) {
                    doDelete(str);
                    return true;
                }
                else if (item.getTitle().equals("Block User")) {
                    doBlock(context,CURRENT_RECEIVER_ID);
                    return true;
                }
                else if (item.getTitle().equals("Unblock User")) {
                    doUnblock(context,CURRENT_RECEIVER_ID);
                    return true;
                }
                else {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
        });
        popup.show();
    }

    @Override
    public void onEvent(int i,View v) {
        final ChatListPOJO bean = (ChatListPOJO) arrayList.get(i);
        openPopup(v,getActivity(),bean.getChatuser_strid(),bean.getReceiverid());
    }
}
