package com.fairtok.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.fairtok.R;
import com.fairtok.chat.adapter.ChatAdapter;
import com.fairtok.chat.bean.ChatMessage;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;
import com.fairtok.chat.httphandler.CompleteTaskListner;
import com.fairtok.view.search.FetchUserActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity implements CompleteTaskListner {

    private EditText messageET;
    private ListView messagesContainer;
    private ImageView sendBtn,ivOnline;
    private ChatAdapter adapter;
    private ArrayList<ChatMessage> chatHistory;

    PrefHandler pref;
    CircleImageView oponent_img;
    TextView toolbar,tvblockMsg,tvStatus;
    RelativeLayout sendLayout,blockedLayout;
    ImageView ivMenu;

    private int mInterval = 10000; // 30 seconds by default, can be changed later
    private Handler mHandler;
    String last_msg_id = "0",last_msg_id_receiver = "0";

    public void initToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.chat_statusbar_color));
        }
        toolbar = (TextView) findViewById(R.id.title);
        toolbar.setText(Utils.CURRENT_RECEIVER_NAME);

        findViewById(R.id.back_arrow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_chat);
        initToolbar();
        Utils.hideKeyboardForActivity(ChatActivity.this);
        pref = new PrefHandler(this);
        initControls();

        LinearLayout llAdview = findViewById(R.id.llAdview);
        LoadAds.loadAdmob(llAdview.getContext(),llAdview,getString(R.string.banner_ad_id));
    }

    @Override
    protected void onNewIntent(Intent i1)
    {
        if(i1.hasExtra("clicked"))
        {
            handleThirdUserFromNotificationClick(i1);
            i1.removeExtra("clicked");
        }
        super.onNewIntent(i1);
    }

    public void handleThirdUserFromNotificationClick(Intent i1)
    {
        oponent_img = (CircleImageView) findViewById(R.id.oponent_img);
        String sender_name = i1.getStringExtra("CURRENT_RECEIVER_NAME");
        String sender_id = i1.getStringExtra("CURRENT_RECEIVER_ID");
        String sender_image = i1.getStringExtra("CURRENT_RECEIVER_IMAGE");

        if(sender_id.matches(Utils.CURRENT_RECEIVER_ID))
        {

        }
        else{

            Utils.CURRENT_RECEIVER_NAME = sender_name;
            Utils.CURRENT_RECEIVER_ID = sender_id;
            if(!TextUtils.isEmpty(sender_image) && sender_image != "0")
            {
                Utils.CURRENT_RECEIVER_IMAGE = sender_image;
            }


            toolbar.setText(Utils.CURRENT_RECEIVER_NAME);
            if(Utils.CURRENT_RECEIVER_IMAGE != "0")
            {
                try {
                    Picasso.get().load(Utils.CURRENT_RECEIVER_IMAGE).into(oponent_img);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            getChatHistory(false);
        }
    }

    private void initControls() {

        oponent_img = (CircleImageView) findViewById(R.id.oponent_img);
        ivMenu = (ImageView) findViewById(R.id.ivMenu);
        ivOnline = (ImageView) findViewById(R.id.ivOnline);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openPopup(view,ChatActivity.this);
            }
        });

        

        if(Utils.CURRENT_RECEIVER_IMAGE != "0" && Utils.CURRENT_RECEIVER_IMAGE.length()>3 && Utils.CURRENT_RECEIVER_IMAGE!=null)
        {
            Picasso.get().load(Utils.CURRENT_RECEIVER_IMAGE).into(oponent_img);
        }


        oponent_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChatActivity.this,ProfileImageActivity.class);
                i.putExtra("profile",Utils.CURRENT_RECEIVER_IMAGE);
                i.putExtra("username",Utils.CURRENT_RECEIVER_NAME);
                startActivity(i);
            }
        });

        messagesContainer = (ListView) findViewById(R.id.messagesContainer);
        messageET = (EditText) findViewById(R.id.messageEdit);
        sendBtn = (ImageView) findViewById(R.id.chatSendButton);

        tvblockMsg = (TextView) findViewById(R.id.tvblockMsg);

        sendLayout = (RelativeLayout) findViewById(R.id.sendLayout);
        blockedLayout = (RelativeLayout) findViewById(R.id.blockedLayout);

        if(Utils.isImBlocked || Utils.isReciverBlocked){

            if(Utils.isReciverBlocked)
            {
                tvblockMsg.setText("You blocked this person.");
            }
            sendLayout.setVisibility(View.GONE);
            blockedLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            sendLayout.setVisibility(View.VISIBLE);
            blockedLayout.setVisibility(View.GONE);
        }


        chatHistory = new ArrayList<>();

        findViewById(R.id.userLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(view.getContext(), FetchUserActivity.class);
                intent.putExtra("userid", Utils.CURRENT_RECEIVER_ID);
                startActivity(intent);
            }
        });

        getChatHistory(true);

        adapter = new ChatAdapter(ChatActivity.this, chatHistory);
        messagesContainer.setAdapter(adapter);

        messagesContainer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageET.getText().toString();

                if(pref.isLoggedIn()){
//                    if(pref.getProfileStatus().matches("1"))
//                    {
                        if (TextUtils.isEmpty(messageText)) {
                            return;
                        }

                        ChatMessage chatMessage = new ChatMessage();
                        chatMessage.setId("4562");//dummy
                        chatMessage.setMessage(messageText);
                        String time = (String) android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date());
                        chatMessage.setDate(time);
                        chatMessage.setMe(true);

                        sendMessage(messageText);

                        messageET.setText("");

                        displayMessage(chatMessage);

                    //}
//                    else
//                    {
//                        if (pref.isUserBecameGuru() == false)
//                            Utils.showPopup(ChatActivity.this, getString(R.string.profile_disable_msg));
//                        else
//                            Utils.showAprovalPendingPopup(ChatActivity.this);
//                    }
                }
                else {

                    if (TextUtils.isEmpty(messageText)) {
                        return;
                    }

                    ChatMessage chatMessage = new ChatMessage();
                    chatMessage.setId("4562");//dummy
                    chatMessage.setMessage(messageText);
                    String time = (String) android.text.format.DateFormat.format("yyyy-MM-dd HH:mm:ss", new Date());
                    chatMessage.setDate(time);
                    chatMessage.setMe(true);

                    sendMessage(messageText);

                    messageET.setText("");

                    displayMessage(chatMessage);
                }
            }
        });

        findViewById(R.id.rl_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBtn.performClick();
            }
        });

        Intent i1 = getIntent();

        if(i1.hasExtra("clicked"))
        {
            handleThirdUserFromNotificationClick(i1);
            i1.removeExtra("clicked");
        }

        mHandler = new Handler();

    }

    public void displayMessage(ChatMessage message) {
        adapter.add(message);
        adapter.notifyDataSetChanged();
        scroll();
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                updateStatus(last_msg_id); //this function can change value of mInterval.
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }


    @Override
    public void onResume() {
        Utils.isChatScreenOpen = true;
        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("com.fairtok.push"));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        if(!Utils.isOnLine) {

            try {
                PrefHandler pref = new PrefHandler(ChatActivity.this);

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "makemeonline"));
                params.add(new Pair<>("userid", pref.getuId()));

                new AsyncHttpsRequest("", ChatActivity.this, params, ChatActivity.this,
                        111, false).execute(Utils.MakeMeOnline_URL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        startRepeatingTask();

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        Utils.isChatScreenOpen = false;

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equalsIgnoreCase("com.fairtok.push")) {
                    final String message = intent.getStringExtra("message");
                    String sender_id = intent.getStringExtra("sender_id");
                    String sender_name = intent.getStringExtra("sender_name");
                    String msg_id = intent.getStringExtra("msg_id");
                    String strid = intent.getStringExtra("strid");

                    ChatMessage msg = new ChatMessage();
                    msg.setId(msg_id);
                    msg.setMe(false);
                    msg.setMessage(message);
                    msg.setChatuser_strid(strid);

                    try
                    {

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String datetime = dateformat.format(c.getTime());
                        System.out.println(datetime);

                        msg.setDate(datetime);
                    }
                    catch (Exception e)
                    {
                        try {
                            msg.setDate("" + new Date());
                        }
                        catch (Exception e2)
                        {
                            e2.printStackTrace();
                        }
                        e.printStackTrace();
                    }

                    displayMessage(msg);

                    last_msg_id_receiver = msg_id;
                    markAsReadChat(last_msg_id_receiver);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    };

    public void sendMessage(String msg)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "send_chatmsg"));
        params.add(new Pair<>("senderid",pref.getuId()));
        params.add(new Pair<>("receiverid",Utils.CURRENT_RECEIVER_ID));
        params.add(new Pair<>("msg",msg));
        new AsyncHttpsRequest("", this, params, this, 0, false).execute(Utils.SEND_MSG);
    }

    private void getChatHistory(boolean showDialog)
    {
        String msg;
        if(showDialog)
            msg = "Wait...";
        else
            msg = "";
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "chat_history"));
        params.add(new Pair<>("current_uid",pref.getuId()));
        params.add(new Pair<>("senderid",pref.getuId()));
        params.add(new Pair<>("receiverid",Utils.CURRENT_RECEIVER_ID));

        new AsyncHttpsRequest(msg, this, params, this, 1, false).execute(Utils.Chat_HISTORY_URL);
    }

    private void getReadChat()
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "chatmsg_read"));
        params.add(new Pair<>("senderid",pref.getuId()));
        params.add(new Pair<>("receiverid",Utils.CURRENT_RECEIVER_ID));

        new AsyncHttpsRequest("Wait...!", this, params, this, 3, false).execute(Utils.READ_CHAT_URL);
    }

    private void deleteAllChat()
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("tag", "chat_delete"));
        params.add(new Pair<>("str",chatHistory.get(0).getChatuser_strid()));
        new AsyncHttpsRequest("Wait...!", this, params, this, 6, false).execute(Utils.DELETE_CHAT_URL);
    }

    public void updateStatus(String msg_id)
    {
        try {

            List<Pair<String, String>> params = new ArrayList<>();
            params.add(new Pair<>("tag", "islive"));
            params.add(new Pair<>("sender_userid", pref.getuId()));
            params.add(new Pair<>("receiver_userid", Utils.CURRENT_RECEIVER_ID));
            params.add(new Pair<>("msgid", msg_id));

            if (chatHistory.size() > 0) {
                try {
                    String str = chatHistory.get(0).getChatuser_strid();
                    if (str != null) {
                        if (TextUtils.isEmpty(str.trim()))
                            str = pref.getuId() + "-" + Utils.CURRENT_RECEIVER_ID;
                        params.add(new Pair<>("str", str));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                params.add(new Pair<>("str", pref.getuId() + "-" + Utils.CURRENT_RECEIVER_ID));
            }
            new AsyncHttpsRequest("", this, params, this, 7, false).execute(Utils.IS_ON_LINE_URL);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void markAsReadChat(String msgid)
    {
        try {
            if(chatHistory.size()>0) {
                List<Pair<String, String>> params = new ArrayList<>();

                params.add(new Pair<>("tag", "readchat_msg"));
                params.add(new Pair<>("str", chatHistory.get(0).getChatuser_strid()));
                params.add(new Pair<>("userid", pref.getuId()));
                params.add(new Pair<>("msgid", msgid));

                new AsyncHttpsRequest("", this, params, this, 8, false).execute(Utils.MARK_AS_READ_URL);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void completeTask(String result, int response_code) {
        if(response_code == 0)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");
                //{"tag":"send_chatmsg","success":0,"error":3,"error_msg":"Sorry, You can chat with maximum 10 users Daily."}
                if (success == 1) {
                    Log.d("paras", "paras msg sent successfuly ");
                    last_msg_id = obj.getString("lastmsgid");
                }
                else
                {
                    if(error == 2)
                    {
                        String error_msg = obj.getString("error_msg");
                        Utils.showAlert(error_msg,sendLayout);

                        Utils.isImBlocked = true;
                        if(Utils.isImBlocked)
                        {

                            tvblockMsg.setText("You are no longer able to send the messages to this person you're blocked.");

                            sendLayout.setVisibility(View.GONE);
                            blockedLayout.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            sendLayout.setVisibility(View.VISIBLE);
                            blockedLayout.setVisibility(View.GONE);
                        }
                    }
                    else if(error == 3)
                    {
                        String error_msg = obj.getString("error_msg");
                        Utils.showAlert(error_msg,sendLayout);

                        tvblockMsg.setText(error_msg);
                        sendLayout.setVisibility(View.GONE);
                        blockedLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(response_code == 1)
        {
            try
            {
                chatHistory = new ArrayList<>();

                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1)
                {
                    JSONObject chatObj = obj.getJSONObject("post_data");

                    int iBlocked = chatObj.getInt("iblocked");
                    int iamBlocked = chatObj.getInt("iamblocked");

                    if(iBlocked == 1)
                    {
                        Utils.isReciverBlocked = true;
                    }
                    else {
                        Utils.isReciverBlocked = false;
                    }
                    if(iamBlocked == 1)
                    {
                        Utils.isImBlocked = true;
                    }
                    else
                    {
                        Utils.isImBlocked = false;
                    }

                    if(Utils.isImBlocked || Utils.isReciverBlocked){

                        if(Utils.isReciverBlocked)
                        {
                            tvblockMsg.setText("You blocked this person.");
                        }
                        sendLayout.setVisibility(View.GONE);
                        blockedLayout.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        sendLayout.setVisibility(View.VISIBLE);
                        blockedLayout.setVisibility(View.GONE);
                    }

                    JSONArray jarr = chatObj.getJSONArray("data_arr");

                    for(int i=0;i<jarr.length();i++) {

                        JSONObject jobj = jarr.getJSONObject(i);

                        ChatMessage msg = new ChatMessage();
                        msg.setId(jobj.getString("chatrec_id"));
                        String senderid = jobj.getString("senderid");

                        if(senderid.equals(pref.getuId())) {
                            msg.setMe(true);
                            last_msg_id = jobj.getString("chatrec_id");
                            try
                            {
                                msg.setIsRead(Integer.parseInt(jobj.getString("isread_receive")));
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else {
                            last_msg_id_receiver  = jobj.getString("chatrec_id");
                            msg.setMe(false);
                            try {
                                msg.setIsRead(Integer.parseInt(jobj.getString("isread_send")));
                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        msg.setSenderid(senderid);
                        msg.setReceiver_fullname(jobj.getString("receiver_fullname"));
                        msg.setMessage(jobj.getString("msg"));
                        msg.setDate(jobj.getString("chat_time"));
                        msg.setChatrec_id(jobj.getString("chatrec_id"));
                        msg.setReceiver_photo(jobj.getString("receiver_photo"));
                        msg.setChatrec_id(jobj.getString("receiverid"));
                        msg.setChatuser_strid(jobj.getString("chatuser_strid"));

                        chatHistory.add(msg);

                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            adapter = new ChatAdapter(ChatActivity.this, chatHistory);
            messagesContainer.setAdapter(adapter);

            updateStatus(last_msg_id);
            getReadChat();
            markAsReadChat(last_msg_id_receiver);

        }
        else if(response_code == 3)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Log.d("paras", "paras msg read successfuly ");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(response_code == 4)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Utils.isReciverBlocked = true;
                    tvblockMsg.setText("You blocked this person.");
                    sendLayout.setVisibility(View.GONE);
                    blockedLayout.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(response_code == 5)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Utils.isReciverBlocked = false;
                    sendLayout.setVisibility(View.VISIBLE);
                    blockedLayout.setVisibility(View.GONE);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(response_code == 6)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Utils.showAlert("Conversation deleted successfully",sendLayout);
                    onBackPressed();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(response_code == 7)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {

                    String isread_r_id = obj.getString("isread_r_id");

                    if(isread_r_id.matches("1"))
                    {
                        for(int i=0;i<chatHistory.size();i++)
                        {
                            chatHistory.get(i).setIsRead(1);
                        }

                        adapter.notifyDataSetChanged();
                    }

                    String islive_r_id = obj.getString("islive_r_id");

                    if(islive_r_id.matches("1"))
                    {
                        tvStatus.setVisibility(View.VISIBLE);
                        tvStatus.setText("Online");

                        ivOnline.setVisibility(View.VISIBLE);
                    }
                    else {

                        ivOnline.setVisibility(View.INVISIBLE);

                        String receiver_lastseen = obj.getString("receiver_lastseen");
                        if(receiver_lastseen.matches("last seen at 0")) {
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText("Away");
                        }
                        else
                        {
                            tvStatus.setVisibility(View.VISIBLE);
                            tvStatus.setText(receiver_lastseen);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(response_code == 8)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Log.v("paras","paras msg has been read");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        else if(response_code == 111)
        {
            try
            {
                JSONObject obj = new JSONObject(result);
                int success = obj.getInt("success");
                int error = obj.getInt("error");

                if (success == 1) {
                    Log.d("paras", "paras user is now online ");
                    Utils.isOnLine = true;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }


    private void doDelete(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_delete_chat));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

               deleteAllChat();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void doBlock(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_block_user));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "block_usr"));
                params.add(new Pair<>("senderid",pref.getuId()));
                params.add(new Pair<>("receiverid",Utils.CURRENT_RECEIVER_ID));
                Log.v("param","param = "+ params.toArray());
                new AsyncHttpsRequest("Wait...!", context, params, ChatActivity.this, 4, false).execute(Utils.BLOCK_USER_URL);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }



    private void doUnblock(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);//, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.sure_block_unuser));
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                List<Pair<String, String>> params = new ArrayList<>();
                params.add(new Pair<>("tag", "unblock_usr"));
                params.add(new Pair<>("senderid",pref.getuId()));
                params.add(new Pair<>("receiverid",Utils.CURRENT_RECEIVER_ID));
                Log.v("param","param = "+ params.toArray());
                new AsyncHttpsRequest("Wait...!", context, params, ChatActivity.this, 5, false).execute(Utils.UNBLOCK_USER_URL);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    private void updateMenuTitles(Menu menu) {
        MenuItem blockMenuItem = menu.findItem(R.id.block);
        if(Utils.isReciverBlocked){
            blockMenuItem.setTitle("Unblock User");
        } else {
            blockMenuItem.setTitle("Block User");
        }
    }

    public void openPopup(final View view,final Context context)
    {
        PopupMenu popup = new PopupMenu(context, view);
        MenuInflater inflater1 = popup.getMenuInflater();
        inflater1.inflate(R.menu.menu_overflow_seller, popup.getMenu());

        Menu menu = popup.getMenu();
        updateMenuTitles(menu);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getTitle().equals("Delete Chat")) {
                    doDelete(context);
                    return true;
                }
                else if (item.getTitle().equals("Block User")) {
                    doBlock(context);
                    return true;
                }
                else if (item.getTitle().equals("Unblock User")) {
                    doUnblock(context);
                    return true;
                }
                else {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
        });
        popup.show();
    }
}