package com.fairtok.chat;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PrefHandler
{
	SharedPreferences pref;
	Editor editor;
	Context _context;
	int PRIVATE_MODE = 0;
	private static final String PREF_NAME = "fairTok_local_pref";

	String PROFILE = "profile";
	String LIVE_PROFILE = "live_profile";
	String IS_SELLER = "isSeller";
	String NAME = "name";
	String EMAIL = "email";
	String CREATED_DATE = "created_date";
	String VARIFIED_WITH = "verified_with";
	String recentLocation1 = "recentLocation1";
	String recentLocation2 = "recentLocation2";
    String IS_FIRST_TIME_LAUNCH = "isFirstTimeLaunch";
	String DISTANCE_FILTER = "DistanceFilter";
	String CATEGORY_FILTER = "CategoryFilter";
	String referralUrl = "referralUrl";
	String sharedVideoUrl = "sharedVideoUrl";

	public PrefHandler(Context context)
	{
		try
		{
			this._context = context;
			pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
			editor = pref.edit();
			editor.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public String getRecentLocation1()
	{
		return pref.getString(recentLocation1,"");
	}

	public void setRecentLocation1(String RecentLocation1)
	{
		editor.putString(recentLocation1,RecentLocation1);
		editor.commit();
	}

    public String getCurrentLocationAddr()
    {
        return pref.getString("currentLocation","");
    }

    public void setCurrentLocationAddr(String myLocation)
    {
        editor.putString("currentLocation",myLocation);
        editor.commit();
    }

    public String getCurrentLat()
    {
        return pref.getString("Latitude","19.0330");
    }

    public void setCurrentLat(String myLat)
    {
        editor.putString("Latitude", myLat);
        editor.commit();
    }

    public String getCurrentLng()
    {
        return pref.getString("Longitude","73.0297");
    }

    public void setCurrentLng(String myLng)
    {
        editor.putString("Longitude", myLng);
        editor.commit();
    }

	public String getRecentLocation2()
	{
		return pref.getString(recentLocation2,"");
	}

	public void setRecentLocation2(String RecentLocation2)
	{
		editor.putString(recentLocation2,RecentLocation2);
		editor.commit();
	}


	public String getRecentLat1()
	{
		return pref.getString("recent_lat1","0");
	}

	public void setRecentLat1(String myLat)
	{
		editor.putString("recent_lat1", myLat);
		editor.commit();
	}

	public String getRecentLng1()
	{
		return pref.getString("recent_lng1","0");
	}

	public void setRecentLng1(String myLng)
	{
		editor.putString("recent_lng1", myLng);
		editor.commit();
	}

	public String getRecentLat2()
	{
		return pref.getString("recent_lat2","0");
	}

	public void setRecentLat2(String myLat)
	{
		editor.putString("recent_lat2", myLat);
		editor.commit();
	}

	public String getRecentLng2()
	{
		return pref.getString("recent_lng2","0");
	}

	public void setRecentLng2(String myLng)
	{
		editor.putString("recent_lng2", myLng);
		editor.commit();
	}

	public int getVerifiedWith()
	{
		return pref.getInt(VARIFIED_WITH,0);
	}

	public void setVerifiedWith(int verifiedWith)
	{
		editor.putInt(VARIFIED_WITH,verifiedWith);
		editor.commit();
	}

	public String getuId()
	{
		return pref.getString("uId","0");
	}

	public void setuId(String uId)
	{
		editor.putString("uId",uId);
		editor.commit();
	}

	public String getEmail()
	{
		return pref.getString(EMAIL,"");
	}

	public void setEmail(String email)
	{
		editor.putString(EMAIL,email);
		editor.commit();
	}


	public String getMobile()
	{
		return pref.getString("mobile","0000000000");
	}

	public void setMobile(String mobileNo)
	{
		editor.putString("mobile",mobileNo);
		editor.commit();
	}


	public String getName()
	{
		return pref.getString(NAME,null);
	}

	public void setName(String name)
	{
		editor.putString(NAME,name);
		editor.commit();
	}

	public String getCreatedDate()
	{
		return pref.getString(CREATED_DATE,null);
	}

	public void setCreatedDate(String name)
	{
		editor.putString(CREATED_DATE,name);
		editor.commit();
	}

	public String getProfile()
	{
		return pref.getString(PROFILE,Utils.DEFAULT_PROFILE_URL);
	}

	public void setProfile(String profile)
	{
		editor.putString(PROFILE,profile);
		editor.commit();
	}
	public void setLiveProfile(String profile)
	{
		editor.putString(LIVE_PROFILE, profile);
		editor.commit();
	}
	public String getLiveProfile()
	{
		return pref.getString(LIVE_PROFILE, Utils.DEFAULT_PROFILE_URL);
	}

	public boolean isSeller()
	{
		return pref.getBoolean(IS_SELLER,false);
	}

	public void setIsSeller(boolean isSeller)
	{
		editor.putBoolean(IS_SELLER,isSeller);
		editor.commit();
	}

	public boolean isUserBecameGuru()
	{
		return pref.getBoolean("isUserBecameGuru",false);
	}

	public void setIsUserBecameGuru(boolean isUserBecameGuru)
	{
		editor.putBoolean("isUserBecameGuru",isUserBecameGuru);
		editor.commit();
	}

    public boolean isFirstTimeLaunch()
    {
        return pref.getBoolean("isFirstTime",true);
    }

    public void setIsFirstTimeLaunch(boolean isFirstTime)
    {
        editor.putBoolean("isFirstTime",isFirstTime);
        editor.commit();
    }

	public void clearPreference()
	{
		setuId("0");
		setIsLoggedIn(false);
		setIsSeller(false);
		setLanguage("");
		setProfile(Utils.DEFAULT_PROFILE_URL);
	}

	public boolean isLoggedIn()
	{
		return pref.getBoolean("isLoggedIn",false);
	}

	public void setIsLoggedIn(boolean isLoggedIn)
	{
		editor.putBoolean("isLoggedIn",isLoggedIn);
		editor.commit();
	}

	public String getIsVarified()
	{
		return pref.getString("isverified",null);
	}

	public void setIsVarified(String varified)
	{
		editor.putString("isverified",varified);
		editor.commit();
	}

	public int getFilteredCategory()
	{
		return pref.getInt(CATEGORY_FILTER,0);
	}

	public void setFilteredCategory(int category)
	{
		editor.putInt(CATEGORY_FILTER,category);
		editor.commit();
	}

	public int getFilteredDistance()
	{
		return pref.getInt(DISTANCE_FILTER,250);
	}

	public void setFilteredDistance(int category)
	{
		editor.putInt(DISTANCE_FILTER,category);
		editor.commit();
	}

	public int getCATEGORY_FILTER()
	{
		return pref.getInt("isCategoryFilter",0);
	}

	public void setCATEGORY_FILTER(int category)
	{
		editor.putInt("isCategoryFilter",category);
		editor.commit();
	}

	public String getProfileStatus()
	{
		return pref.getString("profileStatus","0");
	}

	public void setProfileStatus(String status)
	{
		editor.putString("profileStatus",status);
		editor.commit();
	}

	public String getProfileUpdated()
	{
		return pref.getString("profileUpdated","0");
	}

	public void setProfileUpdated(String status)
	{
		editor.putString("profileUpdated",status);
		editor.commit();
	}

	public String getReason()
	{
		return pref.getString("reason","0");
	}

	public void setReason(String reason)
	{
		editor.putString("reason",reason);
		editor.commit();
	}

	public String getisVerified()
	{
		return pref.getString("verified","0");
	}

	public void setVerified(String verified)
	{
		editor.putString("verified",verified);
		editor.commit();
	}

	public int getDistanceFilter()
	{
		return pref.getInt("isDistanceFilter",1);
	}

	public void setDistanceFilter(int is_distance)
	{
		editor.putInt("isDistanceFilter",is_distance);
		editor.commit();
	}

	public String getCurrentCategory()
	{
		return pref.getString("current_category","");
	}

	public void setCurrentCategory(String category)
	{
		editor.putString("current_category",category);
		editor.commit();
	}

	public String getPostLocationAddr()
	{
		return pref.getString("postLocation","");
	}

	public void setPostLocationAddr(String myLocation)
	{
		editor.putString("postLocation",myLocation);
		editor.commit();
	}

	public long getPostLat()
	{
		return pref.getLong("post_Latitude",0);
	}

	public void setPostLat(double myLat)
	{
		editor.putLong("post_Latitude", Double.doubleToLongBits(myLat));
		editor.commit();
	}

	public long getPostLng()
	{
		return pref.getLong("post_Longitude",0);
	}

	public void setPostLng(double myLng)
	{
		editor.putLong("post_Longitude", Double.doubleToLongBits(myLng));
		editor.commit();
	}


	public boolean inOTPScreen()
	{
		return pref.getBoolean("inOTPScreen",false);
	}

	public void setinOTPScreen(boolean inOTPScreen)
	{
		editor.putBoolean("inOTPScreen",inOTPScreen);
		editor.commit();
	}


	public boolean getIsMobileVerified()
	{
		return pref.getBoolean("IsMobileVerified",false);
	}

	public void setIsMobileVerified(boolean IsMobileVerified)
	{
		editor.putBoolean("IsMobileVerified",IsMobileVerified);
		editor.commit();
	}

    public void setCity(String city) {
		editor.putString("city",city);
		editor.commit();
    }

    public String getCity()
	{
		return pref.getString("city","0");
	}

	public String getCountry()
	{
		return pref.getString("Country","");
	}

	public void setCountry(String joinDate)
	{
		editor.putString("Country",joinDate);
		editor.commit();
	}

	public String getZip()
	{
		return pref.getString("zip","");
	}

	public void setZip(String zip)
	{
		editor.putString("zip",zip);
		editor.commit();
	}

	public String getState()
	{
		return pref.getString("state","");
	}

	public void setState(String stat)
	{
		editor.putString("state",stat);
		editor.commit();
	}

	public String getAdEnabled()
	{
		return pref.getString("adEnabled","2");
	}

	public void setAdEnabled(String adEnabled)
	{
		editor.putString("adEnabled",adEnabled);
		editor.commit();
	}

	public String getAppVersionAdmin()
	{
		return pref.getString("appVersion","2");
	}

	public void setAppVersionAdmin(String adEnabled)
	{
		editor.putString("appVersion",adEnabled);
		editor.commit();
	}

	public boolean getIsLanguageSet()
	{
		return pref.getBoolean("IsLanguageSet",false);
	}

	public void setIsLanguageSet(boolean IsLanguageSet)
	{
		editor.putBoolean("IsLanguageSet",IsLanguageSet);
		editor.commit();
	}

	public String getLanguage()
	{
		return pref.getString("language","");
	}

	public void setLanguage(String language)
	{
		editor.putString("language",language);
		editor.commit();
	}

	//followers_threshold

	public int getFollowersThreshold()
	{
		return pref.getInt("followers_threshold",100);
	}

	public void setFollowersThreshold(int followers_threshold)
	{
		editor.putInt("followers_threshold",followers_threshold);
		editor.commit();
	}

	public int getAudioCharges()
	{
		return pref.getInt("audioCallCost",15);
	}

	public void setAudioCharges(int followers_threshold)
	{
		editor.putInt("audioCallCost",followers_threshold);
		editor.commit();
	}

	public int getVideoCharges()
	{
		return pref.getInt("videoCallCost",50);
	}

	public void setVideoCharges(int followers_threshold)
	{
		editor.putInt("videoCallCost",followers_threshold);
		editor.commit();
	}


	public String getReferralUrl()
	{
		return pref.getString("referalUrl", "test");
	}

	public void setReferralUrl(String referralUrl)
	{
		editor.putString("referalUrl",referralUrl);
		editor.commit();
	}

	public void setInTime(Long inTime){
		editor.putLong("intime", inTime);
		editor.commit();
	}
	public Long getIntime(){
		return pref.getLong("intime", 0);
	}
}