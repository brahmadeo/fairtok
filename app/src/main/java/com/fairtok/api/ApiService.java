package com.fairtok.api;

import com.fairtok.model.Explore;
import com.fairtok.model.LiveUserData;
import com.fairtok.model.SpinWinModel;
import com.fairtok.model.comment.Comment;
import com.fairtok.model.follower.Follower;
import com.fairtok.model.music.Musics;
import com.fairtok.model.music.SearchMusic;
import com.fairtok.model.notification.Notification;
import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.SearchUser;
import com.fairtok.model.user.User;
import com.fairtok.model.videos.Video;
import com.fairtok.model.wallet.CoinPlan;
import com.fairtok.model.wallet.CoinRate;
import com.fairtok.model.wallet.MyWallet;
import com.fairtok.model.wallet.RewardingActions;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface ApiService {

    @FormUrlEncoded
    @POST("User/registration")
    Single<User> registrationUser(@FieldMap HashMap<String, Object> hashMap);

    @POST("Post/sound_list")
    Single<Musics> getSoundList(@Header("Authorization") String token);

    @POST("Post/favourite_sound")
    Single<SearchMusic> getFavSoundList(@Header("Authorization") String token, @Body JsonObject ids);

    @FormUrlEncoded
    @POST("Post/user_list_search")
    Single<SearchUser> searchUser(@Field("keyword") String keyword,
                                  @Field("count") int count,
                                  @Field("start") int start);

    @FormUrlEncoded
    @POST("Post/sound_list_search")
    Single<SearchMusic> searchSoundList(@Header("Authorization") String token,
                                        @Field("keyword") String keyword);

    @FormUrlEncoded
    @POST("User/notification_setting")
    Single<RestResponse> updateToken(@Header("Authorization") String token,
                                     @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST("User/check_username")
    Single<RestResponse> checkUsername(@Field("user_name") String userName);


    @GET("User/logout")
    Single<RestResponse> logOutUser(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("Post/add_comment")
    Single<RestResponse> addComment(@Header("Authorization") String token,
                                    @Field("post_id") String postId,
                                    @Field("comment") String comment,
                                    @Field("parent_id") String parentId);

    @FormUrlEncoded
    @POST("Post/follow_unfollow")
    Single<RestResponse> followUnFollow(@Header("Authorization") String token,
                                        @Field("to_user_id") String toUserId);


    @FormUrlEncoded
    @POST("Post/delete_post")
    Single<RestResponse> deletePost(@Header("Authorization") String token,
                                    @Field("post_id") String postId);


//    FTransactionId,FPaymentMode,FAmount

    @FormUrlEncoded
    @POST("Wallet/purchase_coin")
    Single<RestResponse> purchaseCoin(@Header("Authorization") String token,
                                      @Field("coin") String coinAmount,
                                      @Field("transaction_id") String FTransactionId,
                                      @Field("mode") String FPaymentMode,
                                      @Field("amount") String FAmount,
                                      @Field("user_name") String user_name,
                                      @Field("status") String status);

    @FormUrlEncoded
    @POST("User/purchase_coin_plan")
    Single<RestResponse> purchaseCoinNew(@Header("Authorization") String token,
                                      @Field("plan_id") String planId,
                                      @Field("transaction_id") String FTransactionId,
                                      @Field("mode") String FPaymentMode,
                                      @Field("response_message") String response_message,
                                      @Field("response_status") String response_status);

    @FormUrlEncoded
    @POST("Wallet/redeem_request")
    Single<RestResponse> sendRedeemRequest(@Header("Authorization") String token,
                                           @Field("amount") String coinAmount,
                                           @Field("redeem_request_type") String requestTypeType,
                                          /* @Field("silverOrGold") String silverOrGold,*/
                                           @Field("account") String account);




    @FormUrlEncoded
    @POST("Post/like_unlike")
    Single<RestResponse> likeUnlike(@Header("Authorization") String token,
                                    @Field("post_id") String postId);

    @FormUrlEncoded
    @POST("Post/comment_like_unlike")
    Single<RestResponse> likeUnlikeComment(@Header("Authorization") String token,
                                    @Field("post_id") String postId,@Field("user_id") String userId);


    @FormUrlEncoded
    @POST("Wallet/send_coin")
    Single<RestResponse> sendCoin(@Header("Authorization") String token,
                                  @Field("coin") String coin,
                                  @Field("to_user_id") String toUserId);

    @FormUrlEncoded
    @POST("User/live_send_coin")
    Single<RestResponse> sendCoinNew(@Header("Authorization") String token,
                                  @Field("live_id") String live_id,
                                  @Field("join_id") String join_id,
                                  @Field("coins") String coins,
                                  @Field("coin_type") String coin_type);

    @FormUrlEncoded
    @POST("Wallet/send_paid_coin")
    Single<RestResponse> sendPaidCoin(@Header("Authorization") String token,
                                  @Field("coin") String coin,
                                  @Field("to_user_id") String toUserId);


    @Multipart
    @POST("User/user_profile_update")
    Single<User> updateUser(@Header("Authorization") String token,
                            @PartMap HashMap<String, RequestBody> hasMap,
                            @Part MultipartBody.Part user_profile);


    @FormUrlEncoded
    @POST("Post/single_hash_tag_video")
    Single<Video> fetchHasTagVideo(@Field("hash_tag") String hashTag,
                                   @Field("count") int count,
                                   @Field("start") int start,
                                   @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("Post/hash_tag_search_video")
    Single<Video> searchVideo(@Field("keyword") String keyword,
                              @Field("count") int count,
                              @Field("start") int start,
                              @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("User/user_details")
    Single<User> getUserDetails(@Field("user_id") String user_id,
                                @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("Post/user_videos")
    Single<Video> getUserVideos(@Field("user_id") String user_id,
                                @Field("count") int count,
                                @Field("start") int start,
                                @Field("my_user_id") String myUserId);
    @FormUrlEncoded
    @POST("User/live_users")
    Single<JSONObject> getAllLiveUsers(@Header("Authorization") String Authorization,
                                       @Field("type") String type);

    @FormUrlEncoded
    @POST("Post/user_likes_videos")
    Single<Video> getUserLikedVideos(@Field("user_id") String user_id,
                                     @Field("count") int count,
                                     @Field("start") int start,
                                     @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("Post/user_private_videos")
    Single<Video> getUserPrivateVideos(@Field("user_id") String user_id,
                                     @Field("count") int count,
                                     @Field("start") int start,
                                     @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("Post/user_draft_videos")
    Single<Video> getUserDraftVideos(@Field("user_id") String user_id,
                                     @Field("count") int count,
                                     @Field("start") int start,
                                     @Field("my_user_id") String myUserId);

    @FormUrlEncoded
    @POST("Post/new_explore_hash_tag_video")
    Single<Explore> getExploreVideos(@Field("count") int count,
                                     @Field("start") int start,
                                     @Field("my_user_id") String myUserId);


    @FormUrlEncoded
    @POST("Post/sound_video")
    Single<Video> getSoundVideos(@Field("count") int count,
                                 @Field("start") int start,
                                 @Field("sound_id") String soundId,
                                 @Field("my_user_id") String myUserId);



    @FormUrlEncoded
    @POST("Video/video_list")
    Single<Video> getPostVideos(@Field("type") String type,
                                @Field("count") int count,
                                @Field("start") int start,
                                @Field("user_id") String myUserId,
                                @Field("language") String language);

    @Headers("Accept: application/json")
    @Multipart
    @POST("Post/s3_add_post")
    Single<User> uploadPost(@Header("Authorization") String token,
                            @PartMap HashMap<String, RequestBody> hasMap,
                            @Part MultipartBody.Part postVideo,
                            @Part MultipartBody.Part post_image,
                            @Part MultipartBody.Part post_sound,
                            @Part MultipartBody.Part sound_image);


    @GET("Wallet/coin_plan")
    Single<CoinPlan> getCoinPlans(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST("Post/commet_list")
    Single<Comment> getPostComments(@Field("post_id") String postId,
                                    @Field("user_id") String userId,
                                    @Field("count") int count,
                                    @Field("start") int start);


    @FormUrlEncoded
    @POST("Post/delete_comment")
    Single<RestResponse> deleteComment(@Header("Authorization") String token,
                                       @Field("comments_id") String commentsId);

    @FormUrlEncoded
    @POST("Post/following_list")
    Single<Follower> getFollowingList(@Header("Authorization") String token,
                                      @Field("user_id") String userId,
                                      @Field("count") int count,
                                      @Field("start") int start);


    //live_user_list
    @FormUrlEncoded
    @POST("Post/live_user_list")
    Single<Follower> getLiveUsersList(@Header("Authorization") String token,
                                      @Field("user_id") String userId,
                                      @Field("count") int count,
                                      @Field("start") int start);

    @FormUrlEncoded
    @POST("Post/follower_list")
    Single<Follower> getFollowerList(@Header("Authorization") String token,
                                     @Field("user_id") String userId,
                                     @Field("count") int count,
                                     @Field("start") int start);


    @FormUrlEncoded
    @POST("Post/report")
    Single<RestResponse> reportSomething(@FieldMap HashMap<String, Object> fieldMap);

    @GET("Wallet/my_wallet_coin")
    Single<MyWallet> getMyWalletDetails(@Header("Authorization") String token);

    @GET("Wallet/coin_rate")
    Single<CoinRate> getCoinRate(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("User/notification_list")
    Single<Notification> getNotificationList(@Header("Authorization") String token,
                                             @Field("user") String userId,
                                             @Field("count") int count,
                                             @Field("start") int start);

    @FormUrlEncoded
    @POST("Wallet/add_coin")
    Single<RestResponse> rewardUser(@Header("Authorization") String token,
                                    @Field("rewarding_action_id") String rewardActionId);

    @FormUrlEncoded
    @POST("User/saveAppUsage")
    Single<RestResponse> timeSpend(@Header("Authorization") String token,
                                    @Field("type") String type,
                                    @Field("spend_time") Long spend_time);

    @FormUrlEncoded
    @POST("User/saveAppUsage")
    Single<RestResponse> reportAudience(@Header("Authorization") String token,
                                   @Field("type") boolean type,
                                   @Field("spend_time") boolean spend_time);
    @FormUrlEncoded
    @POST("User/saveAppUsage")
    Single<RestResponse> blockAudience(@Header("Authorization") String token,
                                        @Field("type") String type);

    @FormUrlEncoded
    @POST("Post/increase_video_view")
    Single<RestResponse> increaseView(@Field("post_id") String postId);


    @GET("Wallet/rewarding_action")
    Single<RewardingActions> getRewardingAction(@Header("Authorization") String token);

    @Multipart
    @POST("User/verify_request")
    Single<RestResponse> verifyRequest(@Header("Authorization") String token,
                                       @PartMap HashMap<String, RequestBody> hasMap,
                                       @Part MultipartBody.Part photo_id_image,
                                       @Part MultipartBody.Part photo_with_id_image);
    @GET("User/home_image")
    Single<SpinWinModel> fetchImage(@Header("Authorization") String token);

    @Multipart
    @POST("User/change_live_thumbnail")
    Single<LiveUserData> changeThumbnail(@Header("Authorization") String token,
                                @Part MultipartBody.Part thumbnail

    );
}
