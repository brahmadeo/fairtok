package com.fairtok.viewmodel;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.R;
import com.fairtok.model.user.RestResponse;
import com.fairtok.utils.Global;
import com.fairtok.view.wallet.RedeemActivity;
import com.fairtok.view.wallet.WalletActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class RedeemViewModel extends ViewModel {

    public String coindCount;
    public String coinRate;
    public String requestType;
    public String accountId;
    public String silverOrGold;

    public String dimonds;

    public ObservableBoolean isLoading = new ObservableBoolean();
    public MutableLiveData<RestResponse> redeem = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();

    public void afterPaymentAccountChanged(CharSequence s) {
        accountId = s.toString();
    }

    public void callApiToRedeem(Activity activity) {

        Log.v("wdwdqqd", dimonds);
        disposable.add(Global.initRetrofit().sendRedeemRequest(Global.ACCESS_TOKEN, dimonds, requestType, accountId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((redeem, throwable) -> {


                    if (redeem != null && redeem.getStatus() != null) {

                        if (!redeem.getStatus()){
                            showMessageDialog(redeem.getMessage(), activity);
                        }
                        else
                            this.redeem.setValue(redeem);
                    }
                }));

    }

    private void showMessageDialog(String message, Activity activity) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity,
                R.style.CustomBottomSheetDialogTheme);
        bottomSheetDialog.setContentView(R.layout.dialog_message);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();

        TextView tvMessage = bottomSheetDialog.findViewById(R.id.tvMessage);

        tvMessage.setText(message);

        bottomSheetDialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
            bottomSheetDialog.dismiss();


        });

    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
