package com.fairtok.viewmodel;

import android.content.Context;
import android.util.Pair;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.LiveUsersAdapter;
import com.fairtok.chat.Utils;
import com.fairtok.chat.httphandler.AsyncHttpsRequest;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class LiveUsersViewModel extends ViewModel {

    public LiveUsersAdapter adapter = new LiveUsersAdapter();
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public int start = 0;
    public String hashtag;
    ObservableBoolean isloading = new ObservableBoolean(true);
    private int count = 20;
    private CompositeDisposable disposable = new CompositeDisposable();
    public static String userId;


    public void getLiveUserData(String msg,String userId, Context context, Object obj)
    {
        List<Pair<String, String>> params = new ArrayList<>();
        params.add(new Pair<>("userId",userId));
        new AsyncHttpsRequest(msg, context, params, obj, 0, false).execute(Utils.GET_LIVE_USERS);
    }


    public void onLoadMore() {

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}

