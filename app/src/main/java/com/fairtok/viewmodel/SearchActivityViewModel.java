package com.fairtok.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.SearchUserAdapter;
import com.fairtok.adapter.VideoListAdapter;
import com.fairtok.model.user.SearchUser;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Global;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SearchActivityViewModel extends ViewModel {
    public ObservableInt searchtype = new ObservableInt(0);
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public String search_text;
    public int userStart = 0;
    public int videoStart = 0;
    public ObservableBoolean noUserData = new ObservableBoolean(false);
    public ObservableBoolean noVideoData = new ObservableBoolean(false);
    public SearchUserAdapter searchUseradapter = new SearchUserAdapter();
    public VideoListAdapter videoListAdapter = new VideoListAdapter();
    //public ObservableBoolean isloading = new ObservableBoolean(true);
    public MutableLiveData<Boolean> isloading = new MutableLiveData<>();
    private int count = 10;
    private CompositeDisposable disposable = new CompositeDisposable();
    public List<SearchUser.User> userList = new ArrayList<>();
    public List<Video.Data> videoList = new ArrayList<>();

    public void searchForUser(boolean isLoadMore) {

        disposable.add(Global.initRetrofit().searchUser(search_text, count, userStart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isloading.setValue(true))
                .doOnTerminate(() -> {
                    onLoadMoreComplete.setValue(true);
                    isloading.setValue(false);
                })
                .subscribe((searchUser, throwable) -> {
                    if (searchUser != null && searchUser.getData() != null) {

                        if (isLoadMore) {
                            userList.addAll(searchUser.getData());
                            searchUseradapter.loadMore(searchUser.getData());

                        } else {
                            userList = searchUser.getData();
                            searchUseradapter.updateData(searchUser.getData());
                        }
                        userStart = userStart + count;

                        isloading.setValue(false);
                    }
                    noUserData.set(searchUseradapter.mList.isEmpty());
                }));
    }

    public void searchForVideos(boolean isLoadMore) {

        disposable.add(Global.initRetrofit().searchVideo(search_text, count, videoStart, Global.USER_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> {
                    onLoadMoreComplete.setValue(true);
                    isloading.setValue(false);})
                .subscribe((searchVideo, throwable) -> {
                    if (searchVideo != null && searchVideo.getData() != null) {

                        if (isLoadMore) {
                            videoList.addAll(searchVideo.getData());
                            videoListAdapter.loadMore(searchVideo.getData());

                        } else {
                            videoList = searchVideo.getData();
                            videoListAdapter.updateData(searchVideo.getData());

                        }
                        isloading.setValue(false);
                        videoStart = videoStart + count;
                    }
                    noVideoData.set(videoListAdapter.mList.isEmpty());

                }));
    }

    public void afterTextChanged(CharSequence s) {
        search_text = s.toString();

        if (searchtype.get() == 1) {
            userStart = 0;
            searchForUser(false);
        } else {
            videoStart = 0;
            videoListAdapter.word = search_text;
            searchForVideos(false);
        }
    }

    public void onUserLoadMore() {

        searchForUser(true);
    }

    public void onVideoLoadMore() {
        searchForVideos(true);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
