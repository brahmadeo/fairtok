package com.fairtok.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.RecommendFamilyAdapter;
import com.fairtok.utils.Global;
import com.google.gson.Gson;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class RecommendFamilyViewModel extends ViewModel {
    public int userVidStart = 0;
    public int likeVidStart = 0;
    public int draftStart = 0;
    public int privateStart = 0;
    public int count = 6;
    public static RecommendFamilyAdapter adapter = new RecommendFamilyAdapter();
    private CompositeDisposable disposable = new CompositeDisposable();
    public ObservableBoolean noUserVideos = new ObservableBoolean(false);
    ObservableBoolean isloading = new ObservableBoolean(true);
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();

    public void fetchUserVideos(boolean isLoadMore)
    {
        adapter.recommendFamilyViewModel = this;
        try {
            disposable.add(Global.initRetrofit().getUserVideos("38882", count, userVidStart, Global.USER_ID)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .doOnSubscribe(disposable1 -> isloading.set(true))
                    .doOnTerminate(() -> {
                        onLoadMoreComplete.setValue(true);
                        isloading.set(false);
                    })
                    .subscribe((videos, throwable) -> {
                        if (videos != null && videos.getData() != null) {
                            if (isLoadMore) {
                                adapter.loadMore(videos.getData());
                            } else {
                                if (!new Gson().toJson(videos.getData()).equals(new Gson().toJson(adapter.mList))) {
                                    adapter.updateData(videos.getData());
                                    noUserVideos.set(adapter.mList.isEmpty());
                                }
                            }
                            userVidStart = userVidStart + count;

                        }

                    }));
        }
        catch (Exception e){e.printStackTrace();}
    }
}
