package com.fairtok.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.utils.Global;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ReportViewmodel extends ViewModel {

    public int reportType;
    public String postId;
    public String userId;
    public String reason;
    public String description = "";
    public String contactInfo = "";
    public MutableLiveData<Boolean> isValid = new MutableLiveData<>();
    public MutableLiveData<String> toast = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    private ObservableBoolean isLoading = new ObservableBoolean(false);

    public void afterUserNameTextChanged(CharSequence s) {
        description = s.toString();
    }

    public void afterContactDetailsChanged(CharSequence s) {
        contactInfo = s.toString();
    }


    public void callApiToReport() {
        if (!description.isEmpty() && !contactInfo.isEmpty()) {
            HashMap<String, Object> hashMap = new HashMap<>();
            if (reportType == 1) {
                hashMap.put("report_type", "report_video");
                hashMap.put("post_id", postId);
            } else {
                hashMap.put("report_type", "report_user");
                hashMap.put("user_id", userId);
            }
            if (reportType == 2)
                hashMap.put("description", "Block - "+description);
            else
                hashMap.put("description", description);

            hashMap.put("reason", reason);

            hashMap.put("contact_info", contactInfo);

            disposable.add(Global.initRetrofit().reportSomething(hashMap)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .doOnSubscribe(disposable1 -> isLoading.set(true))
                    .doOnTerminate(() -> isLoading.set(false))
                    .subscribe((report, throwable) -> {
                        if (report != null && report.getStatus()) {
                            toast.setValue("Reported Successfully");
                        }
                    }));
        } else {
            isValid.setValue(false);
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
