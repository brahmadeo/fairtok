package com.fairtok.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class JoinfamilyViewModel extends ViewModel {
    public MutableLiveData<Integer> onItemClick = new MutableLiveData<>();
    public ObservableBoolean isRecommended = new ObservableBoolean(false);
    public ObservableBoolean isRating = new ObservableBoolean(false);

    public void setOnItemClick(int type){
        onItemClick.setValue(type);
    }
}
