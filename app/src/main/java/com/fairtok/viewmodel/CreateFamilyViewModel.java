package com.fairtok.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CreateFamilyViewModel extends ViewModel {
    public String imageUri = "";
    public MutableLiveData<Integer> onItemClick = new MutableLiveData<>();
    public void setOnItemClick(int type){
        onItemClick.setValue(type);
    }
}

