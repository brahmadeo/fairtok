package com.fairtok.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.CommentAdapter;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CommentSheetViewModel extends ViewModel {
    public String postId;
    public String userId;
    public String comment;
    public ObservableInt commentCount = new ObservableInt(0);
    public MutableLiveData<Boolean>  isLoading = new MutableLiveData(false);
    public MutableLiveData<Boolean> isEmpty = new MutableLiveData<Boolean>(false);
    public CommentAdapter adapter = new CommentAdapter();
    public int start = 0;
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public SessionManager sessionManager;
    private int count = 15;
    private CompositeDisposable disposable = new CompositeDisposable();

    public ObservableBoolean isLiked = new ObservableBoolean(false);


    public void afterCommentTextChanged(CharSequence s) {
        comment = s.toString();
    }


    public void fetchComments(boolean isLoadMore) {

        disposable.add(Global.initRetrofit().getPostComments(postId, userId,count, start)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.setValue(true))
                .doOnTerminate(() -> {
                    onLoadMoreComplete.setValue(true);
                    isLoading.setValue(false);
                })
                .subscribe((comment, throwable) -> {

                    if (comment != null && comment.getData() != null && !comment.getData().isEmpty()) {
                       // sessionManager.saveComment(comment);
                        if (isLoadMore) {
                            adapter.loadMore(comment.getData());
                        } else {
                            adapter.updateData(comment.getData());
                        }
                        start = start + count;
                    }
                    isEmpty.setValue(adapter.mList.isEmpty());

                }));
    }


    public void onLoadMore() {
        fetchComments(true);
    }

    public void addComment() {
        if (!TextUtils.isEmpty(comment)) {
            callApiToSendComment();
        }
    }

    private void callApiToSendComment() {
        disposable.add(Global.initRetrofit().addComment(Global.ACCESS_TOKEN, postId, comment,Global.PARENT_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.setValue(true))
                .doOnTerminate(() -> isLoading.setValue(false))
                .subscribe((comment, throwable) -> {
                    if (comment != null && comment.getStatus() != null) {
                        Log.d("ADDED", "Success");

                        start = 0;
                        fetchComments(false);
                        onLoadMoreComplete.setValue(false);
                        if(Global.PARENT_ID!="0")
                            commentCount.set(commentCount.get() + 1);
                        Global.PARENT_ID="0";
                    }
                }));
    }

    public void callApitoDeleteComment(String commentId, int position) {
        disposable.add(Global.initRetrofit().deleteComment(Global.ACCESS_TOKEN, commentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.setValue(true))
                .doOnTerminate(() -> isLoading.setValue(false))
                .subscribe((deleteComment, throwable) -> {
                    if (deleteComment != null && deleteComment.getStatus() != null) {
                        Log.d("DELETED", "Success");
                        adapter.mList.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeRemoved(position, adapter.mList.size());
                        commentCount.set(commentCount.get() - 1);

                    }
                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
