package com.fairtok.viewmodel;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.model.music.Musics;
import com.otaliastudios.cameraview.controls.Flash;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class CameraViewModel extends ViewModel {
    public ObservableBoolean isRecording = new ObservableBoolean(false);
    public ObservableBoolean selectedUri = new ObservableBoolean(false);
    public ObservableBoolean isFlashOn = new ObservableBoolean(false);
    public ObservableBoolean isFacingFront = new ObservableBoolean(false);
    public ObservableBoolean isEnabled = new ObservableBoolean(false);
    public ObservableBoolean isStartRecording = new ObservableBoolean(false);
    public ObservableBoolean is15sSelect = new ObservableBoolean(true);
    public ObservableInt soundTextVisibility = new ObservableInt(View.INVISIBLE);
    public MutableLiveData<Long> onDurationUpdate = new MutableLiveData<>(15000L);
    public MutableLiveData<Integer> onItemClick = new MutableLiveData<>();
    public MutableLiveData<Musics.SoundList> onSoundSelect = new MutableLiveData<>();

    public com.otaliastudios.cameraview.CameraView mCameraView;

    public ArrayList<String> videoPaths = new ArrayList<>();
    public int count = 0;
    public String parentPath = "";
    public long duration = 15000;
    public String soundId = "";
    public MediaPlayer audio;


    public void onClickFlash() {

        if(isFlashOn.get()==false)
            isFlashOn.set(true);
        else
            isFlashOn.set(false);

        if (isFlashOn.get()==true)
        {
            mCameraView.setFlash(Flash.TORCH);
        }
        else
        {
            mCameraView.setFlash(Flash.OFF);
        }
    }



    public void setOnItemClick(int type) {
        onItemClick.setValue(type);
    }

    public void onSelectSecond(boolean isSelected15s) {
        is15sSelect.set(isSelected15s);
        onDurationUpdate.setValue(isSelected15s ? 20000L : 30000L);
    }

    public void createAudioForCamera() {
        File file = new File(parentPath.concat("/recordSound.aac"));
        if (file.exists()) {
            audio = new MediaPlayer();
            try {

                audio.setDataSource(parentPath.concat("/recordSound.aac"));
                audio.prepare();
                soundTextVisibility.set(View.VISIBLE);
                onDurationUpdate.setValue((long) audio.getDuration());

                Log.i("TAG", "createAudioForCamera: " + duration);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onCleared() {
        super.onCleared();

    }

}
