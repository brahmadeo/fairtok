package com.fairtok.viewmodel;

import android.content.Context;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.MyApplication;
import com.fairtok.adapter.LikedVideosAdapter;
import com.fairtok.chat.PrefHandler;
import com.fairtok.utils.Global;
import com.google.gson.Gson;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class LikedVideosViewModel extends ViewModel {
    public int userVidStart = 0;
    public int likeVidStart = 0;
    public int draftStart = 0;
    public int privateStart = 0;
    public int count = 6;
    public int vidType;
    public static LikedVideosAdapter adapter = new LikedVideosAdapter();
    public String userId = Global.USER_ID;
    public ObservableBoolean noUserVideos = new ObservableBoolean(false);
    public ObservableBoolean noLikedVideos = new ObservableBoolean(false);
    public MutableLiveData<Boolean> noDraftVideos = new MutableLiveData<Boolean>(false);
    public MutableLiveData<Boolean> noPrivateVideos = new MutableLiveData<Boolean>(false);

    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    ObservableBoolean isloading = new ObservableBoolean(true);
    private CompositeDisposable disposable = new CompositeDisposable();



    public void fetchUserLikedVideos(boolean isLoadMore, Context context) {
        PrefHandler prefHandler  = new PrefHandler(context);
        adapter.videosViewModel = this;
        try {
            //if (MyApplication.sessionManager.getUser() != null && MyApplication.sessionManager.getUser().getData().getUserId().equals(userId))
            {
                disposable.add(Global.initRetrofit().getUserLikedVideos(userId, count, likeVidStart, prefHandler.getuId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .doOnSubscribe(disposable1 -> isloading.set(true))
                        .doOnTerminate(() -> {
                            onLoadMoreComplete.setValue(true);
                            isloading.set(false);
                        })
                        .subscribe((videos, throwable) -> {
                            if (videos != null && videos.getData() != null) {
                                if (isLoadMore) {
                                    adapter.loadMore(videos.getData());
                                } else {
                                    if (!new Gson().toJson(videos.getData()).equals(new Gson().toJson(adapter.mList))) {
                                        adapter.updateData(videos.getData());
                                        noLikedVideos.set(adapter.mList.isEmpty());
                                    }
                                }
                                likeVidStart = likeVidStart + count;
                            }

                        }));
            }
//            else {
//                noLikedVideos.set(adapter.mList.isEmpty());
//            }
        }
        catch (Exception e){e.printStackTrace();}
    }

    public void fetchPrivateVideos(boolean isLoadMore, PrefHandler prefHandler) {

        adapter.videosViewModel = this;
        try {
            //if (prefHandler.getuId()!= "0" && prefHandler.getuId().equals(userId))
            {
                disposable.add(Global.initRetrofit().getUserPrivateVideos(userId, count, privateStart, prefHandler.getuId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .doOnSubscribe(disposable1 -> isloading.set(true))
                        .doOnTerminate(() -> {
                            onLoadMoreComplete.setValue(true);
                            isloading.set(false);
                        })
                        .subscribe((videos, throwable) -> {
                            if (videos != null && videos.getData() != null) {
                                if (isLoadMore) {
                                    adapter.loadMore(videos.getData());
                                } else {
                                    if (!new Gson().toJson(videos.getData()).equals(new Gson().toJson(adapter.mList))) {
                                        adapter.updateData(videos.getData());
                                        noPrivateVideos.setValue(adapter.mList.isEmpty());
                                    }
                                }

                                adapter.notifyDataSetChanged();
                                privateStart = privateStart + count;
                            }

                        }));
            }
//            else {
//                noPrivateVideos.setValue(adapter.mList.isEmpty());
//            }
        }
        catch (Exception e){e.printStackTrace();}
    }

    public void draftVideos(boolean isLoadMore) {

        adapter.videosViewModel = this;
        try {
            if (MyApplication.sessionManager.getUser() != null && MyApplication.sessionManager.getUser().getData().getUserId().equals(userId)) {
                disposable.add(Global.initRetrofit().getUserDraftVideos(userId, count, draftStart, Global.USER_ID)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .unsubscribeOn(Schedulers.io())
                        .doOnSubscribe(disposable1 -> isloading.set(true))
                        .doOnTerminate(() -> {
                            onLoadMoreComplete.setValue(true);
                            isloading.set(false);
                        })
                        .subscribe((videos, throwable) -> {
                            if (videos != null && videos.getData() != null) {
                                if (isLoadMore) {
                                    adapter.loadMore(videos.getData());
                                } else {
                                    if (!new Gson().toJson(videos.getData()).equals(new Gson().toJson(adapter.mList))) {
                                        adapter.updateData(videos.getData());
                                        noDraftVideos.setValue(adapter.mList.isEmpty());
                                    }
                                }
                                draftStart = draftStart + count;
                            }

                        }));
            } else {
                noDraftVideos.setValue(adapter.mList.isEmpty());
            }
        }
        catch (Exception e){e.printStackTrace();}
    }




    public void onUserLikedVideoLoadMore(Context context) {
        fetchUserLikedVideos(true,context);
    }

    public void onUserPrivateVideoLoadMore(PrefHandler prefHandler) {
        fetchPrivateVideos(true,prefHandler);
    }

    public void onUserDraftVideoLoadMore(PrefHandler prefHandler) {
        //draftVideos(true);
        fetchPrivateVideos(true,prefHandler);
    }


    public void deletePost(String postId, int position) {
        try {
            disposable.add(Global.initRetrofit().deletePost(Global.ACCESS_TOKEN, postId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe((deletePost, throwable) -> {

                        if (deletePost != null && deletePost.getStatus() != null) {
                            adapter.mList.remove(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeRemoved(position, adapter.mList.size());

                        }
                    }));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
