package com.fairtok.viewmodel;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.FamousCreatorAdapter;
import com.fairtok.adapter.VideoFullAdapter;
import com.fairtok.model.user.RestResponse;
import com.fairtok.utils.Global;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ForUViewModel extends ViewModel {

    public VideoFullAdapter adapter = new VideoFullAdapter();
    public MutableLiveData<Boolean> showBroadcastersDialog = new MutableLiveData<>(false);
    public MutableLiveData<Boolean> onLoadMoreComplete = new MutableLiveData<>();
    public MutableLiveData<Boolean> onShareSuccess = new MutableLiveData<>();
    public int start = 0;
    public int count = 10;
    public String postType;
    public FamousCreatorAdapter famousAdapter = new FamousCreatorAdapter();
    public ObservableBoolean isEmpty = new ObservableBoolean(false);
    public MutableLiveData<RestResponse> coinSend = new MutableLiveData<>();
    public boolean isSent=false;
    ObservableBoolean isloading = new ObservableBoolean(true);
    private CompositeDisposable disposable = new CompositeDisposable();
    public String userId = "";
    public MutableLiveData<RestResponse> followApi = new MutableLiveData<>();
    public ObservableInt isMyAccount = new ObservableInt(0);
    public String language;

    public void fetchPostVideos(boolean isLoadMore) {
        Log.v("lkjqlks", start+"---"+count+"---"+postType+"---"+Global.USER_ID+"---");
        disposable.add(Global.initRetrofit().getPostVideos(postType, count, start, Global.USER_ID,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> onLoadMoreComplete.setValue(true))
                .subscribe((video, throwable) -> {
                    if (video != null && video.getData() != null && !video.getData().isEmpty()) {

                       //Collections.shuffle(video.getData()); // Paras

                        for(int i=0;i<video.getData().size();i++)
                        {
                            if((i>0 && i%5==0))
                            {
                                video.getData().get(i).setShowPopup(true);
                            }
                        }

                        if (isLoadMore) {
                            adapter.loadMore(video.getData());
                        } else {
//                            if(video.getData().size()>3)
//                            {
//                                video.getData().set(2,null);
//                            }
                            adapter.updateData(video.getData());
                        }
                        //Log.i("videoUrlll ", video.getData().get(0).getPostVideo());
                        isloading.set(false);
                        start = start + count;
                    } else {
                        if (adapter.mList.isEmpty() && postType.equals("following")) {
                            fetchFamousVideos();
                            isEmpty.set(true);
                        }
                    }

                }));
    }

    public void followUnfollow() {

        disposable.add(Global.initRetrofit().followUnFollow(Global.ACCESS_TOKEN, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((followRequest, throwable) -> {
                    if (followRequest != null && followRequest.getStatus() != null) {
                        if (isMyAccount.get() == 1) {
                            isMyAccount.set(2);
                        } else {
                            isMyAccount.set(1);
                        }
                        followApi.setValue(followRequest);
                    }
                }));
    }

    private void fetchFamousVideos() {

        disposable.add(Global.initRetrofit().getPostVideos("trending", count, 0, Global.USER_ID,language)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnTerminate(() -> onLoadMoreComplete.setValue(true))
                .subscribe((video, throwable) -> {
                    if (video != null && video.getData() != null && !video.getData().isEmpty()) {
                        famousAdapter.updateData(video.getData());
                    }
                }));
    }

    public void onLoadMore() {
        fetchPostVideos(true);
    }

    public void likeUnlikePost(String postId) {

        disposable.add(Global.initRetrofit().likeUnlike(Global.ACCESS_TOKEN, postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((likeRequest, throwable) -> {

                }));
    }

    public void sendBubble(String toUserId, String coin) {

        disposable.add(Global.initRetrofit().sendCoin(Global.ACCESS_TOKEN, coin, toUserId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())

                .doOnTerminate(() -> {
                    onLoadMoreComplete.setValue(true);
                })
                .subscribe((coinSend, throwable) -> {
                    if (coinSend != null && coinSend.getStatus() != null) {
                        this.isSent=true;
                        this.coinSend.setValue(coinSend);
                    }

                }));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
