package com.fairtok.viewmodel;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.model.user.User;
import com.fairtok.s3.Util;
import com.fairtok.utils.Const;
import com.fairtok.utils.CustomDialogBuilder;
import com.fairtok.utils.Global;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.content.ContentValues.TAG;

public class EditProfileViewModel extends ViewModel {
    public User user = null;
    public String imageUri = "";
    public ArrayList<String> imageUriList = new ArrayList<>();
    public ObservableBoolean isUsernameLoading = new ObservableBoolean(false);
    public ObservableBoolean isUsernameAvailable = new ObservableBoolean(true);
    public ObservableInt length = new ObservableInt();
    public MutableLiveData<Boolean> updateProfile = new MutableLiveData<>();
    public MutableLiveData<String> toast = new MutableLiveData<>();
    public String cur_userName = "";
    private String newUserName = "";
    private CompositeDisposable disposable = new CompositeDisposable();
    private String fullName = "", bio = "",fbUrl = "", instaUrl = "", youtubeUrl = "";
    public String language = "";
    public static ProgressDialog progressDialog;


    public void afterUserNameTextChanged(CharSequence s) {
        newUserName = s.toString();

        if (!cur_userName.equals(newUserName)) {
            checkForUserName();
        } else {
            if (!newUserName.isEmpty()) {
                isUsernameAvailable.set(false);
                isUsernameLoading.set(false);
            }
            isUsernameAvailable.set(true);
            isUsernameLoading.set(false);
        }
    }

    public void afterTextChanged(CharSequence charSequence, int type) {
        switch (type) {
            case 1:
                fullName = charSequence.toString();
                break;
            case 2:
                bio = charSequence.toString();
                length.set(bio.length());
                break;
            case 3:
                fbUrl = charSequence.toString();
                break;
            case 4:
                instaUrl = charSequence.toString();
                break;
            case 5:
                youtubeUrl = charSequence.toString();
                break;
        }
    }

    private void checkForUserName() {
        if (newUserName.contains("fair") || newUserName.contains("fairtok") || newUserName.contains("feir")) {
            Utils.isValidUserName=false;
            isUsernameAvailable.set(false);
            toast.setValue("You can't use this username, Please try another");
        } else {

            Utils.isValidUserName=true;

        disposable.add(Global.initRetrofit().checkUsername(newUserName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isUsernameLoading.set(true))
                .doOnTerminate(() -> isUsernameLoading.set(false))
                .subscribe((checkUsername, throwable) -> {
                    if (checkUsername != null && checkUsername.getStatus() != null) {
                        isUsernameAvailable.set(checkUsername.getStatus());
                    }
                }));
        }
    }

    boolean isImageUpload=false;

    public void updateUser(Context context, String gender, CustomDialogBuilder customDialogBuilder) {
        setupProgressDialog(context);
        PrefHandler prefHandler = new PrefHandler(context);

        if (fullName == null || fullName.isEmpty() || fullName.length() < 4) {
            toast.setValue("invalid full name");

        } else if (newUserName == null || newUserName.isEmpty() || newUserName.length() < 4) {
            toast.setValue("invalid Username");
        } else if (isUsernameAvailable.get()) {
            customDialogBuilder.showLoadingDialog();
            HashMap<String, RequestBody> hashMap = new HashMap<>();
            hashMap.put("full_name", toRequestBody(fullName));
            hashMap.put("user_name", toRequestBody(newUserName));
            hashMap.put("bio", toRequestBody(bio));
            hashMap.put("fb_url", toRequestBody(fbUrl!=null? fbUrl : ""));
            hashMap.put("insta_url", toRequestBody(instaUrl!=null? instaUrl : ""));
            hashMap.put("youtube_url", toRequestBody(youtubeUrl!=null? youtubeUrl : ""));
            hashMap.put("language", toRequestBody(language));
            hashMap.put("gender", toRequestBody(gender));

            MultipartBody.Part body = null;
            if (imageUri != null && !imageUri.isEmpty()) {

                File file = new File(imageUri);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                body = MultipartBody.Part.createFormData("user_profile", file.getName(), requestFile);
                isImageUpload=true;
                String fileName="FairtokUserPic_"+prefHandler.getuId()+".jpg";
                //hashMap.put("userProfilePic", toRequestBody(fileName));
                hashMap.put("user_profile", toRequestBodyFile(fileName));
                uploadFile(context,file,fileName);
            }
            else
            {
                isImageUpload=false;
            }

            Log.i("paramsss_update ", Global.ACCESS_TOKEN+ " , "+ hashMap.toString());
            disposable.add(Global.initRetrofit().updateUser(Global.ACCESS_TOKEN, hashMap, body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe((updateUser, throwable) -> {
                        if (updateUser != null && updateUser.getStatus()) {
                            Log.i("userprofileee ", updateUser.getData().getUserProfile());
                            user = updateUser;
                            prefHandler.setProfile(user.getData().getUserProfile());
                            if(!isImageUpload)
                                updateProfile.setValue(true);

                            deleteCache(context);
                            hiddProgress();
                            //toast.setValue("Your Profile has been updated successfully");
                        }
                    }));
        }
    }
    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public void uploadFile(Context context,File file,String fileName)
    {
        setupProgressDialog(context);
        // Initializes TransferUtility, always do this before using it.
        Util util = new Util();
        TransferUtility transferUtility = util.getTransferUtility(context);

        TransferObserver observer = transferUtility.upload(fileName, file);
        observer.setTransferListener(new UploadListener());
    }

    public RequestBody toRequestBody(String value) {
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }
    public RequestBody toRequestBodyFile(String value) {
        return RequestBody.create(MediaType.parse("image/*"), value);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    public void updateData() {
        fullName = user.getData().getFullName();
        newUserName = user.getData().getUserName();
        bio = user.getData().getBio();
        fbUrl = user.getData().getFbUrl();
        instaUrl = user.getData().getInstaUrl();
        youtubeUrl = user.getData().getYoutubeUrl();
    }

    /*
     * A TransferListener class that can listen to a upload task and be notified
     * when the status changes.
     */
    class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);
            updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));

            long _bytesCurrent = bytesCurrent;
            long _bytesTotal = bytesTotal;


            float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
            Log.d("percentage","" +percentage);

            progressDialog.setProgress((int) percentage);
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

            if(newState==TransferState.COMPLETED)
                updateList();
        }
    }

    public void updateList()
    {
        //
        updateProfile.setValue(true);
    }

    protected static void setupProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Updating profile...");
        progressDialog.setMessage("");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();
    }

    public void hiddProgress(){
        progressDialog.dismiss();
    }
}
