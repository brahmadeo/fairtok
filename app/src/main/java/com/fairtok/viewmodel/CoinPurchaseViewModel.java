package com.fairtok.viewmodel;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.adapter.CoinPlansAdapter;
import com.fairtok.model.user.RestResponse;
import com.fairtok.openlive.activities.LiveActivityNew;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CoinPurchaseViewModel extends ViewModel {

    public CoinPlansAdapter adapter = new CoinPlansAdapter();
    public ObservableBoolean isLoading = new ObservableBoolean(false);
    public MutableLiveData<RestResponse> purchase = new MutableLiveData<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    public String coins = "";
    public String coinsAmount = "";

    public void fetchCoinPlans() {

        disposable.add(Global.initRetrofit().getCoinPlans(Global.ACCESS_TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((purchase, throwable) -> {

                    if (purchase != null && purchase.getData() != null && !purchase.getData().isEmpty()) {
                        adapter.updateData(purchase.getData());
                    }

                }));
    }

    public void purchaseCoin(String FTransactionId, String FPaymentMode, String FAmount, String user_name,
                             String message, String coinPanId, String FTransactionStatus) {


        disposable.add(Global.initRetrofit().purchaseCoinNew(
                Global.ACCESS_TOKEN,
                coinPanId,
                FTransactionId,
                FPaymentMode,
                message,
                FTransactionStatus)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((purchase, throwable) -> {

                    if (purchase != null) {
                        this.purchase.setValue(purchase);

                        if (LiveActivityNew.tvUserCoin!=null)
                        {

                            SessionManager sessionManager = new SessionManager(LiveActivityNew.tvUserCoin.getContext());
                            LiveActivityNew.tvUserCoin.setText(sessionManager.getUser().getData().getPaidCoins());
                        }
                    }

                }));

        /*disposable.add(Global.initRetrofit().purchaseCoin(Global.ACCESS_TOKEN,
                coins,FTransactionId,FPaymentMode,FAmount,user_name,status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isLoading.set(true))
                .doOnTerminate(() -> isLoading.set(false))
                .subscribe((purchase, throwable) -> {

                    if (purchase != null) {
                        this.purchase.setValue(purchase);
                    }



                }));*/
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
