package com.fairtok.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.model.SpinWinModel;
import com.fairtok.model.wallet.CoinRate;
import com.fairtok.model.wallet.MyWallet;
import com.fairtok.utils.Global;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SpinWinViewModel extends ViewModel {
    public MutableLiveData<MyWallet> myWallet = new MutableLiveData<>();
    public MutableLiveData<SpinWinModel> spinWin = new MutableLiveData<>();
    public MutableLiveData<CoinRate> coinRate = new MutableLiveData<>();
    public String myImage;

    public CompositeDisposable disposable = new CompositeDisposable();

    public void fetchMyWallet() {
        Log.i("access_token ", Global.ACCESS_TOKEN);
        disposable.add(Global.initRetrofit().getMyWalletDetails(Global.ACCESS_TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((wallet, throwable) -> {
                    if (wallet != null && wallet.getStatus() != null) {
                        myWallet.setValue(wallet);
                    }
                }));
    }

    public void fetchCoinRate() {
        disposable.add(Global.initRetrofit().getCoinRate(Global.ACCESS_TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((coinRate, throwable) -> {
                    if (coinRate != null && coinRate.getStatus() != null) {
                        this.coinRate.setValue(coinRate);
                    }
                }));
    }
    public void fetchImage() {
        disposable.add(Global.initRetrofit().fetchImage(Global.ACCESS_TOKEN)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((homeImage, throwable) -> {
                    if (homeImage != null && homeImage.getStatus() != null) {
                        Log.i("imageee ", homeImage.getData().getValue());
                        this.spinWin.setValue(homeImage);
                    }
                }));
    }



    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
