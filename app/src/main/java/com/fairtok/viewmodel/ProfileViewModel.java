package com.fairtok.viewmodel;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.fairtok.model.user.RestResponse;
import com.fairtok.model.user.User;
import com.fairtok.model.videos.Video;
import com.fairtok.utils.Const;
import com.fairtok.utils.Global;
import com.fairtok.view.profile.ProfileFragment;
import com.squareup.picasso.Picasso;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ProfileViewModel extends ViewModel {

    public MutableLiveData<User> user = new MutableLiveData<>();
    public MutableLiveData<Integer> onItemClick = new MutableLiveData<>();
    public MutableLiveData<Integer> selectPosition = new MutableLiveData<>();
    public MutableLiveData<Intent> intent = new MutableLiveData<>();
    public ObservableBoolean isloading = new ObservableBoolean(false);
    public ObservableBoolean isBackBtn = new ObservableBoolean(false);
    public String userId = "";
    public MutableLiveData<RestResponse> followApi = new MutableLiveData<>();
    public ObservableBoolean isLikedVideos = new ObservableBoolean(false);
    public ObservableBoolean isPrivateVideos = new ObservableBoolean(false);
    public ObservableInt isMyAccount = new ObservableInt(0);
    private final CompositeDisposable disposable = new CompositeDisposable();

    public void setOnItemClick(int type) {

        onItemClick.setValue(type);
    }

    public void onSocialClick(int type) {
        String url = "";
        if (user.getValue() != null) {
            switch (type) {
                case 1:
                    url = user.getValue().getData().getFbUrl();
                    break;
                case 2:
                    url = user.getValue().getData().getInstaUrl();
                    break;
                default:
                    url = user.getValue().getData().getYoutubeUrl();
                    break;
            }
        }

        intent.setValue(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public void fetchUserById(String userid) {
        disposable.add(Global.initRetrofit().getUserDetails(userid, Global.USER_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .doOnSubscribe(disposable1 -> isloading.set(true))
                .doOnTerminate(() -> isloading.set(false))
                .subscribe((user, throwable) -> {
                    if (user != null && user.getData() != null) {
                        this.user.setValue(user);
                        Log.v("jhjh", this.user.getValue().getData().getUserProfile());

                        if (ProfileFragment.img_user!=null){
                            Picasso.get().
                                    load(Const.BASE_URL_IMAGE.concat(this.user.getValue().getData().getUserProfile())).
                                    into(ProfileFragment.img_user);

                        }


                        if (isMyAccount.get() != 0) {
                            if (user.getData().getIsFollowing() == 1) {
                                isMyAccount.set(1);
                            } else {
                                isMyAccount.set(2);
                            }
                        }
                    }
                }));
    }

    public void followUnfollow() {

        disposable.add(Global.initRetrofit().followUnFollow(Global.ACCESS_TOKEN, userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe((followRequest, throwable) -> {
//                    if (followRequest != null && followRequest.getStatus() != null)
                    {
                        if (isMyAccount.get() == 1) {
                            isMyAccount.set(2);
                        } else {
                            isMyAccount.set(1);
                        }
                        followApi.setValue(followRequest);

                        try {
                            if (ProfileVideosViewModel.adapter != null && ProfileVideosViewModel.adapter.mList != null && ProfileVideosViewModel.adapter.mList.size() > 0) {
                                for (Video.Data data : ProfileVideosViewModel.adapter.mList) {
                                    if (data.getIsFollow() == 1)
                                        data.setIsFollow(0);
                                    else
                                        data.setIsFollow(1);
                                }
                                ProfileVideosViewModel.adapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }
    public String getTotalTimeSpent(){
        Long millis = 1230000056l;

        int month = (int)TimeUnit.MILLISECONDS.toDays(millis)/30;
        int weaks = ((int)TimeUnit.MILLISECONDS.toDays(millis) - (int)TimeUnit.MILLISECONDS.toDays(millis))/7;
        Log.i("timeeee ", month+" , "+ weaks);
        String formatedTime =  String.format("%02d : %02d : %02d : %02d",
                TimeUnit.MILLISECONDS.toDays(millis),

                TimeUnit.MILLISECONDS.toHours(millis) -
                        TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),

                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line

                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

        return formatedTime;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
