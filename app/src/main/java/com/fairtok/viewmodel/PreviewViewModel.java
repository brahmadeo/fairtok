package com.fairtok.viewmodel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.databinding.ObservableInt;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.fairtok.MyApplication;
import com.fairtok.chat.PrefHandler;
import com.fairtok.chat.Utils;
import com.fairtok.model.ServiceData;
import com.fairtok.s3.MyService;
import com.fairtok.s3.Util;
import com.fairtok.utils.Global;
import com.fairtok.utils.SessionManager;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.content.ContentValues.TAG;

public class PreviewViewModel extends ViewModel {
    public String postDescription = "", hashTag = "";
    public String videoPath = " ", videoThumbnail = "", soundImage = "", soundPath = "", soundId = "",soundBy="";
    public Boolean isFront = false;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    public MutableLiveData<Boolean> isToast = new MutableLiveData<>();
    public MutableLiveData<TransferState> uploadStatus = new MutableLiveData<>();
    public MutableLiveData<String> onClickUpload = new MutableLiveData<>();
    public ObservableInt position = new ObservableInt(0);
    public SessionManager sessionManager;
    public PrefHandler prefHandler;
    private CompositeDisposable disposable = new CompositeDisposable();
    public MultipartBody.Part body=null,body1=null,body2=null,body3=null;
    public HashMap<String, RequestBody> hashMap;

    // The TransferUtility is the primary class for managing transfer to S3
    static TransferUtility transferUtility;
    public ObservableInt publish_position = new ObservableInt(0);

    private ProgressDialog mProgressDialog;

    public static ProgressDialog progressDialog;

    // Reference to the utility class
    static Util util;

    public void onDescriptionTextChanged(CharSequence text) {
        postDescription = text.toString();
        position.set(postDescription.length());
    }

    public void uploadPost(Context context) {

        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("The video is uploading...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMax(100);

        // Initializes TransferUtility, always do this before using it.
        util = new Util();
        transferUtility = util.getTransferUtility(context);


        onClickUpload.setValue("hash");

        hashMap = new HashMap<>();

        if(Utils.isDuetVideo)
        {
            hashMap.put("isDuet", toRequestBody("1"));
        }
        else
        {
            hashMap.put("isDuet", toRequestBody("0"));
        }

        hashMap.put("duetVideoId", toRequestBody(Utils.VideoId));
        hashMap.put("creater_id", toRequestBody(Utils.VideoCreaterId));
        hashMap.put("creater_full_name", toRequestBody(Utils.VideoCreaterName));
        hashMap.put("creator_user_name", toRequestBody(Utils.VideoCreaterUserName));
        hashMap.put("language", toRequestBody(prefHandler.getLanguage()));

        switch (publish_position.get())
        {
            case 1:
                hashMap.put("private_video", toRequestBody("0"));
                hashMap.put("draft", toRequestBody("1"));
                break;

            case 2:
                hashMap.put("private_video", toRequestBody("1"));
                hashMap.put("draft", toRequestBody("0"));
                break;

            default:
                hashMap.put("private_video", toRequestBody("0"));
                hashMap.put("draft", toRequestBody("0"));
                break;

        }

        hashMap.put("post_description", toRequestBody(postDescription));

        hashMap.put("post_hash_tag", toRequestBody(hashTag));
        hashMap.put("is_orignal_sound", toRequestBody(soundPath == null || soundPath.isEmpty() ? "0" : "1"));

        if (soundId != null)
        {
            hashMap.put("sound_id", toRequestBody(soundId));
            if (soundBy != null && !soundBy.isEmpty()) {
                hashMap.put("sound_title", toRequestBody(soundBy));
            }

        } else {
            hashMap.put("sound_title", toRequestBody("Original sound by " + sessionManager.getUser().getData().getFullName()));
        }

        hashMap.put("duration", toRequestBody("1:00"));
        hashMap.put("singer", toRequestBody(sessionManager.getUser().getData().getUserName()));
        //MultipartBody.Part body;

        prepareParamUpload(videoThumbnail,"post_image",context);


        prepareParamUpload(soundPath,"post_sound",context);

            if (soundImage != null) {

                prepareParamUpload(soundImage,"sound_image",context);
            }


        prepareParamUpload(videoPath,"post_video",context);


//        ProgressRequestBody requestBody = new ProgressRequestBody(file, percentage -> {
//
//        });
//        body = MultipartBody.Part.createFormData("post_video", file.getName(), requestBody);
//
//
//        ProgressRequestBody requestBody2 = new ProgressRequestBody(file1, percentage -> {
//
//        });
//
//        body1 = MultipartBody.Part.createFormData("post_image", file1.getName(), requestBody2);
//
//        body2 = null;
//        body3 = null;
//        if (soundId == null || soundId.isEmpty())
//        {
//            File file2 = new File(soundPath);
//            RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), file2);
//            body2 = MultipartBody.Part.createFormData("post_sound", file2.getName(), requestFile2);
//
//            if (soundImage != null) {
//                File file3 = new File(soundImage);
//                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), file3);
//                body3 = MultipartBody.Part.createFormData("sound_image", file1.getName(), requestFile3);
//            }
//        }

        MyApplication.data = new ServiceData(Global.ACCESS_TOKEN,
                hashMap,
                body,
                body1,
                body2,
                body3);

        isLoading.setValue(true);

        Utils.clearDuetData();
    }

    private void prepareParamUpload(String filePath,String paramName,Context context)
    {
        boolean isVideo=false;
        File file = new File(filePath);
        String extension = FilenameUtils.getExtension(file.getAbsolutePath());
        String fileName = "fairtok_" + uniqueCurrentTimeMS() + "." + extension;
        hashMap.put(paramName, toRequestBody(fileName));
        if(extension.contains("mp4")) {
            setupProgressDialog(context);
            isVideo=true;
        }
        else
        {
            isVideo=false;
        }
        beginUpload(file,fileName,isVideo);
    }

    public RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }

    private static final AtomicLong LAST_TIME_MS = new AtomicLong();
    public static long uniqueCurrentTimeMS() {
        long now = System.currentTimeMillis();
        while(true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now)
                now = lastTime+1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now))
                return now;
        }
    }



    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUpload(File file,String keyFileName,boolean isVideo) {
        TransferObserver observer = transferUtility.upload(keyFileName, file);

        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */

        if(isVideo)
            observer.setTransferListener(new UploadListener());
    }

    /*
     * Begins to upload the file specified by the file path.
     */
    private void beginUploadInBackground(File file,String keyFileName,Context mycontext) {
        // Wrap the upload call from a background service to
        // support long-running downloads. Uncomment the following
        // code in order to start a upload from the background
        // service.
        Context context = mycontext.getApplicationContext();
        Intent intent = new Intent(context, MyService.class);
        intent.putExtra(MyService.INTENT_KEY_NAME, keyFileName);
        intent.putExtra(MyService.INTENT_TRANSFER_OPERATION, MyService.TRANSFER_OPERATION_UPLOAD);
        intent.putExtra(MyService.INTENT_FILE, file);
        context.startService(intent);

        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        //observer.setTransferListener(new UploadListener());
    }

    /*
     * A TransferListener class that can listen to a upload task and be notified
     * when the status changes.
     */
     class UploadListener implements TransferListener {

        // Simply updates the UI list when notified.
        @Override
        public void onError(int id, Exception e) {
            Log.e(TAG, "Error during upload: " + id, e);
            updateList();
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            Log.d(TAG, String.format("onProgressChanged: %d, total: %d, current: %d",
                    id, bytesTotal, bytesCurrent));

            long _bytesCurrent = bytesCurrent;
            long _bytesTotal = bytesTotal;


            float percentage =  ((float)_bytesCurrent /(float)_bytesTotal * 100);
            Log.d("percentage","" +percentage);

            progressDialog.setProgress((int) percentage);
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.d(TAG, "onStateChanged: " + id + ", " + newState);

            if(newState==TransferState.COMPLETED)
                updateList();
        }
    }

    public void updateList()
    {
        progressDialog.dismiss();
        uploadStatus.setValue(TransferState.COMPLETED);
    }

    protected static void setupProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Uploading video...");
        progressDialog.setMessage("");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();
    }

}
